package com.hcl.symantec.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import com.elasticpath.rest.client.CortexClient;
import com.elasticpath.rest.client.header.HeaderKeys;

@SuppressWarnings("serial")
@SlingServlet(paths="/bin/epcortex/billing", methods = "GET", metatype=true)
public class Globe_servlet2 extends SlingAllMethodsServlet
{
	final Logger logger = Logger.getLogger(Globe_servlet2.class);
	String token = null;
	
	@Reference
	com.elasticpath.rest.client.CortexClientFactory clientFactory;
	
	@Override
	protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws ServletException, IOException
	{
		String store = LoadProperties.getStore();
		
		String cardName = req.getParameter("cardName");
		String cardNumber = req.getParameter("cardNumber");
		String ccomp = req.getParameter("ccomp");
		String month = req.getParameter("month");
		String year = req.getParameter("year");
		String cvv = req.getParameter("cvv");
		String zipcode = req.getParameter("zipcode");
		String city = req.getParameter("city");
		String state = req.getParameter("state");
		String country = req.getParameter("country");
		String stdCode = req.getParameter("mobilecc");
		String mobileNumber = req.getParameter("mobileno");
		int lastDigits = Integer.parseInt(cardNumber.substring(cardNumber.length()-4));
		
		logger.info(cardName);
		logger.info(cardNumber);
		logger.info(ccomp);
		logger.info(month);
		logger.info(year);
		logger.info(cvv);
		logger.info(zipcode);
		logger.info(city);
		logger.info(state);
		logger.info(country);
		logger.info(stdCode);
		logger.info(mobileNumber);
		logger.info(lastDigits);
		
		try
		{
			token = AuthenticationService.generateToken();
		} catch (Exception e) {
			resp.getWriter().write("Something went wrong");
		}

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
		headers.putSingle(HeaderKeys.X_EP_USER_ID_KEY, "customerId");
		headers.putSingle(HeaderKeys.X_EP_USER_ROLES_KEY, "PUBLIC");
		headers.putSingle(HeaderKeys.AUTHORIZATION_KEY, token);
		CortexClient client = clientFactory.create(headers, store);

		resp .setContentType("text/plain");
		ProductSearch productSearch = new ProductSearch();
		String productListUrl = productSearch.getProductListByKeyword(client);
		logger.info("9142016----productListUrl----- :"+productListUrl);
		String productSelected = productSearch.getProductBasedOnSelection(client, productListUrl);
		logger.info("9142016----productSelected----- :"+productSelected);
		String productAvailability = productSearch.getProductAvailability(client, productSelected);
		logger.info("9142016----productAvailability----- :"+productAvailability);
		String availabilityValue = productSearch.getProductAvailabilityCheck(client, productAvailability);
		logger.info("9142016----availabilityValue----- :"+availabilityValue);
		if(!availabilityValue.equals("AVAILABLE")){
			resp.getWriter().print("not available");
		}
		else{
			AddProductToCart addProductToCart = new AddProductToCart();
			String value = addProductToCart.addProductToCart(client, productSelected);
			logger.info("9142016----value----- :"+value);
			String cartHref = addProductToCart.productAddResponse(client, value);
			logger.info("9142016----cartHref----- :"+cartHref);
			String orderHref = addProductToCart.productOrder(client, cartHref);
			logger.info("9142016----orderHref----- :"+orderHref);
			String billingaddressinfoHref = addProductToCart.productBillingAddressInfo(client, orderHref);
			String addressFormHref = addProductToCart.getAddressFormURL(client, billingaddressinfoHref, country, city, state, cardName);
			logger.info(addressFormHref);
			String taxHref = addProductToCart.productTax(client, orderHref);
			logger.info("9142016----taxHref----- :"+taxHref);
			String tax = addProductToCart.getProductTax(client, taxHref);
			logger.info("9142016----tax----- :"+tax);
			String totalHref = addProductToCart.productTotal(client, orderHref);
			logger.info("9142016----totalHref----- :"+totalHref);
			String total = addProductToCart.getProductTotal(client, totalHref);
			logger.info("9142016----total----- :"+total);
			resp.getWriter().write(cardName+","+ccomp+","+lastDigits+","+tax+","+total);
		}
	}
}