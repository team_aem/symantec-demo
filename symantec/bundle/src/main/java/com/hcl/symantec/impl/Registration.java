package com.hcl.symantec.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;

import com.elasticpath.rest.client.CortexClient;
import com.elasticpath.rest.client.header.HeaderKeys;
 

@SuppressWarnings("serial")
@SlingServlet(paths="/bin/registration", methods = "GET", metatype=true)
public class Registration extends HttpServlet
{
	final Logger logger = Logger.getLogger(Registration.class);
	@Reference
	com.elasticpath.rest.client.CortexClientFactory clientFactory;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String token = null;
		String store = LoadProperties.getStore();
		logger.info("Get Method started login page vasudev");
		String familyName = request.getParameter("family-name");
		String givenName = request.getParameter("given-name");
		String email = request.getParameter("email");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		
		logger.info("---------------");
		logger.info("Family name: "+familyName);
		logger.info("Given name: "+givenName);
		logger.info("Email: "+email);
		logger.info("password1: "+password1);
		logger.info("password2: "+password2);
		logger.info("---------------");
		
		try
		{
			token = AuthenticationService.generateToken();
		} catch (Exception e) {
			response.getWriter().write("Something went wrong");
		}

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
		headers.putSingle(HeaderKeys.X_EP_USER_ID_KEY, "customerId");
		headers.putSingle(HeaderKeys.X_EP_USER_ROLES_KEY, "PUBLIC");
		headers.putSingle(HeaderKeys.AUTHORIZATION_KEY, token);
		CortexClient client = clientFactory.create(headers, store);
		if(password1.equals(password2)){
			RegistrationForm registrationForm = new RegistrationForm();
			String responseURL = registrationForm.registerUser(client, familyName, givenName, password1, email);
			logger.info("Response url"+responseURL);
			String emailHref = registrationForm.responseOfRegistration(client, responseURL);
			String emailElementHref = registrationForm.getRegistrationEmailElement(client, emailHref);
			String respEmail = registrationForm.getRegistrationEmail(client, emailElementHref);
			logger.info("++++++++++++++++++++++++++++"+respEmail);
			if(respEmail.equals(email)){
				response.getWriter().write(respEmail);
			}
		}
	}
}