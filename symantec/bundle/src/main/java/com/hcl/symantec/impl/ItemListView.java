package com.hcl.symantec.impl;

import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

public class ItemListView 
{
	
	@JsonPath("$.links[0].href")
	private String href;
	@JsonPath("$.links[1].href")
	private String emailHref;
	@JsonPath("$.links[2].href")
	private String emailElement;
	@JsonPath("$.email")
	private String email;
	
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getEmailHref() {
		return emailHref;
	}

	public void setEmailHref(String emailHref) {
		this.emailHref = emailHref;
	}

	public String getEmailElement() {
		return emailElement;
	}

	public void setEmailElement(String emailElement) {
		this.emailElement = emailElement;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}