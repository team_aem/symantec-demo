package com.hcl.symantec.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.elasticpath.rest.client.CortexClient;
import com.elasticpath.rest.client.CortexResponse;
import com.hcl.symantec.impl.ItemListView;

public class ProductSearch {

	final Logger logger = Logger.getLogger(ProductSearch.class);
	

	public String getProductListByKeyword(CortexClient client) throws IOException
	{
		String store = LoadProperties.getStore();
		Map<String, String> input = new HashMap<String, String>();
		input.put("keywords", "Norton Security Plus");
		CortexResponse<Object> clientResponse = client.post("searches/"+store+"/keywords/items", input, Object.class);
		logger.info("Client Response: "+clientResponse.getResponse().getHeaders().get("Location").get(0).toString());
		return clientResponse.getResponse().getHeaders().get("Location").get(0).toString();
	}

	public String getProductBasedOnSelection(CortexClient client, String itemLocation)
	{
		CortexResponse<ItemListView> clientResponse = client.get( itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), ItemListView.class);
		String href = clientResponse.getCortexView().getHref();
		return href;
	}

	public String getProductDetailsBasedOnHref(CortexClient client, String href)
	{
        CortexResponse<ItemDescriptionView> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), ItemDescriptionView.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String definitionHref = clientResponse.getCortexView().getDefinitionHref();
        logger.info("href:-"+definitionHref);
        return definitionHref;
	}
	
	public String getProductAvailability(CortexClient client, String href)
	{
        CortexResponse<ItemDescriptionView> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), ItemDescriptionView.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String definitionHref = clientResponse.getCortexView().getAvailableHref();
        logger.info("href:-"+definitionHref);
        return definitionHref;
	}
	
	public String getProductAddToCartView(CortexClient client, String href)
	{
        CortexResponse<AddToCartView> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), AddToCartView.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String definitionHref = clientResponse.getCortexView().getAddtocartform();
        logger.info("href:-"+definitionHref);
        return definitionHref;
	}
	
	public String getProductAvailabilityCheck(CortexClient client, String href)
	{
        CortexResponse<ItemAvailability> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), ItemAvailability.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String definitionHref = clientResponse.getCortexView().getState();
        return definitionHref;
	}
	
	public String getProductPriceBasedOnHref(CortexClient client, String href)
	{
        CortexResponse<ItemDescriptionView> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), ItemDescriptionView.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String definitionHref = clientResponse.getCortexView().getPriceHref();
        logger.info("href:-"+definitionHref);
        return definitionHref;
	}
	
	public String getProductName(CortexClient client, String href)
	{
        CortexResponse<ItemDetails> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), ItemDetails.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String name = clientResponse.getCortexView().getName();
        logger.info("href:-"+name);
        return name;
	}
	
	public String getProductDescription(CortexClient client, String href)
	{
        CortexResponse<ItemDetails> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), ItemDetails.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String description = clientResponse.getCortexView().getDescription();
        logger.info("href:-"+description);
        return description;
	}
	
	public String getProductPrice(CortexClient client, String href)
	{
        CortexResponse<ItemPrice> clientResponse = client.get(href.substring(href.lastIndexOf("cortex") + 7), ItemPrice.class);
        logger.info("clientResponse:-"+clientResponse.toString());
        String price = clientResponse.getCortexView().getPrice();
        logger.info("href:-"+price);
        return price;
	}

}