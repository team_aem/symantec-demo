package com.hcl.symantec.impl;

import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

public class ItemListView2 {
	
	@JsonPath("$.links[?(@.rel=='cart')].href")
	private Iterable<String> cartHref;
	@JsonPath("$.links[?(@.rel=='order')].href")
	private Iterable<String> orderHref;
	@JsonPath("$.links[?(@.rel=='tax')].href")
	private Iterable<String> taxHref;
	@JsonPath("$.links[?(@.rel=='total')].href")
	private Iterable<String> totalHref;
	@JsonPath("$.links[?(@.rel=='billingaddressinfo')].href")
	private Iterable<String> billingaddressinfoHref;
	@JsonPath("$.links[?(@.rel=='addressform')].href")
	private Iterable<String> addressformHref;
	
	public String getCartHref() {
		return cartHref.iterator().next();
	}
	
	public String getOrderHref() {
		return orderHref.iterator().next();
	}
	
	public String getTaxHref() {
		return taxHref.iterator().next();
	}
	
	public String getTotalHref() {
		return totalHref.iterator().next();
	}

	public String getBillingaddressinfoHref() {
		return billingaddressinfoHref.iterator().next();
	}

	public String getAddressformHref() {
		return addressformHref.iterator().next();
	}

}