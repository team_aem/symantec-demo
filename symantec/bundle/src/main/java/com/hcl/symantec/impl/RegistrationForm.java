package com.hcl.symantec.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.elasticpath.rest.client.CortexClient;
import com.elasticpath.rest.client.CortexResponse;

public class RegistrationForm {

	final Logger logger = Logger.getLogger(RegistrationForm.class);

	public String registerUser(CortexClient client, String familyName, String givenName, String password1, String email) throws IOException
	{
		String store = LoadProperties.getStore();
		Map<String, String> input = new HashMap<String, String>();
		input.put("family-name", familyName);
		input.put("given-name", givenName);
		input.put("password", password1);
		input.put("username", email);
		CortexResponse<Object> clientResponse = client.post("registrations/"+store+"/newaccount", input, Object.class);
		logger.info("Client Response: "+clientResponse.getResponse().getHeaders().get("Location").get(0).toString());
		return clientResponse.getResponse().getHeaders().get("Location").get(0).toString();
	}
	
	public String responseOfRegistration(CortexClient client, String itemLocation)
	{
		CortexResponse<ItemListView> clientResponse = client.get( itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), ItemListView.class);
		String emailHref = clientResponse.getCortexView().getEmailHref();
		return emailHref;
	}
	
	public String getRegistrationEmailElement(CortexClient client, String itemLocation)
	{
		CortexResponse<ItemListView> clientResponse = client.get( itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), ItemListView.class);
		String emailElementHref = clientResponse.getCortexView().getEmailElement();
		return emailElementHref;
	}
	
	public String getRegistrationEmail(CortexClient client, String itemLocation)
	{
		CortexResponse<ItemListView> clientResponse = client.get( itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), ItemListView.class);
		String email = clientResponse.getCortexView().getEmail();
		return email;
	}
}