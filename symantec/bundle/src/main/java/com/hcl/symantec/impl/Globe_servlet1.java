package com.hcl.symantec.impl;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import com.elasticpath.rest.client.CortexClient;
import com.elasticpath.rest.client.header.HeaderKeys;

@SuppressWarnings("serial")
@SlingServlet(paths="/bin/epcortex/product", methods = "GET", metatype=true)
public class Globe_servlet1 extends SlingAllMethodsServlet{
	final Logger logger = Logger.getLogger(Globe_servlet1.class);
	String token="";
	@Reference
	com.elasticpath.rest.client.CortexClientFactory clientFactory;
	@Override
	protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws ServletException, IOException
	{
		String store = LoadProperties.getStore();
		try
		{
			token = AuthenticationService.generateToken();
		} catch (Exception e) {
			resp.getWriter().write("Something went wrong");
		}

		MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>();
		headers.putSingle(HeaderKeys.X_EP_USER_ID_KEY, "customerId");
		headers.putSingle(HeaderKeys.X_EP_USER_ROLES_KEY, "PUBLIC");
		headers.putSingle(HeaderKeys.AUTHORIZATION_KEY, token);
		CortexClient client = clientFactory.create(headers, store);

		resp .setContentType("text/plain");
		ProductSearch productSearch = new ProductSearch();
		String productListUrl = productSearch.getProductListByKeyword(client);
		String productSelected = productSearch.getProductBasedOnSelection(client, productListUrl);
		String productDetails = productSearch.getProductDetailsBasedOnHref(client, productSelected);
		String productPrice = productSearch.getProductPriceBasedOnHref(client, productSelected);
		String name = productSearch.getProductName(client, productDetails);
		String description = productSearch.getProductDescription(client, productDetails);
		String price = productSearch.getProductPrice(client, productPrice);
		ServletContext sc= getServletContext();
		sc.setAttribute("name", name);
		sc.setAttribute("description", description);
		sc.setAttribute("price", price);
		RequestDispatcher dispatcher=req.getRequestDispatcher("/content/symantec/checkout.html");
		
		dispatcher.forward(req,resp);
		
		logger.info("Session entered");
		resp.getWriter().write(name+"="+description+"="+price);
		
	}
}