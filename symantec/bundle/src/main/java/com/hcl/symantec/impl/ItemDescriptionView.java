package com.hcl.symantec.impl;

import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

public class ItemDescriptionView {

	@JsonPath("$.links[0].href")//available
	private String availableHref;
	
	@JsonPath("$.links[3].href")//definition
	private String definitionHref;
	
	@JsonPath("$.links[5].href")//price
	private String priceHref;

	public String getDefinitionHref() {
		return definitionHref;
	}

	public void setDefinitionHref(String definitionHref) {
		this.definitionHref = definitionHref;
	}

	public String getPriceHref() {
		return priceHref;
	}

	public void setPriceHref(String priceHref) {
		this.priceHref = priceHref;
	}

	public String getAvailableHref() {
		return availableHref;
	}

	public void setAvailableHref(String availableHref) {
		this.availableHref = availableHref;
	}
	
	
}