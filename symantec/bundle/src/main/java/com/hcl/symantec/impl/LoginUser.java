package com.hcl.symantec.impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
 

@SuppressWarnings("serial")
@SlingServlet(paths="/bin/login", methods = "GET", metatype=true)
public class LoginUser extends HttpServlet
{
	final Logger logger = Logger.getLogger(LoginUser.class);
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		logger.info("Get Method started login page vasudev");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		response.setContentType("text/plain");
		response.getWriter().print(email);
	}
}