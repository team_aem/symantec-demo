package com.hcl.symantec.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class AuthenticationService {
	
	private String authKey;
	
	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
	
	public static String generateToken() throws IOException, JSONException 
	{
		String store = LoadProperties.getStore();
		String ipaddress = LoadProperties.getIp();
		final Logger logger = Logger.getLogger(AuthenticationService.class);
		String tokenURL = "http://"+ipaddress+"/cortex/oauth2/tokens";
		URL url = new URL(tokenURL);
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("grant_type", "password");
		params.put("username", "");
		params.put("password", "");
		params.put("role", "PUBLIC");
		params.put("scope", store);
		StringBuilder postData = new StringBuilder();
		for (Map.Entry<String, String> param : params.entrySet()) {
			if (postData.length() != 0)
				postData.append('&');
			postData.append(param.getKey());
			postData.append('=');
			postData.append(param.getValue());
		}
		byte[] postDataBytes = postData.toString().getBytes("UTF-8");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		conn.setDoOutput(true);
		conn.getOutputStream().write(postDataBytes);

		Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		StringBuilder sb = new StringBuilder();
		for (int c; (c = in.read()) >= 0;)
			sb.append((char) c);
		String response = sb.toString();
		JSONObject responseJson = new JSONObject(response);
		logger.info(responseJson.get("token_type") + " " + responseJson.get("access_token"));
		return responseJson.get("token_type") + " " + responseJson.get("access_token");
	}

	public void authentication() throws Exception{
		generateToken();
	}

}