package com.hcl.symantec.impl;

import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

public class ItemPrice {

	@JsonPath("$.purchase-price[0].display")
	private String price;

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
}
