package com.hcl.symantec.impl;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

@SuppressWarnings("serial")
@SlingServlet(paths="/bin/review", methods = "GET", metatype=true)
public class Review extends SlingAllMethodsServlet
{
	final Logger logger = Logger.getLogger(Review.class);
	
	@Override
	protected void doGet(final SlingHttpServletRequest req,final SlingHttpServletResponse resp) throws ServletException, IOException 
	{
		logger.info("Get Method started");
		String name = req.getParameter("name");
		String price = req.getParameter("price");
		
		logger.info("+++++++++++++++++++++Name :"+name);	
		logger.info("+++++++++++++++++++++Name :"+price);
		
        resp.getWriter().print(name+","+price);
	}
}