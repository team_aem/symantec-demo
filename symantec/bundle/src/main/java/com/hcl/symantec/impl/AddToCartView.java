package com.hcl.symantec.impl;

import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

public class AddToCartView {

	@JsonPath("$.links[?(@.rel=='addtocartform')].href")
	private Iterable<String> addtocartform;
	
	public String getAddtocartform()
	{
		return addtocartform.iterator().next();
	}

}