package com.hcl.symantec.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.contrib.jsonpath.annotation.JsonPath;

public class TaxDetails {

	@JsonProperty("$.total.display")
	private String tax;
	@JsonPath("$.cost[0].display")
	private String total;

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

}