package com.hcl.symantec.impl;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemAvailability {
	
	@JsonProperty("state")
	private String state;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}