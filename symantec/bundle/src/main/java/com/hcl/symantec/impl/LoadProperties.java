package com.hcl.symantec.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadProperties {
	
	public static String getIp() throws IOException {
		String ipaddress;
		String port;
		Properties prop = new Properties();
		InputStream input = null;
		input = LoadProperties.class.getClassLoader().getResourceAsStream("config.properties");
		prop.load(input);
		ipaddress = prop.getProperty("ipaddress");
		port = prop.getProperty("port");
		return ipaddress+":"+port;
	}
	
	public static String getStore() throws IOException {
		String store;
		Properties prop = new Properties();
		InputStream input = null;
		input = LoadProperties.class.getClassLoader().getResourceAsStream("config.properties");
		prop.load(input);
		store = prop.getProperty("store");
		return store;
	}
}
