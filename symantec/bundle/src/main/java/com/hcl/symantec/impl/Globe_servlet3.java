package com.hcl.symantec.impl;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

@SuppressWarnings("serial")
@SlingServlet(paths="/bin/epcortex/signout", methods = "GET", metatype=true)
public class Globe_servlet3 extends SlingAllMethodsServlet{
	final Logger logger = Logger.getLogger(Globe_servlet1.class);
	String token="";
	@Reference
	com.elasticpath.rest.client.CortexClientFactory clientFactory;
	@Override
	protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws ServletException, IOException
	{
		ServletContext sc= getServletContext();
		sc.removeAttribute("name");
		sc.removeAttribute("description");
		sc.removeAttribute("price");
		RequestDispatcher dispatcher=req.getRequestDispatcher("/content/symantec/checkout.html");
		dispatcher.forward(req,resp);
		logger.info("removed from context------"+sc.getAttribute("name"));
		logger.info("logout is done");
	}
}