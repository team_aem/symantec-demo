package com.hcl.symantec.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.elasticpath.rest.client.CortexClient;
import com.elasticpath.rest.client.CortexResponse;


public class AddProductToCart {
	final Logger logger = Logger.getLogger(AddProductToCart.class);

	public String addProductToCart(CortexClient client, String href){
		CortexResponse<AddToCartView> addTocart = client.get( href.substring(href.lastIndexOf("cortex") + 7), AddToCartView.class);
		String addTocartForm = addTocart.getCortexView().getAddtocartform();
		CortexResponse<AddToDefaultCartActionView> addToDefaultCart = client .get(addTocartForm.substring(addTocartForm .lastIndexOf("cortex") + 7), AddToDefaultCartActionView.class);
		return selectQuantity(client, addToDefaultCart.getCortexView().getHref(), 1);
	}

	public String selectQuantity(CortexClient client, String href, int qty) {
		Map<String, Integer> quantity = new HashMap<String, Integer>();
		quantity.put("quantity", qty);
		CortexResponse<Object> postResponse = client.post( href.substring(href.lastIndexOf("cortex") + 7), quantity, Object.class);
		return postResponse.getResponse().getHeaders().get("Location").get(0).toString();
	}

	public String productAddResponse(CortexClient client, String itemLocation) {
		CortexResponse<ItemListView2> clientResponse = client.get( itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), ItemListView2.class);
		logger.info("productAddResponse response is " + clientResponse.getCortexView().toString() + " response " + clientResponse.getResponse().toString());
		logger.info("1111111" + clientResponse);
		String href = clientResponse.getCortexView().getCartHref();
		logger.info("2222222" + href);
		return href;
	}
	
	public String productOrder(CortexClient client, String itemLocation) {
		CortexResponse<ItemListView2> clientResponse = client.get( itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), ItemListView2.class);
		String href = clientResponse.getCortexView().getOrderHref();
		return href;
	}
	
	public String productBillingAddressInfo(CortexClient client, String itemLocation) {
		CortexResponse<ItemListView2> clientResponse = client.get(itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), ItemListView2.class);
		String href = clientResponse.getCortexView().getBillingaddressinfoHref();
		return href;
	}

	public String getAddressFormURL(CortexClient client, String itemLocation,  String country, String city, String state, String givenname)
	{
		CortexResponse<ItemListView2> clientResponse = client.get(itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), ItemListView2.class);
		String addressformHref = clientResponse.getCortexView().getAddressformHref();
		CortexResponse<AddToDefaultCartActionView> addToDefaultCart = client.get(addressformHref.substring(addressformHref.lastIndexOf("cortex") + 7), AddToDefaultCartActionView.class);
		return selectBilling(client, addToDefaultCart.getCortexView().getHref(), country, city, state, givenname);
	}
		
	public String selectBilling(CortexClient client, String href, String country, String city, String state, String givenname)
	{
		Map<String, String> address = new HashMap<String, String>();
		address.put("country-name", country);
		address.put("extended-address", "extendedaddress");
		address.put("locality", city);
		address.put("postal-code", "12345");
		address.put("region", state);
		address.put("street-address", "street address");
			
		Map<String, String> name = new HashMap<String, String>();
		name.put("family-name", "firstname");
		name.put("given-name", givenname);
			
		Map<String, Object> billing = new HashMap<String, Object>();
		billing.put("address", address);
		billing.put("name", name);
			
		CortexResponse<Object> postResponse = client.post(href.substring(href.lastIndexOf("cortex") + 7), billing, Object.class);
		logger.info(postResponse.getResponse().getHeaders().get("Location").get(0).toString());
		return postResponse.getResponse().getHeaders().get("Location").get(0).toString();
	}
	 
	public String productTax(CortexClient client, String itemLocation) {
		CortexResponse<ItemListView2> clientResponse = client.get( itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), ItemListView2.class);
		String href = clientResponse.getCortexView().getTaxHref();
		return href;
	}
	
	public String getProductTax(CortexClient client, String itemLocation) {
		CortexResponse<TaxDetails> clientResponse = client.get( itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7), TaxDetails.class);
		String tax = clientResponse.getCortexView().getTax();
		return tax;
	}

	public String productTotal(CortexClient client, String itemLocation) {
	 CortexResponse<ItemListView2> clientResponse = client.get(
	 itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7),
	  ItemListView2.class); String href =
	  clientResponse.getCortexView().getTotalHref(); return href; }
	 

	public String getProductTotal(CortexClient client, String itemLocation) {
		CortexResponse<TaxDetails> clientResponse = client.get(
				itemLocation.substring(itemLocation.lastIndexOf("cortex") + 7),
				TaxDetails.class);
		String total = clientResponse.getCortexView().getTotal();
		return total;
	}
}