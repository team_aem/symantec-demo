<%@ include file="/libs/foundation/global.jsp" %>
<html>


    <cq:includeClientLib categories="symantec"/>
    <head></head>

<div>

<div class="headerTopPanel positioning">
            <div>





   <div class="headerPane">
        <div class="symLogo">
            <a href="" title="Norton Store"><img src="${properties.fileReference}" alt="header_Logo"></a>

        </div>
        <div class="symUtil">
            <div class="symUtilRgt">	
                <div class="localizationCtryDropDown sym_header_drop_down" id="countryDropDown" onmouseout="" onmouseover="">
                    <div class="dropDownLink localizationCtry" id="localizationCtryLink">
                        <label style="background: url("/content/dam/symantec/sym-dropdown-arrow.gif"); ">United States</label>
                    </div>
                    <div id="localizationPaneCtry" class="localizationPaneCtry dropDownPane" style="visibility: hidden; border: none; top: 33px;">
                        <div id="localizationPaneCtry2" class="localizationPaneBorder dropDownTopBorder" style="margin-left: 102px;"></div>
                        <div class="localizationPaneCtryBdy">
                            <div class="localizationPaneCtryTxt">
                                <ul>
                                    <li>
                                        <a href="/es-ar/"><span class="countryEntry">Argentina</span></a>
                                    </li><li>
                                        <a href="/en-au/"><span class="countryEntry">Australia</span></a>
                                    </li><li>
                                        <a href="/nl-be/"><span class="countryEntry">België</span></a>
                                    </li><li>
                                        <a href="/fr-be/"><span class="countryEntry">Belgique</span></a>
                                    </li><li>
                                        <a href="/pt-br/"><span class="countryEntry">Brasil</span></a>
                                    </li><li>
                                        <a href="/en-kh/"><span class="countryEntry">Cambodia</span></a>
                                    </li><li>
                                        <a href="/en-ca/"><span class="countryEntry">Canada</span></a>
                                    </li><li>
                                        <a href="/fr-ca/"><span class="countryEntry">Canada Français</span></a>
                                    </li><li>
                                        <a href="/es-cl/"><span class="countryEntry">Chile</span></a>
                                    </li><li>
                                        <a href="/en-cn/"><span class="countryEntry">China</span></a>
                                    </li><li>
                                        <a href="/es-co/"><span class="countryEntry">Colombia</span></a>
                                    </li><li>
                                        <a href="/cs-cz/"><span class="countryEntry">Česká republika</span></a>
                                    </li><li>
                                        <a href="/da-dk/"><span class="countryEntry">Danmark</span></a>
                                    </li><li>
                                        <a href="/de-de/"><span class="countryEntry">Deutschland</span></a>
                                    </li><li>
                                        <a href="/es-es/"><span class="countryEntry">España</span></a>
                                    </li><li>
                                        <a href="/fr-fr/"><span class="countryEntry">France</span></a>
                                    </li><li>
                                        <a href="/en-gr/"><span class="countryEntry">Greece</span></a>
                                    </li><li>
                                        <a href="/en-hk/"><span class="countryEntry">Hong Kong</span></a>
                                    </li><li>
                                        <a href="/en-is/"><span class="countryEntry">Iceland</span></a>
                                    </li><li>
                                        <a href="/en-in/"><span class="countryEntry">India</span></a>
                                    </li><li>
                                        <a href="/en-id/"><span class="countryEntry">Indonesia</span></a>
                                    </li><li>
                                        <a href="/en-ie/"><span class="countryEntry">Ireland</span></a>
                                    </li><li>
                                        <a href="/en-il/"><span class="countryEntry">Israel</span></a>
                                    </li><li>
                                        <a href="/it-it/"><span class="countryEntry">Italia</span></a>
                                    </li><li>
                                        <a href="/en-kr/"><span class="countryEntry">Korea, Republic of (South)</span></a>
                                    </li><li>
                                        <a href="/hu-hu/"><span class="countryEntry">Magyarország</span></a>
                                    </li><li>
                                        <a href="/en-my/"><span class="countryEntry">Malaysia</span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="localizationPaneCtryTxt">
                                <ul>
                                    <li>
                                        <a href="/es-mx/"><span class="countryEntry">México</span></a>
                                    </li><li>
                                        <a href="/nl-nl/"><span class="countryEntry">Nederland</span></a>
                                    </li><li>
                                        <a href="/en-nz/"><span class="countryEntry">New Zealand</span></a>
                                    </li><li>
                                        <a href="/no-no/"><span class="countryEntry">Norge</span></a>
                                    </li><li>
                                        <a href="/de-at/"><span class="countryEntry">Österreich</span></a>
                                    </li><li>
                                        <a href="/es-pe/"><span class="countryEntry">Perú</span></a>
                                    </li><li>
                                        <a href="/en-ph/"><span class="countryEntry">Philippines</span></a>
                                    </li><li>
                                        <a href="/pl-pl/"><span class="countryEntry">Polska</span></a>
                                    </li><li>
                                        <a href="/pt-pt/"><span class="countryEntry">Portugal</span></a>
                                    </li><li>
                                        <a href="/ru-ru/"><span class="countryEntry">Россия</span></a>
                                    </li><li>
                                        <a href="/en-pr/"><span class="countryEntry">Puerto Rico</span></a>
                                    </li><li>
                                        <a href="/ro-ro/"><span class="countryEntry">România</span></a>
                                    </li><li>
                                        <a href="/de-ch/"><span class="countryEntry">Schweiz</span></a>
                                    </li><li>
                                        <a href="/en-sg/"><span class="countryEntry">Singapore</span></a>
                                    </li><li>
                                        <a href="/en-za/"><span class="countryEntry">South Africa</span></a>
                                    </li><li>
                                        <a href="/fr-ch/"><span class="countryEntry">Suisse</span></a>
                                    </li><li>
                                        <a href="/fi-fi/"><span class="countryEntry">Suomi</span></a>
                                    </li><li>
                                        <a href="/sv-se/"><span class="countryEntry">Sverige</span></a>
                                    </li><li>
                                        <a href="/it-ch/"><span class="countryEntry">Svizzera</span></a>
                                    </li><li>
                                        <a href="/en-tw/"><span class="countryEntry">Taiwan</span></a>
                                    </li><li>
                                        <a href="/en-th/"><span class="countryEntry">Thailand</span></a>
                                    </li><li>
                                        <a href="/tr-tr/"><span class="countryEntry">Türkiye</span></a>
                                    </li><li>
                                        <a href="/en-gb/"><span class="countryEntry">United Kingdom</span></a>
                                    </li><li>
                                        <a href="/en-us/"><span class="countryEntry">United States</span></a>
                                    </li><li>
                                        <a href="/es-ve/"><span class="countryEntry">Venezuela</span></a>
                                    </li><li>
                                        <a href="/en-vn/"><span class="countryEntry">Vietnam</span></a>
                                    </li>
                                </ul>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div class="gSnavBtm">
                            <div class="gs4">
                                <b></b>
                            </div>
                            <div class="gs3">
                                <b></b>
                            </div>
                            <div class="gs2">
                            </div>
                            <div class="gs1">
                                <b></b>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="shoppingDropDown" class="localizationShopDropDown sym_header_drop_down" onmouseout="" onmouseover="">
                    <span id="pipeLeft"></span>
                    <div id="localizationShopLink" class="dropDownLink localizationShop">
                        <img src="/content/dam/symantec/cart.gif" alt="Shopping Cart" width="14" height="11">
                        <label id="idb">Cart</label>
                    </div>
                    <span id="pipeRight"></span>
                    <div id="localizationPaneShop" class="localizationPaneShop dropDownPane" style="visibility: hidden; top: 33px;">
                        <div id="localizationPaneShop2" class="localizationPaneShopBorder dropDownTopBorder" style="margin-left: 77px;"></div>
						<div class="localizationPaneShopBdy">
                            <div class="localizationPaneShopTxt">
                                <h3><a href="./en-us/mf/landingProductFeatures?2-1.ILinkListener-hf_pnl_mf_nprd_HeaderTopPanel_0-viewCart&amp;random=39f1cc2f-f28e-426f-ad7f-33ef962585f9">View Cart</a></h3>
                                <ul>
                                </ul>
                                <img src="//test-static.norton.com/estore/images/assets/images/master/misc/spacer.gif" style="width:140px;height:1px;border-bottom:1px solid #DDD;margin-bottom:5px;margin-top:10px;" alt="">
                                <br>
                                <h3><a href="&#9; http://buy.norton.com/ ">For Home</a></h3>
                                <ul>
                                    <li>
                                        <a href="    /en-us/norton-software&#10;">Shop for Norton Products</a>
                                    </li>
                                    <li>
                                        <a href="    /en-us/mf/upgradeCenter&#10;">Upgrades</a>
                                    </li>
                                    <li>
                                        <a href="    /en-us/mf/upgradeRenewal&#10;">Renewals</a>
                                    </li>
                                    <li>
                                        <a href="    /en-us/mf/landingPromotion&#10;"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="gSnavBtm">
                            <div class="gs4">
                                <b></b>
                            </div>
                            <div class="gs3">
                                <b></b>
                            </div>
                            <div class="gs2">
                            </div>
                            <div class="gs1">
                                <b></b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="searchPane">
                    <form class="" name="searchSym" id="search" method="get" action="./en-us/mf/landingProductFeatures"><div style="width:0px;height:0px;position:absolute;left:-100px;top:-100px;overflow:hidden"><input type="hidden" name="search_hf_0" id="search_hf_0"><input type="hidden" name="2-1.IFormSubmitListener-hf_pnl_mf_nprd_HeaderTopPanel_0-searchForm" value=""><input type="hidden" name="random" value="1943c37d-259e-4701-bd48-da671325f34b"></div>
                        <input type="text" class="inputBdr" name="searchText" value="Search" size="20" maxlength="100" onfocus="Sym.Input.clear(this)" onblur="Sym.Input.write(this, 'Search ')" onkeypress="if (wicketKeyCode(event) == 13) { document.getElementById('submitLink').onclick();return false;};" id="idc">
                        <a id="submitLink" href="#" onclick="var e=document.getElementById('search_hf_0'); e.name='searchPageLink'; e.value='x';var f=document.getElementById('search');var ff=f;if (ff.onsubmit != undefined) { if (ff.onsubmit()==false) return false; }f.submit();e.value='';e.name='';return false;"><img src="/content/dam/symantec/sym-search-arrow.gif" title="Search" alt="Ok"></a>
                    </form>
                </div>

                <div id="vendLogoHead">
                    
                </div>
            </div>
        </div>
    </div>
    <div style="clear:both">
    </div>
    <div style="position: absolute; left: 50%; margin-left: 170px; top: -440px; z-index: 3333;" id="cartCount">
        <table cellpadding="0" cellspacing="0" border="0" width="175px;" id="idd" style="display: none;">
            <tbody><tr>
                <td>
                    <div class="divCartOverlayLeft">
                    </div>
                </td>
                <td class="tdCartOverlayMid">
                    <div style="font-family: Arial, Helvetica; font-size:.95em;text-align:left;width:90%;padding:0px 0px 10px 10px;">
                        <label>You have <strong>0</strong> items in your Shopping Cart</label>
                        <br>
                    </div>
                    <a href="./en-us/mf/landingProductFeatures?2-1.ILinkListener-hf_pnl_mf_nprd_HeaderTopPanel_0-viewCartDiv-goToCart&amp;random=9b6f0825-fef7-4ce5-9ea4-7cb347b3f029"><img alt="Go to Cart" class="jqmClose" src="//test-static.norton.com/estore/images//en/Non-Product/Icons/cart.gif" onclick="Sym.CartCount.hide();"></a>
                    <div style="width:100%;text-align:right;">
                        <a title="" class="jqmClose cartCountClose" id="ide" href="./en-us/mf/landingProductFeatures?2-1.ILinkListener-hf_pnl_mf_nprd_HeaderTopPanel_0-viewCartDiv-removeViewCartDiv&amp;random=347d4085-281b-4a8c-834f-47bc0abe5715" onclick="var wcall=wicketAjaxGet('./en-us/mf/landingProductFeatures?2-1.IBehaviorListener.0-hf_pnl_mf_nprd_HeaderTopPanel_0-viewCartDiv-removeViewCartDiv&amp;random=feb9e9c9-5836-43f7-bd0a-8d8896c78b24',function() { }.bind(this),function() { }.bind(this), function() {return Wicket.$('ide') != null;}.bind(this));return !wcall;">x</a>
                        <br>
                        <br>
                    </div>
                </td>
                <td>
                    <div class="divCartOverlayRight">
                    </div>
                </td>
            </tr>
        </tbody></table>
    </div>
	<div class="user">
		<span class="signin">
			<a rel="shadowbox;height=410;width=650" id="signin" href="https://buy-dev12.norton.com/estore/SSOSignin?targetUrl=https%3A%2F%2Fbuy-dev12.norton.com%2Fmf%2FlandingProductFeatures%3F&amp;locale=en&amp;displocale=iso3:USA&amp;displang=iso3:ENG">Sign In</a>
			<span id="signintext">to access your Norton Account</span>	
		</span>
		
		<span class="signout">
			
		</span>
		<span class="seperator"> | </span>
		
	</div>	
</div>
        </div>

    </div>
</html>