<%@ include file="/libs/foundation/global.jsp" %>
<html>

    <cq:includeClientLib categories="symantec"/>

    <head></head>

    <%
    String software = properties.get("software", "software");
	String upgrade = properties.get("upgrade", "upgrade");
	String free = properties.get("free", "free");
	String live = properties.get("live", "live");
	String promotions = properties.get("promotions", "promotions");
	String user = properties.get("user", "user");
	String member = properties.get("member", "member");
	String find = properties.get("find", "find");
	String cust = properties.get("cust", "cust");
    %>
<div>
  <div id="lftNavPane" style="width:40%; margin-top:11px;">
        <div class="gSnavTop">
            <div class="gs1">
                <b></b>
            </div>
            <div class="gs2">
            </div>
            <div class="gs3">
                <b></b>
            </div>
            <div class="gs4">
                <b></b>
            </div>
        </div>
        <div id="lftNav" style="border-left: #DDD 1px solid;
    border-right: #DDD 1px solid;">
            <div id="divlftNavExpnd" style="visibility: hidden; display: none;">
                <div class="lftNavMainNavSw lftNavOnDownLeftArrow">
                    <span onclick="Sym.LeftNav.collapseCategoryList('divlftNavExpnd');Sym.LeftNav.expandCategoryList('divlftNavClpse')" style="

																		background: transparent url("/content/dam/symantec/orangeArrowSmDN.gif") no-repeat scroll left !important;"></span>
                    <a title="" href="/en-us/norton-software">Software</a>
                </div>
                <div class="lftNavSubNav">
                    <a href="/en-us/all-in-one-security"><span>All-In-One Security</span></a>
                </div><div class="lftNavSubNav">
                    <a href="/en-us/backup-and-restore"><span>Backup and Restore</span></a>
                </div><div class="lftNavSubNav">
                    <a href="/en-us/identity-protection-software"><span>Identity Protection Software</span></a>
                </div><div class="lftNavSubNav">
                    <a href="/en-us/mac"><span>Mac</span></a>
                </div><div class="lftNavSubNav">
                    <a href="/en-us/other"><span>Other</span></a>
                </div><div class="lftNavSubNav">
                    <a href="/en-us/pc-security"><span>PC Security</span></a>
                </div><div class="lftNavSubNav">
                    <a href="/en-us/pc-tuneup"><span>PC Tuneup</span></a>
                </div><div class="lftNavSubNav">
                    <a href="/en-us/support-service"><span>Support Service</span></a>
                </div>
            </div>
            <div id="divlftNavClpse" style="visibility: visible; display: block;">
                <div class="lftNavMainNavSw">
                    <span class="arrowL" onclick="Sym.LeftNav.collapseCategoryList('divlftNavClpse');Sym.LeftNav.expandCategoryList('divlftNavExpnd')"></span>
                    <a href="#" title=""><%=software%></a>
                </div>
            </div>
            <div class="lftNavMainNav">
                <a href="http://buy-dev12.norton.com/en-us/mf/multiUserPack" title="" class="lftNavArrowMargin"><span><%=upgrade%></span></a>
            </div><div class="lftNavMainNav">
                <a href="http://us.norton.com/downloads/" title="" class="lftNavArrowMargin"><span><%=free%></span></a>
            </div><div class="lftNavMainNav">
                <a href="http://buy-dev12.norton.com/en-us/mf/premiumServices" title="" class="lftNavArrowMargin"><span><%=live%></span></a>
            </div><div class="lftNavMainNav">
                <a href="http://buy-dev12.norton.com/en-us/mf/landingPromotion" title="" class="lftNavArrowMargin"><span><%=promotions%></span></a>
            </div><div class="lftNavMainNav">
                <a href="http://buy-dev12.norton.com/en-us/mf/multiUserPack" title="" class="lftNavArrowMargin"><span><%=user%></span></a>
            </div><div class="lftNavMainNav">
                <a href="http://buy-dev12.norton.com/en-us/mf/multiUserPack" title="" class="lftNavArrowMargin" style="display: block;"><span><%=member%></span></a>
            </div>
            <div class="lftNavBottomPadding">
            </div>
        <div class="lftNavdottedOverline" style="background:'/content/dam/symantec/sym-cssDottedHorizontal.gif'; !important">
            </div>
            <div class="lftNavMainNav">
                <a title="" class=" lftNavArrowMargin" href="https://www.mynortonaccount.com/amsweb/redirect.do?tok=&amp;fpage=orderHistory&amp;product_lang=EN"><%=find%>   </a>
            </div>
            <div id="lftNavHflyoutDiv">
                <ul id="lftNavHflyoutMenu">
                    <li>
                        <a class="parent" href="http://buy.norton.com/support"><%=cust%></a>

                    </li>
                </ul>
            </div>
        </div>
        <div class="gSnavBtm">
            <div class="gs4">
                <b></b>
            </div>
            <div class="gs3">
                <b></b>
            </div>
            <div class="gs2">
            </div>
            <div class="gs1">
                <b></b>
            </div>
        </div>
    </div>
    <div id="lftNavPane2" class="lftNavPaneMargin">
        <div class="gSnavTop">
            <div class="gs1">
                <b></b>
            </div>
            <div class="gs2">
            </div>
            <div class="gs3">
                <b></b>
            </div>
            <div class="gs4">
                <b></b>
            </div>
        </div>
        <div id="lftNav">
            <div id="lftNavOnNoArrows" class="lftNavMainNav">
                <a class="gryLnk" href="    /en-us/mf/landingProductFeatures&#9;&#10;">Home and Home Office</a>
            </div>
            <div class="lftNavMainNav">
                <a title="" class=" lftNavArrowMargin" href="    /en-us/mf/landingProductFeatures&#9;&#10;"> Business Store </a>
            </div>
        </div>
        <div class="gSnavBtm">
            <div class="gs4">
                <b></b>
            </div>
            <div class="gs3">
                <b></b>
            </div>
            <div class="gs2">
            </div>
            <div class="gs1">
                <b></b>
            </div>
        </div>
    </div>
</div>

    

</html>