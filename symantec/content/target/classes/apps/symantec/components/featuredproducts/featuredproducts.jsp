<%@ include file="/libs/foundation/global.jsp"%>
<html>
<cq:includeClientLib categories="symantec_avi" />
<head>
<script type="text/javascript">
    $(document).ready(function()
		{
            $.ajax({
            	type : "GET",
            	url : "/bin/epcortex/product",
            	success : function(data)
            	{
                    if(data.includes('CQ.WCM.edit({"path":"/content/symantec/checkout/jcr:content/par4","type":"foundation/components/parsys","csp":"loginmaincomp|page/par4|parsys","isContainer":true});')){
						var originaldata = data.split('CQ.WCM.edit({"path":"/content/symantec/checkout/jcr:content/par4","type":"foundation/components/parsys","csp":"loginmaincomp|page/par4|parsys","isContainer":true});');
                    	var product = originaldata[1].split('</html>');
                    	var product1 = product[1].split('=');
                    	$('#prod_name').text(product1[0]);
                		$('#prod_price').text(product1[2]);
                		$('#prod_desc').text(product1[1]);
                    }
                    if(data.includes('<cq data-path="/content/symantec/checkout/jcr:content/par4" data-config="{&quot;path&quot;:&quot;/content/symantec/checkout/jcr:content/par4&quot;,&quot;type&quot;:&quot;foundation/components/parsys&quot;,&quot;csp&quot;:&quot;loginmaincomp|page/par4|parsys&quot;,&quot;isContainer&quot;:true}"></cq>')){
						var originaldata = data.split('<cq data-path="/content/symantec/checkout/jcr:content/par4" data-config="{&quot;path&quot;:&quot;/content/symantec/checkout/jcr:content/par4&quot;,&quot;type&quot;:&quot;foundation/components/parsys&quot;,&quot;csp&quot;:&quot;loginmaincomp|page/par4|parsys&quot;,&quot;isContainer&quot;:true}"></cq>');
                    	var product = originaldata[1].split('</html>');
                    	var product1 = product[1].split('=');
                    	$('#prod_name').text(product1[0]);
                		$('#prod_price').text(product1[2]);
                		$('#prod_desc').text(product1[1]);
                    }
            	},
                error:function(error)
				{
					alert(error);
            	}
        	});
    });
</script>
</head>
<%
	String desc = properties.get("description", "");
%>
<div class="storePageProductOptionTable positioning" style="margin-left: -268px; width: 720px; background-color: white;">
	<form id="productForm">
	<table cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<th scope="col" class="productOptionHeading" colspan="4"><span
					class="spanHeadingDark">Featured Products</span></th>
			</tr>
			<tr>
				<td
					class="tdBorderRight tdBorderLeft tdBorderTop fourWideOptionColumn"
					align="center">
					<div class="ProductImage">
						<a href="http://buy-dev12.norton.com/en-us/norton-security/MggKNTMxNzAxFho3bmFMbmlkclBndWRvRnRjdGFlZXJ1ByJzMDAxMDAwASgw/">
						<img width="140" height="140" class="imgProductBoxshot" src="${properties.fileReference}" alt="prod_image" >
						</a>
					</div>
				</td>
			</tr>

			<tr>
				<td class="tdBorderRight tdBorderLeft fourWide alignTop"><a
					href="http://buy-dev12.norton.com/en-us/norton-security/MggKNTMxNzAxFho3bmFMbmlkclBndWRvRnRjdGFlZXJ1ByJzMDAxMDAwASgw/">
						<span id="prod_name" class="spanProdTitle" name="prod_name"></span>
				</a></td>
			</tr>


			<tr>
				<td class="tdBorderRight tdBorderLeft alignTop"><span
					class="spanProdDesc"><span><%=desc %></span>&nbsp; <a
						class="moreInfo"
						href="http://buy-dev12.norton.com/en-us/norton-security/MggKNTMxNzAxFho3bmFMbmlkclBndWRvRnRjdGFlZXJ1ByJzMDAxMDAwASgw/"
						onclick="var w = window.open(href, '', 'scrollbars=yes,location=no,menuBar=no,resizable=no,status=no,toolbar=no,width=850,height=550,left=200,top=200'); if(w.blur) w.focus(); return false;">More
							Info</a> </span></td>
			</tr>

			<tr>
				<td class="tdBorderRight tdBorderLeft wasNowElem">
					<div class="prd_salePrice">
						<span id="prod_price" class="price"></span>
					</div>
					<span class="spanSeatCnt" id="prod_desc"></span>
				</td>
			</tr>

			<tr>
				<td class="tdBorderLeft tdBorderRight tdBorderBottom">
					<div class="divBuyActionLink">
						<a href="/content/symantec/checkout.html?wcmmode=disabled">
						 <img class="estore_button sym_button" src="/content/dam/symantec/btn_buynow.png" alt="buy_now"> 
						 </a> 

						<!--<input type="button" value="Buy Now" id="product1">-->
					</div>
				</td>

				<td class="tdBorderRight tdBorderBottom">
					<div class="divBuyActionLink"></div>
				</td>

			</tr>
		</tbody>
	</table>
</form>
</div>

</html>