<%@ include file="/libs/foundation/global.jsp" %>
<html>
<!--<cq:includeClientLib categories="symantec_lib"/>-->

<%
String textyears = properties.get("years", "");%>


<div class="footer-area">
    <div class="container" id="footerContainer">

        <div class="symLogo"><img src="${properties.fileReference}" alt="footer_logo"></div>
        <div class="symCopyRight aemenabled" id="symCopyRight"><%=textyears %></div>
        <div class="footer_links aemenabled">

            <%
        String title[] = properties.get("title", new String[0]);
        String links[] = properties.get("path", new String[0]);
        
        for(int i=0; i<title.length ;i++)
        {  %>
        <a href="<%=links[i]%>"><%=title[i]%></a>&nbsp;
      <% }
		%>

              <!--
          </span>
          <span>
            <a href="http://www.symantec.com/about/profile/policies/privacy.jsp" target="_blank">Privacy Notice</a>
          </span>
          <span id="refundPolicy" style="display: none;">
            <a href="http://www.norton.com/store_return_policy" target="_blank">Refund Policy</a>-->

        </div>
      
    </div>
    </div>
</html>