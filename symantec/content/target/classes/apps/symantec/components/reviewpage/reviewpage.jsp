<%@ include file="/libs/foundation/global.jsp"%>

<cq:includeClientLib categories="symantec_lib" />
<html>
<head>
    <script type="text/javascript">
    function expandbillpage(){
    	$("#billingSection").show();
		$("#loginSection").hide();
		$("#reviewSection").hide();
    }
    </script>
</head>
<body>
	<%
    	String name = null;
		String desc = null;
    	String price = null;

    ServletContext sc = getServletContext();
	name = sc.getAttribute("name").toString();
    desc = sc.getAttribute("description").toString();
    price = sc.getAttribute("price").toString();

		String title1 = properties.get("title1", "Review Your Order");
		String title2 = properties.get("title2", "Customer Information");
		String title3 = properties.get("title3", "Your Order");
		String heading = properties.get("heading", "Norton Automatic Renewal Service");
		String paragraph = properties.get("paragraph", "paragraph");
		String subtotal = properties.get("subtotal", "Sub total");
		String tax = properties.get("tax", "tax");
		String total = properties.get("total", "Total");
		String button1 = properties.get("button", "default text");
	%>
	<div id="reviewSection" style="width: 570px;" class="container">
		<div style="width: 570px;" class="container">
			<h2 style="background: #9B9B9B;">
				<span style="display: inline-block; width: 210px; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">Welcome,<span id ="user_name2"></span></span>
				<a href="#" style="color: white; margin-left: 250px;" onclick="logout();">Log out</a>
			</h2>
		</div>

		<div style="width: 570px;" class="container">
			<h2 style="background: #9B9B9B;">
				<span
					style="display: inline-block; width: 210px; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">Billing
					Information</span> <a href="#" style="color: white; margin-left: 250px;" onclick="expandbillpage()">Edit</a>
			</h2>
		</div>
		<div style="width: 570px;" class="container">

			<h2><%=title1 %></h2>

			<h3><%=title2 %></h3>

			<table>
				<tr>
					<td>Name:</td>
                    <td><span id ="card_holder"></span></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Payment Method:&nbsp;</td>
					<td><span id ="card_type"></span>&nbsp;</td>
					<td>ending in&nbsp;</td>
					<td><span id ="c_comp"></span></td>
				</tr>
			</table>

			<h3><%=title3 %></h3>
			<b><%=name %></b><br><%=desc %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Price:
				<%=price %></b><br> Subscription Service with Download Qty: 1<br>
			<br>
			_____________________________________________________________________<br>
			<br> <b style="color: black; font-size: 14px; opacity: 0.5;"><%=heading%></b><br>
			<p style="color: black; font-size: 12px; opacity: 0.5;"><%=paragraph%></p>
			<br>
			<div style="background: #FFF8E0; width: 570px;" class="container">
				<table bgcolor=#FFF8E0 width=50% align="right">
					<tr align="right">
						<td align="right"><%=subtotal%></td>
						<td align="right"><%=price %></td>
					</tr>
					<tr>
						<td align="right"><%=tax%></td>
						<td align="right"><span id="tax_bill"></span></td>
					</tr>
					<tr>
						<td align="right"><%=total%></td>
						<td align="right"><span id="total_bill"></span></td>
					</tr>
				</table>
			</div>
			<br>

			<div align="right">
				Your subscription begins when you complete your purchase.<br> <br>
				<form id="reviewForm">
					<input class="button" type="SUBMIT" value="${properties.button}" onclick="reviewpageform();">
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function reviewpageform()
		{
			var email = $("#user_name2").text();
			var total = $("#total_bill").text();
			$.ajax({
				type : 'GET',
				url : '/bin/review',
				data : $('#reviewForm').serialize() + '&email=' + email + "&total" + total,
				cache : false,
				success : function(data)
				{
					window.location.href = "/content/symantec/confirmationpage.html?email="+email+"&total="+total+"";
				},
				error : function(error) {
					alert(error);
				}
			});
		}
	</script>
</body>
</html>