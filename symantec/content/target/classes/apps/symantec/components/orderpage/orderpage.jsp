<%@ include file="/libs/foundation/global.jsp"%>
<!--<cq:includeClientLib categories="symantec_lib" />-->
<html>
<head>
</head>
<body>
	<% 
    String name = null;
	String desc = null;
    String price = null;

    ServletContext sc = getServletContext();
	name = sc.getAttribute("name").toString();
    desc = sc.getAttribute("description").toString();
    price = sc.getAttribute("price").toString();
	%>

    <div class="container" style="width: 300px;">
		<h2>${properties.title}</h2>

		<div class="left">
			<img src="/content/dam/symantec/top_icon.png"/ >
            <b style="font-size: 14px; margin-left: 20px;"><%=name %></b>
            <p style="font-size: 11px; float: right; margin-top: -20px;"><%=desc %></p>
            <p style="font-size: 11px; float: right; margin-left:72px; margin-top: -5px;">${properties.para1} <br> Qty: 1</p>
		</div>

		<div>
            <b style = "float: right; font-color: black; font-weight">Price : <%=price %></b>
		</div>

		<p style="color: #aaa">_____________________________________</p>

		<div class="left">
			<input type="text" placeholder="Enter the coupon code">&nbsp&nbsp<a
				href="#" ;style="font-color: red;">${properties.apply}</a>
		</div>
		<br />

        <table style="width: 100%; margin-top: 30px;" border="0">
            <tr style="color: #666; font-weight: 600;" >
                <td align="left">Subtotal:</td>
                <td align="right"><%=price %></td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr style="color: #666; font-weight: 600; height: 30px;">
                <td align="left">Tax:</td>
                <td align="right"><span id="tax">May Apply</span></td>
            </tr>
            <tr><td colspan="2"><p style="color: #aaa">_____________________________________</p></td></tr>
            <tr style="font-weight: 600; color: black;">
                <td align="left">Total:</td>
                <td align="right"><span id="total"><%=price %></span></td>
            </tr>
        </table>
		<br><br>
		<div style="opacity: 0.5;">We accept:</div>
		<br />
			<img src="${properties.fileReference}" alt="image" /> 
            <img src="${properties.fileReference1}" alt="image" /> 
            <img src="${properties.fileReference2}" alt="image" /> 
            <img src="${properties.fileReference3}" alt="image" />
            <img src="${properties.fileReference4}" alt="image" />
            <img src="${properties.fileReference5}" alt="image" /><br><br>
            <img src="${properties.fileReference6}" alt="image" />
        	<img src="${properties.fileReference7}" alt="image" /><br>
	</div>
</body>
</html>