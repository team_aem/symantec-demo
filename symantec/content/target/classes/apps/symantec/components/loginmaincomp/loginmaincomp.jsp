<%@ include file="/libs/foundation/global.jsp" %>
<cq:include script="/libs/wcm/core/components/init/init.jsp"/>
<cq:includeClientLib categories="symantec_lib" />
<html>
	<head>
	</head>
    <body>
		<div>
			<cq:include path="par1" resourceType="foundation/components/parsys"/>
		</div>
        <div class="container">
        <div style="width:70%; float:left;">
            <cq:include path="par2" resourceType="foundation/components/parsys"/>
        </div>
        <div style="width:30%; float:right;">
            <cq:include path="par3" resourceType="foundation/components/parsys"/>
        </div>
            </div>
        <div>
            <cq:include path="par4" resourceType="foundation/components/parsys"/>
		</div>
	</body>
</html>