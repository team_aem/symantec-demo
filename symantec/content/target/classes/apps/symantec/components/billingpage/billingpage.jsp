<%@ include file="/libs/foundation/global.jsp"%>
<cq:includeClientLib categories="symantec_lib" />
<!DOCTYPE html>
<head>
<meta charset="ISO-8859-1">
<style type="text/css">
body {
	color: #595959;
	font: 14px/1.3 Arial, Helvetica, sans-serif;
}
</style>
</head>
<body>
<%
		String payment = properties.get("payment", "payment");
		String holder = properties.get("holder", "holder");
		String card = properties.get("card", "card");
		String expiry = properties.get("expiry", "expiry");
		String security = properties.get("security", "security");
		String cvv = properties.get("cvv", "CVV para");
		String zip = properties.get("zip", "zip");
		String city = properties.get("city", "city");
		String state = properties.get("state", "state");
		String country = properties.get("country", "country");
		String phone = properties.get("phone", "Phone");
		String last = properties.get("last", "mobile para");
%>
	<div id="billingSection" style="width: 570px;" class="container">
		<div style="width: 570px;" class="container">
			<h2 style="background: #9B9B9B;">
				<span style="display: inline-block; width: 210px; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">Welcome,
                    <span id="user_name1"></span>
                </span>
                <a href="#" style="color: white; margin-left: 250px;" onclick="logout();">Log out</a>
			</h2>
		</div>

		<div style="width: 570px;" class="container">
			<h2>Billing Information</h2>
			<br />
			<form id="billingForm">
				<%=payment%><br /> <select class="text"
					id="paymentMethod" style="width: 250px;">
					<option value="CC" name="Credit Card">Credit Card</option>
					<option value="DW" name="PayPal">PayPal</option>
				</select><br /> <br /><%=holder%><br /> <input class="text"
					style="width: 250px;" name="cardName" type="text"
					placeholder="enter the name on your card"
					onblur="cardHolderName();" /><br />
				<p id="cname"></p>

				<%=card%><br /> <input class="text" style="width: 250px;"
					name="cardNumber" type="text"
					placeholder="example: 1111111111111111" onblur="cardnumber();" />
				<span id="ccomp" name="ccomp"></span> <br />
				<p id="cnumber"></p>

				<%=expiry%><br>
				<table>
					<tr>
						<td><select class="text" name="month" style="width: 150px;"
							onblur="expmonth();">
								<option selected="selected" value="-1">Month</option>
								<option value="01">01-January</option>
								<option value="02">02-February</option>
								<option value="03">03-March</option>
								<option value="04">04-April</option>
								<option value="05">05-May</option>
								<option value="06">06-June</option>
								<option value="07">07-July</option>
								<option value="08">08-August</option>
								<option value="09">09-September</option>
								<option value="10">10-October</option>
								<option value="11">11-November</option>
								<option value="12">12-December</option>
						</select>
							<p id="expmonth"></p></td>
						<td><select class="text" name="year" style="width: 90px;"
							onblur="expyear();" >
							<option selected="selected" value="-1">Year</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
							<option value="2018">2018</option>
							<option value="2019">2019</option>
							<option value="2020">2020</option>
							<option value="2021">2021</option>
							<option value="2022">2022</option>
							<option value="2023">2023</option>
							<option value="2024">2024</option>
							<option value="2025">2025</option>
							<option value="2026">2026</option>
							<option value="2027">2027</option>
							<option value="2028">2028</option>
							<option value="2029">2029</option>
							<option value="2030">2030</option>
							<option value="2031">2031</option>
							<option value="2032">2032</option>
							<option value="2033">2033</option>
							<option value="2034">2034</option>
							<option value="2035">2035</option>
							<option value="2036">2036</option> </select>
							<p id="expyear"></p></td>
					</tr>
				</table>

				<%=security%><br>
				<table>
					<tr>
						<td><input class="text" name="cvv" style="width: 150px;"
							type="password" placeholder="example: 123"
							onblur="securitycode();" />
							<p id="scode"></p></td>
						<td><img id="question1" align="middle"
							src="/content/dam/symantec/icon_question.png" /></td>
					</tr>
				</table>

				<!-- -------------------------------------------------------- -->


				<div
					style="overflow: hidden; border: 1px solid #595959; width: 550px;"
					id="divmsg1">
					<div class="left">
						<img class="cvvimage"
							src="/content/dam/Norton Images/img_visa_cvv_177x115.png"
							alt="cvv" />
					</div>
					<div class="left">
						<p class="divdata1"><%=cvv%></p>
						<p class="divdata1hide" id="hide1">Hide Security Code info</p>
					</div>
				</div>

				<!-- -------------------------------------------------------- -->

				<%=zip%><br> <input class="text" name="zipcode"
					style="width: 150px;" type="text" placeholder="example: 12345"
					onblur="zipCode();" /><br>
				<p id="zipCodeid"></p>

				<%=city%><br> <input class="text" name="city" style="width: 250px;"
					type="text" placeholder="City" onblur="cityFunction();" /><br>
				<p id="cityid"></p>

				<%=state%><br> <select class="text" name="state"
					style="width: 250px;" onblur="stateFunction();">
					<option value="-1">Select State</option>
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>
				</select><br>
				<p id="stateid"></p>

				<%=country%><br> <select class="text" name="country"
					style="width: 250px;" onblur="countryFunction();">
					<option value="AI">Anguilla</option>
					<option value="AG">Antigua and Barbuda</option>
					<option value="AR">Argentina</option>
					<option value="AW">Aruba</option>
					<option value="BS">Bahamas</option>
					<option value="BB">Barbados</option>
					<option value="BZ">Belize</option>
					<option value="BM">Bermuda</option>
					<option value="BO">Bolivia</option>
					<option value="BR">Brazil</option>
					<option value="CA">Canada</option>
					<option value="KY">Cayman Islands</option>
					<option value="CL">Chile</option>
					<option value="CO">Colombia</option>
					<option value="CR">Costa Rica</option>
					<option value="DM">Dominica</option>
					<option value="DO">Dominican Republic</option>
					<option value="EC">Ecuador</option>
					<option value="SV">El Salvador</option>
					<option value="GD">Grenada</option>
					<option value="GU">Guam</option>
					<option value="GT">Guatemala</option>
					<option value="GY">Guyana</option>
					<option value="HT">Haiti</option>
					<option value="HN">Honduras</option>
					<option value="JM">Jamaica</option>
					<option value="JP">Japan</option>
					<option value="MX">Mexico</option>
					<option value="MS">Montserrat</option>
					<option value="NI">Nicaragua</option>
					<option value="PA">Panama</option>
					<option value="PY">Paraguay</option>
					<option value="PE">Peru</option>
					<option value="PR">Puerto Rico</option>
					<option value="KN">Saint Kitts And Nevis</option>
					<option value="LC">Saint Lucia</option>
					<option value="VC">Saint Vincent and the Grenadines</option>
					<option value="GS">South Georgia and the South Sandwich
						Islands</option>
					<option value="SR">Suriname</option>
					<option value="TT">Trinidad and Tobago</option>
					<option value="TC">Turks and Caicos Islands</option>
					<option value="US" selected>United States</option>
					<option value="UY">Uruguay</option>
					<option value="VE">Venezuela</option>
					<option value="VG">Virgin Islands, British</option>
					<option value="VI">Virgin Islands, U.S.</option>
				</select>
				<p id="countryid"></p>

				<%=phone%> (optional)<br> <select class="text"
					name="mobilecc" style="width: 90px;" id="callingCode"
					style="width: 60px;">
					<option value="AI" data-phonecode="+1" data-countrycode="AI">Anguilla
						(+1)</option>
					<option value="AG" data-phonecode="+1" data-countrycode="AG">Antigua
						and Barbuda (+1)</option>
					<option value="AR" data-phonecode="+54" data-countrycode="AR">Argentina
						(+54)</option>
					<option value="AW" data-phonecode="+297" data-countrycode="AW">Aruba
						(+297)</option>
					<option value="BS" data-phonecode="+297" data-countrycode="BS">Bahamas
						(+297)</option>
					<option value="BB" data-phonecode="+1" data-countrycode="BB">Barbados
						(+1)</option>
					<option value="BZ" data-phonecode="+501" data-countrycode="BZ">Belize
						(+501)</option>
					<option value="BM" data-phonecode="+1" data-countrycode="BM">Bermuda
						(+1)</option>
					<option value="BO" data-phonecode="+591" data-countrycode="BO">Bolivia
						(+591)</option>
					<option value="BR" data-phonecode="+55" data-countrycode="BR">Brazil
						(+55)</option>
					<option value="CA" data-phonecode="+1" data-countrycode="CA">Canada
						(+1)</option>
					<option value="KY" data-phonecode="+1" data-countrycode="KY">Cayman
						Islands (+1)</option>
					<option value="CL" data-phonecode="+56" data-countrycode="CL">Chile
						(+56)</option>
					<option value="CO" data-phonecode="+57" data-countrycode="CO">Colombia
						(+57)</option>
					<option value="CR" data-phonecode="+506" data-countrycode="CR">Costa
						Rica (+506)</option>
					<option value="DM" data-phonecode="+1" data-countrycode="DM">Dominica
						(+1)</option>
					<option value="DO" data-phonecode="+1" data-countrycode="DO">Dominican
						Republic (+1)</option>
					<option value="EC" data-phonecode="+593" data-countrycode="EC">Ecuador
						(+593)</option>
					<option value="SV" data-phonecode="+503" data-countrycode="SV">El
						Salvador (+503)</option>
					<option value="GD" data-phonecode="+1" data-countrycode="GD">Grenada
						(+1)</option>
					<option value="GU" data-phonecode="+1" data-countrycode="GU">Guam
						(+1)</option>
					<option value="GT" data-phonecode="+502" data-countrycode="GT">Guatemala
						(+502)</option>
					<option value="GY" data-phonecode="+592" data-countrycode="GY">Guyana
						(+592)</option>
					<option value="HT" data-phonecode="+509" data-countrycode="HT">Haiti
						(+509)</option>
					<option value="HN" data-phonecode="+594" data-countrycode="HN">Honduras
						(+594)</option>
					<option value="JM" data-phonecode="+1" data-countrycode="JM">Jamaica
						(+1)</option>
					<option value="JP" data-phonecode="+81" data-countrycode="JP">Japan
						(+81)</option>
					<option value="MX" data-phonecode="+52" data-countrycode="MX">Mexico
						(+52)</option>
					<option value="MS" data-phonecode="+1" data-countrycode="MS">Montserrat
						(+1)</option>
					<option value="NI" data-phonecode="+505" data-countrycode="NI">Nicaragua
						(+505)</option>
					<option value="PA" data-phonecode="+507" data-countrycode="PA">Panama
						(+507)</option>
					<option value="PY" data-phonecode="+595" data-countrycode="PY">Paraguay
						(+595)</option>
					<option value="PE" data-phonecode="+51" data-countrycode="PE">Peru
						(+51)</option>
					<option value="PR" data-phonecode="+1" data-countrycode="PR">Puerto
						Rico (+1)</option>
					<option value="KN" data-phonecode="+1" data-countrycode="KN">Saint
						Kitts And Nevis (+1)</option>
					<option value="LC" data-phonecode="+1" data-countrycode="LC">Saint
						Lucia (+1)</option>
					<option value="VC" data-phonecode="+1" data-countrycode="VC">Saint
						Vincent and the Grenadines (+1)</option>
					<option value="GS" data-phonecode="+1" data-countrycode="GS">South
						Georgia and the South Sandwich Islands (+1)</option>
					<option value="SR" data-phonecode="+597" data-countrycode="SR">Suriname
						(+597)</option>
					<option value="TT" data-phonecode="+1" data-countrycode="TT">Trinidad
						and Tobago (+1)</option>
					<option value="TC" data-phonecode="+1" data-countrycode="TC">Turks
						and Caicos Islands (+1)</option>
					<option value="US" data-phonecode="+1" data-countrycode="US">United
						States (+1)</option>
					<option value="UY" data-phonecode="+598" data-countrycode="UY">Uruguay
						(+598)</option>
					<option value="VE" data-phonecode="+58" data-countrycode="VE">Venezuela
						(+58)</option>
					<option value="VG" data-phonecode="+1" data-countrycode="VG">Virgin
						Islands, British (+1)</option>
					<option value="VI" data-phonecode="+1" data-countrycode="VI">Virgin
						Islands, U.S. (+1)</option>
				</select> <input class="text" style="width: 150px;" max="10" type="text"
					placeholder="(000) 000-0000" name="mobileno"
					onblur="mobileFunction();"> <img id="question2"
					src="/content/dam/symantec/icon_question.png"><br>
				<p id="mobileNumberID"></p>
				<br>

				<!-- -------------------------------------------------------- -->


				<div
					style="overflow: hidden; border: 1px solid #595959; width: 550px;"
					id="divmsg2">
					<div class="left">
						<p class="divdata2"><%=last %></p>
						<p class="divdata2hide" id="hide2">Hide Info</p>
					</div>
				</div>


				<!-- -------------------------------------------------------- -->

				<br> <input class="button" type="button" value="Next"
					onclick="formSubmit();">
			</form>
		</div>
		<div style="width: 570px;" class="container">
			<h2 style="background: #9B9B9B">Review Your Order</h2>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			//alert("fff");
			$("#reviewSection").hide();

		});

		function formSubmit(e) {
			//alert("fddd");
			//e.preventDefault();
			var text = $("#ccomp").text();
			$.ajax({
				type : 'GET',
				url : '/bin/epcortex/billing',
				data : $('#billingForm').serialize() + '&ccomp=' + text,
				cache : false,
				success : function(data)

				{
					var data1 = data.split(',');

					$("#billingSection").hide();
                	$("#loginSection").hide();
                	$("#reviewSection").show();
                
					$("#card_holder").text(data1[0]);
                 	$("#card_type").text(data1[1]);
                 	$("#c_comp").text(data1[2]);
                	$("#tax").text(data1[3]);
                	$("#total").text(data1[4]);
                	$("#tax_bill").text(data1[3]);
                	$("#total_bill").text(data1[4]);

				},
				error : function(error) {
					alert(error);
				}
			});
		}
	</script>

</body>
</html>
