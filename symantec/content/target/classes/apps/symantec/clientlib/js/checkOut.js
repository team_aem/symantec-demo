// v08.24

/*
 * jqModal - Minimalist Modaling with jQuery
 *   (http://dev.iceburg.net/jquery/jqModal/)
 *
 * Copyright (c) 2007,2008 Brice Burgess <bhb@iceburg.net>
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * $Version: 03/01/2009 +r14
 */

/*
 * jquery.qtip. The jQuery tooltip plugin

 *
 * Copyright (c) 2009 Craig Thompson
 * http://craigsworks.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Launch  : February 2009
 * Version : 1.0.0-rc3
 * Released: Tuesday 12th May, 2009 - 00:00
 * Debug: jquery.qtip.debug.js
 */
// begin JQM
!function(a){a.fn.jqm=function(d){var e={overlay:50,overlayClass:"jqmOverlay",closeClass:"jqcheckOut.jsmClose",trigger:".jqModal",ajax:f,ajaxText:"",target:f,modal:f,toTop:f,onShow:f,onHide:f,onLoad:f};return this.each(function(){return this._jqm?c[this._jqm].c=a.extend({},c[this._jqm].c,d):(b++,this._jqm=b,c[b]={c:a.extend(e,a.jqm.params,d),a:f,w:a(this).addClass("jqmID"+b),s:b},e.trigger&&a(this).jqmAddTrigger(e.trigger),void 0)})},a.fn.jqmAddClose=function(a){return l(this,a,"jqmHide")},a.fn.jqmAddTrigger=function(a){return l(this,a,"jqmShow")},a.fn.jqmShow=function(b){return this.each(function(){b=b||window.event,a.jqm.open(this._jqm,b)})},a.fn.jqmHide=function(b){return this.each(function(){b=b||window.event,a.jqm.close(this._jqm,b)})},a.jqm={hash:{},open:function(b,g){var i=c[b],k=i.c,l="."+k.closeClass,m=parseInt(i.w.css("z-index")),m=m>0?m:3e3,n=a("<div></div>").css({height:"100%",width:"100%",position:"fixed",left:0,top:0,"z-index":m-1,opacity:k.overlay/100});if(i.a)return f;if(i.t=g,i.a=!0,i.w.css("z-index",m),k.modal?(d[0]||j("bind"),d.push(b)):k.overlay>0?i.w.jqmAddClose(n):n=f,i.o=n?n.addClass(k.overlayClass).prependTo("body"):f,e&&(a("html,body").css({height:"100%",width:"100%"}),n)){n=n.css({position:"absolute"})[0];for(var o in{Top:1,Left:1})n.style.setExpression(o.toLowerCase(),"(_=(document.documentElement.scroll"+o+" || document.body.scroll"+o+"))+'px'")}if(k.ajax){var p=k.target||i.w,q=k.ajax,p="string"==typeof p?a(p,i.w):a(p),q="@"==q.substr(0,1)?a(g).attr(q.substring(1)):q;p.html(k.ajaxText).load(q,function(){k.onLoad&&k.onLoad.call(this,i),l&&i.w.jqmAddClose(a(l,i.w)),h(i)})}else l&&i.w.jqmAddClose(a(l,i.w));return k.toTop&&i.o&&i.w.before('<span id="jqmP'+i.w[0]._jqm+'"></span>').insertAfter(i.o),k.onShow?k.onShow(i):i.w.show(),h(i),f},close:function(b){var e=c[b];return e.a?(e.a=f,d[0]&&(d.pop(),d[0]||j("unbind")),e.c.toTop&&e.o&&a("#jqmP"+e.w[0]._jqm).after(e.w).remove(),e.c.onHide?e.c.onHide(e):(e.w.hide(),e.o&&e.o.remove()),f):f},params:{}};var b=0,c=a.jqm.hash,d=[],e=a.browser.msie&&"6.0"==a.browser.version,f=!1,g=a('<iframe src="javascript:false;document.write(\'\');" class="jqm"></iframe>').css({opacity:0}),h=function(b){e&&(b.o?b.o.html('<p style="width:100%;height:100%"/>').prepend(g):a("iframe.jqm",b.w)[0]||b.w.prepend(g)),i(b)},i=function(b){try{a(":input:visible",b.w)[0].focus()}catch(c){}},j=function(b){a()[b]("keypress",k)[b]("keydown",k)[b]("mousedown",k)},k=function(b){var e=c[d[d.length-1]],f=!a(b.target).parents(".jqmID"+e.s)[0];return f&&i(e),!f},l=function(b,d,e){return b.each(function(){var b=this._jqm;a(d).each(function(){this[e]||(this[e]=[],a(this).click(function(){for(var a in{jqmShow:1,jqmHide:1})for(var b in this[a])c[this[a][b]]&&c[this[a][b]].w[a](this);return f})),this[e].push(b)})})}}(jQuery);
// end JQM
$.jqm.params.modal=true;
// begin qtip
// an old version of qtip for compatibility reasons.

//Updating qtip js with some known issues fixes for IE8,IE9,IE10

(function(f){f.fn.qtip=function(B,u){var y,t,A,s,x,w,v,z;if(typeof B=="string"){if(typeof f(this).data('qtip') === 'object' && f(this).data('qtip')){f.fn.qtip.log.error.call(self,1,f.fn.qtip.constants.NO_TOOLTIP_PRESENT,false)}if(B=="api"){return f(this).data("qtip").interfaces[f(this).data("qtip").current]}else{if(B=="interfaces"){return f(this).data("qtip").interfaces}}}else{if(!B){B={}}if(typeof B.content!=="object"||(B.content.jquery&&B.content.length>0)){B.content={text:B.content}}if(typeof B.content.title!=="object"){B.content.title={text:B.content.title}}if(typeof B.position!=="object"){B.position={corner:B.position}}if(typeof B.position.corner!=="object"){B.position.corner={target:B.position.corner,tooltip:B.position.corner}}if(typeof B.show!=="object"){B.show={when:B.show}}if(typeof B.show.when!=="object"){B.show.when={event:B.show.when}}if(typeof B.show.effect!=="object"){B.show.effect={type:B.show.effect}}if(typeof B.hide!=="object"){B.hide={when:B.hide}}if(typeof B.hide.when!=="object"){B.hide.when={event:B.hide.when}}if(typeof B.hide.effect!=="object"){B.hide.effect={type:B.hide.effect}}if(typeof B.style!=="object"){B.style={name:B.style}}B.style=c(B.style);s=f.extend(true,{},f.fn.qtip.defaults,B);s.style=a.call({options:s},s.style);s.user=f.extend(true,{},B)}return f(this).each(function(){if(typeof B=="string"){w=B.toLowerCase();A=f(this).qtip("interfaces");if(typeof A=="object"){if(u===true&&w=="destroy"){while(A.length>0){A[A.length-1].destroy()}}else{if(u!==true){A=[f(this).qtip("api")]}for(y=0;y<A.length;y++){if(w=="destroy"){A[y].destroy()}else{if(A[y].status.rendered===true){if(w=="show"){A[y].show()}else{if(w=="hide"){A[y].hide()}else{if(w=="focus"){A[y].focus()}else{if(w=="disable"){A[y].disable(true)}else{if(w=="enable"){A[y].disable(false)}}}}}}}}}}}else{v=f.extend(true,{},s);v.hide.effect.length=s.hide.effect.length;v.show.effect.length=s.show.effect.length;if(v.position.container===false){v.position.container=f(document.body)}if(v.position.target===false){v.position.target=f(this)}if(v.show.when.target===false){v.show.when.target=f(this)}if(v.hide.when.target===false){v.hide.when.target=f(this)}t=f.fn.qtip.interfaces.length;for(y=0;y<t;y++){if(typeof f(this).data("qtip")==="object"&&f(this).data("qtip")){}}x=new d(f(this),v,t);f.fn.qtip.interfaces[t]=x;if(typeof f(this).data('qtip') === 'object' && f(this).data('qtip')){if(typeof f(this).attr("qtip")==="undefined"){f(this).data("qtip").current=f(this).data("qtip").interfaces.length}f(this).data("qtip").interfaces.push(x)}else{f(this).data("qtip",{current:0,interfaces:[x]})}if(v.content.prerender===false&&v.show.when.event!==false&&v.show.ready!==true){v.show.when.target.bind(v.show.when.event+".qtip-"+t+"-create",{qtip:t},function(C){z=f.fn.qtip.interfaces[C.data.qtip];z.options.show.when.target.unbind(z.options.show.when.event+".qtip-"+C.data.qtip+"-create");z.cache.mouse={x:C.pageX,y:C.pageY};p.call(z);z.options.show.when.target.trigger(z.options.show.when.event)})}else{x.cache.mouse={x:v.show.when.target.offset().left,y:v.show.when.target.offset().top};p.call(x)}}})};function d(u,t,v){var s=this;s.id=v;s.options=t;s.status={animated:false,rendered:false,disabled:false,focused:false};s.elements={target:u.addClass(s.options.style.classes.target),tooltip:null,wrapper:null,content:null,contentWrapper:null,title:null,button:null,tip:null,bgiframe:null};s.cache={mouse:{},position:{},toggle:0};s.timers={};f.extend(s,s.options.api,{show:function(y){var x,z;if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"show")}if(s.elements.tooltip.css("display")!=="none"){return s}s.elements.tooltip.stop(true,false);x=s.beforeShow.call(s,y);if(x===false){return s}function w(){f(this).css({opacity:""});if(s.options.position.type!=="static"){s.focus()}s.onShow.call(s,y);if(f.browser.msie){s.elements.tooltip.get(0).style.removeAttribute("filter")}}s.cache.toggle=1;if(s.options.position.type!=="static"){s.updatePosition(y,(s.options.show.effect.length>0))}if(typeof s.options.show.solo=="object"){z=f(s.options.show.solo)}else{if(s.options.show.solo===true){z=f("div.qtip").not(s.elements.tooltip)}}if(z){z.each(function(){if(f(this).qtip("api").status.rendered===true){f(this).qtip("api").hide()}})}if(typeof s.options.show.effect.type=="function"){s.options.show.effect.type.call(s.elements.tooltip,s.options.show.effect.length);s.elements.tooltip.queue(function(){w();f(this).dequeue()})}else{switch(s.options.show.effect.type.toLowerCase()){case"fade":s.elements.tooltip.fadeIn(s.options.show.effect.length,w);break;case"slide":s.elements.tooltip.slideDown(s.options.show.effect.length,function(){w();if(s.options.position.type!=="static"){s.updatePosition(y,true)}});break;case"grow":s.elements.tooltip.show(s.options.show.effect.length,w);break;default:s.elements.tooltip.show(null,w);s.elements.tooltip.css({opacity:""});break}s.elements.tooltip.addClass(s.options.style.classes.active)}return f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_SHOWN,"show")},hide:function(y){var x;if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"hide")}else{if(s.elements.tooltip.css("display")==="none"){return s}}clearTimeout(s.timers.show);s.elements.tooltip.stop(true,false);x=s.beforeHide.call(s,y);if(x===false){return s}function w(){s.onHide.call(s,y)}s.cache.toggle=0;if(typeof s.options.hide.effect.type=="function"){s.options.hide.effect.type.call(s.elements.tooltip,s.options.hide.effect.length);s.elements.tooltip.queue(function(){w();f(this).dequeue()})}else{switch(s.options.hide.effect.type.toLowerCase()){case"fade":s.elements.tooltip.fadeOut(s.options.hide.effect.length,w);break;case"slide":s.elements.tooltip.slideUp(s.options.hide.effect.length,w);break;case"grow":s.elements.tooltip.hide(s.options.hide.effect.length,w);break;default:s.elements.tooltip.hide(null,w);break}s.elements.tooltip.removeClass(s.options.style.classes.active)}return f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_HIDDEN,"hide")},updatePosition:function(w,x){var C,G,L,J,H,E,y,I,B,D,K,A,F,z;if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"updatePosition")}else{if(s.options.position.type=="static"){return f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.CANNOT_POSITION_STATIC,"updatePosition")}}G={position:{left:0,top:0},dimensions:{height:0,width:0},corner:s.options.position.corner.target};L={position:s.getPosition(),dimensions:s.getDimensions(),corner:s.options.position.corner.tooltip};if(s.options.position.target!=="mouse"){if(s.options.position.target.get(0).nodeName.toLowerCase()=="area"){J=s.options.position.target.attr("coords").split(",");for(C=0;C<J.length;C++){J[C]=parseInt(J[C])}H=s.options.position.target.parent("map").attr("name");E=f('img[usemap="#'+H+'"]:first').offset();G.position={left:Math.floor(E.left+J[0]),top:Math.floor(E.top+J[1])};switch(s.options.position.target.attr("shape").toLowerCase()){case"rect":G.dimensions={width:Math.ceil(Math.abs(J[2]-J[0])),height:Math.ceil(Math.abs(J[3]-J[1]))};break;case"circle":G.dimensions={width:J[2]+1,height:J[2]+1};break;case"poly":G.dimensions={width:J[0],height:J[1]};for(C=0;C<J.length;C++){if(C%2==0){if(J[C]>G.dimensions.width){G.dimensions.width=J[C]}if(J[C]<J[0]){G.position.left=Math.floor(E.left+J[C])}}else{if(J[C]>G.dimensions.height){G.dimensions.height=J[C]}if(J[C]<J[1]){G.position.top=Math.floor(E.top+J[C])}}}G.dimensions.width=G.dimensions.width-(G.position.left-E.left);G.dimensions.height=G.dimensions.height-(G.position.top-E.top);break;default:return f.fn.qtip.log.error.call(s,4,f.fn.qtip.constants.INVALID_AREA_SHAPE,"updatePosition");break}G.dimensions.width-=2;G.dimensions.height-=2}else{if(s.options.position.target.add(document.body).length===1){G.position={left:f(document).scrollLeft(),top:f(document).scrollTop()};G.dimensions={height:f(window).height(),width:f(window).width()}}else{if(typeof s.options.position.target.attr("qtip")!=="undefined"){G.position=s.options.position.target.qtip("api").cache.position}else{G.position=s.options.position.target.offset()}G.dimensions={height:s.options.position.target.outerHeight(),width:s.options.position.target.outerWidth()}}}y=f.extend({},G.position);if((/right/i).test(G.corner)){y.left+=G.dimensions.width}if((/bottom/i).test(G.corner)){y.top+=G.dimensions.height}if((/((top|bottom)Middle)|center/).test(G.corner)){y.left+=(G.dimensions.width/2)}if((/((left|right)Middle)|center/).test(G.corner)){y.top+=(G.dimensions.height/2)}}else{G.position=y={left:s.cache.mouse.x,top:s.cache.mouse.y};G.dimensions={height:1,width:1}}if((/right/i).test(L.corner)){y.left-=L.dimensions.width}if((/bottom/i).test(L.corner)){y.top-=L.dimensions.height}if((/((top|bottom)Middle)|center/).test(L.corner)){y.left-=(L.dimensions.width/2)}if((/((left|right)Middle)|center/).test(L.corner)){y.top-=(L.dimensions.height/2)}I=(f.browser.msie)?1:0;B=(f.browser.msie&&parseInt(f.browser.version.charAt(0))===6)?1:0;if(s.options.style.border.radius>0){if((/Left/i).test(L.corner)){y.left-=s.options.style.border.radius}else{if((/Right/i).test(L.corner)){y.left+=s.options.style.border.radius}}if((/Top/i).test(L.corner)){y.top-=s.options.style.border.radius}else{if((/Bottom/i).test(L.corner)){y.top+=s.options.style.border.radius}}}if(I){if((/top/i).test(L.corner)){y.top-=I}else{if((/bottom/i).test(L.corner)){y.top+=I}}if((/left/i).test(L.corner)){y.left-=I}else{if((/right/i).test(L.corner)){y.left+=I}}if((/leftMiddle|rightMiddle/).test(L.corner)){y.top-=1}}if(s.options.position.adjust.screen===true){y=o.call(s,y,G,L)}if(s.options.position.target==="mouse"&&s.options.position.adjust.mouse===true){if(s.options.position.adjust.screen===true&&s.elements.tip){K=s.elements.tip.attr("rel")}else{K=s.options.position.corner.tooltip}y.left+=((/right/i).test(K))?-6:6;y.top+=((/bottom/i).test(K))?-6:6}if(!s.elements.bgiframe&&f.browser.msie&&parseInt(f.browser.version.charAt(0))==6){f("select, object").each(function(){A=f(this).offset();A.bottom=A.top+f(this).height();A.right=A.left+f(this).width();if(y.top+L.dimensions.height>=A.top&&y.left+L.dimensions.width>=A.left){k.call(s)}})}y.left+=s.options.position.adjust.x;y.top+=s.options.position.adjust.y;F=s.getPosition();if(y.left!=F.left||y.top!=F.top){z=s.beforePositionUpdate.call(s,w);if(z===false){return s}s.cache.position=y;if(x===true){s.status.animated=true;s.elements.tooltip.animate(y,200,"swing",function(){s.status.animated=false})}else{s.elements.tooltip.css(y)}s.onPositionUpdate.call(s,w);if(typeof w!=="undefined"&&w.type&&w.type!=="mousemove"){f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_POSITION_UPDATED,"updatePosition")}}return s},updateWidth:function(z){if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"updateWidth")}else{if(z!=undefined&&typeof z!=="number"){return f.fn.qtip.log.error.call(s,2,"newWidth must be of type number","updateWidth")}}var B=s.elements.contentWrapper.siblings().add(s.elements.tip).add(s.elements.button),y=s.elements.wrapper.add(s.elements.contentWrapper.children()),A=s.elements.tooltip,w=s.options.style.width.max,x=s.options.style.width.min;if(!z){if(typeof s.options.style.width.value==="number"){z=s.options.style.width.value}else{s.elements.tooltip.css({width:"auto"});B.hide();if(f.browser.msie){y.css({zoom:""})}z=s.getDimensions().width;if(!s.options.style.width.value){z=Math.min(Math.max(z,x),w)}}}if(z%2){z-=1}s.elements.tooltip.width(z);B.show();if(s.options.style.border.radius){s.elements.tooltip.find(".qtip-betweenCorners").each(function(C){f(this).width(z-(s.options.style.border.radius*2))})}if(f.browser.msie){y.css({zoom:1});if(s.elements.bgiframe){s.elements.bgiframe.width(z).height(s.getDimensions.height)}}return f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_WIDTH_UPDATED,"updateWidth")},updateStyle:function(w){var z,A,x,y,B;if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"updateStyle")}else{if(typeof w!=="string"||!f.fn.qtip.styles[w]){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.STYLE_NOT_DEFINED,"updateStyle")}}s.options.style=a.call(s,f.fn.qtip.styles[w],s.options.user.style);s.elements.content.css(q(s.options.style));if(s.options.content.title.text!==false){s.elements.title.css(q(s.options.style.title,true))}s.elements.contentWrapper.css({borderColor:s.options.style.border.color});if(s.options.style.tip.corner!==false){if(f("<canvas>").get(0).getContext){z=s.elements.tooltip.find(".qtip-tip canvas:first");x=z.get(0).getContext("2d");x.clearRect(0,0,300,300);y=z.parent("div[rel]:first").attr("rel");B=b(y,s.options.style.tip.size.width,s.options.style.tip.size.height);h.call(s,z,B,s.options.style.tip.color||s.options.style.border.color)}else{if(f.browser.msie){z=s.elements.tooltip.find('.qtip-tip [nodeName="shape"]');z.attr("fillcolor",s.options.style.tip.color||s.options.style.border.color)}}}if(s.options.style.border.radius>0){s.elements.tooltip.find(".qtip-betweenCorners").css({backgroundColor:s.options.style.border.color});if(f("<canvas>").get(0).getContext){A=g(s.options.style.border.radius);s.elements.tooltip.find(".qtip-wrapper canvas").each(function(){x=f(this).get(0).getContext("2d");x.clearRect(0,0,300,300);y=f(this).parent("div[rel]:first").attr("rel");r.call(s,f(this),A[y],s.options.style.border.radius,s.options.style.border.color)})}else{if(f.browser.msie){s.elements.tooltip.find('.qtip-wrapper [nodeName="arc"]').each(function(){f(this).attr("fillcolor",s.options.style.border.color)})}}}return f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_STYLE_UPDATED,"updateStyle")},updateContent:function(A,y){var z,x,w;if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"updateContent")}else{if(!A){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.NO_CONTENT_PROVIDED,"updateContent")}}z=s.beforeContentUpdate.call(s,A);if(typeof z=="string"){A=z}else{if(z===false){return}}if(f.browser.msie){s.elements.contentWrapper.children().css({zoom:"normal"})}if(A.jquery&&A.length>0){A.clone(true).appendTo(s.elements.content).show()}else{s.elements.content.html(A)}w=0;x=s.elements.content.find("img");if(x.length){if(f.fn.qtip.preload){x.each(function(){preloaded=f('body > img[src="'+f(this).attr("src")+'"]:first');if(preloaded.length>0){f(this).attr("width",preloaded.innerWidth()).attr("height",preloaded.innerHeight())}});B()}else{x.bind("load error",function(){if(++w===x.length){B()}})}}else{B()}function B(){s.updateWidth();if(y!==false){if(s.options.position.type!=="static"){s.updatePosition(s.elements.tooltip.is(":visible"),true)}if(s.options.style.tip.corner!==false){n.call(s)}}}s.updateWidth();if(y!==false){if(s.options.position.type!=="static"){s.updatePosition(s.elements.tooltip.is(":visible"),true)}if(s.options.style.tip.corner!==false){n.call(s)}}s.onContentUpdate.call(s);return f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_CONTENT_UPDATED,"loadContent")},loadContent:function(w,z,A){var y;if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"loadContent")}y=s.beforeContentLoad.call(s);if(y===false){return s}if(A=="post"){f.post(w,z,x)}else{f.get(w,z,x)}function x(B){s.onContentLoad.call(s);f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_CONTENT_LOADED,"loadContent");s.updateContent(B)}return s},updateTitle:function(w){if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"updateTitle")}else{if(!w){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.NO_CONTENT_PROVIDED,"updateTitle")}}returned=s.beforeTitleUpdate.call(s);if(returned===false){return s}if(s.elements.button){s.elements.button=s.elements.button.clone(true)}s.elements.title.html(w);if(s.elements.button){s.elements.title.prepend(s.elements.button)}s.onTitleUpdate.call(s);return f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_TITLE_UPDATED,"updateTitle")},focus:function(A){var y,x,w,z;if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"focus")}else{if(s.options.position.type=="static"){return f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.CANNOT_FOCUS_STATIC,"focus")}}y=parseInt(s.elements.tooltip.css("z-index"));x=32001+f("div.qtip[qtip]").length-1;if(!s.status.focused&&y!==x){z=s.beforeFocus.call(s,A);if(z===false){return s}f("div.qtip[qtip]").not(s.elements.tooltip).each(function(){if(f(this).qtip("api").status.rendered===true){w=parseInt(f(this).css("z-index"));if(typeof w=="number"&&w>-1){f(this).css({zIndex:parseInt(f(this).css("z-index"))-1})}f(this).qtip("api").status.focused=false}});s.elements.tooltip.css({zIndex:x});s.status.focused=true;s.onFocus.call(s,A);f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_FOCUSED,"focus")}return s},disable:function(w){if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"disable")}if(w){if(!s.status.disabled){s.status.disabled=true;f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_DISABLED,"disable")}else{f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.TOOLTIP_ALREADY_DISABLED,"disable")}}else{if(s.status.disabled){s.status.disabled=false;f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_ENABLED,"disable")}else{f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.TOOLTIP_ALREADY_ENABLED,"disable")}}return s},destroy:function(){var w,x,y;x=s.beforeDestroy.call(s);if(x===false){return s}if(s.status.rendered){s.options.show.when.target.unbind("mousemove.qtip",s.updatePosition);s.options.show.when.target.unbind("mouseout.qtip",s.hide);s.options.show.when.target.unbind(s.options.show.when.event+".qtip");s.options.hide.when.target.unbind(s.options.hide.when.event+".qtip");s.elements.tooltip.unbind(s.options.hide.when.event+".qtip");s.elements.tooltip.unbind("mouseover.qtip",s.focus);s.elements.tooltip.remove()}else{s.options.show.when.target.unbind(s.options.show.when.event+".qtip-create")}if(typeof s.elements.target.data("qtip")=="object"){y=s.elements.target.data("qtip").interfaces;if(typeof y=="object"&&y.length>0){for(w=0;w<y.length-1;w++){if(y[w].id==s.id){y.splice(w,1)}}}}f.fn.qtip.interfaces.splice(s.id,1);if(typeof y=="object"&&y.length>0){s.elements.target.data("qtip").current=y.length-1}else{s.elements.target.removeData("qtip")}s.onDestroy.call(s);f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_DESTROYED,"destroy");return s.elements.target},getPosition:function(){var w,x;if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"getPosition")}w=(s.elements.tooltip.css("display")!=="none")?false:true;if(w){s.elements.tooltip.css({visiblity:"hidden"}).show()}x=s.elements.tooltip.offset();if(w){s.elements.tooltip.css({visiblity:"visible"}).hide()}return x},getDimensions:function(){var w,x;if(!s.status.rendered){return f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.TOOLTIP_NOT_RENDERED,"getDimensions")}w=(!s.elements.tooltip.is(":visible"))?true:false;if(w){s.elements.tooltip.css({visiblity:"hidden"}).show()}x={height:s.elements.tooltip.outerHeight(),width:s.elements.tooltip.outerWidth()};if(w){s.elements.tooltip.css({visiblity:"visible"}).hide()}return x}})}function p(){var s,w,u,t,v,y,x;s=this;s.beforeRender.call(s);s.status.rendered=true;s.elements.tooltip='<div qtip="'+s.id+'" class="qtip '+(s.options.style.classes.tooltip||s.options.style)+'"style="display:none; -moz-border-radius:0; -webkit-border-radius:0; border-radius:0;position:'+s.options.position.type+';">  <div class="qtip-wrapper" style="position:relative; overflow:hidden; text-align:left;">    <div class="qtip-contentWrapper" style="overflow:hidden;">       <div class="qtip-content '+s.options.style.classes.content+'"></div></div></div></div>';s.elements.tooltip=f(s.elements.tooltip);s.elements.tooltip.appendTo(s.options.position.container);s.elements.tooltip.data("qtip",{current:0,interfaces:[s]});s.elements.wrapper=s.elements.tooltip.children("div:first");s.elements.contentWrapper=s.elements.wrapper.children("div:first").css({background:s.options.style.background});s.elements.content=s.elements.contentWrapper.children("div:first").css(q(s.options.style));if(f.browser.msie){s.elements.wrapper.add(s.elements.content).css({zoom:1})}if((/unfocus/i).test(s.options.hide.when.event)){s.elements.tooltip.attr("unfocus",true)}if(typeof s.options.style.width.value=="number"){s.updateWidth()}if(f("<canvas>").get(0).getContext||f.browser.msie){if(s.options.style.border.radius>0){m.call(s)}else{s.elements.contentWrapper.css({border:s.options.style.border.width+"px solid "+s.options.style.border.color})}if(s.options.style.tip.corner!==false){e.call(s)}}else{s.elements.contentWrapper.css({border:s.options.style.border.width+"px solid "+s.options.style.border.color});s.options.style.border.radius=0;s.options.style.tip.corner=false;f.fn.qtip.log.error.call(s,2,f.fn.qtip.constants.CANVAS_VML_NOT_SUPPORTED,"render")}if((typeof s.options.content.text=="string"&&s.options.content.text.length>0)||(s.options.content.text.jquery&&s.options.content.text.length>0)){u=s.options.content.text}else{if(typeof s.elements.target.attr("title")=="string"&&s.elements.target.attr("title").length>0){u=s.elements.target.attr("title").replace("\\n","<br />");s.elements.target.attr("title","")}else{if(typeof s.elements.target.attr("alt")=="string"&&s.elements.target.attr("alt").length>0){u=s.elements.target.attr("alt").replace("\\n","<br />");s.elements.target.attr("alt","")}else{u=" ";f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.NO_VALID_CONTENT,"render")}}}if(s.options.content.title.text!==false){j.call(s)}s.updateContent(u);l.call(s);if(s.options.show.ready===true){s.show()}if(s.options.content.url!==false){t=s.options.content.url;v=s.options.content.data;y=s.options.content.method||"get";s.loadContent(t,v,y)}s.onRender.call(s);f.fn.qtip.log.error.call(s,1,f.fn.qtip.constants.EVENT_RENDERED,"render")}function m(){var F,z,t,B,x,E,u,G,D,y,w,C,A,s,v;F=this;F.elements.wrapper.find(".qtip-borderBottom, .qtip-borderTop").remove();t=F.options.style.border.width;B=F.options.style.border.radius;x=F.options.style.border.color||F.options.style.tip.color;E=g(B);u={};for(z in E){u[z]='<div rel="'+z+'" style="'+((/Left/).test(z)?"left":"right")+":0; position:absolute; height:"+B+"px; width:"+B+'px; overflow:hidden; line-height:0.1px; font-size:1px">';if(f("<canvas>").get(0).getContext){u[z]+='<canvas height="'+B+'" width="'+B+'" style="vertical-align: top"></canvas>'}else{if(f.browser.msie){G=B*2+3;u[z]+='<v:arc stroked="false" fillcolor="'+x+'" startangle="'+E[z][0]+'" endangle="'+E[z][1]+'" style="width:'+G+"px; height:"+G+"px; margin-top:"+((/bottom/).test(z)?-2:-1)+"px; margin-left:"+((/Right/).test(z)?E[z][2]-3.5:-1)+'px; vertical-align:top; display:inline-block; behavior:url(#default#VML)"></v:arc>'}}u[z]+="</div>"}D=F.getDimensions().width-(Math.max(t,B)*2);y='<div class="qtip-betweenCorners" style="height:'+B+"px; width:"+D+"px; overflow:hidden; background-color:"+x+'; line-height:0.1px; font-size:1px;">';w='<div class="qtip-borderTop" dir="ltr" style="height:'+B+"px; margin-left:"+B+'px; line-height:0.1px; font-size:1px; padding:0;">'+u.topLeft+u.topRight+y;F.elements.wrapper.prepend(w);C='<div class="qtip-borderBottom" dir="ltr" style="height:'+B+"px; margin-left:"+B+'px; line-height:0.1px; font-size:1px; padding:0;">'+u.bottomLeft+u.bottomRight+y;F.elements.wrapper.append(C);if(f("<canvas>").get(0).getContext){F.elements.wrapper.find("canvas").each(function(){A=E[f(this).parent("[rel]:first").attr("rel")];r.call(F,f(this),A,B,x)})}else{if(f.browser.msie){F.elements.tooltip.append('<v:image style="behavior:url(#default#VML);"></v:image>')}}s=Math.max(B,(B+(t-B)));v=Math.max(t-B,0);F.elements.contentWrapper.css({border:"0px solid "+x,borderWidth:v+"px "+s+"px"})}function r(u,w,s,t){var v=u.get(0).getContext("2d");v.fillStyle=t;v.beginPath();v.arc(w[0],w[1],s,0,Math.PI*2,false);v.fill()}function e(v){var t,s,y,u,x,w;t=this;if(t.elements.tip!==null){t.elements.tip.remove()}s=t.options.style.tip.color||t.options.style.border.color;if(t.options.style.tip.corner===false){return}else{if(!v){v=t.options.style.tip.corner}}y=b(v,t.options.style.tip.size.width,t.options.style.tip.size.height);t.elements.tip='<div class="'+t.options.style.classes.tip+'" dir="ltr" rel="'+v+'" style="position:absolute; height:'+t.options.style.tip.size.height+"px; width:"+t.options.style.tip.size.width+'px; margin:0 auto; line-height:0.1px; font-size:1px;"></div>';t.elements.tooltip.prepend(t.elements.tip);if(f("<canvas>").get(0).getContext){w='<canvas height="'+t.options.style.tip.size.height+'" width="'+t.options.style.tip.size.width+'"></canvas>'}else{if(f.browser.msie){u=t.options.style.tip.size.width+","+t.options.style.tip.size.height;x="m"+y[0][0]+","+y[0][1];x+=" l"+y[1][0]+","+y[1][1];x+=" "+y[2][0]+","+y[2][1];x+=" xe";w='<v:shape fillcolor="'+s+'" stroked="false" filled="true" path="'+x+'" coordsize="'+u+'" style="width:'+t.options.style.tip.size.width+"px; height:"+t.options.style.tip.size.height+"px; line-height:0.1px; display:inline-block; behavior:url(#default#VML); vertical-align:"+((/top/).test(v)?"bottom":"top")+'"></v:shape>';w+='<v:image style="behavior:url(#default#VML);"></v:image>';t.elements.contentWrapper.css("position","relative")}}t.elements.tip=t.elements.tooltip.find("."+t.options.style.classes.tip).eq(0);t.elements.tip.html(w);if(f("<canvas>").get(0).getContext){h.call(t,t.elements.tip.find("canvas:first"),y,s)}if((/top/).test(v)&&f.browser.msie&&parseInt(f.browser.version.charAt(0))===6){t.elements.tip.css({marginTop:-4})}n.call(t,v)}function h(t,v,s){var u=t.get(0).getContext("2d");u.fillStyle=s;u.beginPath();u.moveTo(v[0][0],v[0][1]);u.lineTo(v[1][0],v[1][1]);u.lineTo(v[2][0],v[2][1]);u.fill()}function n(u){var t,w,s,x,v;t=this;if(t.options.style.tip.corner===false||!t.elements.tip){return}if(!u){u=t.elements.tip.attr("rel")}w=positionAdjust=(f.browser.msie)?1:0;t.elements.tip.css(u.match(/left|right|top|bottom/)[0],0);if((/top|bottom/).test(u)){if(f.browser.msie){if(parseInt(f.browser.version.charAt(0))===6){positionAdjust=((/top/).test(u))?-3:1}else{positionAdjust=((/top/).test(u))?1:2}}if((/Middle/).test(u)){t.elements.tip.css({left:"50%",marginLeft:-(t.options.style.tip.size.width/2)})}else{if((/Left/).test(u)){t.elements.tip.css({left:t.options.style.border.radius-w})}else{if((/Right/).test(u)){t.elements.tip.css({right:t.options.style.border.radius+w})}}}if((/top/).test(u)){t.elements.tip.css({top:-positionAdjust})}else{t.elements.tip.css({bottom:positionAdjust})}}else{if((/left|right/).test(u)){if(f.browser.msie){positionAdjust=(parseInt(f.browser.version.charAt(0))===6)?1:((/left/).test(u)?1:2)}if((/Middle/).test(u)){t.elements.tip.css({top:"50%",marginTop:-(t.options.style.tip.size.height/2)})}else{if((/Top/).test(u)){t.elements.tip.css({top:t.options.style.border.radius-w})}else{if((/Bottom/).test(u)){t.elements.tip.css({bottom:t.options.style.border.radius+w})}}}if((/left/).test(u)){t.elements.tip.css({left:-positionAdjust})}else{t.elements.tip.css({right:positionAdjust})}}}s="padding-"+u.match(/left|right|top|bottom/)[0];x=t.options.style.tip.size[(/left|right/).test(s)?"width":"height"];t.elements.tooltip.css("padding",0);t.elements.tooltip.css(s,x);if(f.browser.msie&&parseInt(f.browser.version.charAt(0))==6){v=parseInt(t.elements.tip.css("margin-top"))||0;v+=parseInt(t.elements.content.css("margin-top"))||0;t.elements.tip.css({marginTop:v})}}function j(){var s=this;if(s.elements.title!==null){s.elements.title.remove()}s.elements.title=f('<div class="'+s.options.style.classes.title+'">').css(q(s.options.style.title,true)).css({zoom:(f.browser.msie)?1:0}).prependTo(s.elements.contentWrapper);if(s.options.content.title.text){s.updateTitle.call(s,s.options.content.title.text)}if(s.options.content.title.button!==false&&typeof s.options.content.title.button=="string"){s.elements.button=f('<a class="'+s.options.style.classes.button+'" style="float:right; position: relative"></a>').css(q(s.options.style.button,true)).html(s.options.content.title.button).prependTo(s.elements.title).click(function(t){if(!s.status.disabled){s.hide(t)}})}}function l(){var t,v,u,s;t=this;v=t.options.show.when.target;u=t.options.hide.when.target;if(t.options.hide.fixed){u=u.add(t.elements.tooltip)}if(t.options.hide.when.event=="inactive"){s=["click","dblclick","mousedown","mouseup","mousemove","mouseout","mouseenter","mouseleave","mouseover","touchstart"];function y(z){if(t.status.disabled===true){return}clearTimeout(t.timers.inactive);t.timers.inactive=setTimeout(function(){f(s).each(function(){u.unbind(this+".qtip-inactive");t.elements.content.unbind(this+".qtip-inactive")});t.hide(z)},t.options.hide.delay)}}else{if(t.options.hide.fixed===true){t.elements.tooltip.bind("mouseover.qtip",function(){if(t.status.disabled===true){return}clearTimeout(t.timers.hide)})}}function x(z){if(t.status.disabled===true){return}if(t.options.hide.when.event=="inactive"){f(s).each(function(){u.bind(this+".qtip-inactive",y);t.elements.content.bind(this+".qtip-inactive",y)});y()}clearTimeout(t.timers.show);clearTimeout(t.timers.hide);if(t.options.show.delay>0){t.timers.show=setTimeout(function(){t.show(z)},t.options.show.delay)}else{t.show(z)}}function w(z){if(t.status.disabled===true){return}if(t.options.hide.fixed===true&&(/mouse(out|leave)/i).test(t.options.hide.when.event)&&f(z.relatedTarget).parents("div.qtip[qtip]").length>0){z.stopPropagation();z.preventDefault();clearTimeout(t.timers.hide);return false}clearTimeout(t.timers.show);clearTimeout(t.timers.hide);t.elements.tooltip.stop(true,true);t.timers.hide=setTimeout(function(){t.hide(z)},t.options.hide.delay)}if((t.options.show.when.target.add(t.options.hide.when.target).length===1&&t.options.show.when.event==t.options.hide.when.event&&t.options.hide.when.event!=="inactive")||t.options.hide.when.event=="unfocus"){t.cache.toggle=0;v.bind(t.options.show.when.event+".qtip",function(z){if(t.cache.toggle==0){x(z)}else{w(z)}})}else{v.bind(t.options.show.when.event+".qtip",x);if(t.options.hide.when.event!=="inactive"){u.bind(t.options.hide.when.event+".qtip",w)}}if((/(fixed|absolute)/).test(t.options.position.type)){t.elements.tooltip.bind("mouseover.qtip",t.focus)}if(t.options.position.target==="mouse"&&t.options.position.type!=="static"){v.bind("mousemove.qtip",function(z){t.cache.mouse={x:z.pageX,y:z.pageY};if(t.status.disabled===false&&t.options.position.adjust.mouse===true&&t.options.position.type!=="static"&&t.elements.tooltip.css("display")!=="none"){t.updatePosition(z)}})}}function o(u,v,A){var z,s,x,y,t,w;z=this;if(A.corner=="center"){return v.position}s=f.extend({},u);y={x:false,y:false};t={left:(s.left<f.fn.qtip.cache.screen.scroll.left),right:(s.left+A.dimensions.width+2>=f.fn.qtip.cache.screen.width+f.fn.qtip.cache.screen.scroll.left),top:(s.top<f.fn.qtip.cache.screen.scroll.top),bottom:(s.top+A.dimensions.height+2>=f.fn.qtip.cache.screen.height+f.fn.qtip.cache.screen.scroll.top)};x={left:(t.left&&((/right/i).test(A.corner)||!t.right)),right:(t.right&&((/left/i).test(A.corner)||!t.left)),top:(t.top&&!(/top/i).test(A.corner)),bottom:(t.bottom&&!(/bottom/i).test(A.corner))};if(x.left){if(z.options.position.target!=="mouse"){s.left=v.position.left+v.dimensions.width}else{s.left=z.cache.mouse.x}y.x="Left"}else{if(x.right){if(z.options.position.target!=="mouse"){s.left=v.position.left-A.dimensions.width}else{s.left=z.cache.mouse.x-A.dimensions.width}y.x="Right"}}if(x.top){if(z.options.position.target!=="mouse"){s.top=v.position.top+v.dimensions.height}else{s.top=z.cache.mouse.y}y.y="top"}else{if(x.bottom){if(z.options.position.target!=="mouse"){s.top=v.position.top-A.dimensions.height}else{s.top=z.cache.mouse.y-A.dimensions.height}y.y="bottom"}}if(s.left<0){s.left=u.left;y.x=false}if(s.top<0){s.top=u.top;y.y=false}if(z.options.style.tip.corner!==false){s.corner=new String(A.corner);if(s.corner.match(/^(right|left)/)){if(y.x!==false){s.corner=s.corner.replace(/(left|right)/,y.x.toLowerCase())}}else{if(y.x!==false){s.corner=s.corner.replace(/Left|Right|Middle/,y.x)}if(y.y!==false){s.corner=s.corner.replace(/top|bottom/,y.y)}}if(s.corner!==z.elements.tip.attr("rel")){e.call(z,s.corner)}}return s}function q(u,t){var v,s;v=f.extend(true,{},u);for(s in v){if(t===true&&(/(tip|classes)/i).test(s)){delete v[s]}else{if(!t&&(/(width|border|tip|title|classes|user)/i).test(s)){delete v[s]}}}return v}function c(s){if(typeof s.tip!=="object"){s.tip={corner:s.tip}}if(typeof s.tip.size!=="object"){s.tip.size={width:s.tip.size,height:s.tip.size}}if(typeof s.border!=="object"){s.border={width:s.border}}if(typeof s.width!=="object"){s.width={value:s.width}}if(typeof s.width.max=="string"){s.width.max=parseInt(s.width.max.replace(/([0-9]+)/i,"$1"))}if(typeof s.width.min=="string"){s.width.min=parseInt(s.width.min.replace(/([0-9]+)/i,"$1"))}if(typeof s.tip.size.x=="number"){s.tip.size.width=s.tip.size.x;delete s.tip.size.x}if(typeof s.tip.size.y=="number"){s.tip.size.height=s.tip.size.y;delete s.tip.size.y}return s}function a(){var s,t,u,x,v,w;s=this;u=[true,{}];for(t=0;t<arguments.length;t++){u.push(arguments[t])}x=[f.extend.apply(f,u)];while(typeof x[0].name=="string"){x.unshift(c(f.fn.qtip.styles[x[0].name]))}x.unshift(true,{classes:{tooltip:"qtip-"+(arguments[0].name||"defaults")}},f.fn.qtip.styles.defaults);v=f.extend.apply(f,x);w=(f.browser.msie)?1:0;v.tip.size.width+=w;v.tip.size.height+=w;if(v.tip.size.width%2>0){v.tip.size.width+=1}if(v.tip.size.height%2>0){v.tip.size.height+=1}if(v.tip.corner===true){v.tip.corner=(s.options.position.corner.tooltip==="center")?false:s.options.position.corner.tooltip}return v}function b(v,u,t){var s={bottomRight:[[0,0],[u,t],[u,0]],bottomLeft:[[0,0],[u,0],[0,t]],topRight:[[0,t],[u,0],[u,t]],topLeft:[[0,0],[0,t],[u,t]],topMiddle:[[0,t],[u/2,0],[u,t]],bottomMiddle:[[0,0],[u,0],[u/2,t]],rightMiddle:[[0,0],[u,t/2],[0,t]],leftMiddle:[[u,0],[u,t],[0,t/2]]};s.leftTop=s.bottomRight;s.rightTop=s.bottomLeft;s.leftBottom=s.topRight;s.rightBottom=s.topLeft;return s[v]}function g(s){var t;if(f("<canvas>").get(0).getContext){t={topLeft:[s,s],topRight:[0,s],bottomLeft:[s,0],bottomRight:[0,0]}}else{if(f.browser.msie){t={topLeft:[-90,90,0],topRight:[-90,90,-s],bottomLeft:[90,270,0],bottomRight:[90,270,-s]}}}return t}function k(){var s,t,u;s=this;u=s.getDimensions();t='<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:false" style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=\'0\'); border: 1px solid red; height:'+u.height+"px; width:"+u.width+'px" />';s.elements.bgiframe=s.elements.wrapper.prepend(t).children(".qtip-bgiframe:first")}f(document).ready(function(){f.fn.qtip.cache={screen:{scroll:{left:f(window).scrollLeft(),top:f(window).scrollTop()},width:f(window).width(),height:f(window).height()}};var s;f(window).bind("resize scroll",function(t){clearTimeout(s);s=setTimeout(function(){if(t.type==="scroll"){f.fn.qtip.cache.screen.scroll={left:f(window).scrollLeft(),top:f(window).scrollTop()}}else{f.fn.qtip.cache.screen.width=f(window).width();f.fn.qtip.cache.screen.height=f(window).height()}for(i=0;i<f.fn.qtip.interfaces.length;i++){var u=f.fn.qtip.interfaces[i];if(u.status.rendered===true&&(u.options.position.adjust.scroll&&t.type==="scroll"||u.options.position.adjust.resize&&t.type==="resize")){u.updatePosition(t,true)}}},100)});f(document).bind("touchstart.qtip",function(t){if(f(t.target).parents("div.qtip").length===0){f(".qtip[unfocus]").each(function(){var u=f(this).qtip("api");if(f(this).is(":visible")&&!u.status.disabled&&f(t.target).add(u.elements.target).length>1){u.hide(t)}})}});f(document).bind("mousedown.qtip",function(t){if(f(t.target).parents("div.qtip").length===0){f(".qtip[unfocus]").each(function(){var u=f(this).qtip("api");if(f(this).is(":visible")&&!u.status.disabled&&f(t.target).add(u.elements.target).length>1){u.hide(t)}})}})});f.fn.qtip.interfaces=[];f.fn.qtip.log={error:function(){return this}};f.fn.qtip.constants={};f.fn.qtip.defaults={content:{prerender:false,text:false,url:false,data:null,title:{text:false,button:false}},position:{target:false,corner:{target:"bottomRight",tooltip:"topLeft"},adjust:{x:0,y:0,mouse:true,screen:false,scroll:true,resize:true},type:"absolute",container:false},show:{when:{target:false,event:"mouseover"},effect:{type:"fade",length:100},delay:140,solo:false,ready:false},hide:{when:{target:false,event:"mouseout"},effect:{type:"fade",length:100},delay:0,fixed:false},api:{beforeRender:function(){},onRender:function(){},beforePositionUpdate:function(){},onPositionUpdate:function(){},beforeShow:function(){},onShow:function(){},beforeHide:function(){},onHide:function(){},beforeContentUpdate:function(){},onContentUpdate:function(){},beforeContentLoad:function(){},onContentLoad:function(){},beforeTitleUpdate:function(){},onTitleUpdate:function(){},beforeDestroy:function(){},onDestroy:function(){},beforeFocus:function(){},onFocus:function(){}}};f.fn.qtip.styles={defaults:{background:"white",color:"#111",overflow:"hidden",textAlign:"left",width:{min:0,max:250},padding:"5px 9px",border:{width:1,radius:0,color:"#d3d3d3"},tip:{corner:false,color:false,size:{width:13,height:13},opacity:1},title:{background:"#e1e1e1",fontWeight:"bold",padding:"7px 12px"},button:{cursor:"pointer"},classes:{target:"",tip:"qtip-tip",title:"qtip-title",button:"qtip-button",content:"qtip-content",active:"qtip-active"}},cream:{border:{width:3,radius:0,color:"#F9E98E"},title:{background:"#F0DE7D",color:"#A27D35"},background:"#FBF7AA",color:"#A27D35",classes:{tooltip:"qtip-cream"}},light:{border:{width:3,radius:0,color:"#E2E2E2"},title:{background:"#f1f1f1",color:"#454545"},background:"white",color:"#454545",classes:{tooltip:"qtip-light"}},dark:{border:{width:3,radius:0,color:"#303030"},title:{background:"#404040",color:"#f3f3f3"},background:"#505050",color:"#f3f3f3",classes:{tooltip:"qtip-dark"}},red:{border:{width:3,radius:0,color:"#CE6F6F"},title:{background:"#f28279",color:"#9C2F2F"},background:"#F79992",color:"#9C2F2F",classes:{tooltip:"qtip-red"}},green:{border:{width:3,radius:0,color:"#A9DB66"},title:{background:"#b9db8c",color:"#58792E"},background:"#CDE6AC",color:"#58792E",classes:{tooltip:"qtip-green"}},blue:{border:{width:3,radius:0,color:"#ADD9ED"},title:{background:"#D0E9F5",color:"#5E99BD"},background:"#E5F6FE",color:"#4D9FBF",classes:{tooltip:"qtip-blue"}}}})(jQuery);

//end qtip

//begin luhn check
!function(a){var b=a.ecom={},c=/^\d+$/,d={visa:{css:"visa",name:"Visa"},master:{css:"masterCard",name:"MasterCard"},ax:{css:"americanExpress",name:"American Express"},discover:{css:"discover",name:"Discover"},jcb:{css:"jcb",name:"JCB"},diners:{css:"dinersClub",name:"Diners Club"},unknown:{css:"unknown",name:"unknown"}};b.trim=function(a){return(a+"").replace(/^\s+|\s+$/g,"")},b.luhnCheck=function(a){for(var b=0,c=a.length,d=c%2,e=0;c>e;e++){var f=parseInt(a.charAt(e),10);e%2===d&&(f*=2),f>9&&(f-=9),b+=f}return 0===b%10},b.cardTypes=function(){var a,b,c,e;for(b={},a=c=40;49>=c;a=++c)b[a]=d.visa;for(a=e=50;59>=e;a=++e)b[a]=d.master;return b[34]=b[37]=d.ax,b[60]=b[62]=b[64]=b[65]=d.discover,b[35]=d.jcb,b[30]=b[36]=b[38]=b[39]=d.diners,b}(),a.ecom.validateCardNumber=function(a){return a=(a+"").replace(/\s+|-/g,""),a.length>=10&&a.length<=19&&b.luhnCheck(a)},a.ecom.validateExpiry=function(a,d){var e,f,g=parseInt(a,10);return a=b.trim(a),d=b.trim(d),!c.test(a)||!c.test(d)||1>g||g>12?!1:(f=new Date(d,a),e=new Date,f.setMonth(f.getMonth()-1),f.setMonth(f.getMonth()+1,1),f>e)},a.ecom.validateCVC=function(a){return a=b.trim(a),c.test(a)&&a.length>=3&&a.length<=4},a.ecom.cardType=function(a){return b.cardTypes[a.slice(0,2)]||d.unknown}}(window.wa||(window.wa={}));
// end luhn check

// Enhance wa with maestro check
(function(wa){
	var maestro = {
        css: "maestro",
        name: "Maestro"
    };
	var maestroRegx = new RegExp("50(1659|4317|5433)|58(1149|8602)|6(16788|27252|3(0490|3857|9098))|"
		+ "5612(4[2345]|5[0678])|"
		+ "5817(0[27]|15|2[14]|3[16789]|4[023456789]|5[016]|6[69]|7[38]|8[47]|9[017])|"
		+ "67(0(3\\d\\d|"
		+ "5(06|10|3[06789]|41|5[18]|85|9[23])|"
		+ "6(19|27|98)|"
		+ "8(0[0789]|1[0123]|21|59|88|90)|"
		+ "9(8[24]|99))|"
		+ "11(24|38|[456]9)|"
		+ "4(3(0[039]|1(2|[6-8])|2([0-4]|6|8)|3(1|[3-9])|4(0|2|[4-8])|5([0-2]|[7-8])|6(0|[3-5]|[7-9])|7(0|[3-4]|6|[8-9])|8([0-1]|[5-6]|9)|9(0|[2-4]|9))|"
		+ "4(0[04689]|1(0|[2-3]|5|[7-9])|2([0-3]|[5-6]|[8-9])|3(0|[3-9])|4([1-2]|[4-9])|5[2-9]|[6-7][0-9]|8([0-3]|[5-6]|[8-9])|9([0-5]|8))|"
		+ "5(0([0-6]|9)|1([0-1]|[3-5]|[7-9])|2[0-9]|3([0-2]|[4-5]|[7-9])|4([2-6]|[8-9])|5(0|[2-7]|9)|6([1-2]|[4-9])|7[12479]|8[1-9]|9([0-6]|8))|"
		+ "6(0([0-2]|9)|1[07]|"
		+ "2([0-5]|[7-9])|3([2-5]|7|9)|4([0-7]|9)|5([0-2]|[4-5]|[8-9])|6([0-1]|[3-7]|9)|"
		+ "7[02789]|8([0-1]|[4-7])|9(0|[3-8]))|7(0[0-9]|1([0-3]|[5-9])|2[0-9]|3(0|[2-7]|9)|4([0-1]|[3-9])|5([0-6]|8)|6([0-2]|[4-7]|9)|[7-8][0-9]|9([024]|[6-9]))|"
		+ "8(3(2|[5-9])|4([1-2]|[4-6]|[8-9])|5([1-3]|[6-9])|"
		+ "6[0-1]))|"
		+ "5(9(05|1([4-6]|9)|2([0-3]|[6-8])|3[0-1]|41|79|8[0-1]|95))|"
		+ "6(18[0-9]|210|4(19|2[34]|3([2-4]|7)|41|66|7[0158]|8[6-9]|9([0-6]|[8-9]))|"
		+ "5(0([2-3]|[7-9])|1([1-3]|[7-9])|2([0-1]|[4-6])|3[478]|4[189]|5[0-8]|6[34]|7[0456]|8[789]|9[18])|"
		+ "6(04|2[6-9]|3[01257]|4[09]|5[58]|7[4-6]|87)|"
		+ "77[04]|8(04|1[14]|2[0459]|33|58|62|79|82|9[28])|"
		+ "9(0[3-5]|1[79]|21|3[38]|41|5[679]|64|78|86|93))|"
		+ "7(0(10|2([1-2]|[4-7])|3[578]|4[19]|52|66|75|87|91)|"
		+ "1(0[278]|15|23|4[24]|6[5-9]|7[01]|8[3468]|97)|"
		+ "2(18|3[12]|4[6-8]|5[134]|78|86|9[3-9])|"
		+ "3(00|22|3[24]|40|55|64|78|8[17]|93)|"
		+ "487|5(44|70|81|9[03])|"
		+ "6(1[89]|2[0-6]|3[3-6]|47|99)|"
		+ "7(0[237]|1[23]|27)))");                     
	var _cardType = wa.ecom.cardType;
	wa.ecom.cardType = function(input){
		var card = _cardType(input);
		if (card.name === 'MasterCard' || card.name === 'Discover' || card.name === 'unknown'){ // MC, Discover, Maestro share BINs.
			var cleanInput = input.replace(/[^\d]/g, '');
			if (cleanInput.length < 6){ // don't identify card until there are enough digts
				return _cardType(''); // if not enought digits, return unknown
			}
			if (maestroRegx.test(cleanInput)){
				console.log('returning maestro');
				return maestro;
			}
			return card;
		}
		return card;
	}
})(wa);

/* jQuery Mask Plugin v1.14.0
 Created by Igor Escobar on 2012-03-10. Please report any bug at http://blog.igorescobar.com

 Copyright (c) 2012 Igor Escobar http://blog.igorescobar.com

 The MIT License (http://www.opensource.org/licenses/mit-license.php)

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 */

// jQuery Mask Plugin v1.14.0
// github.com/igorescobar/jQuery-Mask-Plugin
(function(b){"function"===typeof define&&define.amd?define(["jquery"],b):"object"===typeof exports?module.exports=b(require("jquery")):b(jQuery||Zepto)})(function(b){var y=function(a,e,d){var c={invalid:[],getCaret:function(){try{var r,b=0,e=a.get(0),d=document.selection,f=e.selectionStart;if(d&&-1===navigator.appVersion.indexOf("MSIE 10"))r=d.createRange(),r.moveStart("character",-c.val().length),b=r.text.length;else if(f||"0"===f)b=f;return b}catch(g){}},setCaret:function(r){try{if(a.is(":focus")){var c,
b=a.get(0);b.setSelectionRange?(b.focus(),b.setSelectionRange(r,r)):(c=b.createTextRange(),c.collapse(!0),c.moveEnd("character",r),c.moveStart("character",r),c.select())}}catch(e){}},events:function(){a.on("keydown.mask",function(c){a.data("mask-keycode",c.keyCode||c.which)}).on(b.jMaskGlobals.useInput?"input.mask":"keyup.mask",c.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){a.keydown().keyup()},100)}).on("change.mask",function(){a.data("changed",!0)}).on("blur.mask",function(){n===
c.val()||a.data("changed")||a.trigger("change");a.data("changed",!1)}).on("blur.mask",function(){n=c.val()}).on("focus.mask",function(a){!0===d.selectOnFocus&&b(a.target).select()}).on("focusout.mask",function(){d.clearIfNotMatch&&!p.test(c.val())&&c.val("")})},getRegexMask:function(){for(var a=[],c,b,d,f,l=0;l<e.length;l++)(c=g.translation[e.charAt(l)])?(b=c.pattern.toString().replace(/.{1}$|^.{1}/g,""),d=c.optional,(c=c.recursive)?(a.push(e.charAt(l)),f={digit:e.charAt(l),pattern:b}):a.push(d||
c?b+"?":b)):a.push(e.charAt(l).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));a=a.join("");f&&(a=a.replace(new RegExp("("+f.digit+"(.*"+f.digit+")?)"),"($1)?").replace(new RegExp(f.digit,"g"),f.pattern));return new RegExp(a)},destroyEvents:function(){a.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask "))},val:function(c){var b=a.is("input")?"val":"text";if(0<arguments.length){if(a[b]()!==c)a[b](c);b=a}else b=a[b]();return b},getMCharsBeforeCount:function(a,c){for(var b=0,d=0,
f=e.length;d<f&&d<a;d++)g.translation[e.charAt(d)]||(a=c?a+1:a,b++);return b},caretPos:function(a,b,d,h){return g.translation[e.charAt(Math.min(a-1,e.length-1))]?Math.min(a+d-b-h,d):c.caretPos(a+1,b,d,h)},behaviour:function(d){d=d||window.event;c.invalid=[];var e=a.data("mask-keycode");if(-1===b.inArray(e,g.byPassKeys)){var m=c.getCaret(),h=c.val().length,f=c.getMasked(),l=f.length,k=c.getMCharsBeforeCount(l-1)-c.getMCharsBeforeCount(h-1),n=m<h;c.val(f);n&&(8!==e&&46!==e&&(m=c.caretPos(m,h,l,k)),
c.setCaret(m));return c.callbacks(d)}},getMasked:function(a,b){var m=[],h=void 0===b?c.val():b+"",f=0,l=e.length,k=0,n=h.length,q=1,p="push",u=-1,t,w;d.reverse?(p="unshift",q=-1,t=0,f=l-1,k=n-1,w=function(){return-1<f&&-1<k}):(t=l-1,w=function(){return f<l&&k<n});for(;w();){var x=e.charAt(f),v=h.charAt(k),s=g.translation[x];if(s)v.match(s.pattern)?(m[p](v),s.recursive&&(-1===u?u=f:f===t&&(f=u-q),t===u&&(f-=q)),f+=q):s.optional?(f+=q,k-=q):s.fallback?(m[p](s.fallback),f+=q,k-=q):c.invalid.push({p:k,
v:v,e:s.pattern}),k+=q;else{if(!a)m[p](x);v===x&&(k+=q);f+=q}}h=e.charAt(t);l!==n+1||g.translation[h]||m.push(h);return m.join("")},callbacks:function(b){var g=c.val(),m=g!==n,h=[g,b,a,d],f=function(a,b,c){"function"===typeof d[a]&&b&&d[a].apply(this,c)};f("onChange",!0===m,h);f("onKeyPress",!0===m,h);f("onComplete",g.length===e.length,h);f("onInvalid",0<c.invalid.length,[g,b,a,c.invalid,d])}};a=b(a);var g=this,n=c.val(),p;e="function"===typeof e?e(c.val(),void 0,a,d):e;g.mask=e;g.options=d;g.remove=
function(){var b=c.getCaret();c.destroyEvents();c.val(g.getCleanVal());c.setCaret(b-c.getMCharsBeforeCount(b));return a};g.getCleanVal=function(){return c.getMasked(!0)};g.getMaskedVal=function(a){return c.getMasked(!1,a)};g.init=function(e){e=e||!1;d=d||{};g.clearIfNotMatch=b.jMaskGlobals.clearIfNotMatch;g.byPassKeys=b.jMaskGlobals.byPassKeys;g.translation=b.extend({},b.jMaskGlobals.translation,d.translation);g=b.extend(!0,{},g,d);p=c.getRegexMask();!1===e?(d.placeholder&&a.attr("placeholder",d.placeholder),
a.data("mask")&&a.attr("autocomplete","off"),c.destroyEvents(),c.events(),e=c.getCaret(),c.val(c.getMasked()),c.setCaret(e+c.getMCharsBeforeCount(e,!0))):(c.events(),c.val(c.getMasked()))};g.init(!a.is("input"))};b.maskWatchers={};var A=function(){var a=b(this),e={},d=a.attr("data-mask");a.attr("data-mask-reverse")&&(e.reverse=!0);a.attr("data-mask-clearifnotmatch")&&(e.clearIfNotMatch=!0);"true"===a.attr("data-mask-selectonfocus")&&(e.selectOnFocus=!0);if(z(a,d,e))return a.data("mask",new y(this,
d,e))},z=function(a,e,d){d=d||{};var c=b(a).data("mask"),g=JSON.stringify;a=b(a).val()||b(a).text();try{return"function"===typeof e&&(e=e(a)),"object"!==typeof c||g(c.options)!==g(d)||c.mask!==e}catch(n){}};b.fn.mask=function(a,e){e=e||{};var d=this.selector,c=b.jMaskGlobals,g=c.watchInterval,c=e.watchInputs||c.watchInputs,n=function(){if(z(this,a,e))return b(this).data("mask",new y(this,a,e))};b(this).each(n);d&&""!==d&&c&&(clearInterval(b.maskWatchers[d]),b.maskWatchers[d]=setInterval(function(){b(document).find(d).each(n)},
g));return this};b.fn.masked=function(a){return this.data("mask").getMaskedVal(a)};b.fn.unmask=function(){clearInterval(b.maskWatchers[this.selector]);delete b.maskWatchers[this.selector];return this.each(function(){var a=b(this).data("mask");a&&a.remove().removeData("mask")})};b.fn.cleanVal=function(){return this.data("mask").getCleanVal()};b.applyDataMask=function(a){a=a||b.jMaskGlobals.maskElements;(a instanceof b?a:b(a)).filter(b.jMaskGlobals.dataMaskAttr).each(A)};var p={maskElements:"input,td,span,div",
dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,useInput:function(a){var b=document.createElement("div"),d;a="on"+a;d=a in b;d||(b.setAttribute(a,"return;"),d="function"===typeof b[a]);return d}("input"),watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};b.jMaskGlobals=b.jMaskGlobals||{};p=b.jMaskGlobals=b.extend(!0,{},p,b.jMaskGlobals);
p.dataMask&&b.applyDataMask();setInterval(function(){b.jMaskGlobals.watchDataMask&&b.applyDataMask()},p.watchInterval)});
// end jQuery Mask Plugin v1.14.0

// 
// Creating placeholder text for IE
// comenting out for now to addjust for IE9 password fix - can revisit for IE9 compatability tickets
//!function(a){"use strict";function b(){}function c(){try{return document.activeElement}catch(a){}}function d(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return!0;return!1}function e(a,b,c){return a.addEventListener?a.addEventListener(b,c,!1):a.attachEvent?a.attachEvent("on"+b,c):void 0}function f(a,b){var c;a.createTextRange?(c=a.createTextRange(),c.move("character",b),c.select()):a.selectionStart&&(a.focus(),a.setSelectionRange(b,b))}function g(a,b){try{return a.type=b,!0}catch(c){return!1}}function h(a,b){if(a&&a.getAttribute(B))b(a);else for(var c,d=a?a.getElementsByTagName("input"):N,e=a?a.getElementsByTagName("textarea"):O,f=d?d.length:0,g=e?e.length:0,h=f+g,i=0;h>i;i++)c=f>i?d[i]:e[i-f],b(c)}function i(a){h(a,k)}function j(a){h(a,l)}function k(a,b){var c=!!b&&a.value!==b,d=a.value===a.getAttribute(B);if((c||d)&&"true"===a.getAttribute(C)){a.removeAttribute(C),a.value=a.value.replace(a.getAttribute(B),""),a.className=a.className.replace(A,"");var e=a.getAttribute(I);parseInt(e,10)>=0&&(a.setAttribute("maxLength",e),a.removeAttribute(I));var f=a.getAttribute(D);return f&&(a.type=f),!0}return!1}function l(a){var b=a.getAttribute(B);if(""===a.value&&b){a.setAttribute(C,"true"),a.value=b,a.className+=" "+z;var c=a.getAttribute(I);c||(a.setAttribute(I,a.maxLength),a.removeAttribute("maxLength"));var d=a.getAttribute(D);return d?a.type="text":"password"===a.type&&g(a,"text")&&a.setAttribute(D,"password"),!0}return!1}function m(a){return function(){P&&a.value===a.getAttribute(B)&&"true"===a.getAttribute(C)?f(a,0):k(a)}}function n(a){return function(){l(a)}}function o(a){return function(){i(a)}}function p(a){return function(b){return v=a.value,"true"===a.getAttribute(C)&&v===a.getAttribute(B)&&d(x,b.keyCode)?(b.preventDefault&&b.preventDefault(),!1):void 0}}function q(a){return function(){k(a,v),""===a.value&&(a.blur(),f(a,0))}}function r(a){return function(){a===c()&&a.value===a.getAttribute(B)&&"true"===a.getAttribute(C)&&f(a,0)}}function s(a){var b=a.form;b&&"string"==typeof b&&(b=document.getElementById(b),b.getAttribute(E)||(e(b,"submit",o(b)),b.setAttribute(E,"true"))),e(a,"focus",m(a)),e(a,"blur",n(a)),P&&(e(a,"keydown",p(a)),e(a,"keyup",q(a)),e(a,"click",r(a))),a.setAttribute(F,"true"),a.setAttribute(B,T),(P||a!==c())&&l(a)}var t=document.createElement("input"),u=void 0!==t.placeholder;if(a.Placeholders={nativeSupport:u,disable:u?b:i,enable:u?b:j},!u){var v,w=["text","search","url","tel","email","password","number","textarea"],x=[27,33,34,35,36,37,38,39,40,8,46],y="#ccc",z="placeholdersjs",A=new RegExp("(?:^|\\s)"+z+"(?!\\S)"),B="data-placeholder-value",C="data-placeholder-active",D="data-placeholder-type",E="data-placeholder-submit",F="data-placeholder-bound",G="data-placeholder-focus",H="data-placeholder-live",I="data-placeholder-maxlength",J=100,K=document.getElementsByTagName("head")[0],L=document.documentElement,M=a.Placeholders,N=document.getElementsByTagName("input"),O=document.getElementsByTagName("textarea"),P="false"===L.getAttribute(G),Q="false"!==L.getAttribute(H),R=document.createElement("style");R.type="text/css";var S=document.createTextNode("."+z+" {color:"+y+";}");R.styleSheet?R.styleSheet.cssText=S.nodeValue:R.appendChild(S),K.insertBefore(R,K.firstChild);for(var T,U,V=0,W=N.length+O.length;W>V;V++)U=V<N.length?N[V]:O[V-N.length],T=U.attributes.placeholder,T&&(T=T.nodeValue,T&&d(w,U.type)&&s(U));var X=setInterval(function(){for(var a=0,b=N.length+O.length;b>a;a++)U=a<N.length?N[a]:O[a-N.length],T=U.attributes.placeholder,T?(T=T.nodeValue,T&&d(w,U.type)&&(U.getAttribute(F)||s(U),(T!==U.getAttribute(B)||"password"===U.type&&!U.getAttribute(D))&&("password"===U.type&&!U.getAttribute(D)&&g(U,"text")&&U.setAttribute(D,"password"),U.value===U.getAttribute(B)&&(U.value=T),U.setAttribute(B,T)))):U.getAttribute(C)&&(k(U),U.removeAttribute(B));Q||clearInterval(X)},J);e(a,"beforeunload",function(){M.disable()})}}(this),function(a,b){"use strict";var c=a.fn.val,d=a.fn.prop;b.Placeholders.nativeSupport||(a.fn.val=function(a){var b=c.apply(this,arguments),d=this.eq(0).data("placeholder-value");return void 0===a&&this.eq(0).data("placeholder-active")&&b===d?"":b},a.fn.prop=function(a,b){return void 0===b&&this.eq(0).data("placeholder-active")&&"value"===a?"":d.apply(this,arguments)})}(jQuery,this);

var DEBUG_MODE = false; // Set this value to false for production
var HANDLE_HANDLEBARS_COMPILE_ERROR_FEATURE_FLAG = false; // feature flag for handling handlebars compile error. remove as soon as done. Date added: 2016-05-06.
var AJAX_TIMEOUT_FEATURE_FLAG = false; // feature flag for ajax timeouts. please remove this flag as soon as possible. date addded 2016-05-10.
var PAYPAL_IN_CONTEXT_FEATURE_FLAG = true; // added 2016-06-21
var ROVER_FEATURE_FLAG = true; //feature flag for Rover
var ROVER_PREORDER_FEATURE_FLAG = true; // feature flag for PreOrder
window.geoErrorMsg = "false"; // Used for geo restriction error message

if (AJAX_TIMEOUT_FEATURE_FLAG){
  var AJAX_TIMEOUT = 30000; // in ms
}

if(typeof(console) === 'undefined') {
    console = {}
} 

if(!DEBUG_MODE || typeof(console.log) === 'undefined') {
    console.log = function() {};
}

//OPS-63974
var phoneCodeDropdownStr = 
  '<div class="selectedCallingCode">'+
    '<div id="callingCodeContainer" class="customSelectSkin">'+
      '<select id="callingCode" class="phoneCode">'+
      '</select>'+
      '<span id="callingCodeValue">+1</span>'+
    '</div>'+
  '</div>';

// toggles AR and customer support messaging text on mf and rc comfirmation page
var candyRackLinkUrl = [];
var candyRackremoveCartLink = [];
var removeCartLink = [];
var cartChildItemResult;
var cartItems;
var couponCode;
             var cty;
var deleteCouponCode;
var i;
window.first_hit = true;
logOut_flag = false;
/*omniture Code */

/* Conditional Register Helper to check empty , null string values */
Handlebars.registerHelper('unless_blank', function(item, block) {
  return (item && item.replace(/\s/g,"").length) ? block.fn(this) : block.inverse(this);
});

Handlebars.registerHelper('logic', function(conditional, options) {
  if (conditional) {
    return 'selected="selected"';
  }
});

/* This function handles the logout functionality in the checkout page*/
function logOut(){
	logOut_flag = true;
    // This code is for clearing all the field values and removing any error message starts.
    $(':input').not(':button, :submit, :reset, :checkbox, :radio,select, :input#couponCode.couponcode, :input#couponCode1.couponcode').val('');
    $('#expMonthDropDown').val($("#expMonthDropDown option:first").val());
    $('#expYearDropDown').val($("#expYearDropDown option:first").val());
    $('#stateDropDown').val($("#stateDropDown option:first").val());
	
		/* Added to get Marketing Options selected based on API on logOut CAP-11382*/
		MarketingOptions = window.selectedMarketingOptions;
		for(var i = 0; i < MarketingOptions.length; i++) {
		
		   for(var selected in MarketingOptions[i] ) {
		   console.log( selected + ' == ' +  MarketingOptions[i][selected]);
				if (MarketingOptions[i][selected] == true){
					console.log("MarketingOptions inside logOut"+MarketingOptions[i][selected]);
					$('#e1').prop('checked', true);
				}
				else {
					$(':checkbox, :radio').prop('checked', false);
				}
			}
		}
	
    $('#apassword').val('');
    $('#verifyPassword').val('');
    $('div.errorDiv').addClass('hidden');
    // This code is for clearing all the field values and removing any error message ends.
    $("#LoginInfo").slideDown("slow");
    $('.loginContainer').show();
    $('.clickToggleCopy').hide();
    $('.loginToggle').hide();
    $('.clickToggle').show();
    // Rover Product Feature Check 
    if(ROVER_FEATURE_FLAG && window.cartContainsPhysicalProduct) {
    	console.log("Inside LogOut function for shipping rover check");
  		$("#shippingSec").slideUp();
  		$("#BillingSec").slideUp();
  		$(".shippingInformation .headingBar").addClass("inactiveHeader");
  		$(".billingInformation .headingBar").addClass("inactiveHeader");
  		$("#shippingInformationLink").hide();
	}else{
		$('#BillingSec').slideUp();
		$(".billingInformation .headingBar").addClass("inactiveHeader");
	}
    $('#reviewSec').slideUp();
    $('#logInInfoLink').hide();
    $('#newCartLogOut').hide(); //OPS-87760 cart development logOut functionality
    $('#billingInfoLink').hide();
    $('#emailUserVal').hide();
    $('#userLoginId').show();
    $('input#password').val('');
    $(".row.withImg img").hide();
    $('#email').val($('.headingBar span#emailUserVal span.new_user').text());
    $(".accountInformation .headingBar").removeClass("inactiveHeader");
    $(".orderReview .headingBar").addClass("inactiveHeader");
    // show candyRack on logOut
    $("#candyRack").fadeIn('fast');
    /* OPS-62471 Show subscription AR messaging popup when flag is true */
    $('#subscriptionArMessage').fadeOut('slow');
    $("#cart_trigger").fadeIn('slow')
    if ($("#candyRack").is(":visible")) {
        console.log('Mini-Cart candyRack is visible');
    }
		/* Show CC as Default Payment Method on logOut */
		$('#paymentMethod').prop('selectedIndex', 0);
		console.log("cc selected");
		$('#paypalImg').hide();
		$('#pay-pal').hide();
		/*Boleto Changes */
		$('#boletoImg').hide();
		$('#boletoName').hide();
		/*offline Changes */
		$("#offlinePayment").hide();
		$('#ofbtName').hide();
		/*oxxo hide*/
		$('#oxxoImg').hide();
		$('#oxxoName').hide();
        /*Online Bank Transfer */    
        $('#iBGiropay').hide();
        $('#giropayName').hide();
        $('#iBSofo').hide();
        $('#sofoName').hide();
        $('#iBIdeal').hide();
        $('#idealName').hide();
        $('#iBNordea').hide();
        $('#nordeaName').hide();
	    $('#credit-card').show();
		$('#cardNumberDiv').show();
		$('#expiryDateDiv').show();
		$('#securityCodeDiv').show();
	  $('#cardHolderName').attr('placeholder',$('#credit-card-ph').text());
		$('#showCvvHelp').css("display", "block"); // show CVV Help on logOut
		$('#enterCVV').css("display", "none"); // Hide Enter CVV message on LogOut
		
	}

/* This function applies Coupon  */
function getcouponCode() {
    displaySpinner();
    var couponCode1 = $("#couponCode").val().trim();
    var couponCode2 = $("#couponCode1").val().trim();
    var couponCode = couponCode1 || couponCode2;
    console.log("couponCode" + couponCode);
    $.ajax({
        type: "GET",
        url: "/estore/getShoppingCart/action/applyCoupon/couponCode/" + couponCode,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
        success: function(data) {

        	/*PBL-2311 Empty Cart Condition added in case of getcouponCode */               
            if (typeof data.emptyCartMessage !=='undefined' && data.emptyCartMessage !='') {                    	                   	
	            $("#hideForemptyCart").hide();
	            $(".couponTotalBox").hide();
				$("#acceptedPayments1").hide();
	            $(".editInfoLink").hide();
	            $('#subscriptionArMessage').fadeOut('slow');
	            $("#Norton_Cart").fadeOut();
				$('#euVatToC').hide();
				$('#mini-Cart').hide();
	            $(".productInfoRemove").click(function() {
	            	$("#Norton_Cart").fadeToggle('slow');
	            });

	            refreshTemplates(data);
			            
		   } //empty cart check ends 

        	else {					
            if (couponCode == '' || typeof data.cartCoupons === 'undefined') {

                $.ajax({
                    type: "GET",
                    url: '/estore/getShoppingCart/action/applyCoupon/couponCode/',
                    contentType: "application/json; charset=UTF-8",
                    dataType: "json",
                    timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
                    success: function(data) {
                        var couponError = data.statusMessage;
                        $(".couponErrorDiv").show().text(couponError);
                        $("#couponCode").addClass("redBorder");
                        $("#couponCode1").addClass("redBorder");
                        console.log("statusMessage :" + data.statusMessage);
                       	$("#couponCode").val("");
						$("#couponCode1").val("");
						$('#subscriptionArMessage').fadeOut('slow');
                        /* Added for Omniture */
                        if (typeof updateOmnitureEvents === "function") { 
                            var omniskuCode="none"; //for candyrack calls                       
                            var pagetype="none"; // for getcart and removecart calls
                            var action = "applyCoupon";
                            updateOmnitureEvents(data,action,pagetype,omniskuCode);
                        }
                    }
                });

            } else if (couponCode.toLowerCase() == data.cartCoupons[0].toLowerCase()) {
                $(".couponRow").hide();
                $(".couponErrorDiv").hide();
                $(".discountRow").show();				
				$(".discountRow span#discount, span#overlayDiscount").text(data.cartCoupons[0]);
                $(".discountRow span#discountAmount, span#overlayDiscountAmount").text(data.discountAmount);
                $(".discountRow span#discountAmount, span#overlayDiscountAmount").prepend("-");
                console.log("couponCode :" + data.cartCoupons[0]);
                console.log("discountAmount :" + data.discountAmount);

                console.log("subTotal :" + data.subTotal);
                console.log("Total :" + data.total);

               
                
				checkLAMC();/*LAMC Check */					
				checkExp4(); /*Exp4 Check */
				getSupportedImage(); // getSupportedImage
				refreshTemplates(data);

                /* Added for Omniture */
                if (typeof updateOmnitureEvents === "function") { 
                    var omniskuCode="none"; //for candyrack calls                       
                    var pagetype="none"; // for getcart and removecart calls
                    var action = "applyCoupon";
                    updateOmnitureEvents(data,action,pagetype,omniskuCode);
                }
            }
         }//else block ends
        },
				
				error: genericAjaxErrorHandler,
				
				// This code is for handling the session expiry condition
        statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("Coupon Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("Coupon Session has expired"+xhr.responseText);
                        }
                    }

                }
        }
    })
}

/* This function removes Coupon */
function removeCoupon() {
    displaySpinner();
    var couponCode3 = $("#couponCode").val().trim();
    var couponCode4 = $("#couponCode1").val().trim();
    var deleteCouponCode = couponCode3 || couponCode4;
     var discountValue = $("span#discount").text().trim();
    if(discountValue!="" && deleteCouponCode == ""){
        deleteCouponCode=discountValue;
    }
    $.ajax({
        type: "GET",
        url: "/estore/getShoppingCart/action/deletecoupon/couponCode/" + deleteCouponCode,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
        success: function(data) {

        	/*PBL-2311 Empty Cart Condition added in case of removeCoupon */               
            if (typeof data.emptyCartMessage !=='undefined' && data.emptyCartMessage !='') {                    	                   	
	            $("#hideForemptyCart").hide();
	            $(".couponTotalBox").hide();
				$("#acceptedPayments1").hide();
	            $(".editInfoLink").hide();
	            $('#subscriptionArMessage').fadeOut('slow');
	            $("#Norton_Cart").fadeOut();
				$('#euVatToC').hide();
				$('#mini-Cart').hide();
	            $(".productInfoRemove").click(function() {
	               $("#Norton_Cart").fadeToggle('slow');
	            });

	            refreshTemplates(data);
	            
		   			} //empty cart check ends 

        		else {
	            $(".discountRow").hide();
	            $(".couponRow").show();
				$("#couponCode, #couponCode1").removeClass("redBorder");
	            $("#couponCode").val("");
	            $("#couponCode1").val("");
				checkLAMC();/*LAMC Check */					
				checkExp4(); /*Exp4 Check */
				getSupportedImage(); // getSupportedImage
				refreshTemplates(data);									

				
				}			                     
        	},
					error: genericAjaxErrorHandler,
				
				// This code is for handling the session expiry condition
        statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("Remove coupon Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("Remove coupon Session has expired"+xhr.responseText);
                        }
                    }

                }
        }
    })
}

/* This function adds CandyRack to cart */
$( "a.add-cart" ).on( "click", function(event) {

    displaySpinner();

    event.preventDefault(); 
    var addCandyRackItem  = event.target.id;
    candyRackLinkUrl = $("#" +addCandyRackItem).attr("href");   
    $.ajax({
        type: "GET",
        url: candyRackLinkUrl,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
        success: function(data) {
        	
        		/*PBL-2311 Empty Cart Condition added in case of add cart */               
            if (typeof data.emptyCartMessage !=='undefined' && data.emptyCartMessage !='') {                    	                   	
	            $("#hideForemptyCart").hide();
	            $(".couponTotalBox").hide();
				$("#acceptedPayments1").hide();
	            $(".editInfoLink").hide();
	            $('#subscriptionArMessage').fadeOut('slow');
	            $("#Norton_Cart").fadeOut();
				$('#euVatToC').hide();
				$('#mini-Cart').hide();
	            $(".productInfoRemove").click(function() {
	            $("#Norton_Cart").fadeToggle('slow');
	            });

	            refreshTemplates(data);
	            
   			} //empty cart check ends 

        		else {									
		            var buttonId = $(".add-cart").attr('id');
		            console.log("buttonId:" + buttonId);

		            if (data.candyRackResult.candyRackItems.length >= 0) {

		                console.log("candyRackResult " + data.candyRackResult.candyRackItems);
		                console.log("cartItems Length: " + data.candyRackResult.candyRackItems.length);

		                refreshTemplates(data);

		            	}
						
						checkLAMC();/*LAMC Check */							
						checkExp4(); /*Exp4 Check */	
						getSupportedImage();					

		            	$('#subscriptionArMessage').fadeOut('slow');
						

			            /* Added for Omniture */ // heresup
			            if (typeof updateOmnitureEvents === "function") {   
			                var action = "addcart";
			                var pageType = "candyrack";
			                var omniSkuCode = ";"+ buttonId;
			                updateOmnitureEvents(data, action, pageType, omniSkuCode);
			            }

           			}// else block ends
        			},
							error: genericAjaxErrorHandler,
							// This code is for handling the session expiry condition
			        statusCode: {
			                200: function(xhr){
			                    if(typeof xhr.responseText != "undefined"){
			                        if(xhr.responseText.search("mfsessionExpired") == -1){
			                            console.log("CandyRack Session not expired"+xhr.responseText);
			                        }
			                        else{
			                            window.location.href = '/estore/mf/sessionExpired';
			                            console.log("CandyRack Session has expired"+xhr.responseText);
			                        }
			                    }

			                }
			        }
    				});
					});    

/* This function removes CandyRack */
$( "a.removeCandyRack" ).on( "click", function(event) {
    displaySpinner();
    event.preventDefault(); 
    var removeCandyRackItem  = event.target.id;
    candyRackremove = $("#" +removeCandyRackItem).attr("href"); 
    $.ajax({
        type: "GET",
        url: candyRackremove,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
        success: function(data) {

						/*PBL-2311 Empty Cart Condition added in case of removeCandyRack */               
            if (typeof data.emptyCartMessage !=='undefined' && data.emptyCartMessage !='') {                    	                   	
	            $("#hideForemptyCart").hide();
	            $(".couponTotalBox").hide();
				$("#acceptedPayments1").hide();
	            $(".editInfoLink").hide();
	            $('#subscriptionArMessage').fadeOut('slow');
	            $("#Norton_Cart").fadeOut();
				$('#euVatToC').hide();
				$('#mini-Cart').hide();
	            $(".productInfoRemove").click(function() {
	            	$("#Norton_Cart").fadeToggle('slow');
	            });

	            refreshTemplates(data);			           
		   } //empty cart check ends         	

        	else {
            	for (k = 0; k < data.candyRackResult.candyRackItems.length; k++) {
                candyItemResult = data.candyRackResult.candyRackItems[k];
            }
            $("#candyRack").show();
            $(".candyRackOverlay").show();
            $('#subscriptionArMessage').fadeOut('slow');
			
            refreshTemplates(data);
 			checkLAMC();/*LAMC Check */				
			checkExp4(); /*Exp4 Check */
			getSupportedImage();
		
            /* Added for Omniture */
            if (typeof updateOmnitureEvents === "function") {   
                var action = "removecart";
                var pageType = "candyrack";
                var omniSkuCode = ";"+ removeCandyRackItem + "|" ;
                updateOmnitureEvents(data, action, pageType, omniSkuCode);
            }
           }//else block ends 
        },
		error: genericAjaxErrorHandler,
		// This code is for handling the session expiry condition
        statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("Session has expired"+xhr.responseText);
                        }
                    }

                }
        }	
    });
});

function htmlDecode(value){
	// This function decodes the special characters and html encoding
	// and return a proper presentable text. "Ex: Norton&#8482; to Norton TM"
  return $('<div/>').html(value).text();
}

/* This function removes cart */
function removeCart(skuCode) {
	var removeSkucCode = skuCode;
    displaySpinner();
    $.ajax({
        type: "GET",
        url: removeCartLink[skuCode],
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
        success: function(data) {
        	// In order to hide ITP and corepoduct messages,
        	// if cart is empty.
        	if(typeof data.cartItems == "undefined"){
        		$('#displayMsgITP').hide();
        	}

        //OPS-98189
        if(typeof data.cartItems != "undefined"){
	        window.productFamilyList = [];
	        if(data.cartItems.length>0){
				for(i=0;i<data.cartItems.length;i++){
					window.productFamilyList.push(data.cartItems[i].productFamilyName);
				}
			}
		}	

		// OPS-84917
        	for(var i=0;i<window.cartItems.length;i++){
				if(window.cartItems[i].hasOwnProperty('removedITPProductFromCartFlag')){
					if(window.cartItems[i].removedITPProductFromCartFlag != "" && window.cartItems[i].skuCode == removeSkucCode && window.cartItems[i].removedITPProductFromCartFlag){
						var productName = htmlDecode(window.cartItems[i].productName);					
						$('#displayMsgITP').show();
						$('#removedITP').text(productName);
					}
					else{
						$('#displayMsgITP').hide();
					}
				}
			}
		/*PBL-2311 Empty Cart Condition added in case of removeCart */ 
            if (typeof data.emptyCartMessage !=='undefined' && data.emptyCartMessage !='') {                    	                   	
	            $("#hideForemptyCart").hide();
	            $(".couponTotalBox").hide();
		        $("#acceptedPayments1").hide();
	            $(".editInfoLink").hide();
	            $('#subscriptionArMessage').fadeOut('slow');
	            $("#Norton_Cart").fadeOut();
		        $('#euVatToC').hide();
		        $('#mini-Cart').hide();
	            $(".productInfoRemove").click(function() {
	               $("#Norton_Cart").fadeToggle('slow');
	            });

	            refreshTemplates(data);
	            
   			} //empty cart check ends
						
        	else{
							
        		$('#subscriptionArMessage').fadeOut('slow');

						if(typeof data.enableNonArableCheckBox !== 'undefined'){
									if (data.enableNonArableCheckBox == true) {
									$('#arHeader').hide();
									$('#arHelpLink').hide();
									$("#arMessage p:first").hide();
								}
						}						
		            if (typeof data.cartItems !== 'undefined') {
		                console.log("cartItems innnn: " + data.cartItems.length);
		                if (data.cartItems.length >= 1) {
		                    $("#candyRack").show();
		                    $(".candyRackOverlay").show();

		                    console.log("cartItems: " + data.cartItems);
		                    console.log("cartItems length inside greater than1: " + data.cartItems.length);

		                    refreshTemplates(data);
                                                        					
							checkLAMC();/*LAMC Check */								
							checkExp4(); /*Exp4 Check */														
		                }

		            } else {

		                console.log("cartItems: " + data.cartItems);
		                console.log("emptyCart: " + data.emptyCartMessage);
		                $("#hideForemptyCart").hide();
		                $(".couponTotalBox").hide();
						$("#acceptedPayments1").hide();
		                $(".editInfoLink").hide();
		                $("#Norton_Cart").fadeOut();
						$('#euVatToC').hide();
		                $(".productInfoRemove").click(function() {
		                    $("#Norton_Cart").fadeToggle('slow');
		                });

		                refreshTemplates(data);

		            }
		    }// else check ends

		    /* Added for Omniture */
        if (typeof updateOmnitureEvents === "function") {   
          var omniSkuCode = ";"+ removeSkucCode + "|" ;
		  console.log("omniSkuCode" +omniSkuCode);
          var pagetype="none"; // for getcart and removecart calls
          var action = "removecart";
          updateOmnitureEvents(data, action, pagetype,omniSkuCode);
        }

      },
 				error: genericAjaxErrorHandler,
		// This code is for handling the session expiry condition
        statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("Session has expired"+xhr.responseText);
                        }
                    }

                }
        }		
		
    });
};

/*This function handles the scenario when the user logs in*/
function showHideOnLogin(userEmail){

	checkLAMC();/*LAMC Check */		
	checkExp4(); /*Exp4 Check */	
	createPaymentDropdown(); //OPS-77539
// Condition to check ROVER FEATURE and ShippingInfoVO is not present Show Shipping Section Expanded
if(ROVER_FEATURE_FLAG && window.cartContainsPhysicalProduct){
	if(window.shippingInfoVo && logOut_flag == false){
		$("#shippingInformationLink").show();
  		$("#shippingSec").hide("slow");
		$("#BillingSec").show('slow');
		$(".billingInformation .headingBar").removeClass("inactiveHeader");
	}else{
		$("#shippingSec").show("slow");
  		$("#BillingSec").hide("slow");
  		$(".shippingInformation .headingBar").removeClass("inactiveHeader");
  }
}
else{
	console.log("Inside else condition for non rover");
	$("#shippingInformation").hide();
	$("#BillingSec").show('slow');
	$(".billingInformation .headingBar").removeClass("inactiveHeader");
}
  $("#LoginInfo").slideUp('slow');    
  $("#userLoginId").slideUp();
  $(".accountInformation .headingBar").addClass("inactiveHeader");
  $(".orderReview .headingBar").addClass("inactiveHeader");
  $(".headingBar span#emailUserVal span.new_user").text(userEmail);
  $(".headingBar span#emailUserVal").show();
  $("#reviewSec").slideUp("slow"); 

  //OPS-87760 cart development logOut functionality
    if(typeof window.enableLogoutAction !=='undefined' && window.enableLogoutAction == true){
   		$('#newCartLogOut').show();
   		breadCrumb();   		   		
	}
	else{
		$('#logInInfoLink').show();
	} 	
}

/* This function fills the information in the billing fields which is present in the wallet */
function fillBillingSection(){
  walletinfo = window.walletinformation;
  console.log(walletinfo);
  $('#cardHolderName').val(walletinfo['cardHolderName']);
  $('#cardNumber').val(walletinfo['maskedCardNumber']);
  var CardType = walletinfo['cardType'];
  parseData.displayCardImage(CardType);
  var Month = walletinfo['expDateMonth'];
  var Year = walletinfo['expDateYear'];
  var Country = walletinfo['country'];
  var State = walletinfo['state'];
  console.log("State -- " + State);
  $('#expMonthDropDown option:selected').attr("selected",null);
  $('#expMonthDropDown option[value='+Month+']').attr("selected","selected");    
  $('#expYearDropDown option:selected').attr("selected",null);
  $('#expYearDropDown option[value='+Year+']').attr("selected","selected");
  $('#city').val(walletinfo['city']);
  /*Boleto Changes OPS-63895*/
  $('#cpfcnpj').val(walletinfo['cpfCnpj']);
  $('#address').val(walletinfo['address']);
  $('#zipCode').val(walletinfo['zipCode']);    
  $('#countriesDropDown option:selected').attr("selected",null);
  $('#countriesDropDown option[value='+Country+']').attr("selected","selected");
  $('#stateDropDown option:selected').attr("selected",null);
  $('#stateDropDown option[value='+State+']').attr("selected","selected");
  /*Phone Number Changes for OXXO */
  $('.phone').val(walletinfo['phoneNumber']);
	$('.nsbPhoneNumber').val(walletinfo['phoneNumber']);
	/* VAT Code, IBAN and BIC */
	$('.vatCode').val(walletinfo['vatID']);
	$('.iban').val(walletinfo['maskedAccountNumber']);
	$('.bankCode').val(walletinfo['bankCode']);	

}

/**ops-84033 allowing customer to move forward after updating MOP and removing error from billing country, 
if customer clicks somewhere outside **/
$(document).bind("click", function(event) {
	if ($(event.target).is('input, select')){
		return;
	}
	if($("#countryDiv .errorDiv").hasClass("hidden")){
		return;
	}else if(typeof window.walletinformation !== 'undefined' && window.walletinformation !== null){
		if(typeof window.walletinformation['maskedCardNumber']!== 'undefined' && window.walletinformation['maskedCardNumber']!== null && window.walletinformation['maskedCardNumber']== $("#cardNumber").val()){
			if(window.geoErrorMsg == 'false'){
				$("#countryDiv .errorDiv").addClass("hidden");
				$("#countryDiv #countriesDropDown").removeClass("redBorder");
			}
		}
	}
});


//This API will capture customer Login Information
function logUserIn(event) {
    console.log('logUserIn is firing');
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    status = errorCheck.checkEmail();
    if(status == 'false' || errorCheck.checkEmail() == false){
        return;
    }
    status = errorCheck.checkPassword();
    if(status == 'false' || errorCheck.checkPassword() == false){
        return;
    }
    displaySpinner();
    var loginVo = new Object();
    loginVo.emailid = $('#email').val().trim(); // log in
		console.log("LogIn EmailID :"+loginVo.emailid);
    console.log('before password replace');
    var pwd = $('#password').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');
    loginVo.password = pwd; // log in
    //TODO : Remove this alert
    console.log("in submitLogIn" + " " + loginVo.emailid + " " + loginVo.password);
    $.ajax({
        type: 'POST',
        url: '/estore/getShoppingCart/action/createUser',
        data: JSON.stringify(loginVo),
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        mimeType: 'application/json',
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
        success: function(data) {
            if(typeof data.sessionExpireUrl != "undefined"){
                window.location.href = data.sessionExpireUrl;
            }
            console.log("In Success");
            console.log("LogInsuccess" + data);
            var userEmail = data.email.trim();
            console.log(userEmail);
            showHideOnLogin(userEmail);
            window.userNumberEntry = "1";

            if (typeof updateOmnitureEvents === "function") {    
                var omniskuCode="none";                     
                var pagetype="none"; 
                var action = "logUserIn";
                updateOmnitureEvents(data,action,pagetype,omniskuCode);
            }
            $('#subscriptionArMessage').fadeOut('slow');
            
        },
        error: genericAjaxErrorHandler,
				// This code is for handling the session expiry condition
        statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("MF Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("MF Session has expired"+xhr.responseText);
                        }
                    }

                }
        }
    });

};

//This API will capture customer Login Information for Create Account
function createAccount(event) {
    console.log('createAccount 1 is firing');
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
     status = errorCheck.checkUserId();
    if(status == 'false' || errorCheck.checkUserId() == false){
        return;
    }
    status = errorCheck.checkAccountPassword();
    if(status == 'false' || errorCheck.checkAccountPassword() == false){
        return;
    }
    status = errorCheck.checkAccountPassword();
    if(status == 'false' || errorCheck.checkAccountPassword() == false){
        return;
    }
    status = errorCheck.checkVerifyPwd();
    if(status == 'false' || errorCheck.checkVerifyPwd() == false){
        return;
    }

    displaySpinner();

    var loginVo = new Object();
    loginVo.emailid = $('#username').val().trim(); // log in
		console.log("Create User Login :"+loginVo.emailid);
    var pwd = $('#apassword').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');
    loginVo.password = pwd;
    $.ajax({
        type: 'POST',
        url: '/estore/getShoppingCart/action/createUser',
        data: JSON.stringify(loginVo),
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        mimeType: 'application/json',
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
        success: function(data) {
            console.log("In Create Norton Account");
            console.log("createAccountSuccess" + data);
            var userEmail = data.email.trim();
            console.log(userEmail);
            showHideOnLogin(userEmail);
            $('#subscriptionArMessage').fadeOut('slow');
            window.userNumberEntry = "1";
            if (typeof updateOmnitureEvents === "function") { 
                var omniskuCode="none";                     
                var pagetype="none"; 
                var action = "createAccount";
                updateOmnitureEvents(data,action,pagetype,omniskuCode);
            }
        },
        error: genericAjaxErrorHandler,
		// This code is for handling the session expiry condition
        statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("Session has expired"+xhr.responseText);
                        }
                    }

                }
        }
    });
};

/* Validation check for empty expiry month */
function emptyExpiryMonth(){
    if ($('#expMonthDropDown option:selected').val() == '-1') {
            $('#expMonth').val("");
    } else {
        $('#expMonth').val($('#expMonthDropDown option:selected').val());
    }
    errorCheck.checkCardValidity(true, false, false);
}

/* Validation check for empty expiry year */
function emptyExpiryYear(){
    if ($('#expYearDropDown option:selected').val() == '-1') {
        $('#expYear').val("");
    } else {
        $('#expYear').val($('#expYearDropDown option:selected').val());
    }
    errorCheck.checkCardValidity(true, false, false);
    console.log('firing emptyExpiryYear')
}

/* PayPal Integration CAP-9567 */
function paymentModeSelected(){
	console.log("$('#paymentMethod').val() "+ $('#paymentMethod').val());
		if(ROVER_FEATURE_FLAG && window.cartContainsPhysicalProduct){
	// Pre-Populate data from shipping to billing once checkbox is checked
	if($("#billSameShip").attr("checked")){
		(!$(':input').val() === "");
	}
	}else{
		$(':input').not(':button, :submit, :reset, :checkbox, :radio,select, :input#couponCode.couponcode, :input#couponCode1.couponcode').val('');
		$('#stateDropDown option:selected').attr("selected",null);
		}	
	//$(':input').not(':button, :submit, :reset, :checkbox, :radio,select, :input#couponCode.couponcode, :input#couponCode1.couponcode').val('');
	$('#expMonthDropDown option:selected').attr("selected",null);
    $('#expYearDropDown option:selected').attr("selected",null);
    $(".row.withImg img").hide();
	var selectedPaymentMethod = $('#paymentMethod').val();
	var countryText = $("#countriesDropDown option:selected").text();
	var billingCountry = $('#countriesDropDown').val();
	
	
		
	    switch(selectedPaymentMethod){
    	case 'DW':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#paypalImg').show();
			$('#credit-card').hide();
			$('#pay-pal').show();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();
			$('#boletoImg').hide();
			$('#boletoName').hide();
			$('#cpfcnpjDiv').hide();
			$('#addressDiv').hide();
			$('#alpagoText').hide();
			$('#dineromailText').hide();
			$('#BOLETO, #boleto').hide();
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
			$(".country").removeClass("redBorder");			       
			/*OPS-75720 OFBT */
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
			$('#boletoImg').hide();
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide();
            /*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide();
            $('#webmoney').hide();
			$('#qiwi').hide();
			$('#PR24').hide();
			$('#yandex').hide(); 
            optionalPhone(); //show hide option phone number text   
			if ((cty == 'BR' & typeof cty !=='undefined')|| (countryCodeId == 'BR' && typeof countryCodeId !== 'undefined')){
		    	$('#cpfcnpjDiv').show();
				$('#addressDiv').show();
				$('#alpagoText').show();						
		    }
		    if ((cty == 'MX' & typeof cty !=='undefined')|| (countryCodeId == 'MX' && typeof countryCodeId !== 'undefined')){		    	
				$('#addressDiv').show();
				$('#dineromailText').show();							
		    }
		    checkPayPalMessage(); //Paypal Text in case of in-context flow
		   
			break;			
		case 'BT':
		/*Boleto Changes OPS-63895 */		
		if(selectedPaymentMethod === "BT"){
			if(billingCountry === "BR"){				
			$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#boletoImg').show();
			$('#credit-card').hide();
			$('#boletoName').show();
			$('#cpfcnpjDiv').show();
			$('#addressDiv').show();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();
			$('#alpagoText').show();
			$('#BOLETO, #boleto').show();
			$('#cardHolderName').attr('placeholder',$('#boleto-ph').text());			
			$('#paypalImg').hide();
			$('#pay-pal').hide();
			/*OPS-75720 OFBT */
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide();
            /*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide();
            $('#webmoney').hide();
			$('#qiwi').hide();
			$('#PR24').hide();
			$('#yandex').hide();
			$("#agreePaymentInfoPrePopulated").hide();
			optionalPhone(); //show hide option phone number text
			checkPayPalMessage(); //Paypal Text in case of in-context flow
			}else if(billingCountry !== "BR" && countryCodeId === "BR"){								
				$('#alpagoText').hide();
				$('#cpfcnpjDiv').hide();
				$('#addressDiv').hide();	
				$('#cardNumberDiv').hide();
				$('#expiryDateDiv').hide();
				$('#securityCodeDiv').hide();
				/* Direct Debit */
				$('#directDebit').hide();
				$('#ddName').hide();
				$('#ibanDiv').hide();
				$('#bankCodeDiv').hide();
                /*Online Bank Transfer */    
                $('#iBGiropay').hide();
                $('#giropayName').hide();
                $('#iBSofo').hide();
                $('#sofoName').hide();
                $('#iBIdeal').hide();
                $('#idealName').hide();
                $('#iBNordea').hide();
                $('#nordeaName').hide();
                $('#webmoney').hide();
				$('#qiwi').hide();
				$('#PR24').hide();
				$('#yandex').hide(); 
                $("#agreePaymentInfoPrePopulated").hide();
                optionalPhone(); //show hide option phone number text
                checkPayPalMessage(); //Paypal Text in case of in-context flow			
			}
		else if(offlineCountriesList.split(',').indexOf(countryCodeId)  !== -1 && offlineCountriesList.split(',').indexOf(billingCountry)  !== -1) {
				
			/*OPS-75720 OFBT */
			console.log("Inside OFBT");
			$("#offlinePayment").show();
			$('#ofbtName').show();
			$("#zipCodeDiv").hide();
			$("#cityDiv").hide();
			$("#stateDiv").hide();	
			$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#paypalImg').hide();
			$('#pay-pal').hide();
			$('#credit-card').hide();			
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();
			$('#boletoImg').hide();
			$('#boletoName').hide();
			$('#cpfcnpjDiv').hide();
			$('#addressDiv').hide();
			$('#alpagoText').hide();
			$('#dineromailText').hide();
			$('#BOLETO, #boleto').hide();
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
			$(".country").removeClass("redBorder");			
			$('#OFFLINEBANKTRANSFER, #offlinebanktransfer').show();
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide();
            /*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide();
            $('#webmoney').hide();
			$('#qiwi').hide();
			$('#PR24').hide();
			$('#yandex').hide(); 
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow
			
			}else if(offlineCountriesList.split(',').indexOf(countryCodeId)  !== -1 && offlineCountriesList.split(',').indexOf(billingCountry)  == -1){							
				/*OPS-75720 OFBT */
			$("#offlinePayment").show();
			$('#ofbtName').show();
			$("#zipCodeDiv").hide();
			$("#cityDiv").hide();
			$("#stateDiv").hide();	
			$('#paypalImg').hide();
			$('#pay-pal').hide();
			$('#credit-card').hide();			
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();
			$('#boletoImg').hide();
			$('#boletoName').hide();
			$('#cpfcnpjDiv').hide();
			$('#addressDiv').hide();
			$('#alpagoText').hide();
			$('#dineromailText').hide();
			$('#BOLETO, #boleto').hide();
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
			$(".country").addClass("redBorder");			
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide();
             /*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide();
            $('#webmoney').hide();
			$('#qiwi').hide();
			$('#PR24').hide();
			$('#yandex').hide(); 
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
			checkPayPalMessage(); //Paypal Text in case of in-context flow	
			}
		}
			break;	
			
		/*Direct Debit Changes OPS-73493 */
			
		case "DD":
		if(directDebitCountryList.split(',').indexOf(countryCodeId)  !== -1 && typeof countryCodeId !== "undefined"){
			$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#directDebit').show();
			$('#ddName').show();
			$('#ibanDiv').show();
			$('#bankCodeDiv').show();			
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
			$('#directdebit, #DIRECTDEBIT').show();			
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();
			/*OPS-75720 OFBT */
			$('#oxxoImg').hide();
			$('#paypalImg').hide();
			$('#boletoImg').hide();
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
            /*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide();
            $("#agreePaymentInfoPrePopulated").show();
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow
		}else if(directDebitCountryList.split(',').indexOf(billingCountry)  !== -1){
			$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#directDebit').show();
			$('#ddName').show();
			$('#ibanDiv').show();
			$('#bankCodeDiv').show();			
			$('#directdebit, #DIRECTDEBIT').show();
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());				
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();
			/*OPS-75720 OFBT */
			$('#oxxoImg').hide();
			$('#paypalImg').hide();
			$('#boletoImg').hide();
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
            /*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide(); 
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow      
		}
			break;		
			
		/*Oxxo Changes OPS-73492 */
		case 'HP':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#oxxoImg').show();
			$('#oxxoName').show();			
			$('#credit-card').hide();			
			$('#addressDiv').show();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();
			$('#dineromailText').show();			
			$('#cardHolderName').attr('placeholder',$('#oxxo-ph').text());			
			$('#paypalImg').hide();
			$('#pay-pal').hide();
			$('#boletoImg').hide();
			$("#offlinePayment").hide();
			/*OPS-75720 OFBT */
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide();
            /*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide();
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow
			if ((selectedPaymentMethod === "HP") && ($('#countriesDropDown').val() !== "MX") && (typeof cty !== 'undefined')){			  
				$('#dineromailText').hide();
				$('#addressDiv').hide();
                $("#agreePaymentInfoPrePopulated").hide();
		    }
		    else if((selectedPaymentMethod === "HP") && ($('#countriesDropDown').val() !== "MX") && cty !== "MX"){				
				$('#dineromailText').hide();
                $("#agreePaymentInfoPrePopulated").hide();
			}            
			
			break;
                
            /*Online Bank Transfer Changes OPS-73497 */
		case 'IB_GIROPAY':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#iBGiropay').show();
            $('#giropayName').show();
            $('#iBSofo').hide();
            $('#sofoName').hide();
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();			
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());			
			$('#paypalImg').hide();
			$('#pay-pal').hide();
			$('#boletoImg').hide();
			$("#offlinePayment").hide();
			/*OPS-75720 OFBT */
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
            $('#oxxoImg').hide();
			$('#oxxoName').hide();
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide();
            /*Online Bank Transfer */                
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide(); 
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
		    checkPayPalMessage(); //Paypal Text in case of in-context flow
			break;
        
        case 'IB_SOFO':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#iBSofo').show();
            $('#sofoName').show();
            $('#iBGiropay').hide();
            $('#giropayName').hide(); 
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();			
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());			
			$('#paypalImg').hide();
			$('#pay-pal').hide();
			$('#boletoImg').hide();
			$("#offlinePayment").hide();
			/*OPS-75720 OFBT */
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
            $('#oxxoImg').hide();
			$('#oxxoName').hide();
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide(); 
            /*Online Bank Transfer */    
            $('#iBGiropay').hide();

            $('#giropayName').hide();           
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide();
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
			checkPayPalMessage(); //Paypal Text in case of in-context flow
			break;
                
        case 'IB_NORDEA':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#iBNordea').show();
            $('#nordeaName').show();               
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();			
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());			
			$('#paypalImg').hide();
			$('#pay-pal').hide();
			$('#boletoImg').hide();
			$("#offlinePayment").hide();
			/*OPS-75720 OFBT */
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
            $('#oxxoImg').hide();
			$('#oxxoName').hide();
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide();
            /*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow    
			break;
                
        case 'IB_IDEAL':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#iBIdeal').show();
            $('#idealName').show();                
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();			
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());			
			$('#paypalImg').hide();
			$('#pay-pal').hide();
			$('#boletoImg').hide();
			$("#offlinePayment").hide();
			/*OPS-75720 OFBT */
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
            $('#oxxoImg').hide();
			$('#oxxoName').hide();
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide();                  
			/*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();           
            $('#iBNordea').hide();
            $('#nordeaName').hide();
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow
            // OPS-85079
            createBankDropdown();
			break;	        

		 case 'HP_WEBMONEY-SSL':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#webmoney').show();
            $('#pay-pal').show();
			$('#cityDiv').show();
			$('#zipCodeDiv').show();			
			$('#paypalImg').hide();	                       
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();			
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
			$('#qiwi').hide();
			$('#yandex').hide();
			$('#PR24').hide();
			$("#offlinePayment").hide();
			$('#ofbtName').hide();												
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow    
			break;

		case 'HP_YANDEXMONEY-SSL':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#yandex').show();
            $('#pay-pal').show();
			$('#cityDiv').show();
			$('#zipCodeDiv').show();
			$('#paypalImg').hide();		                         
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();			
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
			$('#webmoney').hide();
			$('#qiwi').hide();
			$('#PR24').hide();
			$("#offlinePayment").hide();
			$('#ofbtName').hide();									
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow   
			break;

		case 'HP_QIWI-SSL':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#qiwi').show();
            $('#pay-pal').show();
			$('#cityDiv').show();
			$('#zipCodeDiv').show();
			$('#paypalImg').hide();			                         
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();			
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
			$('#webmoney').hide();
			$('#yandex').hide();
			$('#PR24').hide();
			$("#offlinePayment").hide();
			$('#ofbtName').hide();												
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow   
			break;

		case 'HP_PRZELEWY-SSL':
    		$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#PR24').show();
            $('#pay-pal').show();
			$('#zipCodeDiv').show();			
			$('#paypalImg').hide();			                         
			$('#credit-card').hide();			
			$('#addressDiv').hide();
			$('#cardNumberDiv').hide();
			$('#expiryDateDiv').hide();
			$('#securityCodeDiv').hide();			
			$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
			$('#webmoney').hide();
			$('#yandex').hide();
			$('#qiwi').hide();
			$("#offlinePayment").hide();
			$('#ofbtName').hide();												
            $("#agreePaymentInfoPrePopulated").hide();
            optionalPhone(); //show hide option phone number text
            checkPayPalMessage(); //Paypal Text in case of in-context flow   
			break;

			
		case 'CC':
			console.log("cc selected");
			$('div.errorDiv').addClass('hidden');
			$('input').removeClass('redBorder');
			$('#callingCode').removeClass('redBorder');
			$('#paypalImg').hide();
			$('#pay-pal').hide();
			$('#credit-card').show();
			$('#cardNumberDiv').show();
			$('#expiryDateDiv').show();
			$('#securityCodeDiv').show();
			$('#boletoImg').hide();
			$('#boletoName').hide();
			$('#cpfcnpjDiv').hide();
			$('#addressDiv').hide();
			$('#cardHolderName').attr('placeholder',$('#credit-card-ph').text());
			$(".country").removeClass("redBorder");
			$('#oxxoImg').hide();
			$('#oxxoName').hide();			
			/*OPS-75720 OFBT */
			$('#oxxoImg').hide();
			$('#paypalImg').hide();
			$('#boletoImg').hide();
			$("#offlinePayment").hide();
			$('#ofbtName').hide();
			/* Direct Debit */
			$('#directDebit').hide();
			$('#ddName').hide();
			$('#ibanDiv').hide();
			$('#bankCodeDiv').hide();
             /*Online Bank Transfer */    
            $('#iBGiropay').hide();
            $('#giropayName').hide();
            $('#iBSofo').hide();
            $('#sofoName').hide();
            $('#iBIdeal').hide();
            $('#idealName').hide();
            $('#iBNordea').hide();
            $('#nordeaName').hide();
            $('#webmoney').hide();
			$('#qiwi').hide();
			$('#PR24').hide();
			$('#yandex').hide();
            optionalPhone(); //show hide option phone number text  
            checkPayPalMessage(); //Paypal Text in case of in-context flow       
			if (countryCodeId === 'BR' && typeof countryCodeId !== 'undefined'){
		    	$('#cpfcnpjDiv').show();
				$('#addressDiv').show();
                $("#agreePaymentInfoPrePopulated").show();
				if(($('#paymentMethod').val() !== "BT") && ($('#countriesDropDown').val() !== "BR")){				
				$('#alpagoText').hide();
				$('#cpfcnpjDiv').hide();
				$('#addressDiv').hide();
                $("#agreePaymentInfoPrePopulated").show();
				}
		    }
		    if ((cty == 'MX' & typeof cty !=='undefined')|| ($('#countriesDropDown').val() == "MX")){		    	
				$('#addressDiv').show();
				$('#dineromailText').show();					
		    }			
			break;
    }
	if(typeof window.walletinformation !== 'undefined' && window.walletinformation !== null ){
		if(window.walletinformation){
			walletPaymentType = window.walletinformation['paymentType'];
			console.log("paymentType in wallet"+walletPaymentType);
			console.log("logOut flag "+logOut_flag);
			
			if (walletPaymentType == ($('#paymentMethod').val()) && logOut_flag == false){
				walletinfo = window.walletinformation;
				console.log(walletinfo);                    
				$('#cardHolderName').val(walletinfo['cardHolderName']);
				$('#cardNumber').val(walletinfo['maskedCardNumber']);
				var CardType = walletinfo['cardType'];
				parseData.displayCardImage(CardType);
				var Month = walletinfo['expDateMonth'];
				var Year = walletinfo['expDateYear'];
				var Country = walletinfo['country'];
				var State = walletinfo['state'];
				console.log("State -- " + State);
				$('#expMonthDropDown option:selected').attr("selected",null);
				$('#expMonthDropDown option[value='+Month+']').attr("selected","selected");    
				$('#expYearDropDown option:selected').attr("selected",null);
				$('#expYearDropDown option[value='+Year+']').attr("selected","selected");
				$('#city').val(walletinfo['city']);
				 /*Boleto Changes OPS-63895*/
    			$('#cpfcnpj').val(walletinfo['cpfCnpj']);
    			$('#address').val(walletinfo['address']);
				$('#zipCode').val(walletinfo['zipCode']);								
				$('#stateDropDown option:selected').attr("selected",null);
				$('#stateDropDown option[value='+State+']').attr("selected","selected");
				$('#cvv').addClass('cvvRc');				
				$('#showCvvHelp').css("display", "none");
				$('#enterCVV').css("display", "block"); 
				/*Phone Number Changes for OXXO */
    			$('.phone').val(walletinfo['phoneNumber']);
				$('.nsbPhoneNumber').val(walletinfo['phoneNumber']);
				/* VAT Code, IBAN and BIC */
				$('.vatCode').val(walletinfo['vatID']);
				$('.iban').val(walletinfo['maskedAccountNumber']);
				$('.bankCode').val(walletinfo['bankCode']);				

			}					
	    }		
	}
	checkExp4(); //Exp4 Check to show/hide auto consent for only exp4 countries and online payments
	
	checkNetBanking();//Based on NetBanking enabling/disabling bank dropdown
}

/**
 * Gets Shipping request data.
 * @param event The event
 * @return If no error in form, the shippingInfoVo object; else null
 */
function shipping(event){

		// Passing only numbers to downstream systems in Phone Number 
		var phNumber = $('#mobileNumber-ship').val();
        $('#mobileNumber-ship').val(phNumber.replace(/[\s-()-]/g, '').trim());
 		/* Empty Validation check starts */
        getErrorMessage();
        parseData.getErrorMessageIndexs();
        parseData.getCurrentData();

        		//Name 
        		 if($('#shippingNameDiv').is(":visible")==true){
					var status = errorCheck.checkCardHolderName('shippingName','shippingName','#shippingName');
					if(status === false){
						return;
					}
				}

        		//Address
        		if($('#shipAddressDiv').is(":visible")==true){
					var status = errorCheck.checkaddress('address-ship','addressShip','#address-ship');
					if(status === false){
						return;
					}
				}
				//Address2 
        		if($('#address2Div-ship').is(":visible")==true){
					var status = errorCheck.checkaddress('address2-ship','address2Ship','#address2-ship');
					if(status === false){
						return;
					}
				}

				// Zip Code
				  if($('#zipCodeDiv-ship').is(":visible")==true){
					var status = errorCheck.checkZipCode('zipCode-ship', 'zipCodeShip', '#zipCode-ship','countryShip');
					if(status === false){
						return;
					}
				}
				// City
				if($('#cityDiv-ship').is(":visible")==true){
					var status = errorCheck.checkCity('city-ship', 'cityShip', '#city-ship'); 
					if(status === false){
						return;
					}
				}
				// State
				if($('#stateDiv-ship').is(":visible")==true){
					var status = errorCheck.checkS('#stateDropShip', 'stateShip'); 
					if(status === false){
						return;
					}
				}
				//Country
				if($('#countryDiv-ship').is(":visible")==true){
						var status = errorCheck.checkCountry();
						if(status === false){
							return;
						} 
				}
                // Phone Number
				if($('#phoneDiv-ship').is(":visible")==true){
					var status = errorCheck.checkPhone('phone-ship', '#countryDropShip', '#callingCodeShip', '#mobileNumber-ship', '#phoneError-ship', 'phoneShip', '#phoneNumber-ship', true);
					if(status === false){
						return;
					}
				}
		var shippingInfoVo = new Object();
                  
                  shippingInfoVo.name = $('#shippingName').val().replace(/"/g,"&quot;")
                                   						 .replace(/&/g,'&amp;')
                                   						 .replace(/</g,'&lt;')
                                    					 .replace(/>/g,'&gt;')
                                    					 .replace(/'/g,'&apos;')
                                   						 .replace(/\//g,'&#x2F;'); // ShippingName
                  shippingInfoVo.address = $('#address-ship').val().replace(/"/g,"&quot;")
                                   						 .replace(/&/g,'&amp;')
                                   						 .replace(/</g,'&lt;')
                                    					 .replace(/>/g,'&gt;')
                                    					 .replace(/'/g,'&apos;')
                                   						 .replace(/\//g,'&#x2F;'); // ShippingAddress
                  shippingInfoVo.address2 = $('#address2-ship').val().replace(/"/g,"&quot;")
                                   						 .replace(/&/g,'&amp;')
                                   						 .replace(/</g,'&lt;')
                                    					 .replace(/>/g,'&gt;')
                                    					 .replace(/'/g,'&apos;')
                                   						 .replace(/\//g,'&#x2F;'); // ShippingAddress2  
                 shippingInfoVo.zipCode = $('#zipCode-ship').val().trim();  // ShippingZip 
                 shippingInfoVo.city = $('#city-ship').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');  // ShippingCity        
                 shippingInfoVo.state = $('#stateDropShip option:selected').val(); // Shipping State
                 shippingInfoVo.country = $('#countryDropShip option:selected').val();  // Shipping Country
				 shippingInfoVo.phoneNumber = $('#mobileNumber-ship').val();  // Shipping Phone
 
				displaySpinner(); 
	            console.log("display spinner"+displaySpinner());               

	            $.ajax({
	                type: 'POST',
	                url: '/estore/getShoppingCart/action/shipping',
	                data: JSON.stringify(shippingInfoVo),
	                contentType: "application/json; charset=UTF-8",
	                dataType: "json",
	                mimeType: 'application/json',
	                timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
	                success: function (data){
	                  console.log(data); 

					   	/*PBL-2311 Empty Cart Condition added in case of applyTax */               
	       		       	if (typeof data.emptyCartMessage !=='undefined' && data.emptyCartMessage !='') {                    	                   	
				            $("#hideForemptyCart").hide();
				            $(".couponTotalBox").hide();
							$("#acceptedPayments1").hide();
				            $(".editInfoLink").hide();
				            $('#subscriptionArMessage').fadeOut('slow');
				            $("#Norton_Cart").fadeOut();
							$('#euVatToC').hide();
							$('#mini-Cart').hide();
				            $(".productInfoRemove").click(function() {
				            	$("#Norton_Cart").fadeToggle('slow');
				            });

				            refreshTemplates(data);
					   } // if ends for empty cart
					   else {				    		
						    $('#subscriptionArMessage').fadeOut('slow');  
						    $("#shippingSec").slideUp("slow");                   					 					                                   
		                    $("#BillingSec").slideDown("slow");                    
		                   	$(".accountInformation .headingBar").addClass("inactiveHeader");
							$(".billingInformation .headingBar").removeClass("inactiveHeader");
							$(".shippingInformation .headingBar").addClass("inactiveHeader");
							$(".orderReview .headingBar").addClass("inactiveHeader");
		                    $("#shippingInformationLink").show();
		                    $("#billingInfoLink").hide();

                    console.log(' billing should show');
									    						
					refreshTemplates(data);
					checkLAMC();/*LAMC Check */					
					checkExp4(); /*Exp4 Check */ 

                   if (typeof updateOmnitureEvents === "function") { 
                       var omniskuCode="none";                     
                       var pagetype="none"; 
                       var action = "shipping";
                       updateOmnitureEvents(data,action,pagetype,omniskuCode);
                   }
                 } //else block end 

				}, // success end
				  error:genericAjaxErrorHandler,
				// This code is for handling the session expiry condition
                statusCode: {
                	200: function(xhr){
	                    if(typeof xhr.responseText != "undefined"){
	                        if(xhr.responseText.search("mfsessionExpired") == -1){
	                            console.log("Shipping Session not expired"+xhr.responseText);
	                        }
	                        else{
	                            window.location.href = '/estore/mf/sessionExpired';
	                            console.log("Shipping Session has expired"+xhr.responseText);
	                        }
	                    }
                	}
				}
			}); //ajax end
	}	// shippingInfo End

/**
 * Gets applyTax request data. The same data can be used for initPaypal.
 * @param event The event
 * @return If no error in form, the bpVo object; else null
 */
function getApplyTaxRequestData(event){
        var phNumber = $('#mobileNumber').val();
        $('#mobileNumber').val(phNumber.replace(/[\s-()-]/g, '').trim());
				/*OPS-65357 NSB Changes*/
		var nsbPhoneNumber = $('input.nsbPhoneNumber').val();
        $('input.nsbPhoneNumber').val(nsbPhoneNumber.replace(/[ -()-]/g, '').trim()); 

        var cpfcnpj = $('input.cpfcnpj').val();
				cpfcnpj = cpfcnpj.replace(/\//g, '');
				cpfcnpj = cpfcnpj.replace(/-/g, '');
				cpfcnpj = cpfcnpj.replace(/\./g, '');

    		var zipOrPostalCode = $('input.zipOrPostalCode').val();			
    		$('input.zipOrPostalCode').val(zipOrPostalCode.replace(/-/g, ''));

        /* Empty Validation check starts */
        getErrorMessage();
        parseData.getErrorMessageIndexs();
        parseData.getCurrentData();
		
       //ops-75656
		if($('#bankDropdownDiv').is(":visible")==true){
			var status = errorCheck.checkBankNameSelected();
			if(status === false){
				return;
			}
		}

        // CardHolderName
		if($('#cardHolderNameDiv').is(":visible")==true){
			console.log('### ALMOST');
			status = errorCheck.checkCardHolderName();
			if(status == 'false' || errorCheck.checkCardHolderName() == false){
				return;
			}
		}
        // CardNumber
		if($('#cardNumberDiv').is(":visible")==true){

			if (typeof window.walletinformation !== 'undefined'){
				var cardValue = $("#cardNumber").val();
				if((window.userNumberEntry == '1' || typeof window.userNumberEntry == 'undefined') && 
						!((window.walletinformation['maskedCardNumber'] == cardValue))) {
					status = errorCheck.checkCardNumber();
					if(status == 'false' || errorCheck.checkCardNumber() == false){
						return;
					}
				}	
			}
	
			else{
				if((window.userNumberEntry == '1' || typeof window.userNumberEntry == 'undefined')) {
					status = errorCheck.checkCardNumber();
						if(status == 'false' || errorCheck.checkCardNumber() == false){
						return;
						}
					}
				}
		}
				
                // Checking for empty expiry dates i.e. month and year.
				if($('#expiryDateDiv').is(":visible")==true){
					errorCheck.checkEmptyEpiryDates();
					//INC0352331: No validation for missing Year on new Checkout Flow for billing profile with expired credit card
					errorCheck.checkCardValidity(false, false, true); //OPS-71301  Added to validate card validity on Next click
					if($('#expMonthDropDown').siblings().hasClass('hidden') == false){
						return;
					}
					if($('#expYearDropDown').siblings().hasClass('hidden') == false){
						return;
					}
				}
                
                // Security Code
				if($('#securityCodeDiv').is(":visible")==true){
					status = errorCheck.checkSecurityCode();
					if(status == 'false' || errorCheck.checkSecurityCode() == false){
						return;
					}
				}
                // City
				if($('#cityDiv').is(":visible")==true){
					status = errorCheck.checkCity();
					if(status == 'false' || errorCheck.checkCity() == false){
						return;
					}
				}
                // State
				if($('#stateDiv').is(":visible")==true){
					status = errorCheck.checkS();
					if(status == 'false' || errorCheck.checkS() == false){
						return;
					}
				}
                // Zip Code
				if($('#zipCodeDiv').is(":visible")==true){
					status = errorCheck.checkZipCode();
					if(status == 'false' || errorCheck.checkZipCode() == false){
						return;
					}
				}
                // Phone Number
				if($('#phoneDiv').is(":visible")==true){
					status = errorCheck.checkPhone();
					if(status == 'false' || errorCheck.checkPhone() == false){
						return;
					}
				}

				// OPS-98189
				//Geolocation Product error check for country field
				var errVal = $('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').hasClass('hidden');
				if(errVal == false || errVal == 'false'){
					return;
				}
				// NSB Company /*OPS-65357 NSB Changes*/
				if($('#companyDiv').is(":visible")==true){
					status = errorCheck.checkCompany();
					if(status == 'false' || errorCheck.checkCompany() == false){
						return;
					}
				}
				// NSB taxId /*OPS-65357 NSB Changes*/
				if($('#taxIdDiv').is(":visible")==true){
					status = errorCheck.checktaxId();
					if(status == 'false' || errorCheck.checktaxId() == false){
						return;
					}
				}
				// NSB phone /*OPS-65357 NSB Changes*/
				if($('#nsbPhoneDiv').is(":visible")==true){
					status = errorCheck.checknsbPhone();
					if(status == 'false' || errorCheck.checknsbPhone() == false){
						return;
					}
				}							
				// Boleto /*OPS-63895 CPF Changes*/
				if($('#cpfcnpjDiv').is(":visible")==true){
					status = errorCheck.checkcpfCnpj();
					if(status == 'false' || errorCheck.checkcpfCnpj() == false){
						return;
					}
				}
				//Address Field
				if($('#addressDiv').is(":visible")==true){
					status = errorCheck.checkaddress();
					if(status == 'false' || errorCheck.checkaddress() == false){
						return;
					}
				}

				/*Boleto Changes */
				if($('#countryDiv').is(":visible")==true){
					// Check if the error is being displayed on the 
					// country dropdown
					status = errorCheck.checkCountry();
						if(status == 'false' || errorCheck.checkCountry() == false){
							return;
						} 

					var billingCty = $('#countriesDropDown').val();
					var paymentMethod = $('#paymentMethod').val();

						if (typeof boleto_code !=='undefined' && (paymentMethod =="BT")){
							if(offlineCountriesList.split(',').indexOf(countryCodeId) !== -1){
								if((offlineCountriesList.split(',').indexOf(billingCty) == -1))	{												
									return;
									}
							}
							else if(countryCodeId === "BR"){	
								if(billingCty !== "BR")	{												
									return;
									}
							}
						}
						/*Oxxo Changes OPS-73492 */
						else if(typeof oxxo_code !=='undefined'){
							if(($('#countriesDropDown').val() !== "MX") && ($('#paymentMethod').val()=='HP'))	{
									return;
							}
					}
				}

                /* Empty Validation check ends */
                  var bpVo = new Object();
                  //ops-75656
                  bpVo.bankNameSelected = $("#bankDropdownList option:selected").text();
                  bpVo.bankCodeSelected = $("#bankDropdownList").val();
                  var cardType = $(".row.withImg img").attr("id");
                  bpVo.cardHolderName = $('#cardHolderName').val().replace(/"/g,"&quot;")
                                   						 .replace(/&/g,'&amp;')
                                   						 .replace(/</g,'&lt;')
                                    					 .replace(/>/g,'&gt;')
                                    					 .replace(/'/g,'&apos;')
                                   						 .replace(/\//g,'&#x2F;'); // CardHolderName
                  bpVo.cardNumber = $('#cardNumber').val().replace(/[-\s]/g, '');  // log in
                  if($('#expMonthDropDown option:selected').val()){
                  	bpVo.month = $('#expMonthDropDown option:selected').val();		
                  }
                  else{
                  	bpVo.month = "-1";
                  }

                  if($('#expYearDropDown option:selected').val()){
                  	bpVo.year = $('#expYearDropDown option:selected').val();
                  }
                  else{
                  	bpVo.year = "-1";
                  }

                  bpVo.cvv = $('#cvv').val();  // log in
                  bpVo.zipCode = $('#zipCode').val().trim();  // log in
                  bpVo.phone = $('#phoneNumber').val();  // log in
                if ($('.stateDropdown').is('input, select')) {
           			 var state = $('#stateDropDown option:selected').val();
       			 }
        		if ($('.optionalState').is(':visible')) {
          			state = $('.optionalState').val();
        		}                 
                 bpVo.state = state; // log in
                 bpVo.country = $('#countriesDropDown option:selected').val();  // log in
                 bpVo.city = $('#city').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');  // log in
				/*OPS-65357 NSB Changes*/
				bpVo.company = $('.company').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');  // NSB Company							
				bpVo.taxId = $('.taxId').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');  // NSB taxId
				bpVo.nsbphone = $('#nsbPhoneNumber').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');  // NSB Phone Number
				bpVo.vatCode = $('.vatCode').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');  // VatCode for OPS-73493
				bpVo.iban = $('.iban').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');  // IBAN
 				bpVo.bankCode = $('.bankCode').val().replace(/"/g,"&quot;")
                                    .replace(/&/g,'&amp;')
                                    .replace(/</g,'&lt;')
                                    .replace(/>/g,'&gt;')
                                    .replace(/'/g,'&apos;')
                                    .replace(/\//g,'&#x2F;');  // BIC                                                                      
                 bpVo.email = $('.headingBar span#emailUserVal span.new_user').text();  // log in

                 /* Boleto Changes for OPS-63895 */

                 bpVo.cpfCnpj = cpfcnpj;  // log in			  
				 var paymentDetailsArr = $('#paymentMethod').val().split('_');                 
				 //Billing Consent for EMEA
				 if($('#agree1').attr('checked')){
					bpVo.autoConsentFlag = "true";
				}else{
					bpVo.autoConsentFlag = "false";
				}
				 
				 /* PayPal Integration CAP-9567 */
				if($('#paymentMethod').val()=='DW'){
					bpVo.paymentMethod = $('#paymentMethod').val();
					bpVo.paymentSubType = window.paypalPaymentSubtype;
					bpVo.cardType = window.paypalPaymentSubtype;
				}
				/* Boleto Changes OPS-63895 */
				else if($('#paymentMethod').val()=='BT'){
					bpVo.paymentMethod = $('#paymentMethod').val();
					bpVo.paymentSubType = window.boletoPaymentSubtype;
					bpVo.cardType = window.boletoPaymentSubtype;                    
				}
				/*Oxxo Changes OPS-73492 */
				else if($('#paymentMethod').val()=='HP'){
					bpVo.paymentMethod = $('#paymentMethod').val();
					bpVo.paymentSubType = window.oxxoPaymentSubtype;
					bpVo.cardType = window.oxxoPaymentSubtype;
				}
				/*DD Changes */
				else if($('#paymentMethod').val()=='DD'){
					bpVo.paymentMethod = $('#paymentMethod').val();
					bpVo.paymentSubType = window.ddPaymentSubtype;
					bpVo.cardType = window.ddPaymentSubtype;
				}
				/*IB Changes */   
				else if(paymentDetailsArr[0] == 'IB'){
					bpVo.paymentMethod = paymentDetailsArr[0];
					bpVo.paymentSubType = paymentDetailsArr[1];
					bpVo.cardType = paymentDetailsArr[1];										                 
				}
				//WorldPay Changes 
				else if(paymentDetailsArr[0] == 'HP'){
					bpVo.paymentMethod = paymentDetailsArr[0];
					bpVo.paymentSubType = paymentDetailsArr[1];
					bpVo.cardType = paymentDetailsArr[1];															                
				} 
				else{
					bpVo.paymentMethod = $('#paymentMethod').val();
					console.log("bpVo.paymentMethod" +bpVo.paymentMethod);
					bpVo.paymentSubType = cardType;
					bpVo.cardType = cardType;
				}
				console.log("Payment Method"+bpVo.paymentMethod);
				//Billing and Shipping Same CheckBox information
				if($("#billSameShip").attr("checked")){
					bpVo.billingShippingFlag = "true";
					bpVo.address = $('#address-ship').val();  //log in
					bpVo.address2 = $('#address2-ship').val();
				}else{
					bpVo.billingShippingFlag = "false";
					bpVo.address = $('#address').val();  //log in
					bpVo.address2 = $('#address2').val();
				}
                console.log("bpVo ::" + bpVo);

                return bpVo;
}

// applyTax function (applies tax on the product purchased by the user.)
function applyTax(bpVo){                
       
               displaySpinner(); 
               console.log("display spinner"+displaySpinner());               

            $.ajax({
                type: 'POST',
                url: '/estore/getShoppingCart/action/applyTax',
                data: JSON.stringify(bpVo),
                contentType: "application/json; charset=UTF-8",
                dataType: "json",
                mimeType: 'application/json',
                timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
                success: function (data){
                  console.log("applyTaxsuccess");
                  console.log(data); 
				   				/*PBL-2311 Empty Cart Condition added in case of applyTax */               
                  if (typeof data.emptyCartMessage !=='undefined' && data.emptyCartMessage !='') {                    	                   	
		            $("#hideForemptyCart").hide();
		            $(".couponTotalBox").hide();
					$("#acceptedPayments1").hide();
		            $(".editInfoLink").hide();
		            $('#subscriptionArMessage').fadeOut('slow');
		            $("#Norton_Cart").fadeOut();
					$('#euVatToC').hide();
					$('#mini-Cart').hide();
		            $(".productInfoRemove").click(function() {
		            	$("#Norton_Cart").fadeToggle('slow');
		            });

		            refreshTemplates(data);
		          } 

		          else if (typeof data.statusMessage !=='undefined' && data.statusMessage == "FAIL"){
				    
				    if(typeof data.validVatCode !=='undefined' && data.validVatCode == false) {                    	                   	

				           			$("#BillingSec").show();
                    				$("#reviewSec").hide();									
									var errorMsg = $('#invalidVatCode').text();												
									$('#vatCode').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
									$(".vatCode").addClass("redBorder");
									$('#vatCode').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);
					}				 
					if(typeof data.valideIBAN !=='undefined' && data.valideIBAN == false) { 							
				           			$("#BillingSec").show();
                    				$("#reviewSec").hide();									
									var errorMsg = $('#invalidIBAN').text();
									$('#iban').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
									$(".iban").addClass("redBorder");
									$('#iban').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);
					}															
					if(typeof data.valideBIC !=='undefined' && data.valideBIC == false) {                    	                   	

				           			$("#BillingSec").show();
                    				$("#reviewSec").hide();									
									var errorMsg = $('#invalidBIC').text();
									$('#bankCode').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
									$(".bankCode").addClass("redBorder");
									$('#bankCode').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);
					}		
			}
				    else {									    						
							refreshTemplates(data);
							checkLAMC();/*LAMC Check */					
							checkExp4(); /*Exp4 Check */ 

						    $('#subscriptionArMessage').fadeOut('slow');                    					 					                                   
		                    $("#BillingSec").slideUp("slow");                    
		                   	$(".accountInformation .headingBar").addClass("inactiveHeader");
							$(".billingInformation .headingBar").addClass("inactiveHeader");
							$(".orderReview .headingBar").removeClass("inactiveHeader");
		                    $("#cart_trigger").fadeOut();
		                    $("#candyRack").fadeOut();
		                    $("#reviewSec").slideDown("slow");
		                    $("#billingInfoLink").show();
		                    console.log('billingInfoLink should show');
		                    // following gets width of payment method container and assisgns same width to name container
		                    $('.custInfo .customerInfoRow:first-child .infoHeading').css({'width': ($('.custInfo .customerInfoRow:last-child .infoHeading').width() + 'px')});

					// OPS-85080
	                  if(data.walletInfo){
		                   if(typeof window.idealBankList !== 'undefined' && data.walletInfo.country == 'NL' && data.walletInfo.cardType == 'IDEAL'){
		                  	var bankName = data.walletInfo.bankName;
		                  	$("#netBankingPayment").show();
		                  	$('#paymentBankName').text(bankName);
		                  }
						
		              }

                    if (typeof updateOmnitureEvents === "function") { 
                        var omniskuCode="none";                     
                        var pagetype="none"; 
                        var action = "applyTax";
                        updateOmnitureEvents(data,action,pagetype,omniskuCode);
                    }
                  } //else block end 
				  checkPayPalMessage();
                },
                    error:genericAjaxErrorHandler,
				// This code is for handling the session expiry condition
                statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("ApplyTax Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("ApplyTax Session has expired"+xhr.responseText);
                        }
                    }

                }
				}
            });
        };

/* Added for Image on click of card image click */
function displayImage() {
     $('#cvvCodeImage').slideUp('slow');
     $('#cvvCodeImageCC').slideUp('slow');
     $('#showCvvHelp').show();           
         
    $(".acceptedPayments1 img").on("click", function() {
        var cardImgSrc = $(this).attr("src");
        var cardImgId = $(this).attr("id");
        $(".row.withImg img").show().attr("src", cardImgSrc);
        $(".row.withImg img").attr("id", cardImgId);
        $("input#cardNumber").val("");
    })
    if(typeof window.walletinformation == 'undefined' && window.walletinformation == null){		
         $('#showCvvHelp').show();           
     }
     else {
     $('#showCvvHelp').hide();  
     }
};

/* shows cvv help image and text for two card types - CC or AmEx heresup */
function showCvvHelp () {
    var cardTypeVal = $(".row.withImg img").attr("id");
    console.log("card type image: is" + " " +cardTypeVal);
    $('#cvvCodeImage').slideUp('slow');
    $('#cvvCodeImageCC').slideUp('slow');
     
    if (cardTypeVal == "amex" || cardTypeVal == "AMEX") {
    console.log("card type:"+cardTypeVal);
        $('#cvvCodeImage').slideDown('slow');

    } else {
    console.log("card type:"+cardTypeVal);
        $('#cvvCodeImageCC').slideDown('slow');
    };
    $('#showCvvHelp').fadeToggle('slow');
    if(typeof window.walletinformation == 'undefined' && window.walletinformation == null){	
         $('#showCvvHelp').show();           
     }
     else {
     $('#showCvvHelp').hide();  
     }
     $('#subscriptionArMessage').fadeOut('slow');
};

/* hides cvv help pannel */
function closeCvvHelp () {
    $('#cvvCodeImage').slideUp('slow');
     $('#cvvCodeImageCC').slideUp('slow');
    var cardTypeVal = $(".row.withImg img").attr("id");
    if (cardTypeVal == "amex" || cardTypeVal == "AMEX") {
        $('#cvvCodeImage').slideUp('slow');
    } else {
        $('#cvvCodeImageCC').slideUp('slow');
    };
    $('#showCvvHelp').fadeToggle('slow');
    if(typeof window.walletinformation == 'undefined' && window.walletinformation == null ){
              $('#showCvvHelp').show();          
     }
     else {
     $('#showCvvHelp').hide();  
     }
} 

/* shows/hides phone help pannel */
function showPhoneHelp() {
    $("#phoneHelpText").slideToggle('slow');
    $("#phoneHelpLink").fadeToggle();
    $('#subscriptionArMessage').fadeOut('slow');
}

/* shows/hides shipping phone help pannel */
function showShipPhoneHelp() {
    $("#phoneHelpText-1").slideToggle('slow');
    $("#phoneHelpLink-1").fadeToggle();
}

/* shows/hides VAT ID help pannel */
function showVatIdHelp() {
    $("#vatIdHelpText").fadeToggle();
    $("#vatHelpText").slideToggle('slow');
}

/* shows/hides Iban help pannel */
function showIbanHelp() {
    $("#ibanHelpLink").fadeToggle();
    $("#ibanHelpText").slideToggle('slow');
}

/* shows/hides BAC help pannel */
function showbankCodeHelp() {
    $("#bankCodeHelpLink").fadeToggle();
    $("#bankCodeHelpText").slideToggle('slow');
}

/* shows/hides company help pannel */
function showCompanyHelp () {
    $("#companyHelpText").slideToggle('slow');
    $("#companyHelpLink").fadeToggle();
}

/* shows/hides Tax ID help pannel */
function showTaxIdHelp () {
    $("#taxIdHelpText").slideToggle('slow');
    $("#taxIdHelpLink").fadeToggle();
}

/*OPS-65357 NSB Changes shows/hides NSB phone help panel */
function showNsbPhoneHelp() {
    $("#nsbPhoneHelpText").slideToggle('slow');
    $("#nsbPhoneHelpLink").fadeToggle();
}

/*OPS-65357 NSB Changes shows/hides NSB phone help panel */
function showBillingConsentHelp() {
    $("#billingConsentHelptext").slideToggle('slow');
    $("#billingConsentHelpLink").fadeToggle();
}

// proceed to secure checkout - closes cart overlay and goes to order review
function proceedToReview() {
    $('#checkout').on("click", function() {
        window.cartOverlay = "false"
        $("#Norton_Cart").fadeOut();
        $("body").removeClass('overflowHidden');
        // need to add lagic to determine where in proces customer if to show esit link in billing seciton 
        if (typeof updateOmnitureEvents === "function") {
            var data = "none" ;
            var omniskuCode="none";                     
            var pagetype="none"; 
            var action = "proccedtosecurecheckout";
            updateOmnitureEvents(data,action,pagetype,omniskuCode);
        } 
    });
};

var beforeload = (new Date()).getTime();

function pageloadingtime() {
    var afterload = (new Date()).getTime();
    seconds = Math.round(((afterload - beforeload) / 1000) * Math.pow(10, 3)) / Math.pow(10, 3);
    return seconds;
};

/* This function displays the spinner image */
function displaySpinner() {
    console.log('displaySpinner firing');
    $("#loadingSpinner").show();
    $("#loadingSpinner").css("background", "none");
};

function finalOrderSpinner() {
	$("#spinnerBorder").removeClass("spinnerBorder");
	$("#spinnerBorder").addClass("spinnerBorder1");
	$("#submitOrderWait").show();
    $("#loadingSpinner").show();
    $("#loadingSpinner").css("background", "none");
};

function refreshFooterTemplate(data){
	var templateSource = $("#footer-template").html();
	var template = Handlebars.compile(templateSource);
	var footerHTML = template(data);
	$("#footerContainer").html(footerHTML);
}

$(document).ready(function() {
	
	window.setInterval(function(){checkSesCookie();}, 60000);
	
    if (HANDLE_HANDLEBARS_COMPILE_ERROR_FEATURE_FLAG === true){
      Handlebars.theOriginalCompileMethod = Handlebars.compile;
      Handlebars.compile = function(){
        try {
          return Handlebars.theOriginalCompileMethod.apply(this, arguments);
        }
        catch (e) {
          $('body').html('Something went wrong');
          throw e;
        }
      } // end enhanced compile method
    }
    
    $.ajax({
	       type: "GET",
	       url: '/estore/getShoppingCart/action/footercontent',
	       contentType: "application/json; charset=UTF-8",
	       dataType: "json",
	       success: function(data) {
	    	   console.log("Fetching Footer Data and adding it to the page."+data);
	    	   refreshFooterTemplate(data);
	       },
		   error: genericAjaxErrorHandler,
			
			// This code is for handling the session expiry condition
		   statusCode: {
			   200: function(xhr){
             if(typeof xhr.responseText != "undefined"){
                 if(xhr.responseText.search("mfsessionExpired") == -1){
                     console.log("Footer content Session not expired"+xhr.responseText);
                 }
                 else{
                     window.location.href = '/estore/mf/sessionExpired';
                     console.log("Footer content Session has expired"+xhr.responseText);
                 }
             }

         }
 }
	   });

    $("body").addClass('overflowHidden');
    $("#billingInfoLink").hide();    
    $(".loginToggle").hide();
    $(".clickToggleCopy").hide();
    $('#emailUserVal').hide();
	$("#agreePaymentInfoPrePopulated").hide();
    
    // Preventing the user to enter more than 10 digits on 
    // mobile number field for IE9  
    if ( $.browser.msie ) {
        $(".phone").keypress(function(e){
        var lengthF = $(this).val();
        if (lengthF.length == 10){
            e.preventDefault();
            return false;
        }
        });
    }

	$(document).ajaxStop(function(event, jqXHR, ajaxOptions){
		$("#loadingSpinner").hide();
		$("body").removeClass('overflowHidden');
	});
        
    $("#billingInfoLink").click(function() {   	
    	checkLAMC();/*LAMC Check */	
		checkExp4(); /*Exp4 Check */
		getSupportedImage(); // getSupportedImage

        $("#billingInfoLink").hide();
        $(".accountInformation .headingBar").addClass("inactiveHeader");
		$(".billingInformation .headingBar").removeClass("inactiveHeader");
		$(".orderReview .headingBar").addClass("inactiveHeader");
        $(this).parent().siblings(".editInfoPanelCopy").slideDown('slow', function(){
            $("#reviewSec").slideUp();            
            console.log('Review section slideUp?')
        });
        formatPhoneNumber();
		/*OPS-65357 NSB Changes*/
		formatnsbPhoneNumber();
        $("#cart_trigger").fadeIn();
      	// Fix for INC0312189:Empty candy rack is appearing on proceeding to review and then clicking edit
		if ( $.trim( $('.candyRack').text() ).length !== 0 ) {
			$("#candyRack").fadeIn('fast');
			$("#candyRackOverlay").fadeIn('fast');			
		}
		else {
			$("#candyRack").fadeOut('fast');
			$("#candyRackOverlay").fadeOut('fast');
		}

        if($('#cvv').val().length != 0)
            {
                $('#cvv').val("");
            }
        var nu = $('#cardNumber').val().replace(/[-\s]/g, '');
         if(nu.length >0){
            $('#cardNumber').val('xxxxxxxxxxxx'+nu.slice(12,16));
        }
        window.userNumberEntry = '0';
    });

    // Shipping Info Edit Link
    $("#shippingInformationLink").click(function() {   	
    	checkLAMC();/*LAMC Check */	
		checkExp4(); /*Exp4 Check */
		getSupportedImage(); // getSupportedImage

        $("#shippingInformationLink").hide();
        $("#billingInfoLink").hide();
        $(".accountInformation .headingBar").addClass("inactiveHeader");
        $(".billingInformation .headingBar").addClass("inactiveHeader");
		$(".shippingInformation .headingBar").removeClass("inactiveHeader");
		$(".orderReview .headingBar").addClass("inactiveHeader");
        $(this).parent().siblings("#shippingSec").slideDown('slow', function(){
        	$("#BillingSec").slideUp();
            $("#reviewSec").slideUp();
            if($('#cvv').val().length != 0)
            {
                $('#cvv').val("");
            }           
        });
    });

    // generic toggle function
    $(".clickToggle").click(function() {
        $('div.errorDiv').addClass('hidden');
        $(".clickToggleCopy").slideDown('slow');
        $(".loginContainer").slideUp('slow');
        $(".loginToggle").show();
        $(".clickToggle").hide();

    });
     $(".loginToggle").click(function() {
        $('div.errorDiv').addClass('hidden');
        $(".clickToggleCopy").slideUp('slow');
        $(".loginContainer").slideDown('slow');
        $(".loginToggle").hide();
        $(".clickToggle").show();        
    });

      $("#viewEuVat").click(function() {
   	  $('#subscriptionArMessage').fadeOut('slow');
      $(this).children(".expand-text").toggle();
      $("#euVatCopy").slideToggle('slow');
      $(this).children(".expand-text1").toggle();
    });

     $("#viewMerchantText").click(function() {
      $(this).children(".merchant-header").toggle();
      $("#merchantText").slideToggle('slow');
      $(this).children(".merchant-header1").toggle();
    });

    $("#arHelpLink").click(function() {
        console.log('arHelpLink firing');
        $('#arHelpLinkCopy').slideToggle('slow');
    });

    // OPS-56455 Exp4 AR subscription messaging - show more
		$('#subscriptionArSeeMore').click(function() {
			$('#subscriptionArSeeMore').fadeOut();
			setTimeout(function(){
				$('#showMoreSubscriptionAr').slideToggle('500');
			}, 250);
		});
		// hides subscription AR content Box
		$('#ArCloseX').click(function() {
			$('#subscriptionArMessage').fadeOut();
		});

		$(".closeCart").click(function() {
        window.cartOverlay = "false";
        $("#Norton_Cart").fadeToggle('slow');
        $("body").removeClass('overflowHidden');
    });
    $("#cart_trigger").click(function() {
        window.cartOverlay = "true";
        $("#Norton_Cart").fadeToggle('slow');
        $("body").addClass('overflowHidden');
        $('#subscriptionArMessage').fadeOut('slow');
    });
    
   $('#Norton_Cart').mousedown(function(e) {  // closes cart overlay when background clicked
        var clicked = $(e.target);
        if (clicked.is('.cartContent') || clicked.parents().is('.cartContent')) {
            return;
        } else {
            window.cartOverlay = "false"
            $('#Norton_Cart').fadeToggle('fast');
            $("body").removeClass('overflowHidden');
        }
    });

    // Validations Related Code Ends
    $("#toggleEULA").on("click", function() {
        $("#sub-info-fullB").slideToggle('500');
    });

    $("#arHelpLink").on("click", function() {
        $("#arHelpLinkCopy").slideToggle('500');
    });

    $(".row.withImg input").after("<img src='' border='0' style='display:none;' />");

    /* OPS-76393 Eula Check for Exp4*/
    $("#confirmEmeaSubmit").on("click", function() {

		    if($("#agreeEmeaNonArEula").attr("checked")){		    
			  	console.log("inside checked true");
			    $("#eulaError").hide();			    			
			  } else {	  	
			  	console.log("inside checked false");			    
			   	$("#eulaError").show();
			  }
        
    });
    /* OPS-76393 Eula Check for Exp4*/   	
		$("#agreeEmeaNonArEula").on("click", function() {
			  if($("#agreeEmeaNonArEula").attr("checked")){
			  		console.log("inside checked true on change");
			    	$("#eulaError").hide();
			    	$("#confirmEmeaSubmit").hide();	
			   	    $("#confirmSubmit").show();				        	
			  } else {			  	
			  		console.log("inside checked false on change");
			  		$("#confirmSubmit").hide();	
			    	$("#confirmEmeaSubmit").show();			  		
			   		$("#eulaError").show();
			  }
			  
			});
		// Checkbox Set true on select of archeckboxCopy OPS-93616
		$("#arCheckboxCopy").on("click", function() {
			  if($("#arCheckboxCopy").attr("checked")){
			  		$( "#arCheckbox" ).prop( "checked", true );
			   			        	
			  } else {			  	
			  		$( "#arCheckbox" ).prop( "checked", false );
		
			  }
			  
			});

	
    /*Handlebars*/
    var templateSource = $("#cart-template").html();
    var template = Handlebars.compile(templateSource);
    var totalTemplateSource = $("#total-template").html();
    var totalTemplate = Handlebars.compile(totalTemplateSource);    
    var overlayTemplateSource = $("#overlay-template").html();
    var overlayTemplate = Handlebars.compile(overlayTemplateSource);   
    var crumbTemplateSource = $("#crumb-template").html();
    var crumbTemplate = Handlebars.compile(crumbTemplateSource);
    var candyTemplateSource = $("#candy-template").html();
    var candyTemplate = Handlebars.compile(candyTemplateSource);
    var expMonthTemplateSource = $("#expMonth-template").html();
    var expMonthTemplate = Handlebars.compile(expMonthTemplateSource);
    var expYearTemplateSource = $("#expYear-template").html();
    var expYearTemplate = Handlebars.compile(expYearTemplateSource);
    var countryTemplateSource = $("#country-template").html();
    var countryTemplate = Handlebars.compile(countryTemplateSource);
    var emptyCartTemplateSource = $("#emptyCart-template").html();
    var emptyCartTemplate = Handlebars.compile(emptyCartTemplateSource);
    var reviewTemplateSource = $("#review-template").html();
    var reviewTemplate = Handlebars.compile(reviewTemplateSource);
    var eulaTemplateSource = $("#eula-template").html();
    var eulaTemplate = Handlebars.compile(eulaTemplateSource);
    var reviewTotalTemplateSource = $("#reviewTotal-template").html();
    var reviewTotalTemplate = Handlebars.compile(reviewTotalTemplateSource);
    var pwdTemplateSource = $("#forgotPassword-template").html();
    var pwdTemplate = Handlebars.compile(pwdTemplateSource);
    var customerTemplateSource = $("#customer-template").html();
    var customerTemplate = Handlebars.compile(customerTemplateSource);
   	var arTemplateSource = $("#arMessage-template").html();
    var arTemplate = Handlebars.compile(arTemplateSource);
    var showMoreSubscriptionArTemplateSource = $("#showMoreSubscriptionAr-template").html();
    var showMoreSubscriptionArTemplate = Handlebars.compile(showMoreSubscriptionArTemplateSource);
	var paymentTemplateSource = $("#payment-template").html();
    var paymentTemplate = Handlebars.compile(paymentTemplateSource);
    var cartHTML;
    var overlayHTML;
    var candyHTML;
    var crumbHTML;
    var totalHTML;
    var cardImageHTML;
    var expMonthHTML;
    var expYearHTML;
    var countryHTML;
    var stateHTML;
    var reviewHTML;
    var eulaHTML;
    var reviewTotalHTML;
    var subscriptionArHTML;
    var pwdHTML;
    
    /* Getting the data from JSON  */
    $.ajax({
        type: "GET",
        url: '/estore/getShoppingCart/action/getcart',
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
        success: function(data) {

        	/*PBL-2311 Empty Cart Condition added in case of getcart */               
          	if (typeof data.emptyCartMessage !=='undefined' && data.emptyCartMessage !='') {                    	                   	
				$("#hideForemptyCart").hide();
				$(".couponTotalBox").hide();
				$("#acceptedPayments1").hide();
				$(".editInfoLink").hide();
				$('#subscriptionArMessage').fadeOut('slow');
				$("#Norton_Cart").fadeOut();
				$('#euVatToC').hide();
				$('#mini-Cart').hide();
				$(".productInfoRemove").click(function() {
					$("#Norton_Cart").fadeToggle('slow');
				});

				refreshTemplates(data);

	   		} //empty cart check ends 
		   
		   		else { 
			
	          if (typeof data.cartCoupons !== 'undefined') {
	           	if(typeof data.cartCoupons[0] !== 'undefined'){
								discountCouponVal= data.cartCoupons[0];
								discountAmtVal= data.discountAmount;
								$(".discountRow span#discountAmount, #overlayDiscount").show();
								$(".couponRow").hide();
								$("#discount").text(discountCouponVal);
								$("#discountAmount, #overlayDiscountAmount").text(discountAmtVal);
								$(".discountRow #discountAmount, #overlayDiscountAmount").prepend("-");
							}
						}
			
						/* Added to get Marketing Options selected based on API on logOut CAP-11382*/
						if (typeof data.marketingOptions !== 'undefined') {
							for (var j = 0; j < data.marketingOptions.length; j++) {
	        			console.log("Marketing options Selected "+data.marketingOptions[j].selected);
								if(data.marketingOptions[j].selected == true){
									console.log("Marketing options Selected inside"+data.marketingOptions[j].selected);						
									$('#e1').prop('checked', true);
								}
	            }
						}				
			
						/* PayPal Integration CAP-9567 */
			      	window.errorMessages = data.billingFields;
			     	window.countryStates = data.states;
			      	window.fieldsToValidate = data.fieldsToValidate;
			      	window.returningcustomer = data.existingCustomer;
			      	window.cartItems = data.cartItems;
			      	window.geoRestrictionList = data.geoRestrictionValidationList;
			      	window.walletinformation = data.walletInfo;
			      	window.email = data.email;
			      	window.userCheck = data.createUser;
			      	window.pageFlow = data.flowType;
			      	window.tcFail = data.tcfail;					
					window.bmsflow = data.olbuBMSFlow;
					window.bakflow = data.bakflow;
					window.integratedFlow = data.integratedFlow;
					window.payPalError = data.paymentError;
					/* Added to get Marketing Options selected based on API on logOut CAP-11382*/
					window.selectedMarketingOptions = data.marketingOptions;
					/*OPS-65357 APAC Changes*/
					window.apacCountry = data.apacCountry;					
					window.lamacCount = data.lamcCountry;
					//LAMC CountryList
					window.lamcCountriesList = data.lamcCountriesList;
					//Offline CountryList
					window.offlineCountriesList = data.offlineCountriesList;
					window.exp4CountriesList = data.exp4CountriesList;
					//DD CountryList
					window.directDebitCountryList = data.directDebitCountryList;
					//EMEA CountryList
					window.eucountriesList = data.eucountriesList;
					window.isSelectedCount = false;
					window.omnitureCountry = data.omnitureContent.country;
					console.log("omnitureContent "+ window.omnitureCountry);
					// OPS-71512 Changes to validate not supported card Number
					window.SupportedPaymentValues = data.supportedPymt[0].paymentSubTypes;
					console.log("payment sub type ---"+ window.SupportedPaymentValues);
					//65465
					window.alpaGoCountriesListVal = data.alpaGoCountriesList;
					console.log("window.alpaGoCountriesListVal ---"+ window.alpaGoCountriesListVal);					
					//OPS-77539 
					window.methodOfPaymentList = data.methodOfPaymentList;
					window.supportedPymtData = data.supportedPymt;
					window.pspValue = data.pspValue;					
					window.bankListData = data.ibBankList;
					// WorldPay Changes OPS-73496
					window.mandatoryCityFieldCountryList = data.mandatoryCityFieldCountryList;
					window.enableLogoutAction = data.enableLogoutAction; //OPS-87760 cart development logOut functionality
					window.enableCustomErrorMessage = data.enableCustomErrorMessage; // Custom Error Message change based on acquirer codes OPS-39025, OPS-31067, OPS-28005
					if(data.hasOwnProperty("customPaymentError")){
						window.customPaymentError = data.customPaymentError;	
					}
									
					//OPS-85079
					if(data.hasOwnProperty("adeynIdealBankList")){
						window.idealBankList = data.adeynIdealBankList; 	
					}

					// OPS-87838
					window.paypalMerchantID = data.paypalMerchantID;
					window.paypalEnvironment = data.paypalEnvironment;
					window.enablePayPalContextFlow = data.enablePayPalContextFlow;
					window.paypalCountryList = data.paypalCountryList;
					// OPS-94688
					window.shippingInfoVo = data.shippingInfoVo;
					window.cartContainsPhysicalProduct = data.cartContainsPhysicalProduct;
	
			      $('#stateDropDown').empty();
			      countryStates = window.countryStates;

			      	//OPS-98189
			      	window.productFamilyList = [];
					if(window.cartItems.length>0){
						for(i=0;i<window.cartItems.length;i++){
							productFamilyList.push(window.cartItems[i].productFamilyName);
						}
					}
			
				if (typeof data.cartItems !== 'undefined') {
	           		 for (var countries = 0; countries < countryStates.length; countries++) {
	              	   if (countryStates[countries].default == true) {
	                  	cty = countryStates[countries].countryCode; // set our flag to true
	               }
	            }
						
				/* Added for PBL-2310, Setting State based on Wallet Country, if Wallet is available */
           		if (typeof window.walletinformation !== 'undefined' && window.walletinformation !== null && window.walletinformation['country']){
								walletCountryCode = window.walletinformation['country'];
								getStatePopulated(walletCountryCode, countryStates, '#stateDropDown', 
									'#optionalState', '#stateDropdownhidden option', '#stateDiv label');
				} else {
            		getStatePopulated(cty, countryStates, '#stateDropDown', '#optionalState', 
            			'#stateDropdownhidden option', '#stateDiv label');
				}
				// shipping.
				getStatePopulated(cty, countryStates, '#stateDropShip');  

            	for (var i = 0; i < data.candyRackResult.candyRackItems.length; i++) {
              	candyRackLinkUrl[data.candyRackResult.candyRackItems[i].skuCode] = data.candyRackResult.candyRackItems[i].candyRackBuyLink;
              	console.log("candyRackLinkUrl :" + candyRackLinkUrl[data.candyRackResult.candyRackItems[i].skuCode]);
            	}
						
            	for (var j = 0; j < data.cartItems.length; j++) {
              	removeCartLink[data.cartItems[j].skuCode] = data.cartItems[j].removeCartLink;
              	console.log("removeCartLink :" + removeCartLink[data.cartItems[j].skuCode]);
            	}

	            for (var k = 0; k < data.candyRackResult.candyRackItems.length; k++) {
	               candyRackremoveCartLink[data.candyRackResult.candyRackItems[k].skuCode] = data.candyRackResult.candyRackItems[k].candyRackremoveCartLink;
	               console.log("removeCartLink candyRack :" + candyRackremoveCartLink[data.candyRackResult.candyRackItems[k].skuCode]);
	            }

	            refreshTemplates(data);

				/* OPS-62471 Show subscription AR messaging popup when flag is true */
				if (typeof data.enableEMEAARPopupop !== 'undefined'){
					if (data.enableEMEAARPopupop === true) {
						console.log("enableEMEAARPopupop firing " + data.enableEMEAARPopupop);

				    var showMoreSubscriptionArTemplateSource = $("#showMoreSubscriptionAr-template").html();
				    var showMoreSubscriptionArTemplate = Handlebars.compile(showMoreSubscriptionArTemplateSource);
				    var showMoreSubscriptionArHTML = showMoreSubscriptionArTemplate(data);
        			$("#showMoreSubscriptionAr").html(showMoreSubscriptionArHTML);
					$('#subscriptionArMessage').show();
					}
				}									
 				for (a = 0; a < data.supportedPymt[0].paymentSubTypes.length; a++) {
	 				window.paymentSubType = data.supportedPymt[0].paymentSubTypes[0];
					window.cardTypeName = paymentSubType.value;
 				}

				/*Get the data for State */
			for (i = 0; i < data.states.length; i++) {
					   states = data.states[i];
					   
					/*OPS-65357 APAC Changes*/			   
					if (states.default == true) {
					    window.countryCodeId = states.countryCode; // set our flag to true
						window.defaultCountryName = states.countryName;
					}
					
			/* Added for OPS-54395 Populate Ghost text Ticket
			This will change place holder of Zip Code Field based on the CA and Non CA */
				if (data.states !== 'undefined'){	   
					if (data.states[i].default == true){
						if (data.states[i].countryCode == "CA"){
							$('#zipCode').attr('placeholder',$('#postalCodeText').text());
							console.log("CA true");
						}
						else if (data.states[i].countryCode == "BR"){
							$('#zipCode').attr('placeholder',$('#cepText').text());
							console.log("BR true");
						}
					else{
						$('#zipCode').attr('placeholder',$('#zipCodeText').text());
						console.log("CA false");
						}
					}
				/* Added for OPS-60279 Hide Checkbox for Marketing emails in US*/
					if (data.states[i].default == true){
							if (data.states[i].countryCode == "US"){
								$('.emailCheckbox').hide();
							}
						else{
								  $('.emailCheckbox').show();
							}
					}		
				}	
		}
		/* Get the Data for SupportedPayment from JSON API */
		
	

        for (i = 0; i < data.supportedPymt.length; i++) {
               supportedPymt = data.supportedPymt[i];
			   
               //Get the Data for paymentSubTypes from JSON API
			if(typeof data.supportedPymt !== 'undefined') {
   	  
			   //PayPal Integration CAP-9567
				  console.log("localisedPaymentMethodName :" + data.supportedPymt[i].localisedPaymentMethodName);
				  console.log("paymentMethodCode :" + data.supportedPymt[i].paymentMethodCode);
				   
				  if(data.supportedPymt[i].paymentMethodCode == 'DW'){
						window.paypal_code = data.supportedPymt[i].paymentMethodCode;
						console.log("PayPal code"+window.paypal_code);
						window.paypalPaymentSubtype = data.supportedPymt[i].localisedPaymentMethodName;
						$('#paymentDropDown').show();	
					}
					 //Boleto Changes OPS-63895
					 if(data.supportedPymt[i].paymentMethodCode == "BT"){
						window.boleto_code = data.supportedPymt[i].paymentMethodCode;
						console.log("Boleto code"+window.boleto_code);
						window.boletoPaymentSubtype = data.supportedPymt[i].localisedPaymentMethodName;
						$('#paymentDropDown').show();
					}
					
					 //Oxxo Changes OPS-73492
					 if(data.supportedPymt[i].paymentMethodCode == 'HP'){
						window.oxxo_code = data.supportedPymt[i].paymentMethodCode;
						console.log("OXXO code"+window.oxxo_code);
						window.oxxoPaymentSubtype = data.supportedPymt[i].localisedPaymentMethodName;
						$('#paymentDropDown').show();
					}
					//Direct Debit
					if(data.supportedPymt[i].paymentMethodCode == 'DD'){
						window.direct_debit_code = data.supportedPymt[i].paymentMethodCode;
						console.log("window.direct_debit_code"+window.direct_debit_code);
						window.ddPaymentSubtype = data.supportedPymt[i].localisedPaymentMethodName;
					}
                                
					if(data.supportedPymt[i].paymentMethodCode == 'CC'){
						window.credit_card_code = data.supportedPymt[i].paymentMethodCode;
						console.log("window.credit_card_code"+window.credit_card_code);
					}		
			}

		}				
				  var paymentMethodHTML = paymentTemplate(data);
				  $("#paymentMode").html(paymentMethodHTML);              
			
		/*Boleto Changes */
		window.alpaGoCountriesList = data.alpaGoCountriesList;
		var alpaGobillingCountry = window.alpaGoCountriesList.indexOf(cty);
		var alpaGoCountry = window.alpaGoCountriesList.indexOf(countryCodeId);
		console.log("alpaGoCountry" +alpaGoCountry);
		console.log("alpaGobillingCountry" +alpaGobillingCountry);
		if((alpaGoCountry !== -1 && typeof alpaGoCountry !== 'undefined') || (alpaGobillingCountry !== -1 && typeof alpaGobillingCountry !== 'undefined')) {

			$('#cpfcnpjDiv').show();
			$('#addressDiv').show();
		}		
		checkLAMC();/*LAMC Check */	
		checkExp4(); /*Exp4 Check */
		
            //expYear
            for (k = 0; k < data.years.year.length; k++) {
               yearValue = data.years.year[k];
				var expYearTemplateSource = $("#expYear-template").html();
				var expYearTemplate = Handlebars.compile(expYearTemplateSource);
               var expYearHTML = expYearTemplate(data);
               $("#expYear").html(expYearHTML);

            }
            //expMonth
            for (i = 0; i < data.months.month.length; i++) {
               monthValue = data.months.month[i];
				var expMonthTemplateSource = $("#expMonth-template").html();
				var expMonthTemplate = Handlebars.compile(expMonthTemplateSource);
               var expMonthHTML = expMonthTemplate(data);
               $("#expMonth").html(expMonthHTML);
            }

            console.log('getcart success firing');

            //Getting the data for billingfields fieldname, validationType, errorMessage
            /* for loop to see which field is returning what validationType and errormessage , later can be removed */
            for (var emf = 0; emf < data.billingFields.length; emf++) {
               billingFields = data.billingFields[emf];
               fieldName = data.billingFields[emf].fieldName;
               for (var eml = 0; eml < billingFields.errorList.length; eml++) {
                  errorList = billingFields.errorList[eml];
                  validationType = billingFields.errorList[eml].validationType;
                  errorMessage = billingFields.errorList[eml].errorMessage;
               } //for loop end for billingfields fieldname, validationType, errorMessage 

            }
		}
												
            var errorCheck = {
                    /*This function shows main error message if there is error on any field*/
                    mainError: function() {
                        var errorFound = false;
                        $(".errorDiv").each(function(index) {

                            if (($(this).is(":visible")) && ($(this).text() != "")) {
                                errorFound = true;
                                return false;
                            }
                        });

                        if (!errorFound) {
                            $(".mainerrorDiv").addClass("hidden");
                            $(".headingText").removeClass("hidden");
                        } else {
                            $(".mainerrorDiv").removeClass("hidden");
                            $(".headingText").addClass("hidden");
                        }
                    }, //mainError 

                    /*This function removes the error message */
                    removeError: function(element) {
                        $('.' + element).parent(".errorMessageContainer").find(".errorDiv").addClass("hidden").text("");
                        $('.' + element).removeClass("redBorder");
                        this.mainError();
                    }, //removeError

                    /*This function add red border to field when error is thrown*/
                    addborderCheckbox: function(element) {
                        $('.' + element).css('outline-color', 'red');
                        $('.' + element).css('outline-style', 'solid');
                        $('.' + element).css('outline-width', 'thin');
                    }, //addborderCheckbox

                    /*This function removes the red border on field*/
                    removeBorderCheckbox: function(element) {
                        $('.' + element).css('outline-color', 'none');
                        $('.' + element).css('outline-style', 'none');
                        $('.' + element).css('outline-width', 'none');
                    }, //removeBorderCheckbox

                    //removing space 
                    removeSpace: function(element, value) {
                        var withoutSpace = $.trim(value);
                        $('.' + element).val(withoutSpace);
                        return withoutSpace;

                    }, //removeSpace

                    /*This function gets the error message for empty field */
                    errorForEmptyFields: function(index) {
                        for (var ii = 0; ii < billingfields[index].errorList.length; ii++) {
                            if (billingfields[index].errorList[ii].validationType.toLowerCase() == "isempty") {
                                return billingfields[index].errorList[ii].errorMessage;
                            }
                        }
                    }, //errorForEmptyFields


                    /*This function gets the error message for minimum length*/
                    errorForMinLength: function(index) {
                        for (var ii = 0; ii < billingfields[index].errorList.length; ii++) {
                            if (billingfields[index].errorList[ii].validationType.toLowerCase() == "minlengthvalidator") {
                                var minErrorMessage = billingfields[index].errorList[ii].errorMessage;
                                if (minErrorMessage.indexOf('${val}') >= 0) {
                                    minErrorMessage = minErrorMessage.replace('${val}', '6');
                                }
                                return minErrorMessage;
                            }
                        }
                        return "Minimum 6 characters allowed.";
                    }, //errorForMinLength

                    /*This function gets error message for equal validator*/
                    errorForEqualsValidator: function() {
                        for (var ii = 0; ii < billingfields[index].errorList.length; ii++) {
                            if (billingfields[index].errorList[ii].validationType.toLowerCase() == "notequalsvalidator") {
                                return billingfields[index].errorList[ii].errorMessage;
                            }
                        }
                    }, //errorForEqualsValidator

                    /*This function gets error message for empty field with identifier*/
                    errorForEmptyFieldsWithIdentifier: function(index, identifier) {
                        for (var ii = 0; ii < billingfields[index].errorList.length; ii++) {
                            if (billingfields[index].errorList[ii].validationType.toLowerCase() == "isempty" &&
                                billingfields[index].errorList[ii].identifier == identifier) {


                                return billingfields[index].errorList[ii].errorMessage;
                            }
                        }

                    }, //errorForEmptyFieldsWithIdentifier

                    /*This function get error message for email validation*/
                    errorForEmailValidation: function(index) {
                        for (var ii = 0; ii < billingfields[index].errorList.length; ii++) {
                            if (billingfields[index].errorList[ii].validationType == "EmailValidator" && billingfields[index].errorList[ii].errorMessage != "") {
                                return billingfields[index].errorList[ii].errorMessage;
                            }

                        }
                        return "The email address you have typed is not valid. Please try again.";
                    }, //errorForEmailValidation

                    /*This function get error message for Max length*/
                    errorForMaxLength: function(index) {
                        for (var i = 0; i < billingfields[index].errorList.length; i++) {
                            if (billingfields[index].errorList[i].validationType == "MaxLengthValidator" && billingfields[index].errorList[i].errorMessage != "") {
                                var maxErrorMessage = billingfields[index].errorList[i].errorMessage;
                                if (maxErrorMessage.indexOf('${val}') >= 0) {
                                    maxErrorMessage = maxErrorMessage.replace('${val}', '50');
                                }
                                return maxErrorMessage;
                            }

                        }
                        return "Maximum 50 characters allowed.";

                    }, //errorForMaxLength

                } //errorCheck

				if (data.emptyCartMessage) {           
		            console.log("emptyCart: " + data.emptyCartMessage);
		            $("#hideForemptyCart").hide();
		            $(".couponTotalBox").hide();
					$("#acceptedPayments1").hide();
		            $(".editInfoLink").hide();
		            $("#Norton_Cart").fadeOut();
		            $('#euVatToC').hide();
		            $('#mini-Cart').hide();
		            $(".productInfoRemove").click(function() {
		            	$("#Norton_Cart").fadeToggle('slow');
            		});
            		refreshTemplates(data);
         		}
	         	// This code populates the cart-overlay with coupon code and
	         	// the amound after the coupon is applied Starts
				if (typeof data.cartCoupons !== 'undefined') {
					if(typeof data.cartCoupons[0] !== 'undefined'){
						discountCouponVal= data.cartCoupons[0];
						discountAmtVal= data.discountAmount;
						$(".discountRow").show();
						$(".couponRow").hide();
						$("#overlayDiscount").text(discountCouponVal);
						$("#overlayDiscountAmount").text(discountAmtVal);
						$("#overlayDiscountAmount").prepend("-");
					}
				}     
				// This code populates the cart-overlay with coupon code and
	         	// the amount after the coupon is applied Ends

          /* Code for RC Flow and SSO login Starts */
          if (window.returningcustomer == true || window.userCheck == true){						
                    showHideOnLogin(window.email);
	               if(window.bmsflow || window.bakflow || window.integratedFlow){
						$('#logInInfoLink').hide();
						$('#newCartLogOut').hide(); //OPS-87760 cart development logOut functionality						
					}
				
                    if(typeof data.walletInfo !== 'undefined' && data.walletInfo !== null ){
                        walletinfo = window.walletinformation;
                        console.log(walletinfo);     
						walletCardType = window.walletinformation['cardType'];
						console.log("cardType"+walletCardType)
						walletPaymentType = window.walletinformation['paymentType'];
						console.log("paymentType in wallet"+walletPaymentType);
						//wallet Country
						walletCountryCode = window.walletinformation['country'];
						// OPS-101733 starts
						if(typeof walletCountryCode === 'undefined'){
							walletCountryCode = cty;
						}
						// OPS-101733 ends
						//OPS-101518 starts
						var georestrictedProdList;
						georestrictedProdList = checkProductGeoLocation(window.productFamilyList, walletCountryCode);
						if(georestrictedProdList.length > 0){
							$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
							$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').text(htmlDecode(georestrictedProdList.join(', '))+' '+$('#geoLocationMsg').text());		
							$('#countriesDropDown').addClass("redBorder");
							window.geoErrorMsg = "true";
						}else{
							$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');	
							$('#countriesDropDown').removeClass("redBorder");
							window.geoErrorMsg = "false";		
						}
						//OPS-101518 ends
						console.log("Wallet Country"+walletCountryCode);
						
						// Code for creating banklist dropdown in case of Netherlands and IDeal
						// OPS-85079
						if (data.walletInfo){
						 	var billingCountry = $("#countriesDropDown option:selected").val();
						 	if (data.walletInfo.cardType == "IDEAL" && billingCountry=="NL"){	
						 		$("#paymentMethod option[name='iDeal']").attr("selected","selected")
								createBankDropdown();
								$("#bankDropdownList option").filter(function() {
								    return this.text == data.walletInfo.bankName; 
								}).attr('selected', true);
						 	}
						} //NetBanking changes				
					
						// Code for default selection of payment method dropdown on page refresh(NetBanking)
						//ops-75656
						 if (data.walletInfo){
						 	var billingCountry = $("#countriesDropDown option:selected").val();
						 	if (data.walletInfo.cardType == "NETBANKING" && billingCountry=="IN"){	
						 		$("#paymentMethod option[name='NETBANKING']").attr("selected","selected")
								createBankDropdown();
								$("#bankDropdownList option").filter(function() {

								    return this.text == data.walletInfo.bankName; 
								}).attr('selected', true);
						 	}
						} //NetBanking changes

						  //worldPay Changes OPS-73496
			              if (data.walletInfo){
			                if (data.walletInfo.paymentType === 'IB' || (data.walletInfo.paymentType === 'HP' && data.walletInfo.cardType !== 'OXXO')){			                	
			                  $('#paymentMethod').val(data.walletInfo.paymentType + '_' + data.walletInfo.cardType);
			                }
			                else {
			                  $('#paymentMethod').val(data.walletInfo.paymentType);
			                } 
			              }
						if((walletCardType == 'PayPal' || walletPaymentType == 'DW') && logOut_flag == false){								
							$('#paypalImg').show();
							$('#pay-pal').show();
							$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
							$('#credit-card').hide();								
							$('#cardNumberDiv').hide();
							$('#expiryDateDiv').hide();
							$('#securityCodeDiv').hide();								
							if((alpaGoCountry !== -1 && typeof alpaGoCountry !== 'undefined') || (alpaGobillingCountry !== -1 && typeof alpaGobillingCountry !== 'undefined')) {
								$('#cpfcnpjDiv').show();
								$('#addressDiv').show();
							}
							
							optionalPhone(); //show hide phone number 
							checkPayPalMessage(); //Paypal Text in case of in-context flow																														
						}
						/* Boleto Changes OPS-63895 */
						// Code for default selection of payment method dropdown on page refresh
						else if((walletPaymentType == "BT") && logOut_flag == false){
							console.log("Bank Transfer check");

							if((alpaGoCountry !== -1 && typeof alpaGoCountry !== 'undefined') || (alpaGobillingCountry !== -1 && typeof alpaGobillingCountry !== 'undefined')) {																		
								$('#boletoImg').show();								
								$('#boletoName').show();
								$('#alpagoText').show();
								$('#BOLETO, #boleto').show();
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
								$('#credit-card').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();															
								$('#cpfcnpjDiv').show();
								$('#addressDiv').show();
								optionalPhone(); //show hide phone number 
								checkPayPalMessage(); //Paypal Text in case of in-context flow																							
							}
							
							/* Offline payment method Changes  */
							// Code for default selection of payment method dropdown on page refresh
							else {
								console.log("Inside BT refresh");
								//$('#paymentMethod').prop('selectedIndex', 1);
								$("#offlinePayment").show();
								$('#ofbtName').show();
								$("#zipCodeDiv").hide();
								$("#cityDiv").hide();
								$("#stateDiv").hide();	
								$('div.errorDiv').addClass('hidden');
								$('input').removeClass('redBorder');
								$('#paypalImg').hide();
								$('#pay-pal').hide();
								$('#credit-card').hide();			
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();
								$('#boletoImg').hide();
								$('#boletoName').hide();
								$('#cpfcnpjDiv').hide();
								$('#addressDiv').hide();
								$('#alpagoText').hide();
								$('#dineromailText').hide();
								$('#BOLETO, #boleto').hide();
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
								$(".country").removeClass("redBorder");							
								optionalPhone(); //show hide phone number 
								checkPayPalMessage(); //Paypal Text in case of in-context flow
							}
						}
						else if(walletPaymentType == 'DD' && logOut_flag == false) { //need to update for walletCardType
						
								//$('#paymentMethod').prop('selectedIndex', 2);
								$('#directDebit').show();
								$('#ddName').show();
								$('#ibanDiv').show();
								$('#bankCodeDiv').show();								
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
								$('#directdebit, #DIRECTDEBIT').show();			
								$('#credit-card').hide();			
								$('#addressDiv').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();
								/*OPS-75720 OFBT */
								$('#oxxoImg').hide();
								$('#paypalImg').hide();
								$('#boletoImg').hide();
								$("#offlinePayment").hide();
								$('#ofbtName').hide();
								optionalPhone(); //show hide phone number 
								checkPayPalMessage(); //Paypal Text in case of in-context flow					
						}
							
							/*Oxxo Changes OPS-73492 */
							// Code for default selection of payment method dropdown on page refresh
							else if((walletCardType == 'OXXO' && walletPaymentType == 'HP') && logOut_flag == false){								
								$('#oxxoImg').show();								
								$('#oxxoName').show();
								$('#dineromailText').show();								
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
								$('#credit-card').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();								
								optionalPhone(); //show hide phone number 
								checkPayPalMessage(); //Paypal Text in case of in-context flow
							}
							/* Online Bank Transfer*/
							// Code for default selection of payment method dropdown on page refresh
							else if((walletCardType == 'SOFO' && walletPaymentType == 'IB') && logOut_flag == false){
								//$('#paymentMethod').prop('selectedIndex', 4);
								$('div.errorDiv').addClass('hidden');
								$('input').removeClass('redBorder');
								$('#iBSofo').show();
								$('div.errorDiv').addClass('hidden');
								$('input').removeClass('redBorder');
								$('#iBSofo').show();
								$('#sofoName').show();
								$('#iBGiropay').hide();
								$('#giropayName').hide(); 
								$('#credit-card').hide();			
								$('#addressDiv').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();			
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());								
								$('#paypalImg').hide();
								$('#pay-pal').hide();
								$('#boletoImg').hide();
								$("#offlinePayment").hide();
								/*OPS-75720 OFBT */
								$("#offlinePayment").hide();
								$('#ofbtName').hide();
								$('#oxxoImg').hide();
								$('#oxxoName').hide();
								/* Direct Debit */
								$('#directDebit').hide();
								$('#ddName').hide();
								$('#ibanDiv').hide();
								$('#bankCodeDiv').hide(); 
								/*Online Bank Transfer */    
								$('#iBGiropay').hide();
								$('#giropayName').hide();           
								$('#iBIdeal').hide();
								$('#idealName').hide();
								$('#iBNordea').hide();
								$('#nordeaName').hide();
								optionalPhone(); //show hide phone number
								checkPayPalMessage(); //Paypal Text in case of in-context flow 
							}
							else if((walletCardType == 'GIROPAY' && walletPaymentType == 'IB') && logOut_flag == false){								
								$('div.errorDiv').addClass('hidden');
								$('input').removeClass('redBorder');
								$('#iBGiropay').show();
								$('#giropayName').show();
								$('#iBSofo').hide();
								$('#sofoName').hide();
								$('#credit-card').hide();			
								$('#addressDiv').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();			
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());							
								$('#paypalImg').hide();
								$('#pay-pal').hide();
								$('#boletoImg').hide();
								$("#offlinePayment").hide();
								/*OPS-75720 OFBT */
								$("#offlinePayment").hide();
								$('#ofbtName').hide();
								$('#oxxoImg').hide();
								$('#oxxoName').hide();
								/* Direct Debit */
								$('#directDebit').hide();
								$('#ddName').hide();
								$('#ibanDiv').hide();
								$('#bankCodeDiv').hide();
								/*Online Bank Transfer */                
								$('#iBSofo').hide();
								$('#sofoName').hide();
								$('#iBIdeal').hide();
								$('#idealName').hide();
								$('#iBNordea').hide();
								$('#nordeaName').hide();
								optionalPhone(); //show hide phone number 
								checkPayPalMessage(); //Paypal Text in case of in-context flow 								
							}
							else if((walletCardType == 'NORDEA' && walletPaymentType == 'IB') && logOut_flag == false){								
								$('div.errorDiv').addClass('hidden');
								$('input').removeClass('redBorder');
								$('#iBNordea').show();
								$('#nordeaName').show();               
								$('#credit-card').hide();			
								$('#addressDiv').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();			
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());							
								$('#paypalImg').hide();
								$('#pay-pal').hide();
								$('#boletoImg').hide();
								$("#offlinePayment").hide();
								/*OPS-75720 OFBT */
								$("#offlinePayment").hide();
								$('#ofbtName').hide();
								$('#oxxoImg').hide();
								$('#oxxoName').hide();
								/* Direct Debit */
								$('#directDebit').hide();
								$('#ddName').hide();
								$('#ibanDiv').hide();
								$('#bankCodeDiv').hide();
								/*Online Bank Transfer */    
								$('#iBGiropay').hide();
								$('#giropayName').hide();
								$('#iBSofo').hide();
								$('#sofoName').hide();
								$('#iBIdeal').hide();
								$('#idealName').hide();
								optionalPhone(); //show hide phone number 
								checkPayPalMessage(); //Paypal Text in case of in-context flow																
							}
							else if((walletCardType == 'IDEAL' && walletPaymentType == 'IB') && logOut_flag == false){								
								$('div.errorDiv').addClass('hidden');
								$('input').removeClass('redBorder');
								$('#iBIdeal').show();
								$('#idealName').show();                
								$('#credit-card').hide();			
								$('#addressDiv').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();			
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());								
								$('#paypalImg').hide();
								$('#pay-pal').hide();
								$('#boletoImg').hide();
								$("#offlinePayment").hide();
								/*OPS-75720 OFBT */
								$("#offlinePayment").hide();
								$('#ofbtName').hide();
								$('#oxxoImg').hide();
								$('#oxxoName').hide();
								/* Direct Debit */
								$('#directDebit').hide();
								$('#ddName').hide();
								$('#ibanDiv').hide();
								$('#bankCodeDiv').hide();                  
								/*Online Bank Transfer */    
								$('#iBGiropay').hide();
								$('#giropayName').hide();
								$('#iBSofo').hide();
								$('#sofoName').hide();           
								$('#iBNordea').hide();
								$('#nordeaName').hide();
								optionalPhone(); //show hide phone number 
								checkPayPalMessage(); //Paypal Text in case of in-context flow								
							}
							//Worldpay changes OPS-73496
							else if((walletCardType == 'WEBMONEY-SSL' && walletPaymentType == 'HP') && logOut_flag == false){							
						    		$('div.errorDiv').addClass('hidden');
									$('input').removeClass('redBorder');
									$('#webmoney').show();
						            $('#pay-pal').show();
									$('#cityDiv').show();
									$('#zipCodeDiv').show();											
									$('#paypalImg').hide();	                       
									$('#credit-card').hide();			
									$('#addressDiv').hide();
									$('#cardNumberDiv').hide();
									$('#expiryDateDiv').hide();
									$('#securityCodeDiv').hide();			
									$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
									$('#qiwi').hide();
									$('#yandex').hide();
									$('#PR24').hide();
									$("#offlinePayment").hide();
									$('#ofbtName').hide();												
						            $("#agreePaymentInfoPrePopulated").hide();
						            optionalPhone(); //show hide option phone number text
						            checkPayPalMessage(); //Paypal Text in case of in-context flow															
							}

							else if((walletCardType == 'YANDEXMONEY-SSL' && walletPaymentType == 'HP') && logOut_flag == false){								
								$('div.errorDiv').addClass('hidden');
								$('input').removeClass('redBorder');
								$('#yandex').show();
					            $('#pay-pal').show();
								$('#cityDiv').show();
								$('#zipCodeDiv').show();
								$('#paypalImg').hide();		                         
								$('#credit-card').hide();			
								$('#addressDiv').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();			
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
								$('#webmoney').hide();
								$('#qiwi').hide();
								$('#PR24').hide();
								$("#offlinePayment").hide();
								$('#ofbtName').hide();									
					            $("#agreePaymentInfoPrePopulated").hide();
					            optionalPhone(); //show hide option phone number text
					            checkPayPalMessage(); //Paypal Text in case of in-context flow				
							}

							else if((walletCardType == 'QIWI-SSL' && walletPaymentType == 'HP') && logOut_flag == false){								
								$('div.errorDiv').addClass('hidden');
								$('input').removeClass('redBorder');
								$('#qiwi').show();
					            $('#pay-pal').show();
								$('#cityDiv').show();
								$('#zipCodeDiv').show();
								$('#paypalImg').hide();			                         
								$('#credit-card').hide();			
								$('#addressDiv').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();			
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
								$('#webmoney').hide();
								$('#yandex').hide();
								$('#PR24').hide();
								$("#offlinePayment").hide();
								$('#ofbtName').hide();												
					            $("#agreePaymentInfoPrePopulated").hide();
					            optionalPhone(); //show hide option phone number text
					            checkPayPalMessage(); //Paypal Text in case of in-context flow				
							}

							else if((walletCardType == 'PRZELEWY-SSL' && walletPaymentType == 'HP') && logOut_flag == false){								
								$('div.errorDiv').addClass('hidden');
								$('input').removeClass('redBorder');
								$('#PR24').show();
					            $('#pay-pal').show();								
								$('#zipCodeDiv').show();
								$('#paypalImg').hide();			                         
								$('#credit-card').hide();			
								$('#addressDiv').hide();
								$('#cardNumberDiv').hide();
								$('#expiryDateDiv').hide();
								$('#securityCodeDiv').hide();			
								$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
								$('#webmoney').hide();
								$('#yandex').hide();
								$('#qiwi').hide();
								$("#offlinePayment").hide();
								$('#ofbtName').hide();												
					            $("#agreePaymentInfoPrePopulated").hide();
					            optionalPhone(); //show hide option phone number text
					            checkPayPalMessage(); //Paypal Text in case of in-context flow							
							}															
							/* To change Placeholder for amex card on page refresh*/ 
							else if(typeof data.walletInfo !== 'undefined' && data.walletInfo.cardType == "AMEX"){
									$('#cvv').attr('placeholder',$('#amex-ph').text());	
							}																
							else if(walletPaymentType == 'CC' && logOut_flag == false){								
								console.log("cc selected");
								/*PayPal Hide in case of Credit Card Selection */
								$('#paypalImg').hide();
								$('#pay-pal').hide();
								/*Boleto Hide in case of Credit Card Selection */
								$('#boletoImg').hide();								
								$('#boletoName').hide();
								/*oxxo Hide in case of Credit Card Selection */
								$('#oxxoImg').hide();								
								$('#oxxoName').hide();
								/*BT Hide in case of Credit Card Selection */
								$("#offlinePayment").hide();
								$('#ofbtName').hide();
								$('#credit-card').show();
								$('#cardNumberDiv').show();
								$('#expiryDateDiv').show();
								$('#securityCodeDiv').show();
								$('#cardHolderName').attr('placeholder',$('#credit-card-ph').text());							
								 /*Online Bank Transfer */    
								$('#iBGiropay').hide();
								$('#giropayName').hide();
								$('#iBSofo').hide();
								$('#sofoName').hide();
								$('#iBIdeal').hide();
								$('#idealName').hide();
								$('#iBNordea').hide();
								$('#nordeaName').hide();
								//Worldpay changes OPS-73496
								$('#webmoney').hide();
								$('#qiwi').hide();
								$('#PR24').hide();
								$('#yandex').hide();
								optionalPhone(); //show hide phone number 
								checkPayPalMessage(); //Paypal Text in case of in-context flow									              				
								if((alpaGoCountry !== -1 && typeof alpaGoCountry !== 'undefined') || (alpaGobillingCountry !== -1 && typeof alpaGobillingCountry !== 'undefined')) {
									$('#cpfcnpjDiv').show();
									$('#addressDiv').show();
								}									
							}									
								$('#cardHolderName').val(walletinfo['cardHolderName']);
								$('#cardNumber').val(walletinfo['maskedCardNumber']);
								var CardType = walletinfo['cardType'];
								parseData.displayCardImage(CardType);
								var Month = walletinfo['expDateMonth'];
								var Year = walletinfo['expDateYear'];
								var Country = walletinfo['country'];
								var State = walletinfo['state'];
								console.log("State -- " + State);
								$('#expMonthDropDown option:selected').attr("selected",null);
								$('#expMonthDropDown option[value='+Month+']').attr("selected","selected");    
								$('#expYearDropDown option:selected').attr("selected",null);
								$('#expYearDropDown option[value='+Year+']').attr("selected","selected");
								$('#city').val(walletinfo['city']);
								/*Boleto Changes OPS-63895*/
	    						$('#cpfcnpj').val(walletinfo['cpfCnpj']);
	    						$('#address').val(walletinfo['address']);
								$('#zipCode').val(walletinfo['zipCode']);
								//$('#countriesDropDown option:selected').attr("selected",null);
								//$('#countriesDropDown option[value='+Country+']').attr("selected","selected");
								$('#stateDropDown option:selected').attr("selected",null);
								$('#stateDropDown option[value='+State+']').attr("selected","selected");
								$('#cvv').addClass('cvvRc');							
								$('#showCvvHelp').css("display", "none");
								$('#enterCVV').css("display", "block");
								//OPS-80628 changes for update cvv style
								// Custom Error Message change based on acquirer codes OPS-39025, OPS-31067, OPS-28005
								if((typeof window.enableCustomErrorMessage !== 'undefined' && window.enableCustomErrorMessage == true) && (typeof window.tcFail !== 'undefined' && window.tcFail == true)){	
												var errorMessageVal= window.customPaymentError.errorMessage;
												if(typeof window.customPaymentError !== 'undefined' && errorMessageVal!= ""){
															$('#custom_error').css("display","block");
															$('#custom_error').addClass("errorDiv");	
															$('#custom_error').text(errorMessageVal)
												}											
								}else if((typeof window.customPaymentError == 'undefined') && (typeof window.tcFail !== 'undefined' && window.tcFail == true)){
										if(data.supportedPymt.length !== 'undefined' && data.supportedPymt.length == 1){							 
											 $('#tcfail_error').css("display","block");
											 $('#tcfail_error').addClass("errorDiv");							 			
										}
										if(data.supportedPymt.length !== 'undefined' && data.supportedPymt.length > 1){						
										    $('#paypal_error').css("display","block");
										    $('#paypal_error').addClass("errorDiv");						    	
										}
										$('#cvv').removeClass('cvvRc');							
										$('#showCvvHelp').css("display", "block");
										$('#enterCVV').css("display", "none");
								}

								if((typeof window.enableCustomErrorMessage !== 'undefined' && window.enableCustomErrorMessage == true) && (typeof window.payPalError !== 'undefined' && window.payPalError == true)){	
												var errorMessageVal= window.customPaymentError.errorMessage;
												if(typeof window.customPaymentError !== 'undefined' && errorMessageVal!= ""){
															$('#custom_error').css("display","block");
															$('#custom_error').addClass("errorDiv");	
															$('#custom_error').text(errorMessageVal)
								}
								}else if((typeof window.customPaymentError == 'undefined') && (typeof window.payPalError !== 'undefined' && window.payPalError == true)){									
									$('#paypal_error').css("display","block");
									$('#paypal_error').addClass("errorDiv");
								}					
								/*Phone Number Changes for OXXO */
							    $('.phone').val(walletinfo['phoneNumber']);
								$('.nsbPhoneNumber').val(walletinfo['phoneNumber']);								
								/* VAT Code, IBAN and BIC */
								$('.vatCode').val(walletinfo['vatID']);
								$('.iban').val(walletinfo['maskedAccountNumber']);
								$('.bankCode').val(walletinfo['bankCode']);							

                    }//Code for RC Flow and SSO login Starts			
                    if (typeof updateOmnitureEvents === "function") { 
                        var data = "none"
                        var omniskuCode="none";                     
                        var pagetype="none"; 
                        var action = "returningCustomer";
                        updateOmnitureEvents(data,action,pagetype,omniskuCode);			
             		}
                    
                }
                else {
                    $("#LoginInfo").show();
                    	// Rover Product Feature Check 
	     				if(ROVER_FEATURE_FLAG && window.cartContainsPhysicalProduct){
	  						$("#shippingInformation").show();
						}else{
							$("#shippingInformation").hide();
						}	
							$("#shippingSec").hide();
							$("#BillingSec").hide();
                    		$("#reviewSec").hide();
                    }
              /* Code for RC Flow and SSO login Ends */ 
            } //else end 
		getSupportedImage(); // getSupportedImage

		// *** Code for ITP starts *** 
		if(window.returningcustomer == true && typeof window.cartItems !== "undefined"){
	        for(var i=0; i<window.cartItems.length;i++){
	        	if(window.cartItems[i].hasOwnProperty("noDisplayITPInCartFlag")){
		            if(window.cartItems[i].noDisplayITPInCartFlag && window.cartItems[i].noDisplayITPInCartFlag != ""){
		                var skuCode = window.cartItems[i].skuCode;
		                $('#parentPrice'+skuCode).parent().parent().hide();
		                $('#parentPrice'+skuCode).parent().parent().prev('a').hide();		                
		            }
		        }
	        }
		}
		// *** Code for ITP ends  ***

    // Disabling zoom for iPhone & iPods. Problem when user clicks on <input> or <select>, and device automatically zooms. 
    if (/iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
      console.log('Disabling zoom.');
      $('meta[name="viewport"]').attr('content', 'width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0');
    }


    $('#cardNumber').mask('AAAAAAAAAAAAAAAAAAAA', 
      { 
        onKeyPress: function(input, e, field, options){

          var mask; 

          if (/^x{11}\d{4}$/.test(input)){
            mask = 'xxxxxxxxxxx0000';
          }
          else if (/^x{12}\d{4}$/.test(input)){
            mask = 'xxxxxxxxxxxx0000';
          }
          else if (wa.ecom.cardType(input).name === 'American Express'){
            mask = '0000 000000 00000#';
          }
          else {
            mask = '0000 0000 0000 0000#';
          }
            
          $('#cardNumber').mask(mask, options);
        } // end onKeyPress
      });

    			console.log('outside', data.enablePayPalContextFlow, data.paypalMerchantID, data.paypalEnvironment);
	            if (window.enablePayPalContextFlow && window.paypalMerchantID && window.paypalEnvironment && PAYPAL_IN_CONTEXT_FEATURE_FLAG){ // start Paypal in-context checkout flow
		            // function needed for Paypal in-context checkout
		            window.paypalCheckoutReady = function() {
		            	console.log('inside', data.enablePayPalContextFlow, data.paypalMerchantID, data.paypalEnvironment);
		            	// Some scoping issues. Using global to get initialization parameters for now. TODO: refactor to not use global.
				    	paypal.checkout.setup(window.paypalMerchantID, {
					        environment: window.paypalEnvironment,
					        click: function(event) {

					        	// reset paypal error meessage
					        	$('#paypal_error').css("display","none");
								$('#paypal_error').removeClass("errorDiv");
								$('#paypal_error').removeClass('hidden');

					        	// Check if no errors in form
					        	var bpVo = getApplyTaxRequestData(event);
				                if (!bpVo){
				                	return;
				                }
				             
					        	if ($('#paymentMethod').val() === 'DW' && typeof window.paypalCountryList === 'string' &&
					        		window.paypalCountryList.toLowerCase().indexOf($("#countriesDropDown option:selected").val().toLowerCase()) >= 0 && 
					        		window.paypalCountryList.toLowerCase().indexOf(window.countryCodeId.toLowerCase()) >= 0){ // if paypal express checkout flow

					        		event.preventDefault();

					            	paypal.checkout.initXO();
					            	$.support.cors = true;
					            	displaySpinner();
						            $.ajax({
						                type: "POST", 
						                url: '/estore/getShoppingCart/action/initPaypal',
						                data: JSON.stringify(bpVo),
						                contentType: "application/json; charset=UTF-8",
						                dataType: "json",
					                	mimeType: 'application/json',
					                	timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
						                //Load the minibrowser with the redirection url in the success handler
						                success: function (data) {
						                	if (!data.errorForPaypal){
							                	var token = data.paypalToken;
							                    var url = paypal.checkout.urlPrefix + token;
							                    //Loading Mini browser with redirect url, true for async AJAX calls
							                    paypal.checkout.startFlow(url);
						                	}
						                	else {
						                		//Gracefully Close the minibrowser in case of AJAX errors
						                		$('#paypal_error').css("display","block");
												$('#paypal_error').addClass("errorDiv");
												$('#paypal_error').removeClass('hidden');
						                    	paypal.checkout.closeFlow();
						                	}
						                },
						                error: function (responseData, textStatus, errorThrown) {
						                    //Gracefully Close the minibrowser in case of AJAX errors
						                    $('#paypal_error').css("display","block");
											$('#paypal_error').addClass("errorDiv");
											$('#paypal_error').removeClass('hidden');
						                    paypal.checkout.closeFlow();
						                },
										// This code is for handling the session expiry condition
								        statusCode: {
							                200: function(xhr){
							                    if(typeof xhr.responseText != "undefined"){
							                        if(xhr.responseText.search("mfsessionExpired") == -1){
							                            console.log("Coupon Session not expired"+xhr.responseText);
							                        }
							                        else{
							                        	// reset paypal error meessage
											        	$('#paypal_error').css("display","none");
														$('#paypal_error').removeClass("errorDiv");
														$('#paypal_error').removeClass('hidden');
							                        	paypal.checkout.closeFlow();
							                            window.location.href = '/estore/mf/sessionExpired';
							                            console.log("Coupon Session has expired"+xhr.responseText);
							                        }
							                    }

							                }
								        }
						            });
					        	}
					        	else{ // else default flow
					        		applyTax(bpVo);
					        	}
					    
					            
					        },
					        container: ['billingInfoNextButton'] // put id of where to attach click event listener
					    });
					};

					// Load Paypal script after creating paypalCheckoutReady function
					$.getScript( "//www.paypalobjects.com/api/checkout.js", function( data, textStatus, jqxhr ) {
					})

					// Event listener for url changes caused by Paypal
					$(window).on('hashchange', function(event) {
					  if (/success/.test(location.hash)){
					  	var bpVo = getApplyTaxRequestData(event);
					    if (!bpVo){
					    	return;
					    }
					    bpVo.payerAuthId = location.hash.match(/PayerID=(.*)/)[1]; // assume payerid will come from paypal
					    applyTax(bpVo);
					    /* If the user wants to go to back to billing to edit payment after checking out with paypal,
					 	 * we need to refresh, assuming the hash used for success calls.
					 	 */
					 	$('#billingInfoLink').unbind('click');
					    $('#billingInfoLink').click(function(event){
					    	window.location = '/estore/checkOut';
					    });
					  }
					});

					// auto applyTax call
					if(/paypal/.test(document.referrer) && /success/.test(location.hash) && /PayerID=(.*)/.test(location.hash)){
						$('#paymentMethod').val('DW');
						paymentModeSelected();
						var bpVo = getApplyTaxRequestData(event);
					    if (!bpVo){
					    	return;
					    }
					    bpVo.payerAuthId = location.hash.match(/PayerID=(.*)/)[1]; // assume payerid will come from paypal
						applyTax(bpVo);
					}

				} // end Paypal in-context checkout flow
				else{ // else apply default listener
					$('#billingInfoNextButton').click(function(event){
						// Check if no errors in form
			        	var bpVo = getApplyTaxRequestData(event);
		                if (!bpVo){
		                	return;
		                }
		                applyTax(bpVo);
					});
				}

        }, // getcart ajax end

 		error: genericAjaxErrorHandler,
		// This code is for handling the session expiry condition
        statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("Session has expired"+xhr.responseText);
                        }
                    }

                }
        }		

    }); //ajax end
	
    });

/* Exp4 Country List Change OPS-76393*/
	function checkExp4(){
		console.log("Inside Exp4");

					var selectedCountry = "";
                    var selectedPaymentMethod = "";   

					if(typeof window.walletinformation !== 'undefined' && window.walletinformation !== null){
						    	console.log("selectedCountry"+ selectedCountry);
						    	if(window.isSelectedCount){
						    		selectedCountry = $("#countriesDropDown").val();
                                    selectedPaymentMethod = $('#paymentMethod').val();                                     
						    		window.isSelectedCount = false;
						    	}else{
						    		
						    		selectedCountry = $("#countriesDropDown").val();
                                    selectedPaymentMethod = $('#paymentMethod').val();                                      
						    	}
						    	
						    }else{
						    	selectedCountry = $("#countriesDropDown").val();
                                selectedPaymentMethod = $('#paymentMethod').val();                                
						    }						    					
					var exp4CountriesList = window.exp4CountriesList.indexOf(selectedCountry);
					var incomingexp4CountriesList = window.exp4CountriesList.indexOf(countryCodeId);
					var billingCountry = $("#countriesDropDown option:selected").val();
					var billingexp4CountriesList = 	window.exp4CountriesList.indexOf(billingCountry);			
					if(typeof exp4CountriesList !== 'undefined' && exp4CountriesList !== -1){
						if((typeof incomingexp4CountriesList !== 'undefined' && incomingexp4CountriesList !== -1)|| (typeof billingexp4CountriesList !== 'undefined' && billingexp4CountriesList !== -1)){
							$('#nonEmeaEula').hide();
							$("#confirmSubmit").hide();
							$('#nonEmeaReview').hide();
	                        if (selectedPaymentMethod == 'CC' || selectedPaymentMethod == 'DW' || selectedPaymentMethod == 'DD' ||
                                selectedPaymentMethod == 'IB_GIROPAY' || selectedPaymentMethod == 'IB_SOFO'  || selectedPaymentMethod == 'IB_IDEAL' ){
	                            $("#agreePaymentInfoPrePopulated").show();
	                        }						
							$('#emeaEula').show();
							$('#emeaReview').show();
							$('#privacyCompliance').show();								
							if($("#agreeEmeaNonArEula").attr("checked")){		    
				  				$("#confirmSubmit").show();
				  				$("#confirmEmeaSubmit").hide();		    			
				  			}else {	  				  	
				  				$("#confirmEmeaSubmit").show();
				  				$("#confirmSubmit").hide();
				  			}													        	
				        	$("#refundPolicy").show();

				        	var euCountriesList = window.eucountriesList.indexOf(selectedCountry);
				        	var incomingeuCountriesList = window.eucountriesList.indexOf(countryCodeId);
				        	var billingeuCountriesList = window.eucountriesList.indexOf(billingCountry);
				        	var incomingalpagoCountriesList	=window.alpaGoCountriesListVal.indexOf(countryCodeId);
				        	var billingalpagoCountriesList	=window.alpaGoCountriesListVal.indexOf(billingCountry);      	
						        if(typeof euCountriesList !== 'undefined' && euCountriesList !== -1) {	
						        	if((typeof incomingeuCountriesList !== 'undefined' && incomingeuCountriesList !== -1) || (typeof billingeuCountriesList !== 'undefined' && billingeuCountriesList !== -1)){
							        	console.log("Inside  eu countries");																									
										$('#stateDiv').hide();
										$('#zipCodeDiv').hide();						
										$('#vatDiv').show();														
										$('#euVatToC').show();
										$('#vatCart').show();
										$('#vatReview').show();
									}//eu incoming country and billing country check ends																		
								} //eu countries list check ends
								else if (selectedPaymentMethod == 'HP_QIWI-SSL' || selectedPaymentMethod == 'HP_WEBMONEY-SSL' || selectedPaymentMethod == 'HP_YANDEXMONEY-SSL' || selectedPaymentMethod == 'HP_PRZELEWY-SSL'){
										$('#zipCodeDiv').show();
								}
								else if((typeof incomingalpagoCountriesList !== 'undefined' && incomingalpagoCountriesList !== -1) || 
									(typeof billingalpagoCountriesList !== 'undefined' && billingalpagoCountriesList !== -1)){
										$('#vatDiv').hide();
										$('#euVatToC').hide();							
										$('#zipCodeDiv').show();
										$('#cityDiv').show();
										$('#stateDiv').show();	
								}//alpago country check
								else{		
								console.log("Inside non eu countries");
										$('#vatDiv').hide();			
										$('#euVatToC').hide();							
										$('#zipCodeDiv').hide();
										$('#cityDiv').hide();
										$('#stateDiv').hide();
										$('#vatCart').hide();
										$('#vatReview').hide();	
								}
								showHideMandatoryCity(); //WorldPay changes OPS-73496		
					} //exp4 incoming country and billing country check ends
				}//exp4 if ends
					else{
						$("#agreePaymentInfoPrePopulated").hide();
						$('#emeaEula').hide();						
						$("#refundPolicy").hide();						
			      		$("#confirmEmeaSubmit").hide();
			      		$('#emeaReview').hide();
			      		$('#nonEmeaEula').show();
			      		$("#confirmSubmit").show();
			      		$('#nonEmeaReview').show();	
						$('#vatCart').hide();
						$('#vatReview').hide();
						$('#euVatToC').hide();
						$('#vatDiv').hide();
						$('#privacyCompliance').hide();						
						console.log('outside exp4 line 4380');				
					}     					
		}	


//OPS-65465
		function checkLAMC(){
			    var selectedCountry = "";
				var selectedPaymentMethod = $('#paymentMethod').val();
			    if(typeof window.walletinformation !== 'undefined' && window.walletinformation !== null){
			    	console.log("selectedCountry"+ selectedCountry);
			    	if(window.isSelectedCount){
			    		selectedCountry = $("#countriesDropDown").val();
			    		window.isSelectedCount = false;
			    	}else{
			    		/* Added to check state dropdown should not show in case of refresh if billing country is BR,MX and AR */
			    		selectedCountry = window.walletinformation['country'];	
			    		if(selectedCountry == "BR" || selectedCountry == "MX" || selectedCountry == "AR"){
			    			console.log("omnitureContent "+ window.omnitureCountry+" "+selectedCountry);
			    			if(window.omnitureCountry.toUpperCase() !== selectedCountry){
					    		$('#stateDropDown').empty();
				                if (!$('#stateDropDown').hasClass("hidden")) {
				                    $('#stateDropDown').addClass("hidden");
				                }
				                if ($("#optionalState").hasClass("hidden")) {
				                    $("#optionalState").removeClass("hidden");
				                }
				                 $("#stateDiv label").removeClass("labelDropDown");
			              	}
			    		}
			    	}
			    	
			    }else{
			    	selectedCountry = $("#countriesDropDown").val();

			    }
			    if(window.lamcCountriesList.split(',').indexOf(selectedCountry) >= 0){
				  		lamacCount = true;
				  	}else{
				  		lamacCount = false;
				  	}
			    console.log("selectedCountry"+ selectedCountry+" lamc count"+lamacCount +" "+window.lamacCount);

			    if(lamacCount == true && typeof lamacCount !== 'undefined'){
			    	console.log("OPS-65465 --- isLAMC");
			        $("#stateDiv").hide();

			        $("#cityDiv").hide();
			        $("#zipCodeDiv").hide();
			        $("#addressDiv").hide();
			        $("#cpfcnpjDiv").hide();
			        $('#alpagoText').hide();
			        $('#dineromailText').hide();
			        $('#BOLETO, #boleto').hide();
			           if(selectedCountry == "BR"){
			        		console.log("OPS-65465 --- BR " +window.alpaGoCountriesListVal);
			                    $("#addressDiv").show();
			                    $("#cpfcnpjDiv").show();
			                    $("#zipCodeDiv").show();
			                    $("#stateDiv").show();
			        			$("#cityDiv").show();
			        			$('#alpagoText').show();			        			
			        			$('#BOLETO, #boleto').show();
								$('#dineromailText').hide();								
			            }
			           else if(selectedCountry == "MX"){
			        			console.log("OPS-65465 --- MX");
			                    $("#addressDiv").show();
			                    $("#cpfcnpjDiv").hide();
			                    $("#zipCodeDiv").show();
			                    $("#stateDiv").show();
			        			$("#cityDiv").show();
			        			$('#alpagoText').hide();
			        			$('#dineromailText').show();
			        			$('#BOLETO, #boleto').hide();								
			            }
			           else if(selectedCountry == "CA"){
			        			console.log("OPS-65465 --- CA");
			                    $("#addressDiv").hide();
			                    $("#cpfcnpjDiv").hide();
			                    $("#zipCodeDiv").show();
			                    $("#stateDiv").show();
			        			$("#cityDiv").show();
			        			$('#alpagoText').hide();
			        			$('#dineromailText').hide();
			        			$('#BOLETO, #boleto').hide();								
			            }  
			   }
			   else if((lamacCount == false && typeof lamacCount !== 'undefined' && window.apacCountry == false && typeof window.apacCountry !== 'undefined')){	
				 if(typeof offlineCountriesList !=='undefined' && (offlineCountriesList.split(',').indexOf(countryCodeId) == -1 ) && (selectedPaymentMethod !== "BT" )){
			   		$("#stateDiv").show();
			        $("#cityDiv").show();
			        $("#zipCodeDiv").show();
			        $("#addressDiv").hide();
			        $("#cpfcnpjDiv").hide();
			        $('#alpagoText').hide();
			        $('#BOLETO, #boleto').hide();
			   			if(selectedCountry == "BR"){
			        		console.log("OPS-65465 --- BR " +window.alpaGoCountriesListVal);
			                    $("#addressDiv").show();
			                    $("#cpfcnpjDiv").show();
			                    $("#zipCodeDiv").show();
			                    $("#stateDiv").show();
			        			$("#cityDiv").show();
			        			$('#alpagoText').show();			        			
			        			$('#BOLETO, #boleto').show();
								$('#dineromailText').hide();								
			            }
			           else if(selectedCountry == "MX"){
			        			console.log("OPS-65465 --- MX");
			                    $("#addressDiv").show();
			                    $("#cpfcnpjDiv").hide();
			                    $("#zipCodeDiv").show();
			                    $("#stateDiv").show();
			        			$("#cityDiv").show();
			        			$('#alpagoText').hide();
			        			$('#dineromailText').show();
			        			$('#BOLETO, #boleto').hide();								
			            }
			           else if(selectedCountry == "CA"){
			        			console.log("OPS-65465 --- CA");
			                    $("#addressDiv").hide();
			                    $("#cpfcnpjDiv").hide();
			                    $("#zipCodeDiv").show();
			                    $("#stateDiv").show();
			        			$("#cityDiv").show();
			        			$('#alpagoText').hide();
			        			$('#dineromailText').hide();
			        			$('#BOLETO, #boleto').hide();								
			            }  
			   
			  }
		 }
	}


/* OPS-63974 phoneCode dropdopdown */
	function createCallingCodeDropdown(callingCodeSelector, callingCodeValueSelector, countriesDropDownSelector, mobileNumberSelector, phoneNumberSelector){
		callingCodeSelector = callingCodeSelector || '#callingCode';
		callingCodeValueSelector = callingCodeValueSelector || '#callingCodeValue';
		countriesDropDownSelector = countriesDropDownSelector || '#countriesDropDown';
		mobileNumberSelector = mobileNumberSelector || null;
		phoneNumberSelector = phoneNumberSelector || null;

		// Below if condition if a fix for OPS-85825
		if(typeof window.countryStates !== "undefined"){
			$.each(window.countryStates, function(idx, obj){
			if(obj.phoneFormat == ""){obj.phoneFormat = "(000) 000-0000";}
			var callingListItem = $("<option>").text(obj.countryName+ " ("+obj.phoneCode+")").
	      attr("value", obj.countryCode).attr("data-phoneCode", obj.phoneCode).attr("data-countryCode", obj.countryCode);
			$(callingCodeSelector).append(callingListItem);
			});

		  $(callingCodeSelector + " option").filter(function() {
			  if($(this).attr("data-countryCode") == $(countriesDropDownSelector).val()) {
			  	$(callingCodeSelector).val($(this).attr('data-countryCode'));
	        $(callingCodeValueSelector).text($(this).attr('data-phoneCode'));
			  	setPlaceholderForMobNum(mobileNumberSelector, callingCodeSelector);
			  }
			  return;
		  });

		  $(callingCodeSelector).on("change", function(e) 
		    { 
	       $(callingCodeValueSelector).text($(callingCodeSelector + ' option:selected').attr('data-phoneCode'));
		     setPlaceholderForMobNum(mobileNumberSelector, callingCodeSelector);
		     formatMobileText(mobileNumberSelector, phoneNumberSelector);
		    });
		}
	}

	function setPlaceholderForMobNum(mobileNumberSelector, callingCodeSelector){
		mobileNumberSelector = mobileNumberSelector || null;
 		callingCodeSelector = callingCodeSelector || '#callingCode';

		//check #mobilenumber for nsbphonediv/phonediv
	  	var mobileNumInput;

	  	if (mobileNumberSelector){
	  		mobileNumInput = $(mobileNumberSelector);
	  	}
	  	else{
	  		if($("#phoneDiv").is(":visible")){
		  		mobileNumInput = $("#mobileNumber");			
		  	}else{
		  		mobileNumInput = $("#nsbMobileNumber");
		  	}
	  	}

		var selectedCountryCode = $(callingCodeSelector + " option:selected").attr("data-countryCode");		
		$.each(window.countryStates, function(id, ob){
			if(ob.countryCode == selectedCountryCode){
				if(ob.phoneFormat == ""){
					ob.phoneFormat = "(000) 000-0000";
				}
				mobileNumInput.attr("placeholder", "");
				mobileNumInput.attr("placeholder", ob.phoneFormat);
			}
			return;
		});          
	 }

  function formatMobileText(mobileNumberSelector, phoneNumberSelector){
  	mobileNumberSelector = mobileNumberSelector || null;
 	phoneNumberSelector = phoneNumberSelector || null;

  	//check #mobilenumber, #phonenumber for nsbphonediv/phonediv
  	var mobileNumInput;
  	var phoneNumInput;

  	if($("#phoneDiv").is(":visible")){
  		mobileNumInput = $("#mobileNumber");
  		phoneNumInput = $("#phoneNumber");
  	}else{
  		mobileNumInput = $("#nsbMobileNumber");
  		phoneNumInput = $("#nsbPhoneNumber");
  	}

  	if (mobileNumberSelector){
  		mobileNumInput = $(mobileNumberSelector);
  	}
  	if (phoneNumberSelector){
  		phoneNumInput = $(phoneNumberSelector);
  	}

    if(mobileNumInput.val() ==""){return;}

    var finalString = '';
    
    str1 = mobileNumInput.attr("placeholder");
    //str2 = mobileNumInput.val();
    if(phoneNumInput.val() != ""){
      str2 = phoneNumInput.val();
      }else{
   	 	str2 = mobileNumInput.val();
     }
    var lengthMatched = matchMobNumberLength(str1, str2);
    if(!lengthMatched){
    		mobileNumInput.val(str2);	
    	return;
    }else{
    }
    var len = str1.length;
	var digitCounter = 0;
	for(var i=0;i<len;i++){
	 if(IsNumeric(str1[i])){
	  if(str2[digitCounter]){
	  finalString += str2[digitCounter];
	    digitCounter ++;
	}
	 }else{
	     finalString += str1[i];
	 }
	}
	mobileNumInput.val(finalString);
  }

function IsNumeric(input)
{
    return (input - 0) == input && (''+input).trim().length > 0;
}
function matchMobNumberLength(str1, str2){
	var digits = str1.match(/\d+/g).join("");
	if(str2.length != digits.length){
		return false;	
	}else{
		return true;
	}
	
}

/* end */

//OPS-77539
function getSupportedPaymentArr(billingCountry){
	var SupportedPaymentArr=[];
	for (paymentMethodkey in window.methodOfPaymentList) {
        if (window.methodOfPaymentList.hasOwnProperty(paymentMethodkey)) {
			for(subPaymentKey in window.methodOfPaymentList[paymentMethodkey]){
				supportedCountryArr = window.methodOfPaymentList[paymentMethodkey][subPaymentKey].split(",");
				
				if(supportedCountryArr.indexOf(billingCountry) != -1){
					console.log("paymentMethodkey "+paymentMethodkey+"subPaymentKey "+subPaymentKey);
					SupportedPaymentArr.push(subPaymentKey);
				}
			}

        }
    
    }
    return SupportedPaymentArr;
}


function getSupportedImage() {
				//OPS-77359 in getcart for first Load 
				// Displaying error on the country field if the payment method is not
				var billingCountry = $("#countriesDropDown option:selected").val();
				var supportedPaymentArr = getSupportedPaymentArr(billingCountry);

				// Adding the below code to convert all the elements of array to uppercase
				// in order to satisfy the condition for IB case.
				supportedPaymentArr = supportedPaymentArr.map(function(x){ return x.toUpperCase() });

				//Code for hiding and displaying the supported payment method images starts
				$('#acceptedPayments1 div.acceptedPayments1').children('img').each(function() {					
					$(this).hide();
				});

				$('#cartAcceptedPayments div.cartAcceptedPayments').children('img').each(function() {					
					$(this).hide(); //Showing We Accept logos in new cart
				});

				for(var i=0;i<supportedPaymentArr.length;i++){
					$('#acceptedPayments1 div.acceptedPayments1 #'+supportedPaymentArr[i]).show();					
				} // OPS-77359 in getcart for first Load Ends

				for(var i=0;i<supportedPaymentArr.length;i++){					
					$('#cartAcceptedPayments div.cartAcceptedPayments #'+supportedPaymentArr[i]).show();
				} // OPS-77359 in getcart for first Load Ends Showing We Accept logos in new cart			
}

function createPaymentDropdown(){

	var optionObjectArr = [];
	var selectedCountry = $("#countriesDropDown").val(); 
	var SupportedPaymentArr = getSupportedPaymentArr(selectedCountry);
	getSupportedImage();
	$("#paymentMethod").empty();

	var ccFlag=0;
	$('#acceptedPayments1 div.acceptedPayments1').children('img').each(function() {	
		var optionObject = {}
		if($(this).css("display") != "none"){
			optionObject["optionId"] = $(this).attr("id");
			optionObject["optionText"] = $(this).data("text");
			optionObject["optionName"] = $(this).data("name");
			optionObject["optionValue"] = $(this).data("value");
			if(optionObject["optionValue"] == "CC" && ccFlag == 0){
				ccFlag =1;
				optionObjectArr.push(optionObject);
			}
			if(optionObject["optionValue"] == "CC" && ccFlag == 1){
				return true;
			}
			else{
				optionObjectArr.push(optionObject);
			}
		}				
		
	});
	
	console.log("Print final array "+optionObjectArr);
	for(var i=0;i<optionObjectArr.length;i++){
		$("#paymentMethod").append($("<option></option>").attr("data-opid",optionObjectArr[i].optionId).attr("value",optionObjectArr[i].optionValue).attr("name", optionObjectArr[i].optionName).text(optionObjectArr[i].optionText));
	}

	$("#paymentMethod option").each(function(){
			$(this).show();
			if($(this).text() == ""){
				var thisname = $(this).attr("name");
				$(this).text(thisname);
			}
		});
	/* Hiding Payment Dropdown if only one payment method CC is supported */
	if($("#paymentMethod option").length ==1 && $("#paymentMethod option").val() == "CC"){
		$("#paymentDropDown").hide();
		}else{
			$("#paymentDropDown").show();
	}
	checkNetBanking();//Based on NetBanking enabling/disabling bank dropdown
}

function checkPaymentConfirmation(){
	var billingCountry = $("#countriesDropDown option:selected").val();
	var supportedPaymentArr = getSupportedPaymentArr(billingCountry);
	getSupportedImage();
	var selectedPaymentMethodCode = $("#paymentMethod option:selected").data("opid");
	var selectedPaymentMethodText = $("#paymentMethod option:selected").text();
	var selectedPaymentMethodValue = $("#paymentMethod option:selected").val();
	if(supportedPaymentArr.indexOf(selectedPaymentMethodCode) == -1 && selectedPaymentMethodValue != "CC"){		
		createPaymentDropdown();
		paymentModeSelected();
		PaymentErrorMessage(selectedPaymentMethodText, billingCountry)
		}
		
	else{
		createPaymentDropdown();
		$("#paymentMethod option").each(function()
		{
		   if($(this).data("opid") == selectedPaymentMethodCode){$(this).prop("selected", "selected")}
		});
	}
	checkNetBanking();//Based on NetBanking enabling/disabling bank dropdown
}

function checkPaymentMethodError(){	
	var billingCountry = $("#countriesDropDown option:selected").val();
	var supportedPaymentArr = getSupportedPaymentArr(billingCountry);

	var selectedPaymentMethod;
	getSupportedImage(); // getSupportedImage
	// No Error if only selectedPaymentMethodCode = CC 
	if($('#paymentDropDown').css('display') == 'none' || $('#paymentDropDown').is(":visible") == false){
        selectedPaymentMethodCode = $("#paymentMethod option:selected").val();
        if(selectedPaymentMethodCode == "CC"){        	
            return true;
        }
    }
	var cardVal = $('#cardNumber').val();
	if($("#paymentDropDown").css('display') == 'none' || $('#paymentDropDown').is(":visible") == false){
		selectedPaymentMethod = wa.ecom.cardType(cardVal);
		if(selectedPaymentMethod == "American Express"){
			selectedPaymentMethod = 'AMEX';
		}
	}else{
		selectedPaymentMethod = $("#paymentMethod option:selected").attr('name');		
	}	
    var displayCountryName = $("#countriesDropDown option:selected").text();
	var selectedPaymentMethodName = $("#paymentMethod option:selected").text(); 
	if(supportedPaymentArr.indexOf(selectedPaymentMethod.toUpperCase()) == -1){
			console.log("firing payment dropdown error for billing country"+selectedPaymentMethod);
			console.log("firing payment dropdown error for billing country"+displayCountryName);
			console.log("firing payment dropdown error for billing country"+selectedPaymentMethodName);
	    	PaymentErrorMessage(selectedPaymentMethodName, displayCountryName);
	}
}

function PaymentErrorMessage(selectedPaymentMethod, selectedCountry) {        
        $('#selectedPaymentMethod').text(selectedPaymentMethod);
        var displayCountryName = $("#countriesDropDown option:selected").text();
        $('#selectedCountry').text(displayCountryName);
        var errorMsg = $('#paymentErrorMessage').text();
		$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
		$(".country").addClass("redBorder");
		$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);
		$("#paymentMethod option").each(function(){
			$(this).show();
		});
}

/* This function populates the state dropdown on first page load */
function getStatePopulated(ctryCde, cntryStates, stateDropdownSelector, optionalStateSelector, stateDropdownHiddenOptionSelector, stateDivLabelSelector) {
    var stateCodes = [];
    var stateNames = [];
    var options = $(stateDropdownSelector),
        optionalState = $(optionalStateSelector);
    var selectState = $(stateDropdownHiddenOptionSelector).text();
    countryStates = cntryStates;
    if (ctryCde != null || ctryCde != "") {
        /* first loop over country array and look for the passed in ctryCde */
        var foundIdx = -1; // set a flag to reference the number
        for (var countries = 0; countries < countryStates.length; countries++) {
            if (ctryCde == countryStates[countries].countryCode) {
                foundIdx = countries; // set our flag to true
            }
        }
        if (foundIdx == -1) {
            //disabling states dropdown where a country has state as optional field
            if (!options.hasClass("hidden")) {
                options.addClass("hidden");
            }

            //enabling input field text in case of a country has state field optional
            if (optionalState.hasClass("hidden")) {
                optionalState.removeClass("hidden");
            }

            $("#stateDiv label").removeClass("labelDropDown");
        }
        if (foundIdx > -1) {

            //Added condition for countries which don't have state list and have state as optional
            if (countryStates[foundIdx].stateList.length > 0) {

                if (selectState == undefined || selectState == null || selectState == "") {
                    selectState = $("#stateDropdownhidden option").text();
                }
                options.empty();
                options.append($("<option />").val("-1").text(selectState));

                for (var stateCount = 0; stateCount < countryStates[foundIdx].stateList.length; stateCount++) {
                    var stCode = countryStates[foundIdx].stateList[stateCount].stateCode;
                    var stName = countryStates[foundIdx].stateList[stateCount].stateName;

                    stateCodes.push(stCode);
                    stateNames.push(stName);
                    options.append($("<option />").val(stCode).text(stName));
                }

                //enabling state dropdown for states which already have state list
                if (options.hasClass("hidden")) {
                    options.removeClass("hidden");
                }

                if (!optionalState.hasClass("hidden")) {
                    optionalState.addClass("hidden");
                }

                $(stateDivLabelSelector).addClass("labelDropDown");

            } else {
                if (!options.hasClass("hidden")) {
                    options.addClass("hidden");
                }

                //enabling input field text in case of a country has state field optional
                if (optionalState.hasClass("hidden")) {
                    optionalState.removeClass("hidden");
                }

                $(stateDivLabelSelector).removeClass("labelDropDown");
            }

        }
    }
}

/*Validation for email field */
function validateEmail(){

    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkEmail();
}

/* Validation for password field*/
function validatePassword(){
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkPassword();
}

/* Function for creating the binding between the country and state dropdown */
function setStateOnChange() {
    $('#stateDropDown').empty();
    cty = $("#countriesDropDown option:selected").val();
    parseData.getStates(cty);
}

// JS code for all the validations
var errorMessages;
var cardTypes;

/* Function for formatting the phone number entered by user */
function formatPhoneNumber(mobileNumberSelector, phoneNumberSelector) {
	mobileNumberSelector = mobileNumberSelector || '#mobileNumber';
	phoneNumberSelector = phoneNumberSelector || '#phoneNumber';

	//OPS-63974
	//getMobileVal();
	var isNoPhoneError = $(mobileNumberSelector).parent().find(".errorDiv").hasClass("hidden");
	if(isNoPhoneError){
		formatMobileText(mobileNumberSelector, phoneNumberSelector);	
	}

	$(mobileNumberSelector).removeClass("placeholdersjs");
    // $(".phone").val(function(i, val) {
    //     val = val.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
    //     return val;
    // });
}
/*OPS-63895 Boleto Changes*/
/*Function to format CPF/CNPJ Field*/
function formatCPFCNPJ() {
$('input.cpfcnpj').removeClass("placeholdersjs");
var cpfValue = $(".cpfcnpj").val();
if (cpfValue.length == 11) {
    $(".cpfcnpj").val(function(i, val) {
        val = val.replace(/(\d\d\d)(\d\d\d)(\d\d\d)(\d\d)/, "$1.$2.$3-$4");
        return val;
    });
   }
   else if (cpfValue.length == 14) {
   		$(".cpfcnpj").val(function(i, val) {
        val = val.replace(/(\d\d)(\d\d\d)(\d\d\d)(\d\d\d\d)(\d\d)/, "$1.$2.$3/$4-$5");
        return val;
    });
   }
}

/* Function for formatting the CEP Field for BR */
function formatzipCode(zipCodeSelector) {
	$(zipCodeSelector).removeClass("placeholdersjs");
    $(zipCodeSelector).val(function(i, val) {
        val = val.replace(/(\d\d\d\d\d)(\d\d\d)/, "$1-$2");
        return val;
    });
}

function clearziphyphens(zipCodeSelector){

$(zipCodeSelector).removeClass("placeholdersjs");
    var zipOrPostalCode = $(zipCodeSelector).val();
    $(zipCodeSelector).val(zipOrPostalCode.replace(/-/g, ''));

}

/*OPS-65357 NSB Changes*/
/*Function to format NSB Phone Number Field*/
function formatnsbPhoneNumber() {
	//OPS-63974
	var isNoPhoneError = $("#nsbMobileNumber").parent().find(".errorDiv").hasClass("hidden");
	if(isNoPhoneError){
		formatMobileText();	
	}

	$('input.nsbPhoneNumber').removeClass("placeholdersjs");
    
}

function getErrorMessage() {
    errorMessages = window.errorMessages;
    countryStates = window.countryStates;
    fieldsToValidate = window.fieldsToValidate;
}


// New User Account related Validations
/* Validation for userid entered in case of create account */
function checkUserId() {
console.log("checking new user");
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.autoCheck(USERID);
}

/* Validation for password field in Create account section */
function newAccountPass() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkAccountPassword();
}

/* Verify password related validations */
function verifyPass() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkVerifyPwd();
}

// All validation related functions for Billing Information
// function checkcardholdername() {
function checkcardholdername(element, parseDataPropName, selector) {
	element = element || 'cardHolderName';
	parseDataPropName = parseDataPropName || 'cardHolderName';
	selector = selector || '#cardHolderName';

	console.log('checkcardholdername is firing');
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkCardHolderName(element, parseDataPropName, selector);
    // errorCheck.checkCardHolderName();
}
//ops-75656
function bankNameSelected() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkBankNameSelected();
}

//ops-75656
//This function creates dropdown based on getcart API bankList
function createBankDropdown(){
	var selectedCountry = $("#countriesDropDown").val(); 

	// OSP-85079
	var paymentMethod = $("#paymentMethod option:selected").val();
	if(typeof window.idealBankList !== "undefined" && selectedCountry == "NL" && paymentMethod == "IB_IDEAL"){
		var sortedBankList = (window.idealBankList || []).slice(0).sort(function(a, b){
	      return a.value.localeCompare(b.value);
	    });

		var selectText = $("#bankDropdownList option[value='-1']").text();
		$("#bankDropdownList").empty().append($("<option value='-1'>"+selectText+"</option>"));
		$.each(sortedBankList, function(idx, obj) {
			console.log(obj.key+" "+obj.value);
			$("#bankDropdownList").append($("<option value="+obj.key+">"+obj.value+"</option>"));
		});		
		$('#bankDropdownDiv #bankDropdownList').addClass("bankDropdownList");
		errorCheck.removeError("bankDropdownList");	
		$("#bankDropdownDiv").show();
		
	}
	// OSP-85079

	if(typeof pspValue !== 'undefined' && pspValue == "NETPROPHETS" && selectedCountry == "IN"){
		
    var sortedBankListData = (window.bankListData || []).slice(0).sort(function(a, b){
      return a.value.localeCompare(b.value);
    });

		$.each(sortedBankListData, function(idx, obj) {
			console.log(obj.key+" "+obj.value);
			$("#bankDropdownList").append($("<option value="+obj.key+">"+obj.value+"</option>"));
		});		
		$('#bankDropdownDiv #bankDropdownList').addClass("bankDropdownList")
		errorCheck.removeError("bankDropdownList");	
		$("#bankDropdownDiv").show();
		$("#netbankingName").show();
		$("#netBanking").show();
		$("#netProphetText").show();		
		$('#cardHolderName').attr('placeholder',$('#pay-pal-ph').text());
		$("#credit-card").hide();
		$("#cardNumberDiv").hide();
		$("#expiryDateDiv").hide();
		$("#securityCodeDiv").hide();
	}
}

function checkcardnumber() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkCardNumber();
}

function checkNdisplayCardImage() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkCardImage();
}

function checkccvnumber() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkSecurityCode();
}

/** This function verifies the city information.
 *  @param {string} element The name of the element to verify.
 *  @param {string} parseDataPropName  The name of the property of the parseData object to get value to verify from.
 *  @param {string} errorSelector In case of error, the name of the selector to apply error message to.
 */
function checkcity(element, parseDataPropName, selector) {
	element = element || 'city';
	parseDataPropName = parseDataPropName || 'city';
	selector = selector || '#city';

    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkCity(element, parseDataPropName, selector);
    // errorCheck.autoCheck(CITYID); seems redundant
}

function checkphonenumber(element, countriesDropDownSelector, callingCodeSelector, mobileNumberSelector, phoneErrorSelector, parseDataPhonePropName, phoneNumberSelector, isShipping) {
    element = element || 'phone';
	countriesDropDownSelector = countriesDropDownSelector || '#countriesDropDown';
	callingCodeSelector = callingCodeSelector || '#callingCode';
	mobileNumberSelector = mobileNumberSelector || '#mobileNumber';
	phoneErrorSelector = phoneErrorSelector || '#phoneError';
	parseDataPhonePropName = parseDataPhonePropName || 'phone';
	phoneNumberSelector = phoneNumberSelector || '#phoneNumber';
	isShipping = isShipping || false;

    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkPhone(element, countriesDropDownSelector, callingCodeSelector, 
    	mobileNumberSelector, phoneErrorSelector, parseDataPhonePropName, phoneNumberSelector, isShipping);
    //errorCheck.autoCheck(PHONEID);
}

/* Function clears the formatting present in the mobile number */
function clearhyphens(mobileNumberSelector){
	mobileNumberSelector = mobileNumberSelector || '#mobileNumber';

	var $mobileNumber = $(mobileNumberSelector);
	$mobileNumber.removeClass("placeholdersjs");
    var phNumber = $mobileNumber.val();   
    var isNoPhoneError = $mobileNumber.parent().find(".errorDiv").hasClass("hidden");
	if(isNoPhoneError){
		$mobileNumber.val(phNumber.replace(/[^\d]/g, '')); // send only digits to backend
	}
}

function checkzipcode(zipCodeSelector, citySelector, stateDropdownSelector) {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();

    var element, parseDataZipCodePropName, parseDataCountryPropName;
    if (zipCodeSelector === '#zipCode-ship'){
    	element = 'zipCode-ship';
    	parseDataZipCodePropName = 'zipCodeShip';
    	parseDataCountryPropName = 'countryShip';
    }

    errorCheck.checkZipCode(element, parseDataZipCodePropName, zipCodeSelector, parseDataCountryPropName);
    formatzipCode(zipCodeSelector);
    var zipCd = $(zipCodeSelector).val().trim();
    var plcHolder = $(citySelector).attr('placeholder');
    $.ajax({
        type: 'POST',
        url: '/estore/getShoppingCart/action/zipcodePopulate/zipcode/'+zipCd,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        mimeType: 'application/json',
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
        success: function(data) {
            if(typeof data.state !== "undefined"){
                    $(stateDropdownSelector + ' option:selected').attr("selected",null);
                    $(stateDropdownSelector + ' option[value='+data.state+']').attr("selected","selected");
                }
            else{
                $(stateDropdownSelector + ' option:selected').attr("selected",null);
            }
            if(typeof data.city !== "undefined"){
                    $(citySelector).val(data.city);
                }
            else{
                 $(citySelector).val('');
                $(citySelector).attr('placeholder',plcHolder);
            }
            },
			    error:genericAjaxErrorHandler,
                statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("zipCode Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("zipCode Session has expired"+xhr.responseText);
                        }
                    }

                }
        }
			
        });
	
		}

/*OPS-65357 NSB Changes*/
function checknsbcompany() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkCompany();
    errorCheck.autoCheck(CITYID);
}
/*OPS-73493 VAT Code Changes */
function checkVatCode() {										            
	$('#vatCode').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');						
	$(".vatCode").removeClass("redBorder");  					               
}

/*OPS-73493 IBAN Validation Changes */
function checkIBAN() {
var iBan = $('.iban').val().trim();

if (iBan.length <= 0) {
             var errorMsg = $('#emptyIBAN').text();
						    $('#iban').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');

							$(".iban").addClass("redBorder");
							$('#iban').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);   
                
}else{
	      
$.ajax({
				type: 'POST',
				url: '/estore/getShoppingCart/action/validateIban/iban/'+iBan,
				contentType: "application/json; charset=UTF-8",
				dataType: "json",
				mimeType: 'application/json',
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
			success: function(data) {
				if(typeof data.valideIBAN != 'undefined'){
				console.log("data.valideIBAN"+data.valideIBAN);
				console.log("iBan"+iBan);
						if(data.valideIBAN == false){												
							var errorMsg = $('#invalidIBAN').text();
							$('#iban').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
							$(".iban").addClass("redBorder");
							$('#iban').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);
						}
						else{
							$('#iban').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');						
							$(".iban").removeClass("redBorder");	
						}
				}	          
            },
			error:genericAjaxErrorHandler,
            statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("Session has expired"+xhr.responseText);
                        }
                    }

                }
			}
			
        });
	} //else end
}

/*OPS-73493 BIC Validation Changes */
function checkBankCode() {
var bankCode = $('.bankCode').val().trim();

if (bankCode.length <= 0 ) {
             var errorMsg = $('#emptyBIC').text();
						    $('#bankCode').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
							$(".bankCode").addClass("redBorder");
							$('#bankCode').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);   
                
}else{
	      
$.ajax({
				type: 'POST',
				url: '/estore/getShoppingCart/action/validateBic/bic/'+bankCode,
				contentType: "application/json; charset=UTF-8",
				dataType: "json",
				mimeType: 'application/json',
        timeout: AJAX_TIMEOUT_FEATURE_FLAG ? AJAX_TIMEOUT : undefined,
			success: function(data) {
				if(typeof data.valideBIC != 'undefined'){
				console.log("data.valideBIC"+data.valideBIC);
				console.log("bic"+bankCode);
						if(data.valideBIC == false){												
							var errorMsg = $('#invalidBIC').text();
							$('#bankCode').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
							$(".bankCode").addClass("redBorder");
							$('#bankCode').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);
						}
						else{
							$('#bankCode').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');						
							$(".bankCode").removeClass("redBorder");	
						}
				}	          
            },
			error:genericAjaxErrorHandler,
            statusCode: {
                200: function(xhr){
                    if(typeof xhr.responseText != "undefined"){
                        if(xhr.responseText.search("mfsessionExpired") == -1){
                            console.log("Session not expired"+xhr.responseText);
                        }
                        else{
                            window.location.href = '/estore/mf/sessionExpired';
                            console.log("Session has expired"+xhr.responseText);
                        }
                    }

                }
			}
			
        });
	} //else end
}

/*OPS-65357 NSB Changes*/
function checktaxid() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checktaxId();
    errorCheck.autoCheck(TAXID);
}
/*OPS-65357 NSB Changes*/
function checknsbphonenumber() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checknsbPhone();
    errorCheck.autoCheck(NSBPHONEID);
}
/*OPS-65357 NSB Changes*/
/* Function clears the formatting present in the nsb phone number */
function clearnsbhyphens(){
	$('input.nsbPhoneNumber').removeClass("placeholdersjs");
	var phNumber = $('input.nsbPhoneNumber').val();   
    var isNoPhoneError = $("#nsbMobileNumber").parent().find(".errorDiv").hasClass("hidden");
	if(isNoPhoneError){
		$('input.nsbPhoneNumber').val(phNumber.replace(/[^\d]/g, '').trim());	
	}
}

/*OPS-63895 Boleto Changes*/
function checkCPFCNPJ() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkcpfCnpj();
    errorCheck.autoCheck(CPFCNPJID);
    formatCPFCNPJ();
}
/*Boleto Changes*/
/* Function clears the formatting present in the cpf field */
function clearcpfhyphens(){

$('input.cpfcnpj').removeClass("placeholdersjs");
var cpfcnpj = $('input.cpfcnpj').val();
cpfcnpj = cpfcnpj.replace(/\//g, '');
cpfcnpj = cpfcnpj.replace(/\./g, '');
$('input.cpfcnpj').val(cpfcnpj.replace(/-/g, ''));
}

function checkAddress(element, parseDataPropName, selector) {
	element = element || 'address';
	parseDataPropName = parseDataPropName || 'address';
	selector = selector || '#address';

	getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkaddress(element, parseDataPropName, selector);
    errorCheck.autoCheck(ADDRESSID);
}
		
function checkcountry() {
    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkCountry();
    $('#stateDropDown').empty();

    //OPS-85079
    if($("#paymentMethod option:selected").val() == 'IB_IDEAL'){
    	createBankDropdown();
    }
  	//OPS-65465
	var selectedPaymentMethod = $('#paymentMethod').val();
  	billingCty= $("#countriesDropDown option:selected").val();
	cty= $("#countriesDropDown option:selected").val();	
	countryText = $("#countriesDropDown option:selected").text();
	var incomingalpagoCountriesList	=window.alpaGoCountriesListVal.indexOf(countryCodeId);
	var billingalpagoCountriesList	=window.alpaGoCountriesListVal.indexOf(billingCty); 
	window.isSelectedCount = true;
  	if(window.lamcCountriesList.split(',').indexOf(cty) >= 0){
  		console.log("window.lamcCountriesList "+ window.lamcCountriesList+" "+cty);
  		lamacCount = true;
  		checkLAMC();	
  	}else{
  		lamacCount = false;
  		checkLAMC();
  	}
   	//Exp4 Countries Check
  if(window.exp4CountriesList.split(',').indexOf(cty) !== -1){

		$('#nonEmeaEula').hide();
		$("#confirmSubmit").hide();
		$('#nonEmeaReview').hide();	
        if (selectedPaymentMethod == 'CC' || selectedPaymentMethod == 'DW' || selectedPaymentMethod == 'DD'){
            $("#agreePaymentInfoPrePopulated").show();
        }							
		$('#emeaEula').show();
		$('#emeaReview').show();
		$('#privacyCompliance').show();

		if($("#agreeEmeaNonArEula").attr("checked")){		    
			$("#confirmSubmit").show();
			$("#confirmEmeaSubmit").hide();		    			
		}else {	  				  	
			$("#confirmEmeaSubmit").show();
			$("#confirmSubmit").hide();
		}													        	
    	$("#refundPolicy").show();

	console.log("window.exp4CountriesList "+ window.exp4CountriesList+" "+cty);		
	if(window.eucountriesList.split(',').indexOf(cty) !== -1){
	        	console.log("Inside  eu countries for billingCty");						
				$('#zipCodeDiv').hide();				
				$('#stateDiv').hide();						
				$('#vatDiv').show();														
				$('#euVatToC').show();
				$('#vatCart').show();
				$('#vatReview').show();																		
	}
	else if (selectedPaymentMethod == 'HP_QIWI-SSL' || selectedPaymentMethod == 'HP_WEBMONEY-SSL' || selectedPaymentMethod == 'HP_YANDEXMONEY-SSL' || selectedPaymentMethod == 'HP_PRZELEWY-SSL'){
		$('#zipCodeDiv').show();
	}		
	else if (((selectedPaymentMethod == 'BT' && (typeof window.alpaGoCountriesList.indexOf(cty) !== 'undefined' && window.alpaGoCountriesList.indexOf(cty) == -1)) || (selectedPaymentMethod == 'DD') || (selectedPaymentMethod == 'IB_GIROPAY') || (selectedPaymentMethod == 'IB_SOFO') || (selectedPaymentMethod == 'IB_NORDEA') || (selectedPaymentMethod == 'IB_IDEAL') || (selectedPaymentMethod == 'IB_NETBANKING')) && (window.eucountriesList.split(',').indexOf(cty) == -1)) {
				console.log("Inside non eu countries and not cc for billingCty");	
				$('#zipCodeDiv').hide();
				$('#cityDiv').hide();
				$('#stateDiv').hide();
	}
	else if((typeof incomingalpagoCountriesList !== 'undefined' && incomingalpagoCountriesList !== -1) || (typeof billingalpagoCountriesList !== 'undefined' && billingalpagoCountriesList !== -1)){
			$('#vatDiv').hide();
			$('#euVatToC').hide();
			$('#vatCart').hide();
			$('#vatReview').hide();							
			$('#zipCodeDiv').show();
			$('#cityDiv').show();
			$('#stateDiv').show();
	}//alpago country check
else{	
		console.log("Inside non eu countries for billing country");				
		$('#vatDiv').hide();
		$('#euVatToC').hide();							
		$('#zipCodeDiv').hide();
		$('#cityDiv').hide();
		$('#stateDiv').hide();
		$('#vatCart').hide();
		$('#vatReview').hide();	

	}	
	showHideMandatoryCity();//WorldPay changes OPS-73496 		 	
}//end of exp4 country check
else{
	$("#agreePaymentInfoPrePopulated").hide();
	$('#emeaEula').hide();						
	$("#refundPolicy").hide();						
	$("#confirmEmeaSubmit").hide();
	$('#emeaReview').hide();
	$('#nonEmeaEula').show();
	$("#confirmSubmit").show();
	$('#nonEmeaReview').show();	
	$('#vatCart').hide();
	$('#vatReview').hide();
	$('#euVatToC').hide();
	$('#vatDiv').hide();
	$('#privacyCompliance').hide();														
}  	       
	/* Added for OPS-54395 Populate Ghost text Ticket
	This will change place holder of Zip Code Field based on the CA and Non CA */
	if(typeof countryCodeId !== 'undefined'){
		if (billingCty== "CA"){
			console.log("countries selected :"+billingCty);
			$('#zipCode').attr('placeholder',$('#postalCodeText').text());
		}
		/* Added for Boleto Changes CEP Field Placeholder */
		else if (billingCty== "BR"){
			console.log("countries selected :"+billingCty);
			$('#zipCode').attr('placeholder',$('#cepText').text());
		}
		else {
			$('#zipCode').attr('placeholder',$('#zipCodeText').text());
		}
	}
	// India Payments changes show/hide merchant of record text based on billing country OPS-84153
	if ((typeof pspValue !== 'undefined' && pspValue == "NETPROPHETS") && billingCty == "IN") {			
		$('#merchantCopy').show();
	}
	else {
		$('#merchantCopy').hide();
	}
	
	// OPS-71512 Changes to validate not supported card Number
	var element = 'cardNumber';
	parseData.getErrorMessageIndexs();
	parseData.getCurrentData();
	//remove dashes and spaces
	var cardVal = parseData.cardNumber.replace(/[-\s]/g, '');
	$("#cardNumber").val(cardVal);					
	if (cardVal.length >= 1 && cardVal.indexOf("x") == -1) {
		console.log("cardVal.length" +cardVal.length);
		var value = errorCheck.checkSupportedPayment(cardVal);
		var paymentName = wa.ecom.cardType(cardVal).name;
		if(paymentName =="American Express"){
			paymentName = "Amex";
		}
		if(!value){
				$('#cardNamedd').text(paymentName);
				$('#countryNameIddd').text(countryText);
				var errorMsg = $('#amexCardErrordd').text();
				$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
				$(".country").addClass("redBorder");
				$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);
				$('#cardNumber').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');
				$(".cardNumber").removeClass("redBorder");
				$(".row.withImg img").hide();
				getSupportedImage();
				createPaymentDropdown();
				return false;
		} 
	}	// OPS-71512 Changes to validate not supported card Number ends
						
				 $('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');
				 $('#cardNumber').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');
				 $(".country").removeClass("redBorder");
				 $(".cardNumber").removeClass("redBorder");				 			
				 console.log("no error");				
				/*Boleto Changes */				
				if((selectedPaymentMethod === "BT" && typeof countryCodeId !== "undefined" && typeof boleto_code !== 'undefined')){
					console.log("Bank code"+boleto_code);
						if(countryCodeId === "BR"){
								if((billingCty === "BR")){
								$('#alpagoText').show();
								$('#BOLETO, #boleto').show();
								$("#cpfcnpjDiv").show();
								$("#addressDiv").show();
								$("#zipCodeDiv").show();
								$("#cityDiv").show();
								$("#stateDiv").show();
							}
							else if(billingCty !== "BR"){								
								$('#alpagoText').hide();
								$('#BOLETO, #boleto').hide();
								$("#cpfcnpjDiv").hide();
								$("#addressDiv").hide();
								$("#zipCodeDiv").hide();
								$("#cityDiv").hide();
								$("#stateDiv").hide();
							}
						}               
				}			
				/*Oxxo Changes OPS-73492 */
				if((cty  === "MX") &&  typeof cty !== 'undefined'){
					$('#dineromailText').show();					
				}
				else if(typeof oxxo_code !== 'undefined'){
						if(($('#countriesDropDown').val()) !== "MX" && ($('#paymentMethod').val()=='HP')){							
							$('#dineromailText').hide();							
						}else if(cty !== "MX" && ($('#paymentMethod').val()=='HP')){							
							$('#dineromailText').hide();							
						}
				}
				else if(cty  !== "MX"){
					$('#dineromailText').hide();					
				}else if(($('#countriesDropDown').val() === 'MX') && countryCodeId !== "MX"){
						$('#dineromailText').show();						
				}
				else{
					$(".country").removeClass("redBorder");
				}
	console.log(cty);		
    parseData.getStates(cty);

    //OPS-81361 Front End Panel update based on MOP
	checkPaymentConfirmation();
	optionalPhone();
	checkPayPalMessage(); //Paypal Text in case of in-context flow   
	// OPS-98189
	
	var georestrictedProdList;
	georestrictedProdList = checkProductGeoLocation(window.productFamilyList, billingCty);
	if(georestrictedProdList.length > 0){
		$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
		$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').text(htmlDecode(georestrictedProdList.join(', '))+' '+$('#geoLocationMsg').text());		
		$('#countriesDropDown').addClass("redBorder");
		window.geoErrorMsg = "true";
	}else{
		$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');	
		$('#countriesDropDown').removeClass("redBorder");
		window.geoErrorMsg = "false";		
	}
	
}


function checkProductGeoLocation(productFamilyList, billingCountry){
	var geo_obj = {}
	var geoLocationList = window.geoRestrictionList.split(";").filter(Boolean)
	for(i=0;i<geoLocationList.length;i++){
		var obj_value;
		if(geoLocationList[i].split('=')[1].indexOf(",")>-1){
			obj_value = geoLocationList[i].split('=')[1].split(",");
		}else{
			obj_value = [geoLocationList[i].split('=')[1]];
		}
		geo_obj[geoLocationList[i].split('=')[0]] = obj_value;
	}
	var georestrictedProdList = [];
	for(j=0;j<productFamilyList.length;j++){
		if(geo_obj.hasOwnProperty(productFamilyList[j])){
			if(geo_obj[productFamilyList[j]].indexOf(billingCountry) > -1){

			}else{
				georestrictedProdList.push(productFamilyList[j]);	
			}
		}
	}
	// OPS-101539 starts
	var productNameList = [];
	for(j=0;j<window.cartItems.length;j++){
		for(i=0;i<georestrictedProdList.length;i++){
			if(window.cartItems[j].productFamilyName == georestrictedProdList[i]){
				productNameList.push(window.cartItems[j].productName);
			}
		}
	}
	// OPS-101539 ends
	return productNameList;
}

function checkstate(stateDropDownSelector, stateParseDataPropName) {
	stateDropDownSelector = stateDropDownSelector || '.stateDropdown';
	stateParseDataPropName = stateParseDataPropName || 'state';

    getErrorMessage();
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkS(stateDropDownSelector, stateParseDataPropName); 
}

function checkexpirymonth() {
    getErrorMessage();
    if ($('#expMonthDropDown option:selected').val() == '-1') {
        $('#expMonth').val("");
    } else {
        $('#expMonth').val($('#expMonthDropDown option:selected').val());
    }
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkCardValidity(true, false, false);
}

function checkexpiryyear() {
    getErrorMessage();
    if ($('#expYearDropDown option:selected').val() == '-1') {
        $('#expYear').val("");
    } else {
        $('#expYear').val($('#expYearDropDown option:selected').val());
    }
    parseData.getErrorMessageIndexs();
    parseData.getCurrentData();
    errorCheck.checkCardValidity(false, true, false);
}

function clearonFocus(id){
    var element_id = id;
    if ($('#'+element_id).nextAll('.errorDiv:first').hasClass('hidden') == 'false'){

    }
    else{
        $('#'+element_id).nextAll('.errorDiv:first').addClass('hidden');
        $('#'+element_id).nextAll('.errorDiv:first').addClass('hidden');
        $('#'+element_id).nextAll('.errorDiv:first').addClass('hidden');
    }
}

/* Implementation of Class based code for validations */
var parseData;
var parseData = {
    showAutoRenew: function() {
    },

    addCardType: function() {
        $.each(cardTypes, function() {
            $("#cardTypeDropDown").append($("<option />").val(this.cardValue).text(this.cardType));
        });
    },

    getBPDynData: function() {
        this.fieldsToValidate = window.fieldsToValidate;
    },
    /*This function  fills the country drop down values*/
    getCountry: function() {
        for (var countries = 0; countries < countryStates.length; countries++) {
            ("countryStates length: " + countryStates.length);
            var options = $("#countriesDropDown");
            emptyOption(options);
            $.each(countryStates, function() {
                options.append($("<option />").val(this.countryCode).text(this.countryName));
            });
        }
    },
    /*This function fills the state dropdown values based on country selected*/
    getStates: function(ctryCde) {
        var options = $("#stateDropDown"),
            optionalState = $("#optionalState");
        var selectState = $("#stateDropdownhidden option").text();

        if (ctryCde != null || ctryCde != "") {
            /* first loop over country array and look for the passed in ctryCde */
            var foundIdx = -1; // set a flag to reference the number
            for (var countries = 0; countries < countryStates.length; countries++) {
                if (ctryCde == countryStates[countries].countryCode) {
                    foundIdx = countries; // set our flag to true
                }
            }
            if (foundIdx == -1) {
                //disabling states dropdown where a country has state as optional field
                if (!options.hasClass("hidden")) {
                    options.addClass("hidden");
                }

                //enabling input field text in case of a country has state field optional
                if (optionalState.hasClass("hidden")) {
                    optionalState.removeClass("hidden");
                }


                $("#stateDiv label").removeClass("labelDropDown");
            }
            if (foundIdx > -1) {

                //Added condition for countries which don't have state list and have state as optional
                if (countryStates[foundIdx].stateList.length > 0) {

                    if (selectState == undefined || selectState == null || selectState == "") {
                        selectState = $("#stateDropdownhidden option").text();
                    }
                    options.empty();
                    options.append($("<option />").val("-1").text(selectState));

                    for (var stateCount = 0; stateCount < countryStates[foundIdx].stateList.length; stateCount++) {
                        var stCode = countryStates[foundIdx].stateList[stateCount].stateCode;
                        var stName = countryStates[foundIdx].stateList[stateCount].stateName;

                        this.stateCodes.push(stCode);
                        this.stateNames.push(stName);
                        options.append($("<option />").val(stCode).text(stName));
                    }

                    //enabling state dropdown for states which already have state list
                    if (options.hasClass("hidden")) {
                        options.removeClass("hidden");
                    }

                    if (!optionalState.hasClass("hidden")) {
                        optionalState.addClass("hidden");
                    }

                    $("#stateDiv label").addClass("labelDropDown");

                } else {
                    if (!options.hasClass("hidden")) {
                        options.addClass("hidden");
                    }

                    //enabling input field text in case of a country has state field optional
                    if (optionalState.hasClass("hidden")) {
                        optionalState.removeClass("hidden");
                    }

                    $("#stateDiv label").removeClass("labelDropDown");
                }

            }
        }
    },

    /*This function sets the index of error message for client side validation*/
    getErrorMessageIndexs: function() {
        /* I need to loop over the error message array and find which fields are where */
        for (var eml = 0; eml < errorMessages.length; eml++) {
            var fn = this;
            switch (errorMessages[eml].fieldName) {
                case "email":
                    fn.emailErrorIdx = eml;
                    break;
                case "password":
                    fn.passwordErrorIdx = eml;
                    break;
                case "username":
                    fn.emailErrorIdx = eml;
                    break;
                case "apassword":
                    fn.passwordErrorIdx = eml;
                    break;
                case "verifyPassword":
                    this.verifyPasswordErrorIdx = eml;
                    break;
                case "cardHolderName":
                    this.cardHolderNameErrorIdx = eml;
                    break;
				case "paypalAccountName":
                    this.cardHolderNameErrorIdx = eml;
                    break;
                case "cardNumber":
                    fn.cardNumberErrorIdx = eml;
                    break;
                case "cardType":
                    fn.cardTypeErrorIdx = eml;
                    break;
                case "cvv2Code":
                    fn.securityCodeErrorIdx = eml;
                    break;
                case "city":
                    fn.cityErrorIdx = eml;
                    break;
                case "subCountryDD":
                    fn.stateErrorIdx = eml;
                    break;
                case "country":
                    fn.countryErrorIdx = eml;
                    break;
                case "phoneNumber":
                    fn.phoneErrorIdx = eml;
                    break;
                case "zipOrPostalCode":
                    fn.zipCodeErrorIdx = eml;
                    break;
                case "expDateMonth":
                    this.monthErrorIdx = eml;
                    break;
                case "expDateYear":
                    this.yearErrorIdx = eml;
                    break;
				/*OPS-65357 NSB Changes*/	
				case "company":
                    this.companyErrorIdx = eml;
                    break;
				case "taxId":
                    this.taxIdErrorIdx = eml;
                    break;			
                /*OPS-63895 Boleto Change */
				case "cpfcnpj":
                    fn.cpfcnpjErrorIdx = eml;
                    break;
                case "address":
                    fn.addressErrorIdx = eml;
                    break;                       
            }
        }
    },
    /*This function checks which field need to have client side validation*/
    isFieldToBeValidated: function(element) { 
        this.fieldsToValidate = window.fieldsToValidate;
        var reqFieldsArray = this.fieldsToValidate.split(',');

        for (var i = 0; i < reqFieldsArray.length; i++) {
            if (reqFieldsArray[i] == element) {
                return true;
            }
        }
        return false;
    },
    /*This function sets the value of each field in local variable*/
    getCurrentData: function() {
        // Wicket has populated the data in the form, so I need to grab it for the pop-over layer
        // I'm not making the pop-over layer here, incase I may want to get the data later for something else.
        this.email = ($(".email").val()).toLowerCase();
        this.password = $(".password").val();
        this.username = ($(".username").val()).toLowerCase();
        this.apassword = $(".apassword").val();
        this.verifyPassword = ($(".verifyPassword").val()).toLowerCase();
        this.cardHolderName = $(".cardHolderName").val();
		/* PayPal Integration CAP-9567 */
		this.paypalname = $("#paypalAccountName").val();
        this.cardNumber = $(".cardNumber").val();
        /*Boleto Changes for OPS-63895 */
        this.cpfcnpj = $(".cpfcnpj").val();
        this.address = $("#address").val();
        this.cardType = $('.cardType option:selected').val();
        this.securityCode = $(".cvv").val();
        this.city = $("#city").val();
        this.zipCode = $(".zipOrPostalCode").val();
        this.state = $(".stateDropdown option:selected").val();
        this.country = $(".country option:selected").val();
        this.month = $(".month").val();
        this.year = $(".year").val();
        //OPS-63974
        this.phone = $('#mobileNumber').val() || $('#nsbMobileNumber').val();
		/*OPS-65357 NSB Changes*/
		this.company = $(".company").val();
		this.taxId = $(".taxId").val();
		//OPS-63974
		this.nsbPhoneNumber = $(".nsbPhoneNumber").val();
		//ops-75656
		this.bankName = $("#bankDropdownList").val();

		// shipping
		this.shippingName = $("#shippingName").val();
        this.addressShip = $("#address-ship").val();
        this.address2Ship = $("#address2-ship").val();
		this.zipCodeShip = $('#zipCode-ship').val();
		this.cityShip = $('#city-ship').val();
		this.stateShip = $('#stateDropShip').val();
		this.countryShip = $('#countryDropShip').val();
		this.phoneShip = $('#mobileNumber-ship').val();
    },
    
    displayCardImage: function(cardId) {
        var id_c = cardId;
if(typeof cardId !== 'undefined'){
        
        var cardId = cardId.toUpperCase();
}       
        if (cardId == 'AMERICAN EXPRESS' || id_c == 'American Express') {
            cardId = 'AMEX';
            id_c = 'amex';
        }
        // alert(cardId);
        var imageID = $(".acceptedPayments1 img");

        for (i = 0; i <= imageID.length; i++) {
            if(typeof imageID[i] === 'undefined'){

            }
            else{
            imgID = imageID[i].id;
            if (cardId == imgID) {
                var src = $("#" + imgID).attr("src");
                $(".row.withImg img").show().attr("src", src);
                $(".row.withImg img").attr("id", id_c);

                }
            }
        }
      
    },
    /* State information, call getStates("us") [US is the example] to fill these out */
    stateCodes: [],
    stateNames: [],

}

var EMAILID = 'email';
var USERID = 'username';
var VEMAILID = 'verifyEmail';
var PWDID = 'password';
var APWDID = 'apassword';
var VEPWDID = 'verifypassword';
var CITYID = 'city';
var STATEID = 'state';
var PHONEID = 'phone';
var ZIPID = 'zipOrPostalCode';
/*OPS-65357 NSB Changes*/
var COMPANYID = 'company';
var TAXID = 'taxId';
var NSBPHONEID = 'nsbPhoneNumber';
/*OPS-63895 Boleto Changes */
var CPFCNPJID = 'cpfcnpj';
var ADDRESSID = 'address';
var errorCheck;
var errorCheck = {

    /*This function shows main error message if there is error on any field*/
    mainError: function() {
        var errorFound = false;
        $(".errorDiv").each(function(index) {
            //we need and condtion as if user will toggle from new to login screen it will show the error if we take or condition
            if (($(this).is(":visible")) && ($(this).text() != "")) {
                errorFound = true;
                return false;
            }
        });

        if (!errorFound) {
            $(".mainerrorDiv").addClass("hidden");
            $(".headingText").removeClass("hidden");
        } else {
            $(".mainerrorDiv").removeClass("hidden");
            $(".headingText").addClass("hidden");
        }
    },
    /** This function removes the error message.
     *  @param {string} element The class name of the element to attach the error message to. If null, then selector is used.
     *  @param {string} selector The CSS selector of the element to attach the error message to. Not used if element is not null.
     */
    removeError: function(element, selector) {
    	var errorElementSelector = element ? ('.' + element) : selector;
        $(errorElementSelector).parent(".errorMessageContainer").find(".errorDiv").addClass("hidden").text("");
        $(errorElementSelector).removeClass("redBorder");
        this.mainError();        
    },
    /** This function adds the error message
     *  @param {string} element The class name of the element to attach the error message to. If null, then selector is used.
     *  @param {string} errorMessage  The error message to display.
     *  @param {string} selector The CSS selector of the element to attach the error message to. Not used if element is not null.
     */
    addError: function(element,errorMessage,selector) {
    	var errorElementSelector = element ? ('.' + element) : selector;
        $(errorElementSelector).parent(".errorMessageContainer").find(".errorDiv").removeClass("hidden").text(errorMessage);
        $(errorElementSelector).addClass("redBorder");
        this.mainError();
        //errorFieldBorder();
        if (typeof trackBillingErrors === "function"){
        trackBillingErrors(errorElementSelector,errorMessage);
		}
        console.log('addError this one function is firing for omniture' + errorElementSelector);
        //console.error("Form Error:"+errorMessage)
    },
    /*This function add red border to field when error is thrown*/
    addborderCheckbox: function(element) {
        $('.' + element).css('outline-color', 'red');
        $('.' + element).css('outline-style', 'solid');
        $('.' + element).css('outline-width', 'thin');
    },
    /*This function removes the red border on field*/
    removeBorderCheckbox: function(element) {
        $('.' + element).css('outline-color', 'none');
        $('.' + element).css('outline-style', 'none');
        $('.' + element).css('outline-width', 'none');
    },

    //removing space 
    removeSpace: function(element,value) {
        var withoutSpace = $.trim(value);
        $('.' + element).val(withoutSpace);
        return withoutSpace;

    },
    /*This function gets the error message for empty field */
    errorForEmptyFields: function(index) {
        for (var ii = 0; ii < errorMessages[index].errorList.length; ii++) {
            if (errorMessages[index].errorList[ii].validationType.toLowerCase() == "isempty") {
                return errorMessages[index].errorList[ii].errorMessage;
            }
        }
    },
    /* This function gets the error message for Alphabet only fields */
    errorAlphaOnlyFields: function(index) {
        for (var ii = 0; ii < errorMessages[index].errorList.length; ii++) {
            if (errorMessages[index].errorList[ii].validationType.toLowerCase() == "alphaonlyvalidator") {
                return errorMessages[index].errorList[ii].errorMessage;
            }
        }
    },
    /*This function gets the error message for minimum length*/
    errorForMinLength: function(index) {
        for (var ii = 0; ii < errorMessages[index].errorList.length; ii++) {
            if (errorMessages[index].errorList[ii].validationType.toLowerCase() == "minlengthvalidator") {
                var minErrorMessage = errorMessages[index].errorList[ii].errorMessage;
                if (minErrorMessage.indexOf('${val}') >= 0) {
                    minErrorMessage = minErrorMessage.replace('${val}', '6');
                }
                return minErrorMessage;
            }
        }
        return "Minimum 6 characters allowed.";
    },
    /*This function get error message for Max length*/
    errorForMaxLength: function(index) {
        for (var i = 0; i < errorMessages[index].errorList.length; i++) {
            if (errorMessages[index].errorList[i].validationType == "MaxLengthValidator" && errorMessages[index].errorList[i].errorMessage != "") {
                var maxErrorMessage = errorMessages[index].errorList[i].errorMessage;
                if (maxErrorMessage.indexOf('${val}') >= 0) {
                    maxErrorMessage = maxErrorMessage.replace('${val}', '50');
                }
                return maxErrorMessage;
            }


        }
        return "Maximum 50 characters allowed.";

    },
    /*This function get error message for invalid phone number*/
    errorForInvalidePhone: function(index) {
        for (var i = 0; i < errorMessages[parseData.phoneErrorIdx].errorList.length; i++) {
            if (errorMessages[index].errorList[i].validationType == "PhoneValidator") {
                return errorMessages[index].errorList[i].errorMessage;
            }
        }
        return "Please enter valid phone number."
    },
     /*This function get error message for empty phone number*/
    errorForEmptyPhone: function(index) {
        for (var i = 0; i < errorMessages[parseData.phoneErrorIdx].errorList.length; i++) {
            if (errorMessages[index].errorList[i].validationType == "isEmpty") {
                return errorMessages[index].errorList[i].errorMessage;
            }
        }
        return "Please enter valid phone number."
    },
    errorForEmailValidation: function(index) {
        for (var ii = 0; ii < errorMessages[index].errorList.length; ii++) {
            if (errorMessages[index].errorList[ii].validationType == "EmailValidator" && errorMessages[index].errorList[ii].errorMessage != "") {
                return errorMessages[index].errorList[ii].errorMessage;
            }

        }
        return "Please enter a valid email address.";
    },
    /*This function gets the error message for invalid card holder name*/
    errorForInvalideCardName: function(index) {
        for (var i = 0; i < errorMessages[parseData.cardHolderNameErrorIdx].errorList.length; i++) {
            if (errorMessages[index].errorList[i].validationType == "AlphaNumericValidator") {
                return errorMessages[index].errorList[i].errorMessage;
            }


        }
        return "Card Holder Name should contain only characters."
    },
	/*OPS-65357 NSB Changes*/
	/*This function gets the error message for invalid tax ID*/
	errorForInvalidtaxId: function(index) {
        for (var i = 0; i < errorMessages[parseData.taxIdErrorIdx].errorList.length; i++) {
            if (errorMessages[index].errorList[i].validationType == "AlphaNumericValidator") {
                return errorMessages[index].errorList[i].errorMessage;
            }


        }
        return "taxId should contain only characters."
    },

    /*OPS-63895 Boleto Changes*/
	/*This function gets the error message for invalid CPF*/
	errorForInvalidcpfcnpj: function(index) {
        for (var i = 0; i < errorMessages[parseData.cpfcnpjErrorIdx].errorList.length; i++) {
            if (errorMessages[index].errorList[i].validationType == "CpfCnpjValidator") {
                return errorMessages[index].errorList[i].errorMessage;
            }


        }
        return "cpfCnpj should contain only numbers."
    },

    checkisPwd_VerifyPwdEntered: function() {
        var previousUrl = localStorage.getItem("previousUrl");
        parseData.getErrorMessageIndexs();
        var passwordVal = $('.password').val();
        var verifyPwdVal = $('.verifyPassword').val();
        var isPwdValid = true;
        var isVerfyPwdValid = true;

        if (previousUrl.toLowerCase().indexOf('cart') >= 0 && (window.SSFields.length == undefined || window.SSFields.length == 0)) {
            isPwdValid = false;
            isVerfyPwdValid = false;
        } else {
            if (passwordVal.length <= 0) {
                this.addError("password", this.errorForEmptyFields(parseData.passwordErrorIdx));
                isPwdValid = false;
            }
            if (verifyPwdVal.length <= 0) {
                this.addError("verifyPassword", this.errorForEmptyFields(parseData.verifyPasswordErrorIdx));
                isVerfyPwdValid = false;
            }
        }
        if ((!isPwdValid && !isVerfyPwdValid)) {
            $('.contactInfoDiv').slideDown("fast");
            $('.submittedContactInfo').slideUp("fast");
            $(".submittedContactInfo .email").removeClass("visibilityHidden");
            parseData.expand_collapseArrow(true, false, true);
            contactFormChange.showNewUserScreen();
            uiEffects.userInfoExists = false;
            return false;
        }
        return true;
    },

    /*This function gets error message when email is not in proper format*/
    checkEmail: function() {
        var element = 'email';
        var maxLengthError = "";
        if (parseData.isFieldToBeValidated(element)) {
            var emailVal = parseData.email;
            var withoutSpaceEmail = $.trim(emailVal);
            var reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if (emailVal.length <= 0 || withoutSpaceEmail.length <= 0) {
                this.emailValid = false;
                this.addError(element, this.errorForEmptyFields(parseData.emailErrorIdx));
                return false;
            } else {
                this.emailValid = true;
            }

            if (!reg.test(emailVal) && !reg.test(withoutSpaceEmail)) {
                this.emailValid = false;
                this.addError(element, this.errorForEmailValidation(parseData.emailErrorIdx));
                return false;
            } else {
                this.emailValid = true;
            }
			
			if (emailVal.length > 50) {
                this.emailValid = false;
                this.addError(element, this.errorForMaxLength(parseData.emailErrorIdx));
                return false;
            } else {
                this.emailValid = true;
            }
        }

        if (this.emailValid)
            this.removeError(element);
        return true;
    },
	/*This function gets error message when user id is not in proper format*/
    checkUserId: function() {
        var element = 'username';
        var maxLengthError = "";
        if (parseData.isFieldToBeValidated(element)) {
            var emailVal = parseData.username;
            var withoutSpaceEmail = $.trim(emailVal);
            var reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            if (emailVal.length <= 0 || withoutSpaceEmail.length <= 0) {
                this.emailValid = false;
                this.addError(element, this.errorForEmptyFields(parseData.emailErrorIdx));
                return false;
            } else {
                this.emailValid = true;
            }

            if (!reg.test(emailVal) && !reg.test(withoutSpaceEmail)) {
                this.emailValid = false;
                this.addError(element, this.errorForEmailValidation(parseData.emailErrorIdx));
                return false;
            } else {
                this.emailValid = true;
            }
			
			if (emailVal.length > 50) {
                this.emailValid = false;
                this.addError(element, this.errorForMaxLength(parseData.emailErrorIdx));
                return false;
            } else {
                this.emailValid = true;
            }
        }

        if (this.emailValid)
            this.removeError(element);
        return true;
    },
    checkVerifyEmail: function() {

        var element = 'verifyEmail';
        if (!$("#verifyEmailDiv").is(":visible")) { // incase the element is not shown because this is NOT a new user, just say it passes validation.
            return true;
        }

        if (parseData.isFieldToBeValidated(element)) {

            var verifyEmailVal = parseData.verifyEmail.toLowerCase();
            var emailVal = parseData.email.toLowerCase();

            var withoutSpaceVerEmail = $.trim(verifyEmailVal);
            var withoutSpaceEmail = $.trim(emailVal);

            var reg = /^([a-zA-Z0-9*!#$%&'*+-_.~{:}|?/\=])+\@(([a-zA-Z0-9_-])+\.)+([a-zA-Z0-9_-])+$/;

            if (verifyEmailVal.length <= 0 || withoutSpaceVerEmail.length <= 0) {
                this.addError(element, this.errorForEmptyFields(parseData.verifyEmailErrorIdx));
                return false;
            }

            if ((emailVal != verifyEmailVal) && (withoutSpaceEmail != withoutSpaceVerEmail)) {

                var lst = errorMessages[parseData.verifyEmailErrorIdx].errorList;
                for (var ii = 0; ii < lst.length; ii++) {
                    var val = lst[ii].validationType;
                    if (val == "EqualsValidator") {
                        this.addError(element, lst[ii].errorMessage);
                    }
                }

                return false;
            }
        }

        this.removeError(element);
        return true;
    },
    checkCountry: function() {
        var element = 'country';
        var countryVal = parseData.country;
        if (countryVal == "-1") {
            this.addError(element, this.errorForEmptyFields(parseData.countryErrorIdx));
            return false;
        }
        if(!$("#countryDiv .errorDiv").hasClass("hidden")){
			return false;
		}
			if(!$("#phoneSection .errorDiv").hasClass("hidden")){
			$("#phoneSection .errorDiv").addClass("hidden");
			$("#phoneSection #mobileNumber").removeClass("redBorder");
			$("#phoneSection .phoneCode").removeClass("redBorder")
			
		}
        this.removeError(element);
        return true;
		
	
    },
	/*This function gets error message when state is not selected */
    checkS: function(stateDropDownSelector, stateParseDataPropName) {
    	stateDropDownSelector = stateDropDownSelector || '.stateDropdown';
		stateParseDataPropName = stateParseDataPropName || 'state';

    	var element, selector;

    	 if ($(stateDropDownSelector).is(':visible')) {
            selector = stateDropDownSelector;
            var stateVal = parseData[stateParseDataPropName];
            if (stateVal == "-1") {
	            this.addError(null, this.errorForEmptyFields(parseData.stateErrorIdx), selector);
	            return false;
        	}
        }
        /* State Mandatory in case of Brazil as Billing Country */
        // Code below not refactored
        if ($('.optionalState').is(':visible')) {
            element = 'optionalState';
            var stateVal = $("#optionalState").val();         
            var selectedCountry = $("#countriesDropDown").val();        	

        	 if ((typeof stateVal == 'undefined' || stateVal.length <= 0 ) && selectedCountry == "BR") {        
                this.addError(element, this.errorForEmptyFields(parseData.stateErrorIdx));             
                return false;
            }
        }

        // Line below not fully refactored
        this.removeError(element, selector);
        return true;

    },

    checkPassword: function() {

        var element = 'password';

        if (parseData.isFieldToBeValidated(element)) {

            if (!$("#pwdDiv").is(":visible")) { // incase the element is not shown because this is a new user, just say it passes validation.
                return true;
            }

            var passwordVal = parseData.password;

            if (passwordVal.length <= 0) {
                this.addError(element, this.errorForEmptyFields(parseData.passwordErrorIdx));
                $('#password').val('');
                return false;
            } else if (passwordVal.length < 6) {
                this.addError(element, this.errorForMinLength(parseData.passwordErrorIdx));
                $('#password').val('');
                return false;
            } else if (passwordVal.length > 50) {
                this.addError(element, this.errorForMaxLength(parseData.passwordErrorIdx));
                $('#password').val('');
                return false;
            }
        }

        this.removeError(element);
        return true;
    },
    checkAccountPassword: function() {
        var element = 'apassword';
        if (parseData.isFieldToBeValidated(element)) {
            var passwordVal = parseData.apassword;
            if (passwordVal.length <= 0) {
                this.addError(element, this.errorForEmptyFields(parseData.passwordErrorIdx));
                $("#apassword").val('');
                return false;
            } else if (passwordVal.length < 6) {
                this.addError(element, this.errorForMinLength(parseData.passwordErrorIdx));
                $("#apassword").val('');
                return false;
            } else if (passwordVal.length > 50) {
                this.addError(element, this.errorForMaxLength(parseData.passwordErrorIdx));
                $("#apassword").val('');
                return false;
            }
        }
        this.removeError(element);
        return true;
    },
    checkVerifyPwd: function() {
        var element = 'verifyPassword';
        if (parseData.isFieldToBeValidated(element)) {
            var newPwdVal = $.trim(parseData.apassword.toLowerCase());
            var verifyPwdVal = $.trim(parseData.verifyPassword.toLowerCase());

             if(verifyPwdVal.length <= 0){
                    this.addError(element,this.errorForEmptyFields(parseData.verifyPasswordErrorIdx));
                    return false;
                } 

             if ((verifyPwdVal.length == 0) && (newPwdVal.length ==0)){
                 this.removeError(element);
                 return true;
             } 
             else if (newPwdVal != verifyPwdVal)
            {
                var lst = errorMessages[parseData.verifyPasswordErrorIdx].errorList;
                for (var ii = 0; ii < lst.length; ii++) {
                    var val = lst[ii].validationType;
                    if (val == "EqualsValidator") {
                        this.addError(element, lst[ii].errorMessage);
                        $("#verifyPassword").val('');
                    }
                }

                return false;
            }
        }


        this.removeError(element);
        return true;
    },
	
        /*Code for credit card validation rule*/
    checkCardHolderName: function(element, parseDataPropName, errorSelector) {
    	element = element || 'cardHolderName';
		parseDataPropName = parseDataPropName || 'cardHolderName';
		errorSelector = errorSelector || '#cardHolderName';

        // var element = 'cardHolderName';
        //var element = [element];
        parseData.getErrorMessageIndexs(); // later we will call this only once in ready function
        parseData.getCurrentData();
        var cardHolderName = parseData[parseDataPropName];
        // var cardHolderName = parseData.cardHolderName;
        nameReg = /^[\u002D\u0061-\u007A\u0027\u0041-\u005A \u0020\u0026\u002E+]*$/;

        var cardHolderName_new = $.trim(cardHolderName);
		
		/* PayPal Integration CAP-9567 Error Message for Name */
		/*Boleto Changes for OPS-63895 */
		
		if(($('#paymentMethod').val()=='DW') || ($('#paymentMethod').val()=="BT") || ($('#paymentMethod').val()=='HP') || ($('#paymentMethod').val()=='DD') || ($('#paymentMethod').val()=='IB_GIROPAY') || ($('#paymentMethod').val()=='IB_SOFO')|| ($('#paymentMethod').val()=='IB_IDEAL')|| ($('#paymentMethod').val()=='IB_NORDEA') || ($('#paymentMethod').val()=='IB_NETBANKING')
			|| ($('#paymentMethod').val()=='HP_WEBMONEY-SSL') || ($('#paymentMethod').val()=='HP_YANDEXMONEY-SSL') || ($('#paymentMethod').val()=='HP_QIWI-SSL')  || ($('#paymentMethod').val()=='HP_PRZELEWY-SSL') || (element == 'shippingName')) {
		
			console.log("PayPal error"+ $('#paymentMethod').val());
			
			if (cardHolderName.length <= 0 || cardHolderName_new.length == 0){
				var nameErrorVal = $('#name-error1').text();
				this.addError(null, nameErrorVal, errorSelector);
				// this.addError(element, nameErrorVal);
	            return false;
	        }
	        if (cardHolderName.length > 50) {
	        	this.addError(null, this.errorForMaxLength(parseData.cardHolderNameErrorIdx), errorSelector);
	            // this.addError(element, this.errorForMaxLength(parseData.cardHolderNameErrorIdx));
	            return false;
	        }
	        var Lang = $('#lang').text();
	        if(Lang == 'en'){	        	
	           nameReg = /^[\u002D\u0061-\u007A\u0027\u0041-\u005A \u0020\u0026\u002E+]*$/;
	         }
	        else{
	           nameReg = /^[^\u0030-\u0039]*$/;	                  
	         }      
	        if (!nameReg.test(cardHolderName)) {	        		        
				var nameErrorVal = $('#name-error2').text();
				this.addError(null, nameErrorVal, errorSelector);
				// this.addError(element, nameErrorVal);
	            return false;
	        }		
		}
		else {
			console.log('### made it to first with element = ' + element + cardHolderName);
	        if (cardHolderName.length <= 0 || cardHolderName_new.length == 0) {
	            console.log("cardHolderName"+cardHolderName);
	            this.addError(null, this.errorForEmptyFields(parseData.cardHolderNameErrorIdx), errorSelector);
	            // this.addError(element, this.errorForEmptyFields(parseData.cardHolderNameErrorIdx));
	            return false;
	        }
	        if (cardHolderName.length > 50) {
	        	this.addError(null, this.errorForMaxLength(parseData.cardHolderNameErrorIdx), errorSelector);
	            // this.addError(element, this.errorForMaxLength(parseData.cardHolderNameErrorIdx));
	            return false;
	        }

	        console.log('### made it to second');
	        var Lang = $('#lang').text();
	        if(Lang == 'en'){
	           nameReg = /^[\u002D\u0061-\u007A\u0027\u0041-\u005A \u0020\u0026\u002E+]*$/;
	         }
	        else{
	          	nameReg = /^[^\u0030-\u0039]*$/;	          
	         }      
	        if (!nameReg.test(cardHolderName)) {
	        	this.addError(null, this.errorForInvalideCardName(parseData.cardHolderNameErrorIdx), errorSelector);          
	            // this.addError(element, this.errorForInvalideCardName(parseData.cardHolderNameErrorIdx));

	            return false;
	        }
		
		}
		
		this.removeError(null, errorSelector);
        // this.removeError(element);
        return true;
    },
	// OPS-71512 Changes to validate not supported card Number
    checkSupportedPayment: function(num){
    	
    	var flag;
    	var paymentName = wa.ecom.cardType(num).name;
    	if(paymentName =="American Express"){
    		paymentName = "Amex";
    	}
    	var selectedCountry = $("#countriesDropDown").val(); 
		var supportedPaymentArr = getSupportedPaymentArr(selectedCountry);
		paymentName = paymentName.toUpperCase();
		if(supportedPaymentArr.indexOf(paymentName) != -1){
			flag=1;
		}else{
			flag=0;
		}return flag;
		
    },
//ops-75656
/* This function checks the empty bank and throw error */
 checkBankNameSelected: function() {
        var element = 'bankDropdownList';
        parseData.getErrorMessageIndexs();
        parseData.getCurrentData();
        var bankDropdownList = parseData.bankDropdownList;
        var bankNameErrorVal = $('#bankName-error').text();
       if($("#bankDropdownList").val()==-1){       	
				this.addError(element, bankNameErrorVal);
	            return false;
	        }
        this.removeError(element);
        return true;
    },

	/*This function finds the card type as per the card number entered by the user */
    checkCardType: function() {
        var element = 'cardType';
        parseData.getErrorMessageIndexs();
        parseData.getCurrentData();
        var cardType = parseData.cardType;
        var cardNmbr = parseData.cardNumber;
        if (cardNmbr.length == 0) {
            $(".row.withImg img").hide();
        }
        var num = $(".cardNumber").val();
        var cc = wa.ecom.cardType(num).name;
        var cardTypeSelectedValue = $("#cardTypeDropDown option:selected").val().toLowerCase();
        cc = cc.toLowerCase();
        parseData.cardTypeCheck = parseData.cardTypeCheck != undefined ? parseData.cardTypeCheck.toLowerCase() : "";
        if (cc == "american express" || parseData.cardTypeCheck == "american express") {
            cc = "amex";
            parseData.cardTypeCheck = "amex";
        }
        this.removeError(element);

        if (parseData.isFieldToBeValidated(element)) {
            if (cardType == '-1') {
                this.addError(element, this.errorForEmptyFields(parseData.cardTypeErrorIdx));
                return false;
            }
            if (num.toLowerCase().indexOf("x") >= 0 && parseData.cardTypeCheck != cardTypeSelectedValue) {
                this.addError(element, carNumCardTypeDoestntMatch);
                return false;
            } else if (cc !== cardType && cc != undefined && cc != "" && cc != "unknown") {
                this.addError(element, carNumCardTypeDoestntMatch);
                return false;
            }
        }
        this.removeError(element);
        return true;
    },
    checkCardImage: function(){
        var element = 'cardType';
        parseData.getErrorMessageIndexs();
        parseData.getCurrentData();
        var cardType = parseData.cardType;
        var cardNmbr = parseData.cardNumber;
        if (cardNmbr.length == 0) {
            $(".row.withImg img").hide();

        }
        var num = $(".cardNumber").val();
        var cc = wa.ecom.cardType(num).name;
        parseData.displayCardImage(cc);
        window.userNumberEntry = "1";
    },
    /*This function gets error message if card validity is not selected or inproper*/
    checkCardValidity: function(isMonthChange, isYearChange, isFinalCheck) {
        var element1 = 'month';
        var element2 = 'year';
        this.removeError(element1);
        this.removeError(element2);
        parseData.getErrorMessageIndexs(); // later we will call this only once in ready function
        parseData.getCurrentData();
        var monthVal = parseData.month;
        var yearVal = parseData.year;
		
        if (wa.ecom.validateExpiry(monthVal, yearVal) == false) {
            if (monthVal == -1 || yearVal == -1) {
				if (isYearChange){
					if (monthVal == -1 && yearVal == -1) {
						this.addError(element1, this.errorForEmptyFields(parseData.monthErrorIdx));
						this.addError(element2, this.errorForEmptyFields(parseData.yearErrorIdx));
						window.first_hit = false;
						return false;
					}
				}
               if (monthVal == -1) {
                  if (isMonthChange || isFinalCheck) {
						if (yearVal == -1 && !window.first_hit){
							this.addError(element2, this.errorForEmptyFields(parseData.yearErrorIdx));
						}
                        this.addError(element1, this.errorForEmptyFields(parseData.monthErrorIdx));
						window.first_hit = false;
                        return false;
                    }
					if(!isMonthChange){
                        this.addError(element1, this.errorForEmptyFields(parseData.monthErrorIdx));
                        return false;
                    }
                } 
                if(yearVal == -1) {
                    if (isYearChange || isFinalCheck) {
						this.removeError(element1);
						if (monthVal == -1){
							this.addError(element1, this.errorForEmptyFields(parseData.monthErrorIdx));
						}
                        this.addError(element2, this.errorForEmptyFields(parseData.yearErrorIdx));
						window.first_hit = false;
                        return false;
                    }
                    if(!isYearChange && !window.first_hit){
                        this.addError(element2, this.errorForEmptyFields(parseData.yearErrorIdx));
                        return false;
                    }
                }
            } else {
                for (var i = 0; i < errorMessages[parseData.monthErrorIdx].errorList.length; i++) {
                    if (errorMessages[parseData.monthErrorIdx].errorList[i].validationType == "MonthValidator") {
                        this.MonthValidatorMessage = errorMessages[parseData.monthErrorIdx].errorList[i].errorMessage;
                        this.addError(element1, this.MonthValidatorMessage);
                    }
                }

            }   
            return false;
        }
        return true;
    },
    /*This function gets error message if expiry month or expiry year is not selected
    This funciton gets fired on the click of Next button in the billing section.*/
    checkEmptyEpiryDates: function() {
        var element1 = 'month';
        var element2 = 'year';
        parseData.getErrorMessageIndexs(); // later we will call this only once in ready function
        parseData.getCurrentData();
        var monthVal = parseData.month;
        var yearVal = parseData.year;
        if (monthVal == -1 && yearVal == -1) {
            this.addError(element1, this.errorForEmptyFields(parseData.monthErrorIdx));
            this.addError(element2, this.errorForEmptyFields(parseData.yearErrorIdx));
            return false;
        } else if (monthVal == -1) {
                this.removeError(element2);
                this.addError(element1, this.errorForEmptyFields(parseData.monthErrorIdx));
                return false;
        } else if(yearVal == -1){
                this.removeError(element1);
                this.addError(element2, this.errorForEmptyFields(parseData.yearErrorIdx));
                return false;
        }
        else{
            return true;
        }
        
    },
    /*This function gets error message when card number is not in proper format*/
    checkCardNumber: function() {

        var element = 'cardNumber';
        parseData.getErrorMessageIndexs();
        parseData.getCurrentData();
		
        //remove dashes and spaces
		var cardVal = parseData.cardNumber.replace(/[-\s]/g, '');
    // $("#cardNumber").val(cardVal);
		cty = $("#countriesDropDown option:selected").val();
		countryText = $("#countriesDropDown option:selected").text();

		/*OPS-65357 APAC Changes*/
		/* Place holder for CVV field in case of non amex card */
		if(imgWithId !== 'amex'){
		$('#cvv').attr('placeholder',$('#visa-ph').text());
		}							
			$('#cardNumber').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');
			if(window.geoErrorMsg == 'false' || window.geoErrorMsg == false){
				$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');
			}
			$(".cardNumber").removeClass("redBorder");
			$(".country").removeClass("redBorder");			
															
		if(isNaN(cardVal)){
				for(var i = 0;i<errorMessages[parseData.cardNumberErrorIdx].errorList.length;i++)
					{
						if (errorMessages[parseData.cardNumberErrorIdx].errorList[i].validationType.toLowerCase() == "ccvalidator")
						{
							this.addError(element , errorMessages[parseData.cardNumberErrorIdx].errorList[i].errorMessage); 
							return false;
						}
					}	
			}	
        if (cardVal.indexOf("x") > -1) {
            return true;
        }
        if (cardVal.length <= 0 && cardVal.indexOf("x") == -1) {
            this.addError(element, this.errorForEmptyFields(parseData.cardNumberErrorIdx));
            return false;
        } else if (cardVal.length >= 1 && cardVal.indexOf("x") == -1) {
			//OPS-71512 Changes to validate not supported card Number
			var value = errorCheck.checkSupportedPayment(cardVal);
			var paymentName = wa.ecom.cardType(cardVal).name;
				if(paymentName =="American Express"){
					paymentName = "Amex";

				}
				if(!value){
						$('#cardName').text(paymentName);
						$('#countryNameId').text(countryText);
						var errorMsg = $('#amexCardError').text();
						$('#cardNumber').parent('.errorMessageContainer').find('.errorDiv').removeClass('hidden');
						$('#cardNumber').parent('.errorMessageContainer').find('.errorDiv').text(errorMsg);	
						$(".cardNumber").addClass("redBorder");
						$('#countriesDropDown').parent('.errorMessageContainer').find('.errorDiv').addClass('hidden');
						$(".country").removeClass("redBorder");
						$(".row.withImg img").hide();
						getSupportedImage(); // getSupportedImage
						return false;
				}//OPS-71512 Changes to validate not supported card Number ends	
					
            if (wa.ecom.cardType(cardVal).name != "unknown") {
                var numToHave;
                switch (wa.ecom.cardType(parseData.cardNumber).name) {

                    case "American Express":
                        numToHave = [15];
						/*OPS-65357 APAC Changes*/
						var imgWithId = $('.row.withImg img').attr('id');						
						$('#cvv').attr('placeholder',$('#amex-ph').text());												
                        break;
                    case "Maestro":
                    	numToHave = [12,13,14,15,16,17,18,19];
                    	break;
					default:
	                    numToHave = [16];
	                    break;
                }
                if (numToHave.indexOf(cardVal.length) === -1) {
                    for (var i = 0; i < errorMessages[parseData.cardNumberErrorIdx].errorList.length; i++) {
                        if (errorMessages[parseData.cardNumberErrorIdx].errorList[i].validationType.toLowerCase() == "ccvalidator") {
                            this.addError(element, errorMessages[parseData.cardNumberErrorIdx].errorList[i].errorMessage);
                            $(".row.withImg img").hide();
                            return false;
                        }
                    }
                }
            } else { // there is no card type, so display an error
                for (var i = 0; i < errorMessages[parseData.cardNumberErrorIdx].errorList.length; i++) {
                    if (errorMessages[parseData.cardNumberErrorIdx].errorList[i].validationType.toLowerCase() == "ccvalidator") {
                        this.addError(element, errorMessages[parseData.cardNumberErrorIdx].errorList[i].errorMessage);
                        $(".row.withImg img").hide();
                        return false;
                    }
                }
            }
            if (!wa.ecom.validateCardNumber(cardVal)) {
                for (var i = 0; i < errorMessages[parseData.cardNumberErrorIdx].errorList.length; i++) {
                    if (errorMessages[parseData.cardNumberErrorIdx].errorList[i].validationType.toLowerCase() == "ccvalidator") {
                        this.addError(element, errorMessages[parseData.cardNumberErrorIdx].errorList[i].errorMessage);
                        $(".row.withImg img").hide();
                        return false;
                    }
                }
            }

        }
			
        this.removeError(element);
		//ops 77539
        getSupportedImage(); // getSupportedImage
        return true;

    },
    checkSecurityCode: function() {
        var element = 'cvv';
        parseData.getErrorMessageIndexs(); // later we will call this only once in ready function
        parseData.getCurrentData();
        var securityCodeVal = parseData.securityCode;
        var cardTypeVal = $(".row.withImg img").attr("id");
        var identifier = "";
        if (securityCodeVal.length <= 0) {
            this.addError(element, this.errorForEmptyFields(parseData.securityCodeErrorIdx));
            $('#cvv').val('');
            return false;

        }
         if(typeof cardTypeVal != 'undefined'){
                if (cardTypeVal.toUpperCase() == "AMEX") {
                identifier = cardTypeVal;
                if (securityCodeVal.length != 4) {
                    for (var i = 0; i < errorMessages[parseData.securityCodeErrorIdx].errorList.length; i++) {
                        if (errorMessages[parseData.securityCodeErrorIdx].errorList[i].validationType == "MinLengthValidator") {
                            var errorMessage = errorMessages[parseData.securityCodeErrorIdx].errorList[i].errorMessage;
                            errorMessage = errorMessage.indexOf('${val}') >= 0 ? errorMessage.replace('${val}', '4') : errorMessage;
                            this.addError(element, errorMessage);
                            $('#cvv').val('');
                            return false;
                        }
                    }
                } else if (wa.ecom.validateCVC(securityCodeVal) == false) {
                    for (var i = 0; i < errorMessages[parseData.securityCodeErrorIdx].errorList.length; i++) {
                        if (errorMessages[parseData.securityCodeErrorIdx].errorList[i].validationType == "IntValidator") {
                            this.addError(element, errorMessages[parseData.securityCodeErrorIdx].errorList[i].errorMessage);
                            $('#cvv').val('');
                            return false;
                        }
                    }
                }
                this.removeError(element);
                return true;
            }
        }

        if (securityCodeVal.length != 3) {

            for (var i = 0; i < errorMessages[parseData.securityCodeErrorIdx].errorList.length; i++)

            {
                if (errorMessages[parseData.securityCodeErrorIdx].errorList[i].validationType == "MinLengthValidator") {
                    var errorMessage = errorMessages[parseData.securityCodeErrorIdx].errorList[i].errorMessage;
                    errorMessage = errorMessage.indexOf('${val}') >= 0 ? errorMessage.replace('${val}', '3') : errorMessage;
                    this.addError(element, errorMessage);
                    $('#cvv').val('');
                    return false;
                }
            }
        } else if (wa.ecom.validateCVC(securityCodeVal) == false) {
            for (var i = 0; i < errorMessages[parseData.securityCodeErrorIdx].errorList.length; i++) {
                if (errorMessages[parseData.securityCodeErrorIdx].errorList[i].validationType == "IntValidator") {
                    this.addError(element, errorMessages[parseData.securityCodeErrorIdx].errorList[i].errorMessage);
                    $('#cvv').val('');
                    return false;
                }
            }

        }

        this.removeError(element);
        return true;
    },
    /** This function gets error message if city is empty or length is more than max limit.
     */
    checkCity: function(element, parseDataPropName, errorSelector) {

        if (parseData.isFieldToBeValidated(element)) {
            var cityVal = parseData[parseDataPropName];            
            var cityVal_new = $.trim(cityVal);
           /* Removing below per APAC field rediction OPS-70572 */
           /*Boleto Changes */ 						        
		//WorldPay changes OPS-73496
		var billingCountry = $("#countriesDropDown option:selected").val();  // don't really need counterpart for shipping city
		var incomingCountryCityList = window.mandatoryCityFieldCountryList.indexOf(countryCodeId);
		var billingCountryCityList = window.mandatoryCityFieldCountryList.indexOf(billingCountry);
		if((typeof incomingCountryCityList !== 'undefined' && incomingCountryCityList !== -1) || (typeof billingCountryCityList !== 'undefined' && billingCountryCityList !== -1) || element === 'city-ship'){
	            if (cityVal.length <= 0 || cityVal_new.length == 0) {
	                this.addError(null, this.errorForEmptyFields(parseData.cityErrorIdx), errorSelector);
	                return false;
	            }
        }

            if (cityVal.length > 50) {
                this.addError(null, this.errorForMaxLength(parseData.cityErrorIdx), errorSelector);
                return false;

            }
            //Fix to allow all characters in city field 
            var Lang = $('#lang').text();
            if(Lang == 'en'){
                letters = /^([A-Z a-z\\'\\s.-]*)$/;
            }
            else{
                letters = /^[^\u0030-\u0039]*$/;
            }
            
            if (!letters.test(cityVal)) {
                console.log("inside invalid characters");
                this.addError(null, this.errorAlphaOnlyFields(parseData.cityErrorIdx), errorSelector);
                return false;
            }

        }

        this.removeError(null, errorSelector);
        return true;

    },
    /*This function check whether user has selected the state */
    checkState: function() {

        var element = 'state';
        var stateVal = parseData.state;

        if ($('.stateDropdown').is(':visible')) {
            element = 'stateDropdown';
        }

        if ($('.optionalState').is(':visible')) {
            element = 'optionalState';
        }        
        var ctyCode = parseData.country;
        var foundIdx = -1; // set a flag to reference the number
        for (var countries = 0; countries < countryStates.length; countries++) {

            if (ctyCode == countryStates[countries].countryCode) {
                foundIdx = countries; // set our flag to true
                break;
            }
        }
        if (foundIdx > -1) {
            if (countryStates[foundIdx].stateMandatory) {
                if (stateVal.length <= 0 || stateVal == 0) {
                    this.addError(element, this.errorForEmptyFields(parseData.stateErrorIdx));
                    return false;
                }
            }
        }

        this.removeError(element);
        return true;

    },
    checkPhone: function(element, countriesDropDownSelector, callingCodeSelector, mobileNumberSelector, phoneErrorSelector, parseDataPhonePropName, phoneNumberSelector, isShipping) {
    	element = element || 'phone';
		countriesDropDownSelector = countriesDropDownSelector || '#countriesDropDown';
		callingCodeSelector = callingCodeSelector || '#callingCode';
		mobileNumberSelector = mobileNumberSelector || '#mobileNumber';
		phoneErrorSelector = phoneErrorSelector || '#phoneError';
		parseDataPhonePropName = parseDataPhonePropName || 'phone';
		phoneNumberSelector = phoneNumberSelector || '#phoneNumber';
		isShipping = isShipping || false;

        var maxLengthPhn = "";
        var billingCty= $(countriesDropDownSelector + " option:selected").val();
        var selectedCountryCode = $(callingCodeSelector + " option:selected").attr("data-countryCode");
        var placeholder = $(mobileNumberSelector).attr('placeholder');
        var phoneErrorVal = $(phoneErrorSelector).text();      
        if (parseData.isFieldToBeValidated(element)) {
            var phoneVal = parseData[parseDataPhonePropName];	
           	//OPS-63974 phone number validation changes   
            if (phoneVal.length > 0){
            	if (!isValidPhoneNumber(phoneVal, placeholder)){
	            	this.addError(null, '', callingCodeSelector);
	                this.addError(null, this.errorForInvalidePhone(parseData.phoneErrorIdx), mobileNumberSelector);
	                return false;
                }
                else if (!isShipping && phoneVal.length > 0 && (($('#paymentMethod').val()=='HP' && selectedCountryCode !=="MX") || ((typeof pspValue !== 'undefined' && pspValue == "NETPROPHETS") && billingCty == "IN" && selectedCountryCode !=="IN") || ($('#paymentMethod').val()=='HP_QIWI-SSL' && selectedCountryCode !=="RU"))) {	            
					this.addError(null,'',callingCodeSelector);
	                this.addError(null, phoneErrorVal, mobileNumberSelector);
	                return false;
           		}           		
     		}         
            //Oxxo Changes OPS-73492 and India Payments changes OPS-75651
            //WorldPay Changes
            if (!isShipping && phoneVal.length <= 0 && (($('#paymentMethod').val()=='HP') || ((typeof pspValue !== 'undefined' && pspValue == "NETPROPHETS") && billingCty == "IN") || ($('#paymentMethod').val()=='HP_QIWI-SSL'))) {
				this.addError(null,'',callingCodeSelector);
                this.addError(null, this.errorForEmptyPhone(parseData.phoneErrorIdx), mobileNumberSelector);
                return false;
            }        
            if (phoneVal.length > 50) {            	
                this.addError(null, this.errorForMaxLength(parseData.phoneErrorIdx), mobileNumberSelector);
                return false;
            }

            var tempNum = parseData[parseDataPhonePropName].replace(/[^\d]/g, ''); // send only digits to backend
            tempNum = tempNum != "" ? tempNum : "";
            $(phoneNumberSelector).val(tempNum);
            
        }
        this.removeError(null, mobileNumberSelector);
        this.removeError(null, callingCodeSelector);
        return true;

    },
	/*OPS-65357 NSB Changes*/
	/* This function is for nsb phone number validation */
	checknsbPhone: function() {
        var element = 'nsbPhoneNumber';
        var nsbphonecode = 'phoneCode';
        var maxLengthPhn = "";
        var billingCty= $("#countriesDropDown option:selected").val();
        var selectedCountryCode = $("#callingCode option:selected").attr("data-countryCode");
        var placeholder = $('#nsbMobileNumber').attr('placeholder');
        var phoneErrorVal = $('#phoneError').text();      
        if (parseData.isFieldToBeValidated(element)) {
            var phoneVal = parseData.nsbPhoneNumber;
            //OPS-63974 phone number validation changes         
             if (phoneVal.length > 0){
            	if (!isValidPhoneNumber(phoneVal, placeholder)){
	            	this.addError(nsbphonecode, '');
	                this.addError(element, this.errorForInvalidePhone(parseData.phoneErrorIdx));
	                return false;
                }
                else if (phoneVal.length > 0 && (($('#paymentMethod').val()=='HP' && selectedCountryCode !=="MX") || ((typeof pspValue !== 'undefined' && pspValue == "NETPROPHETS") && billingCty == "IN" && selectedCountryCode !=="IN") || ($('#paymentMethod').val()=='HP_QIWI-SSL' && selectedCountryCode !=="RU"))) {	            	
					this.addError(nsbphonecode,'');
	                this.addError(element, phoneErrorVal);
	                return false;
           		 }       
     		}       
             //Oxxo Changes OPS-73492 and India Payments changes OPS-75651   
             //WorldPay Changes  
            if (phoneVal.length <= 0 && (($('#paymentMethod').val()=='HP') || ((typeof pspValue !== 'undefined' && pspValue == "NETPROPHETS") && billingCty == "IN") || ($('#paymentMethod').val()=='HP_QIWI-SSL'))) {
                this.addError(element, this.errorForEmptyPhone(parseData.phoneErrorIdx));
                return false;
            }            
            if (phoneVal.length > 50) {            	
                this.addError(element, this.errorForMaxLength(parseData.phoneErrorIdx));
                return false;
            }
            var tempNum = parseData.nsbPhoneNumber.replace(/[^\d]/g, ''); // send only digits to backend
            tempNum = tempNum != "" ? tempNum : "";
            $("#nsbPhoneNumber").val(tempNum);

        }
        this.removeError(element);
         this.removeError(nsbphonecode);
        return true;

    },
    /** This function gets error message if zip is empty or not in proper format.
     *  @param {string} element The name of the element to verify.
     *  @param {string} parseDataZipCodePropName  The name of the property of the parseData object to get value to verify from.
     *  @param {string} errorSelector In case of error, the name of the selector to apply error message to.
     */
    checkZipCode: function(element, parseDataZipCodePropName, errorSelector, parseDataCountrySelector) {
    	element = element || 'zipOrPostalCode';
		parseDataZipCodePropName = parseDataZipCodePropName || 'zipCode';
		errorSelector = errorSelector || '#zipCode';
		parseDataCountrySelector = parseDataCountrySelector || 'country';

        if (parseData.isFieldToBeValidated(element)) {
            var zipCodeVal = parseData[parseDataZipCodePropName];
            zipCodeVal = $.trim(zipCodeVal);
            
            var currentCountry = parseData[parseDataCountrySelector]; 
            var identifier = "";

            switch (currentCountry) {
                case "US":
                    postalCodeRegex = /^([0-9]{5})(?:[-\s]{0,1}([0-9]{4}))?$/;
                    identifier = "US";
                    break;
                case "CA":
                    postalCodeRegex = /^([A-Za-z][0-9][A-Za-z])\s{0,1}([0-9][A-Za-z][0-9])$/;
                    identifier = "CA";
                    break;  
                default:
                    postalCodeRegex = /^.*$/;
                    identifier = "";
                    break;
            }
            var ctryCde = $('#countriesDropDown option:selected').val();
            var selectedPaymentMethod = $('#paymentMethod').val();					
            var foundIdx = -1; // set a flag to reference the number
            for (var countries = 0; countries < countryStates.length; countries++) {
                if (ctryCde == countryStates[countries].countryCode) {
                    foundIdx = countries; // set our flag to true
                    break;
                }
            }

            if (foundIdx > -1) {
                var zipMandatory;
                if (countryStates[foundIdx].zipMandatory) {
                    zipMandatory = true
                } else {
                    zipMandatory = false;
                }

                if (zipMandatory) {
                    if (zipCodeVal.length <= 0) {
                        this.addError(null, this.errorForEmptyFields(parseData.zipCodeErrorIdx), errorSelector);
                        return false;
                    }
			  		if ((!postalCodeRegex.test(zipCodeVal)) && (cty !== 'BR' & typeof cty !=='undefined')) {
			                        for (var i = 0; i < errorMessages[parseData.zipCodeErrorIdx].errorList.length; i++) {
			                            if ((errorMessages[parseData.zipCodeErrorIdx].errorList[i].validationType == "ZipCodeValidator")){
			                                this.addError(null, errorMessages[parseData.zipCodeErrorIdx].errorList[i].errorMessage, errorSelector);
			                                return false;
			                            }
			                        }
			        }			                  			            
			      	if ((cty == 'BR' & typeof cty !=='undefined')|| (countryCodeId == 'BR' && typeof countryCodeId !== 'undefined')){      		
			      		if (zipCodeVal.length !== 8 || isNaN(zipCodeVal)){
			                        for (var i = 0; i < errorMessages[parseData.zipCodeErrorIdx].errorList.length; i++) {
			                            if ((errorMessages[parseData.zipCodeErrorIdx].errorList[i].validationType == "ZipCodeValidator")){
			                                this.addError(null, errorMessages[parseData.zipCodeErrorIdx].errorList[i].errorMessage, errorSelector);
			                                return false;
			                            }
			                        }
			     		}  
			     	}                        
                }//zipCodeMandatory
                if ((typeof selectedPaymentMethod !== 'undefined') && (selectedPaymentMethod == 'HP_QIWI-SSL' || selectedPaymentMethod == 'HP_WEBMONEY-SSL' || selectedPaymentMethod == 'HP_YANDEXMONEY-SSL' || selectedPaymentMethod == 'HP_PRZELEWY-SSL')){
     				if (zipCodeVal.length <= 0) {
                        this.addError(null, this.errorForEmptyFields(parseData.zipCodeErrorIdx), errorSelector);
                        return false;
                    }
     			}
            }
        }
        this.removeError(null, errorSelector);
        return true;

    },
	 /*OPS-65357 NSB Changes*/
	 /* This function will validate Company field */
    checkCompany: function() {

        var element = 'company';

        if (parseData.isFieldToBeValidated(element)) {
            var companyVal = parseData.company;
            
            var companyVal_new = $.trim(companyVal);
            if (companyVal.length > 50) {
                this.addError(element, this.errorForMaxLength(parseData.companyErrorIdx));
                return false;

            }            
        }

        this.removeError(element);
        return true;

    },
	/*OPS-65357 NSB Changes*/
	/*NSB Tax ID validation rule*/
    checktaxId: function() {
        var element = 'taxId';
        if (parseData.isFieldToBeValidated(element)) {
            var taxIdVal = parseData.taxId;
            
        taxIdReg = /^[\u0061-\u007A\u0041-\u005A\u0030-\u0039 \\u0020+]*$/;
        var taxIdVal_new = $.trim(taxIdVal);
	
	        if (taxIdVal.length > 50) {
	            this.addError(element, this.errorForMaxLength(parseData.taxIdErrorIdx));
	            return false;
	        }
			
	        if (!taxIdReg.test(taxIdVal)) {            
	            this.addError(element, this.errorForInvalidtaxId(parseData.taxIdErrorIdx));
	            return false;
	        }
		
		}
		
        this.removeError(element);
        return true;
    },

    /*OPS-63895 Boleto Changes*/
	/*cpfcnpj validation rule*/
    checkcpfCnpj: function() {
        var element = 'cpfcnpj';
        if (parseData.isFieldToBeValidated(element)) {
            var cpfcnpjVal = parseData.cpfcnpj;
            	cpfcnpjVal = $('input.cpfcnpj').val();
				cpfcnpjVal = cpfcnpjVal.replace(/\//g, '');
				cpfcnpjVal = cpfcnpjVal.replace(/-/g, '');
				cpfcnpjVal = cpfcnpjVal.replace(/\./g, '');
            
		var cpfcnpjVal_new = $.trim(cpfcnpjVal);
			
		 if (cpfcnpjVal_new.length > 0) {
		 	if(!(cpfcnpjVal_new.length ==11 || cpfcnpjVal_new.length ==14)){
                this.addError(element, this.errorForInvalidcpfcnpj(parseData.cpfcnpjErrorIdx));
                return false;
            }  
         }   
        
			if (cpfcnpjVal.length <= 0 || cpfcnpjVal_new.length == 0) {
	            this.addError(element, this.errorForEmptyFields(parseData.cpfcnpjErrorIdx));
	            return false;
	        }
		}
		
        this.removeError(element);
        return true;
    },

    /*OPS-65357 NSB Changes*/
	 /* This function will validate Company field */
    checkaddress: function(element, parseDataPropName, errorSelector) {

        var element = [element];
        // var element = 'address';

        console.log('inside checkaddress' + 'value of element = ' );
        console.log(element);

        if (parseData.isFieldToBeValidated(element)) {
        	var addressVal = parseData[parseDataPropName];
            // var addressVal = parseData.address;
            var addressVal_new = $.trim(addressVal);
            
            if (addressVal.length > 50) {
            	this.addError(null, this.errorForMaxLength(parseData.addressErrorIdx), errorSelector);
                // this.addError(element, this.errorForMaxLength(parseData.addressErrorIdx));
                return false;

            }
            if ((element == 'address') || (element == 'address-ship')) {
				if (addressVal.length <= 0 || addressVal_new.length == 0) {
					this.addError(null, this.errorForEmptyFields(parseData.addressErrorIdx), errorSelector);
		            // this.addError(element, this.errorForEmptyFields(parseData.addressErrorIdx));
		            return false;
		        }
		    }        
        }

        this.removeError(null, errorSelector);
        // this.removeError(element);
        return true;

    },

	
    autoCheck: function(element) {
        parseData.getCurrentData();
        if (element) {
            switch (element)

            {
                case EMAILID:
                    errorCheck.checkEmail();
                    break;
                case USERID:
                    errorCheck.checkUserId();
                    break;
                case VEPWDID:
                    errorCheck.checkVerifyPwd();
                    break;
                case STATEID:
                    errorCheck.checkState();
                    break;
                case PHONEID:
                    errorCheck.checkPhone();
                    break;
                case ZIPID:
                    errorCheck.checkZipCode();
                    break;
				/*OPS-65357 NSB Changes*/
				case COMPANYID:
                    errorCheck.checkCompany();
                    break;
				case TAXID:
                    errorCheck.checktaxId();
                    break;
				case NSBPHONEID:
                    errorCheck.checknsbPhone();
                    break;
                case CPFCNPJID:
                    errorCheck.checkcpfCnpj();
                    break; 
                case ADDRESSID:
                    errorCheck.checkaddress();
                    break;     
            }
        }
    },


}

// End of validation code


/**
 *  This helper conditionally wraps anchor tags around html.
 *  @param flag The flag to whether wrap html with <a> tags. Will render only if flag is true or "true"
 *  @param options.hash.* Any property added to the hash will be added to the anchor. The property href is required.
 *  Usage ex. {{#symcConditionalAnchor flagFromContext href=hrefFromContext target="_blank"}}html code goes here{{/symcConditionalAnchor}}
 */
Handlebars.registerHelper('symcConditionalAnchor', function(flag, options) {
  if ((flag === true || flag === 'true') && options.hash.href) {
    var html = '<a';
    for (var prop in options.hash){
      html += (' ' + prop + '="' + options.hash[prop] + '"');
    }
    html += ('>'+ options.fn(this) + '</a>');
    return html;
  }
  return options.fn(this);
});


/**
 *  This helper renders html base on a flag.
 *  @param flag This flag determines what to render. Flag does not have to be a boolean (e.g. 0 evaluates to false)
 *  Usage ex. {{#symcIfEqual value1}} Renders if flag evaluates to true {{else}} Renders if flag evaluates to false {{/symcIfEqual}}
 */
Handlebars.registerHelper('symcIf', function(flag, options) {
  if(flag) {
    return options.fn(this);
  }
  return options.inverse(this);
});

/**
 *  This function refreshes several templates
 */
function refreshTemplates(data) {

	// Disable all payment methods except CC if optinTrialware or optinEETrialWare
	if (data.optinTrialWare === true || data.optinEETrialWare === true){
		if (window.methodOfPaymentList){
			window.methodOfPaymentList = {CC: window.methodOfPaymentList.CC};
		}
		if (data.methodOfPaymentList){
			data.methodOfPaymentList = {CC: data.methodOfPaymentList.CC};
		}
	}

  var templateSource = $("#cart-template").html();
  var template = Handlebars.compile(templateSource);
  cartHTML = template(data);
  $("#productInfoBox").html(cartHTML);
  console.log("Inside Update cartHTML:");

  // removing the href link in supporting OS copy in minicart and mf cart - static content has a href hardcoded
  $("#osSupport a").removeAttr("href");
  $("#cartOsSupport a").removeAttr("href");

  var totalTemplateSource = $("#total-template").html();
  var totalTemplate = Handlebars.compile(totalTemplateSource);
  totalHTML = totalTemplate(data);
  $(".subTotal").html(totalHTML);
  $(".cartTotal").html(totalHTML);

  var candyTemplateSource = $("#candy-template").html();
  var candyTemplate = Handlebars.compile(candyTemplateSource);
  candyHTML = candyTemplate(data);
  $("#candyRack").html(candyHTML);
  $(".candyRackOverlay").html(candyHTML);
  console.log("Inside Update candyRack:");

  var overlayTemplateSource = $("#overlay-template").html();
  var overlayTemplate = Handlebars.compile(overlayTemplateSource);
  overlayHTML = overlayTemplate(data);
  $(".cartContentContainer").html(overlayHTML);
  
  //arTemplate
  var arTemplateSource = $("#arMessage-template").html();
  var arTemplate = Handlebars.compile(arTemplateSource);
  var arHTML = arTemplate(data);
  $("#arMessage").html(arHTML);
  
  //order review 
  var reviewTemplateSource = $("#review-template").html();
  var reviewTemplate = Handlebars.compile(reviewTemplateSource);
  var reviewHTML = reviewTemplate(data);
  $("#orderReview").html(reviewHTML);

  //customer template
  var customerTemplateSource = $("#customer-template").html();
  var customerTemplate = Handlebars.compile(customerTemplateSource);
  var customerHTML = customerTemplate(data);
  $("#customerInfo").html(customerHTML);
  // Eula
  var eulaTemplateSource = $("#eula-template").html();
  var eulaTemplate = Handlebars.compile(eulaTemplateSource);
  var eulaHTML = eulaTemplate(data);
  $(".eulaPanel").html(eulaHTML);

  //reviewTotal
  var reviewTotalTemplateSource = $("#reviewTotal-template").html();
  var reviewTotalTemplate = Handlebars.compile(reviewTotalTemplateSource);
  var reviewTotalHTML = reviewTotalTemplate(data);
  $("#reviewTotal").html(reviewTotalHTML);
  
  var emptyCartTemplateSource = $("#emptyCart-template").html();
  var emptyCartTemplate = Handlebars.compile(emptyCartTemplateSource);
  var emptyCartHTML = emptyCartTemplate(data);
  $("#emptyCart").html(emptyCartHTML);

  //Forgot Password Link
  var pwdTemplateSource = $("#forgotPassword-template").html();
  var pwdTemplate = Handlebars.compile(pwdTemplateSource);
  pwdHTML = pwdTemplate(data);
  $(".forgotPassword").html(pwdHTML);

  var crumbTemplateSource = $("#crumb-template").html();
  var crumbTemplate = Handlebars.compile(crumbTemplateSource);
  crumbHTML = crumbTemplate(data);
  $(".breadCrumb").html(crumbHTML);
  
  //candyrack coupon box
	if (typeof data.cartCoupons !== 'undefined') {
	  if(typeof data.cartCoupons[0] !== 'undefined' && data.cartCoupons[0] != ""){          
	    $('#Norton_Cart .discountRow').show();  
	    $('#Norton_Cart .couponRow').hide();
	    /* Added for update discount while update CandyRack */
	    $(".discountRow span#discountAmount, #overlayDiscount").text(data.cartCoupons[0]);
	    $(".discountRow span#discountAmount, #overlayDiscountAmount").text(data.discountAmount);
	    console.log("discountAmount :" + discountAmount);
	    $(".discountRow #discountAmount, #overlayDiscountAmount").prepend("-");         
	  }
	}
	else{
	  $('#Norton_Cart .couponRow').show();
	  $('#Norton_Cart .discountRow').hide();  
	}

	// If condition is fix for OPS-85825
	if(typeof countryStates !== "undefined"){
	  var selectedPrevCountry =  $('#countriesDropDown').val();
	  var defaultCountry = getDefaultCountry(countryStates);
	  var countryTemplateSource = $("#country-template").html();
	  var countryTemplate = Handlebars.compile(countryTemplateSource);
	  var countryHTML = countryTemplate(data);
	  $("#countryDiv").html(countryHTML);  	
	  if (data.walletInfo && data.walletInfo.country){  	
	    $('#countriesDropDown').val(data.walletInfo.country);
	  }else if (selectedPrevCountry) { //to set the prev country before refreshing the temp data  	
	  	$('#countriesDropDown option[value='+selectedPrevCountry+']').attr("selected","selected");
	  }
	  else if (defaultCountry && defaultCountry.countryCode) { // default according to api
	    $('#countriesDropDown').val(defaultCountry.countryCode);
	  }
	  else { // hard-coded default
	    $('#countriesDropDown').val('US');
	  }
	}													
	/* OPS-52844 Messaging for Non-AR'able products and Non AR'able MOP */
	var billingCty = $("#countriesDropDown").val();
	if (typeof data.enableNonArableCheckBox !== 'undefined'){
		if (data.enableNonArableCheckBox == true){
			console.log("enableNonArableCheckBox" +data.enableNonArableCheckBox);
				//India Payments Consent OPS-75651						
				if(typeof data.pspValue !== 'undefined' && data.pspValue == "NETPROPHETS" && billingCty =="IN"){	
					$('#nonArConsent').show();
					$('#merchantCopy').show();		
				}
				else{
					$('#nonArEula').show();
					$('#arEula').hide();
					$('#merchantCopy').hide();		
				}												
					$('#arHeader').hide();
					$('#arHelpLink').hide();
					$('#arMessage').removeClass('sub-info-border-top');

				/*Subscription activation message OPS-77366*/			
				if((typeof data.cartHavingESD !== 'undefined' && data.cartHavingESD == true )) {
					$('.nonARactivateMessage').show();								
				}
				else {
					$('.nonARactivateMessage').hide();							
				}	
			}
			else{
				//India Payments Consent OPS-75651			
				if(typeof data.pspValue !== 'undefined' && data.pspValue == "NETPROPHETS" && billingCty =="IN"){
					$('#arConsent').show();
					$('#merchantCopy').show();
				}
				else{
					console.log("enableNonArableCheckBox" +data.enableNonArableCheckBox);						
					$('#arEula').show();
					$('#nonArEula').hide();
					$('#merchantCopy').hide();							
				}
				$('#arMessage').addClass('sub-info-border-top');
				/*Subscription activation message OPS-75601*/			
				if((typeof data.cartHavingESD !== 'undefined' && data.cartHavingESD == true )) {
					$('.activateMessage').show();								
				}
				else {
					$('.activateMessage').hide();							
				}						
			}
		}

		/* Added for OPS-60279 and OPS-65168 Hide Checkbox for Marketing emails in US
		Moving Marketing Options checkBox in review Section*/ 
	    if (typeof data.states !== 'undefined'){
	      for (i = 0; i < data.states.length; i++) {
	             states = data.states[i];           
	        if (data.states !== 'undefined'){                                         
	          if (data.states[i].default == true){
	                if (data.states[i].countryCode == "US"){
	                  $('.emailCheckbox').hide();
	                }
	            else{
	                  $('.emailCheckbox').show();
	              }
	               
	          }   
	        } 
	      }     
	    }

		/*OPS-65357 show/hide NSB fields in removeCart*/
		if (data.cartContainsNSB == true && typeof data.cartContainsNSB !== 'undefined'){
		console.log('cart container NSB is ' + data.cartContainsNSB);
		$("#phoneDiv").find(".selectedCallingCode").remove();
		$('#phoneDiv').hide();
		$('#companyDiv').show();
		$('#taxIdDiv').show();
		$('#nsbPhoneDiv').show();
		//OPS-63974 
		if($("#nsbPhoneDiv .selectedCallingCode").length==0){
			$("#nsbPhoneDiv .errorMessageContainer").prepend(phoneCodeDropdownStr);
			createCallingCodeDropdown();
		}
		}
		else{
			$("#nsbPhoneDiv").find(".selectedCallingCode").remove();
		$('#phoneDiv').show();
		$('#companyDiv').hide();
		$('#taxIdDiv').hide();
		$('#nsbPhoneDiv').hide();
		//OPS-63974
		if($("#phoneDiv .selectedCallingCode").length==0){
			$("#phoneDiv .errorMessageContainer").prepend(phoneCodeDropdownStr);
			createCallingCodeDropdown();
		}
		}

		// Shipping
		if ($('#callingCodeShip').length === 0){
			var phoneCodeDropdownStrShip = phoneCodeDropdownStr.replace('id="callingCode"', 'id="callingCodeShip"').
				replace('id="callingCodeValue"', 'id="callingCodeValueShip"');
		$('#phoneDiv-ship .errorMessageContainer').prepend(phoneCodeDropdownStrShip);
		createCallingCodeDropdown('#callingCodeShip', '#callingCodeValueShip', '#countryDropShip', '#mobileNumber-ship', 
		  	'#phoneNumber-ship');
		}

		// OPS-85007 bug fix for ITP
		// *** Code for ITP starts *** 
		if(data.existingCustomer == true && typeof data.cartItems !== "undefined"){
		    for(var i=0; i<data.cartItems.length;i++){
		    	if(data.cartItems[i].hasOwnProperty("noDisplayITPInCartFlag")){
		            if(data.cartItems[i].noDisplayITPInCartFlag && data.cartItems[i].noDisplayITPInCartFlag != ""){
		                var skuCode = data.cartItems[i].skuCode;
		                $('#parentPrice'+skuCode).parent().parent().hide();
		                $('#parentPrice'+skuCode).parent().parent().prev('a').hide();              
		            }
		        }
		    }
		}
		// *** Code for ITP ends  ***

		/* CAP-11049 Changes (Adding 45 days message and entitlement )	*/
		if (data.optinTrialWare === true || data.optinEETrialWare === true){
												
			$('.subscriptionTerms').show();
			$('.subscriptionReview').show();
			$('.subscriptionMessageReview').show();				
			$('.subscription-style').show();
			$('.subscription-style-mini').show();
			$('.subscriptionTermsOverlay').show();
			$('.subscription-style-overlay').show();
													
			for(var i = 0; i < data.cartItems.length; i++) {
				$("#subscriptionPrice,#subscription-ar-price,#subscriptionPriceOverlay,#subscription-ar-price-overlay,#subscriptionPriceReview,#subscriptionTotalValue").text(data.cartItems[i].optinTrialArPrice);
				$("#subscriptionEntitlementDetails,#subscriptionMessageReview,#subscriptionEntitlementDetailsCartOverlay").text(data.optinTrialWare ? data.cartItems[i].optinTrialSubscriptionMsg : '');										
			}
		}

		/*OPS-60936 Changes for O365*/      
		if(data.cartContainsO365 == true && typeof data.cartContainsO365 !== 'undefined') {
			console.log("O365 Product GetCart" +data.cartContainsO365);
			$('#candyRack').hide();
			$('#candyRackOverlay').hide();
			$('#productSide').removeClass('col-sm-8').addClass('col-sm-12');
		      
			for (var j = 0; j < data.cartItems.length; j++) {

		   	if (data.cartItems[j].o365BundleItem == true && typeof data.cartItems[j].o365BundleItem !== 'undefined') {
		    	console.log("O365 Parent GetCart" +data.cartItems[j].o365BundleItem);
		     
		    	var skuCode = data.cartItems[j].skuCode;
		    	console.log("assigned skuCode" +skuCode);
		     
		    	/*Mini Cart*/
			    $('#parentPrice'+skuCode).hide();             
			    $('#BundlePrice'+skuCode).show();             
			    /* Overlay */
			    $('#OverlayPrice'+skuCode).hide();              
			    $('#overlayBundlePrice'+skuCode).show();              
			    /*Review*/
			    $('#reviewCartItems #reviewPrice' + skuCode).hide();            
		   	}
		   
			  	else {            
			      var skuCode = data.cartItems[j].skuCode;
			      console.log("assigned skuCode in else" +skuCode);
			      /* Mini Cart*/
			      $('#parentPrice'+skuCode).show();
			      $('#BundlePrice'+skuCode).hide();
			      /* Overlay */
			      $('#OverlayPrice'+skuCode).show();
			      $('#overlayBundlePrice'+skuCode).hide();                
			      /*Review*/
			      $('#reviewCartItems #reviewPrice' + skuCode).show();
			   }
		               
			  for (var k = 0; k < data.cartItems[j].childItemResult.length; k++ ){
			     if (data.cartItems[j].childItemResult[k].o365Product == true && typeof data.cartItems[j].childItemResult[k].o365Product !== 'undefined') {
			      console.log("O365 child GetCart" +data.cartItems[j].childItemResult[k].o365Product);

			      var skuCode = data.cartItems[j].childItemResult[k].skuCode;
			      console.log("assigned skuCode in child" +skuCode);
			      /* Mini Cart*/
			      $('#childPrice'+skuCode).hide();
			      $('#childInfoRemove'+skuCode).hide();               
			      /* Overlay */
			      $('#overlayChildPrice'+skuCode).hide();
			      $('#removeChildProductText'+skuCode).hide();
			      /*Review*/                
			      $('#reviewChildPrice'+skuCode).show();                                        
			      $('.childItems #msOffice'+skuCode).show();

			      // replaces product price in review section with bundle price
			      var bundlePrice = $('#reviewBundlePrice'+skuCode).html();
			      $('#reviewChildPrice'+skuCode).html(bundlePrice);
			      
			     }
			  }
			}         
		}
		// Norton Rover will be the only item in the cart for checkout during Pre-sale - Acquistion OPS-95691
		if(data.enableCoreMessage){
			if(data.preOrder && data.cartContainsPhysicalProduct) {
					if (typeof data.removedItemCoreMessage !== 'undefined') {
						$('#physicalProductMsg').show();
						$('#physicalProductMsgCart').show();
					}
					var fadeOUT = function(){
					  $('#physicalProductMsg').fadeOut('slow');
					  $('#physicalProductMsgCart').fadeOut('slow');
					};
					setTimeout(fadeOUT, 10000);	
			}
		}
		else {
			if (typeof data.coreProductVo !== 'undefined') {
				$('#removeCoreProductMessagingCart').show();
				$('#coreProductMsg').show();	
				var fadeOUT = function(){
				  $('#removeCoreProductMessagingCart').fadeOut('slow');
				  $('#coreProductMsg').fadeOut('slow');
				};
				setTimeout(fadeOUT, 10000);
			}  
		}     

  /*Boleto Changes OPS-63895 */
  if (typeof data.states !== 'undefined' && cty === "BR" ) {
    $('#alpagoText').show();
           
  } else {
    $('#alpagoText').hide();
  }

  /*OXXO Changes OPS-63895 */
  if (typeof data.states !== 'undefined' && cty === "MX" ) {
    $('#dineromailText').show();          
  } else {
    $('#dineromailText').hide();
  } 

	/* OPS-65168 Marketing Options moved to review Section */
	/* Added to get Marketing Options selected based on API on logOut CAP-11382 and OPS-65168*/
	if (typeof data.marketingOptions !== 'undefined') {
		
		for (var j = 0; j < data.marketingOptions.length; j++) {
		   console.log("Marketing options Selected "+data.marketingOptions[j].selected);
			if(data.marketingOptions[j].selected == true){
				console.log("Marketing options Selected inside"+data.marketingOptions[j].selected);						
				$('#e1').prop('checked', true);						
			}
		}
		
	}										
		
	/*OPS-63938 Merge checkout - renewal and fluid upsell from all origins*/
	if(data.integratedFlow == true && typeof data.integratedFlow !== 'undefined'){
		if((data.fluidUpsellUpgrade == true && typeof data.fluidUpsellUpgrade !== 'undefined') || (data.integratedBASFlow == true && typeof data.integratedBASFlow !== 'undefined' ) || (data.integratedBMSFlow == true && typeof data.integratedBMSFlow !== 'undefined')) {				
				$('#cartProration').show();							
				$('#reviewProration').show();																			
				$('#overlayProration').show();
				/* OPS-71521 Fix to hide life and seat message in case of portal addons */
				$('#cartLifeAndSeat').hide();
				$('#overlayLifeAndSeat').hide();
				$('#reviewLifeAndSeat').hide();
			}	
	}
	/*OPS-65357 APAC Changes showing optional text next to State and Zip Code*/
	if (data.apacCountry == true && typeof data.apacCountry !== 'undefined'){
		$('#optionalValue').show();
	    $('#optionalZip').show();
	    $('#cityDiv').hide();
	    $('#stateDiv').hide();
	    $('#zipCodeDiv').hide();
	}	
	if (typeof data.candyRackResult !== 'undefined'){
		if (data.candyRackResult.candyRackItems.length == 0) {
		   $("#candyRack").hide();
		   $("#candyRackOverlay").hide();
		   	$('#productSide').removeClass('col-sm-8').addClass('col-sm-12');
		}

		if (data.candyRackResult.candyRackItems.length > 0) {
		   $("#candyRack").show();
		   $("#candyRackOverlay").show();
		   console.log("candyRackItems :" + data.candyRackResult.candyRackItems);
		   console.log("candyRackItems length :" + data.candyRackResult.candyRackItems.length);
		   var candyTemplateSource = $("#candy-template").html();
		   var candyTemplate = Handlebars.compile(candyTemplateSource); 
		   candyHTML = candyTemplate(data);
		   $("#candyRack").html(candyHTML);
		}
	}

	// OPS-102017 Singleton message in empty cart
	if (data.singletonProduct) {
		$('#emptyCartSingleton').show();
	};

	/* PayPal Integration CAP-9567 */
	// PayPal Integration , hiding Payment Method Text and Card Number
	// Boleto Changes OPS-63895 // Hosted Payment,Bank Transfer and Direct Debit, Online Bank Transfer changes
	var bankNameSelected = $("#bankDropdownList option:selected").text();
	var selectedPaymentMethodName = $("#paymentMethod option:selected").text();
	if(($('#paymentMethod').val()=='DW') || ($('#paymentMethod').val()=="BT") || ($('#paymentMethod').val()=='HP') || ($('#paymentMethod').val()=='DD' ) || ($('#paymentMethod').val()=='IB_SOFO' ) || ($('#paymentMethod').val()=='IB_GIROPAY' ) || ($('#paymentMethod').val()=='IB_IDEAL' ) || ($('#paymentMethod').val()=='IB_NORDEA') || ($('#paymentMethod').val()=='HP_WEBMONEY-SSL') 
		|| ($('#paymentMethod').val()=='HP_YANDEXMONEY-SSL') || ($('#paymentMethod').val()=='HP_QIWI-SSL') || ($('#paymentMethod').val()=='HP_PRZELEWY-SSL')) {		
		$('#payment_method_text').hide();
		$('#card_number').hide();
		$('#paymentMethodSelected').text(selectedPaymentMethodName);
	}else if ($('#paymentMethod').val()=='IB_NETBANKING'){
		$('#payment_method_text').hide();
		$('#card_number').hide();
		$('#paymentMethodSelected').text(selectedPaymentMethodName);
		$('#netBankingPayment').show();	
		$('#paymentBankName').text(bankNameSelected);
	}
	else{	
		$('#payment_method_text').show();
		$('#card_number').show();
		$('#ccCardType').show();
	}

	/* Added for update discount while remove cart */
	$(".discountRow span#discountAmount, #overlayDiscountAmount").text(data.discountAmount);
	console.log("discountAmount :" + discountAmount);
	$(".discountRow #discountAmount, #overlayDiscountAmount").prepend("-");

	optionalPhone();

	if (typeof data.enableNewCartPage !== 'undefined') {
    	console.log('enableNewCartPage is ' + data.enableNewCartPage);
	    if (data.enableNewCartPage === true) {
	    	$("#cartOsSupport a").removeAttr("href");
	    	$('#Norton_Cart').show();
	    	$('#hideForemptyCart').hide();
	    	$('#mini-Cart').hide();
			$('input#couponCode[placeholder]').each(function () {
        		$(this).attr('size', $(this).attr('placeholder').length);
    		});
    		$('#couponTotalBox .couponErrorDiv').width($('#getSizeOfInput').width());
    		$('#Norton_Cart .borderBottom:last').removeClass('borderBottom');
	    }
  }		

	if (typeof data.existingCustomer !=='undefined' && data.existingCustomer == true) {	
		if(typeof data.enableLogoutAction !=='undefined' && data.enableLogoutAction === true){
			$('#newCartLogOut').show();
	 		breadCrumb(); // enable Billing & payment for new cart
		} else {
			$('#logInInfoLink').show();
		}
	}
	//OPS-93616 enabling archeckbox based on googlePLA only for ARable product
	if(typeof data.enableNonArableCheckBox !=='undefined' && data.enableNonArableCheckBox == false){
		if (typeof data.googlePLAFlow !=='undefined' && data.googlePLAFlow == true) {
			var selectedPaymentMethod = $('#paymentMethod').val();
				if (selectedPaymentMethod == 'CC' || selectedPaymentMethod == 'DW' || selectedPaymentMethod == 'DD' || selectedPaymentMethod == 'IB_SOFO' || selectedPaymentMethod == 'IB_GIROPAY' || selectedPaymentMethod == 'IB_IDEAL'){
		            $('.renewSubscription').show();	
		        }	
		}		
	}//google PLA check ends

//ROVER Product Feature Check
if(ROVER_FEATURE_FLAG && data.cartContainsPhysicalProduct){
	$('.nonRover').hide();
	$('.rover').show();
	$('#nonRoverMiniCart').hide();
	$('#roverMiniCart').show();
	$('.shipping-style').show();
	$('#billingSameAsShipping').show();
	$('#shippingAdd').show();
	$("#shipCountry").text($('#countriesDropDown option:selected').text());
	$("#shipPhoneNumber").text($('#mobileNumber-ship').val().replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3'));
	$('#reviewTagLine').show();
	$( "#billSameShip" ).prop( "checked", true );
		// Pre-Populate data from shipping to billing once checkbox is checked
		if($("#billSameShip").attr("checked")){	
			$("#hideForRover").hide();
			if(data.shippingInfoVo){
				$('#zipCode').val(data.shippingInfoVo.zipCode);
		  		$('#city').val(data.shippingInfoVo.city);
		 		$('#stateDropDown').val(data.shippingInfoVo.state);
		  		$('#countriesDropDown').val(data.shippingInfoVo.country);
		  		$('#phoneNumber').val(data.shippingInfoVo.phoneNumber);
		  	}
		  	$("#hideForRover").hide();    			
		}
		//ROVER Product Pre-Order Check
		if(ROVER_PREORDER_FEATURE_FLAG && data.preOrder){
			$('.preOrder').show();
			$('.freeShipping').show();
		}
} //ROVER Product Pre-Order Check Ends

// ROVER Product Feature Check with Shipping Info
// Filling the Shipping fields If shippingInfoVo is present
if(data.shippingInfoVo){
  $('#shippingName').val(data.shippingInfoVo.name);
  $('#address-ship').val(data.shippingInfoVo.address);
  $('#address2-ship').val(data.shippingInfoVo.address2);
  $('#zipCode-ship').val(data.shippingInfoVo.zipCode);
  $('#city-ship').val(data.shippingInfoVo.city);
  $('#stateDropShip').val(data.shippingInfoVo.state);
  $('#countryDropShip').val(data.shippingInfoVo.country);
  $('#mobileNumber-ship').val(data.shippingInfoVo.phoneNumber);

  if(ROVER_FEATURE_FLAG && data.cartContainsPhysicalProduct){
	  $("#shippingSec").hide();
	  $("#shippingInformationLink").show();                  					 					                                   
	  $("#BillingSec").slideDown("slow");
	  $("#reviewSec").hide();
 }else{
 	$("#shippingInformation").hide();
 	$("#BillingSec").slideDown("slow");
	$("#reviewSec").hide();
 }
}

// Pre-Populate data from shipping to billing once checkbox is checked (on click of checkbox)
$("#billSameShip").on("click", function() {
			  if($("#billSameShip").attr("checked")){
			  		$( "#billSameShip" ).prop( "checked", true );
			  		$("#hideForRover").hide(); 
			  		if(data.shippingInfoVo){
						$('#zipCode').val(data.shippingInfoVo.zipCode);
				  		$('#city').val(data.shippingInfoVo.city);
				 		$('#stateDropDown').val(data.shippingInfoVo.state);
				  		$('#countriesDropDown').val(data.shippingInfoVo.country);
				  		$('#phoneNumber').val(data.shippingInfoVo.phoneNumber);
  					}	
			 	}
			 	// Pre-Populate data from wallet to billing fields once checkbox is unchecked
			 	else {		  	
			  		$( "#billSameShip" ).prop( "checked", false );
			  		$("#hideForRover").show();
					if(data.walletInfo){
						$('#zipCode').val(data.walletInfo.zipCode);
				  		$('#city').val(data.walletInfo.city);
				 		$('#stateDropDown').val(data.walletInfo.state);
				  		$('#countriesDropDown').val(data.walletInfo.country);
				  		$('#phoneNumber').val(data.walletInfo.phoneNumber);
  				}
  				// Clearing the billing address fields once checkbox is unchecked and wallet is not present 
  				else{
  					$('#zipCode').val("");
			  		$('#city').val("");
			 		$('#stateDropDown').val("");
			  		$('#countriesDropDown').val(countryCodeId);
			  		$('#phoneNumber').val("");
  				} 			
			  }		  
});

}//end of refresh template

/* Hide Optional for Phone Number in Oxxo OPS-82371 and India Payments */
function optionalPhone(){
var selectedPaymentMethod = $('#paymentMethod').val();
var billingCty= $("#countriesDropDown option:selected").val();

	if ((typeof selectedPaymentMethod !== 'undefined' && selectedPaymentMethod == 'HP') || (typeof selectedPaymentMethod !== 'undefined' && selectedPaymentMethod == 'HP_QIWI-SSL') || ((typeof pspValue !== 'undefined' && pspValue == "NETPROPHETS") && billingCty == "IN")) {			
		$('#optionalPhone').hide();
		$('#optionalnsbPhone').hide();		
	}
	else {
		$('#optionalPhone').show();
		$('#optionalnsbPhone').show();		
	}
}

//ops-75656
//Enabing bank dropdown when Payment Method Selected is NetBanking and India Payment is enabled	
function checkNetBanking() {
	var billingCountry = $("#countriesDropDown option:selected").val();
	if(typeof pspValue !== 'undefined' && pspValue == "NETPROPHETS" && billingCountry =="IN" && $("#paymentMethod option:selected").data("opid")=="NETBANKING"){
		createBankDropdown();	
	}else{
		// OPS-85079 to display the banklist dropdown
		if(typeof window.idealBankList !== "undefined" && $('#paymentMethod').val() == 'IB_IDEAL' && $("#countriesDropDown").val() == 'NL'){
			$("#bankDropdownDiv").show();
		}
		else{
			$("#bankDropdownDiv").hide();
			$("#netbankingName").hide();
			$("#netBanking").hide();
			$("#netProphetText").hide();				
		}
	}
}

function genericAjaxErrorHandler(jqXHR, textStatus, errorThrown) {
  console.log('jqXHR: ' + JSON.stringify(jqXHR) + '; textStatus: ' + textStatus + '; errorThrown: ' + errorThrown);
  if (AJAX_TIMEOUT_FEATURE_FLAG && errorThrown === 'timeout'){
    window.location = '/estore/getShoppingCart/action/continue';
  }
}

/* Enable Second element of the breadcrumb */
function breadCrumb () {
	$('.breadCrumb span:first').removeClass('active');
 	$('.breadCrumb span:nth-child(2)').addClass('active');
}

/**
 * Gets the default from array from api.
 * @param {array} countryStates states array from api
 * @return {object} the default country object
 */
function getDefaultCountry(countryStates){
  for (var i in countryStates){
    if (countryStates[i].default === true){
      return countryStates[i];
    }
  }
  return null;
}

/**
 * Gets the mandatoryCityFieldCountryList from array from api.
 */
function showHideMandatoryCity(){
	var billingCountry = $("#countriesDropDown option:selected").val();
	var incomingCountryCityList = window.mandatoryCityFieldCountryList.indexOf(countryCodeId);
	var billingCountryCityList = window.mandatoryCityFieldCountryList.indexOf(billingCountry);	
						        
		//WorldPay changes OPS-73496
		if((typeof incomingCountryCityList !== 'undefined' && incomingCountryCityList !== -1) || (typeof billingCountryCityList !== 'undefined' && billingCountryCityList !== -1)){
			$('#cityDiv').show();
		}
		else{
			$('#cityDiv').hide();
		}
}

/**
 * This function checks if a phone number is valid. A valid phone is defined to have 
 * only characters in set [\d(/)\-Nn,.*;#+ ].
 */
function isValidPhoneNumber(input){

  var noWeirdCharacters = /[^\d(/)\-Nn,.*;#+ ]/.test(input) === false;
  return noWeirdCharacters;

}

//OPS-87838
//Hiding PayPal second paragraph in case of PayPal in-context flow
// TODO: refactor a little
function checkPayPalMessage() {
	if(window.enablePayPalContextFlow){
		if ($('#paymentMethod').val() === 'DW' && typeof window.paypalCountryList === 'string' 
		&& window.paypalCountryList.toLowerCase().indexOf($("#countriesDropDown option:selected").val().toLowerCase()) >= 0 && window.paypalCountryList.toLowerCase().indexOf(window.countryCodeId.toLowerCase()) >= 0){
			   $('#ppTextMessage').hide();
			   $('#inContextFlow').hide();
			   $('#inContextFields').hide();
			   $('#inContextText').show();
			   $('.inContextFlowShow').show();
			   $('.inContextFlowHide').hide();
		}else{
			console.log("inside paypal inContextFlow hide");
			  $('#ppTextMessage').show();
			  $('#inContextFlow').show();
			  $('#inContextFields').show();
			  $('#inContextText').hide();
			  $('.notInContextFlowShow').show();
			  $('.notInContextFlowHide').hide();
		}
	}
	else {
		$('.notInContextFlowShow').show();
		$('.notInContextFlowHide').hide();
	}
}

//Changes for Estore session out
function getSesCookie(c_name)
{
       var c_value = document.cookie;
       var c_start = c_value.indexOf(" " + c_name + "=");
       if (c_start == -1)
       {
              c_start = c_value.indexOf(c_name + "=");
       }
       if (c_start == -1)
       {
              c_value = null;
       }
       else
       {
              c_start = c_value.indexOf("=", c_start) + 1;
              var c_end = c_value.indexOf(";", c_start);
              if (c_end == -1)
              {
                     c_end = c_value.length;
              }
              c_value = unescape(c_value.substring(c_start,c_end));
       }
       return c_value;
}

function setSesCookie(c_name,value,exdays)
{
       var exdate=new Date();
       exdate.setDate(exdate.getDate() + exdays);
       var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
       document.cookie=c_name + "=" + c_value + ";domain=.norton.com;path=/";
}

/**
  *This method executes in every minute to check the value of storetimeout cookie
  *and show the confirm box if values matches with value of storetimeoutpopup cookie
*/
function checkSesCookie()
{
       var cookieTimeOutValue = getSesCookie("storetimeout");
       var cookieTimeOutPopupValue = getSesCookie("storetimeoutpopup");
       var currenturl = window.location.href;
       if (cookieTimeOutPopupValue != null && cookieTimeOutPopupValue != "" && cookieTimeOutValue != null && cookieTimeOutValue != "" && currenturl != null && currenturl.indexOf("checkOut") != -1)
       {
            var tempCookieTimeOutValue = parseInt(cookieTimeOutValue, 10);
            var tempCookieTimeOutPopupValue = parseInt(cookieTimeOutPopupValue, 10);
            tempCookieTimeOutValue = tempCookieTimeOutValue - 1;
            if(tempCookieTimeOutValue > -1){
                setSesCookie("storetimeout",tempCookieTimeOutValue,365);
            }
            if(tempCookieTimeOutValue == tempCookieTimeOutPopupValue){
			    $("#overLayOuter, #overLay").show();
			}
			if(tempCookieTimeOutValue > 0){
				$("#endSession").click (function(){						
					$.ajax({
						type: "GET",
						url: "/estore/storeSessionEnd"
					});
					$("#overLayOuter, #overLay").hide();
					window.location.assign("/");
				});
				$("#conSession").click (function(){				
					$.ajax({
						type: "GET",
						url: "/estore/storeSessionExtend"
					});
					$("#overLayOuter, #overLay").hide();				
				});   
			}
			if(tempCookieTimeOutValue <= 0){
			  $("#endSession").click (function(){						
					$.ajax({
						type: "GET",
						url: "/estore/storeSessionEnd"
					});
					window.location.assign("/");
				});
				$("#conSession").click (function(){				
					$.ajax({
						type: "GET",
						url: "/estore/storeSessionEnd"
					});
					$("#overLayOuter, #overLay").hide();
					window.location.assign("/");									
				});   
			}
        }      
}
