
	function getCookie(c_name)
	{
		try{
          var name=c_name+"=";
		  var cookies	=  document.cookie.split(';');
		 
		  var cookieHex;
		  for(var i=0;i < cookies.length;i++){
				var c=cookies[i];
				while(c.charAt(0)==' '){
					c=c.substring(1,c.length)
				};
				if(c.indexOf(name)==0){
					cookieHex =  unescape(c.substring(name.length,c.length));
					
				}
				
			}
		  
		  
		  var cookieString = hexToString(cookieHex);
        
	
		  return cookieString;

         }catch(e){

         }
    }

	// Code to handle cookie string obtained from above method
	
	function updateCookie(cookieString,cid,eid) {

		//cid:123,234|eid:aaa,bbb
		
		
		var cidString = "";
		var eidString = "";
		var cidKey = "";
		var eidKey = "";
		var cidValue = "";
		var eidValue = "";
		var strings = cookieString.split("|");
		var finalString = "";
		var cookieName = "tt";
		
		if(strings != null)
		{
		
			// if order of cid and eid changed
			var temp1 = strings[0]; 
			var temp2 = strings[1]; 
			
			
			
			if(temp1.indexOf('CID') == -1  && temp1.indexOf('cid') == -1) {
				
				eidString = temp1;
				cidString = temp2;
			}
			
			if(temp1.indexOf('EID') == -1  && temp1.indexOf('eid') == -1) {
			
				cidString = temp1;
				eidString = temp2;
			}
				
			
		}		

		// CID 
		if(cidString != null)
		{
			var cidStringToken = cidString.split(":");
			cidKey = cidStringToken[0]; //cid
			cidValue = cidStringToken[1]; //123,234
			
			if(cidValue !=null)
			{
				cidValue = cidValue.concat(',').concat(cid);
				
			}
		}else {
				
				cidKey = "CID";
				cidValue = cid;
		}

		//EID			
		if(eidString != null)
		{
			var eidStringToken = eidString.split(":");
			
				eidKey = eidStringToken[0]; //eid
				eidValue = eidStringToken[1]; //aaa,bbb				
				if(eidValue !=null)
				{							
					eidValue = eidValue.concat(',').concat(eid);										
				}
			
		}else {
					
					eidKey = "EID";
					eidValue = eid;
		}
		
		finalString = cidKey.concat(':').concat(cidValue).concat('|').concat(eidKey).concat(':').concat(eidValue);

		
		return finalString;
	}


	function encodeToHex(str){		
			
		var text2='';
		for (i=0;i<str.length;i++)
		text2 += str.charCodeAt(i).toString(16);
		
		return text2;
    }



     function setCookie(name,value,days){
		try{
			value = encodeToHex(value);
			var expires = "" ;
			var path = "/";
			var domain = "norton.com";
			if (days) {
				var date = new Date();
				date.setTime(date.getTime()+(days*24*60*60*1000));
				expires = "; expires="+date.toGMTString();
			}
			
			var cookie_string = name + "=" + escape ( value ) + expires;

     		  if ( path )
					cookie_string += "; path=" + escape ( path );

			  if ( domain )
					cookie_string += "; domain=" + escape ( domain );
					
			  document.cookie = cookie_string;

		}catch(e){}
     }
	
	

	function execute(c_name,cid,eid) {	
	    
    	var exdays= 14;
		var cookieString = getCookie(c_name);
		
		if(cookieString != null)
		{		
			//hexadicmal convertor required to convert cookie							
			var finalCookie = updateCookie(cookieString,cid,eid);			
				
			setCookie(c_name,finalCookie,exdays);
			
		}
		else
		{			
			var new_Cookie = 'CID'.concat(':').concat(cid).concat('|').concat('EID').concat(':').concat(eid);
			setCookie(c_name,unescape(new_Cookie),exdays);
			

		}

	}

	function hexToString(data)
	{
		
		if(data !=null)
		{
		
		var b16_digits = '0123456789abcdef';
		var b16_map = new Array();
		for (var i=0; i<256; i++) {
			b16_map[b16_digits.charAt(i >> 4) + b16_digits.charAt(i & 15)] = String.fromCharCode(i);
		}
		if (!data.match(/^[a-f0-9]*$/i)) return false;// return false if input data is not a valid Hex string
		
		if (data.length % 2) data = '0'+data;
			
		var result = new Array();
		var j=0;
		for (var i=0; i<data.length; i+=2) {
			result[j++] = b16_map[data.substr(i,2)];
		}	
			
		return result.join('');
		}	

		return null;
	}
	