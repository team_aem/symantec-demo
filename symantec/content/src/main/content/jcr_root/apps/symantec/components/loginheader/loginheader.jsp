<%@ include file="/libs/foundation/global.jsp" %>
<html>
<!--<cq:includeClientLib categories="symantec_lib"/>-->


    <head>



    </head>

    <%
String gettext = properties.get("gethelp", "");%>

<header class="background-white" id="header">
  <nav class="navbar">
    <div class="container">
      <div class="navbar-header nortonLogo col-xs-4"><img src="${properties.fileReference}" alt="header_logo"></div>
      <div class="nav navbar-nav navbar-right hidden-xs">
        <div id="signLink">
          <ul class="nav navbar-nav floatRight">
            <li>
              <a href="https://support.norton.com/sp/en/us/norton-renewal-purchase/current/info?inid=hho_store_support_gethelp_header" title="" target="_blank"><%=gettext %></a>
            </li>
          </ul>
        </div>
      </div>
      <div class="nav navbar-nav navbar-right mobileNav col-xs-8 hidden-sm hidden-md hidden-lg floatRight">
        <!--<span>
          <a href="https://support.norton.com/sp/en/us/norton-renewal-purchase/current/info?inid=hho_store_support_gethelp_header" target="_blank">Get Help</a>
        </span>--> 
      </div>
    </div>
  </nav>
</header>

</html>