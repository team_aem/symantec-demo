<%@ include file="/libs/foundation/global.jsp" %>
<cq:include script="/libs/wcm/core/components/init/init.jsp"/>
<html>
    <cq:includeClientLib categories="symantec_avi"/>
    <head>

    </head>

    <body>
		<div class="container">
            <cq:include path="par4" resourceType="foundation/components/parsys"/>
        </div>

        <div class="container">
            <div style="width:50%; float: left;" class="container">
            	<cq:include path="par2" resourceType="foundation/components/parsys"/>
            </div>

        	<div style="width:50%; float:right;" class="container">
            	<cq:include path="par3" resourceType="foundation/components/parsys"/>
            </div>
        </div>
    </body>
</html>