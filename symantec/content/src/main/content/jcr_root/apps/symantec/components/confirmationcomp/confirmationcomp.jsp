<%@ include file="/libs/foundation/global.jsp" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DateFormat" %>
<html>
    <head>
        <cq:includeClientLib categories="symantec_avi"/>
	</head>
    <body style="background-color: white;">
    <%
		String thanksmessage = properties.get("thanksnorton", "thanksnorton");
		String donemessage = properties.get("done", "You are almost done");
		String installmessage = properties.get("install", "Installing software");
		String downloadmessage = properties.get("download", "Downloading Norton");
		String instructionsmessage = properties.get("instructions", "Instructions to be followed");
		String nortonarsmessage = properties.get("nortonars", "Norton Auto Renewal Service");
		String arsdescmessage = properties.get("arsdesc", "Auto renrewal Service Description");
		String quesmessage = properties.get("ques", "Any Questions?");
		String get = properties.get("get", "getstarted");
	%>
    <%
        String email = request.getParameter("email");
		String total = request.getParameter("total");
    %>
    <%
		Calendar cal = Calendar.getInstance();
		Date today = cal.getTime();
		cal.add(Calendar.YEAR, 1);
		Date nextYear = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
	%>
	<div class="container">
		<div class="confirmPage col-xs-12">
			<div id="hideOnLoad" style="display: none;">
				<h2 class="alert"><%=thanksmessage %></h2>
			</div>
			<div class="confirmPageAcq confirmPagePosition">
				<div class="confirmPage mfAquisitionContainer container-fluid">
					<div class="col-xs-12">
						<h1><%=thanksmessage %></h1>
						<h2 class="alert"><%=donemessage %></h2>
						<p class="paymentDetails">We have received your payment of <strong><%=total %></strong> for your Norton subscription for <strong>1 year(s)</strong> and your <strong>Order Number is ND<%= (int) (Math.random() * 1000000) %></strong>. Your subscription is activated and its expiration date is <strong><%out.print(dateFormat.format(cal.getTime()));%></strong>.
                        A copy of this confirmation with your order details will also be sent to <strong><%=email %></strong>,
						and stored in your Norton account.</p>
						<div id="downloadInstruction" style="display: none;">
							<h3><strong>Here is what you must do next to get protected:</strong></h3>
						</div>
						<div id="activationInstructionHeader">
							<h3><%=installmessage%></h3>
						</div>

					<div id="acqInstruction" style="">
						<p class="paymentDetails"></p>
						<ol>
							<li><%=downloadmessage%><a href="http://norton.com/setup"
								target="_blank">norton.com/setup</a>
							</li>
							<li><%=instructionsmessage%></li>
						</ol>
						<p></p>
					</div>


					<div class="confirmButton" id="getStartedButton" style="">
						<a
							href="https://my-int2.norton.com/ssoprecursor/home?psn=TRJVJW9W23KY&amp;pkproxy=JDPQR4HTHMWJ3TX6RR6FQ4GP4&amp;returnurl=%2Fonboard%2Fhome%2Fsetup&amp;service=estore&amp;action=signin&amp;register=1"
							target="_blank"><%=get%></a>
					</div>

					<div class="tcToggleCopy">
						<div id="arable">
							<h3 id="arheader" style="">
								<strong><%=nortonarsmessage%></strong>
							</h3>
							<h3 id="monthlyarHeader" style="display: none;">Norton
								Monthly Subscription</h3>
							<p>
								<%=arsdescmessage%><a href="http://manage.norton.com"
									target="_blank">manage.norton.com</a> or by contacting Norton
								Customer Service.
							</p>
						</div>
						<div id="nonCLTinstructionmessage" style="display: none;">
							<p class="instructionMessage"></p>
						</div>
						<div id="supportText">
							<p>
								<%=quesmessage%> <a
									href="https://support.norton.com/sp/en/us/norton-renewal-purchase/current/info?inid=hho_store_confirm_email_support"
									target="_blank">Norton Support</a>.<br>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- / experience 1 - acquisition -->
		</div>

	</div>
</div>
    </body>
</html>