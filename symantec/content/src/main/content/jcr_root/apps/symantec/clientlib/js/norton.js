$(document).ready(function()
{



/*---------------------------------------*/
    /*LoginPage*/
/*---------------------------------------*/
	$("form.signup").hide();
	$(".login2").hide();

    $("input.button").click(function()
	{
		$("form.login").slideDown();
		$("form.signup").slideUp();
		$(".login2").hide();
		$(".signup2").show();
	});

    $("input.button1").click(function()
	{
		$("form.signup").slideDown();
		$("form.login").slideUp();
		$(".login2").show();
		$(".signup2").hide();
	});

   /* -----------------------*/
    /*Billing Page*/
     /* -----------------------*/

    $("#divmsg1").hide();
            $("#divmsg2").hide();

    		$("#question1").click(function()
			{
				$("#divmsg1").slideDown();
                $("#question1").slideUp();

			});

            $("#question2").click(function()
			{
				$("#divmsg2").slideDown();
                $("#question2").slideUp();

			});

            $("#hide1").click(function()
			{
				$("#divmsg1").slideUp();
                $("#question1").slideDown();

			});

            $("#hide2").click(function()
			{
				$("#divmsg2").slideUp();
                $("#question2").slideDown();

			});

    /*to hide the billing page on loading page*/
    		$('#login2').hide();
			/*$("#login").click(function(e)
			{
                e.preventDefault();
				$.ajax({
					type:'GET',
					url:'/bin/login',
					data: $('#loginF').serialize(),
					cache:false,
					success:function(data)
					{
                        alert(data);
                		$('#billingSection').show();
                        $('#loginSection').hide();
						 return;
					},
					error:function(error)
					{
						alert("error");
            		}
				});
    		});*/
});

function validateEmail()
{
	var x = document.forms["loginform"]["email"].value;
	var txt = "";
	var atpos = x.indexOf("@");
	var dotpos = x.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) 
	{
		txt = "The email address you have typed is not valid. Please try again.";
	}
    			if (x == null || x == "")
				{
           			txt = "Email Address is a required field.";
    			}
        		document.getElementById("eml").innerHTML = txt;
                return false;
			}

			function validatePassword()
			{
    			var txt = "";
    			var x = document.forms["loginform"]["password"].value;
    			if (x == null || x == "")
    			{
					txt = "Password is a required field.";
        		}

    			if(x.length >= 1 && x.length <= 8)
    			{
                	document.forms["loginform"]["password"].value = "";
        			txt = "Password length Cannot be less than 8 Characters.";
    			}
    			document.getElementById("pswd").innerHTML = txt;
			}

			function validateEmail1()
			{
    			var x = document.forms["signupform"]["email"].value;
    			var txt = "";
    			var atpos = x.indexOf("@");
	 			var dotpos = x.lastIndexOf(".");
    			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) 
    			{
        			txt = "The email address you have typed is not valid. Please try again.";
    			}
    			if (x == null || x == "")
				{
           			txt = "Email Address is a required field.";
    			}
        		document.getElementById("eml1").innerHTML = txt;
			}

			function validatePassword1()
			{
    			var txt = "";
    			var x = document.forms["signupform"]["password1"].value;
    			if (x == null || x == "")
    			{
					txt = "Password is a required field.";
        		}

    			if(x.length >= 1 && x.length <= 8)
    			{
                	document.forms["signupform"]["password1"].value = "";
        			txt = "Password length Cannot be less than 8 Characters.";
    			}
    			document.getElementById("pswd1").innerHTML = txt;
			}

			function validatePassword2()
			{
    			var txt = "";
				var y = document.forms["signupform"]["password1"].value;
    			var x = document.forms["signupform"]["password2"].value;
    			if (x == null || x == "")
    			{
					txt = "Verify Password is a required field.";
        		}

    			if(x.length >= 1 && x.length <= 8)
    			{
                	document.forms["signupform"]["password2"].value = "";
        			txt = "Password length Cannot be less than 8 Characters.";
    			}

   				if(x!=y){
        			document.forms["signupform"]["password2"].value = "";
        			txt = "The passwords you have typed do not match. Please try again.";
    			}
    			document.getElementById("pswd2").innerHTML = txt;
			}

function cardHolderName(){
	var x = document.forms["billingForm"]["cardName"].value;
	var letters = /^[A-Za-z]+$/;
	var txt = "";
	if (x == null || x == "")
	{
		txt = "Name on Card is a required field.";
	}
	else if(x.match(letters))
	{}
	else  
	{
		txt = "Card Holder Name should contain only characters";
	}
	document.getElementById("cname").innerHTML = txt;
}

function cardnumber(){
	var x = document.forms["billingForm"]["cardNumber"].value;
	var visa = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
	var amexp = /^(?:3[47][0-9]{13})$/;
	var master = /^(?:5[1-5][0-9]{14})$/;
	var disc = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;
	var jcb = /^(?:(?:2131|1800|35\d{3})\d{11})$/;
	var txt = "";
	var card = "";
	if (x == null || x == "")
	{
		txt = "Credit Card Number is a required field.";
	}
	
	else if(x.match(visa))
	{
		card="Visa";
	}
	else if(x.match(amexp))
	{
		card="American Express";
	}
	else if(x.match(master))
	{
		card="Master Card";
	}
	else if(x.match(disc))
	{
		card="Discover";
	}
	else if(x.match(jcb))
	{
		card="JCB";
	}
	else  
	{
		txt = "unknown is not available for United States as your billing country.";
	}
	document.getElementById("ccomp").innerHTML = card;
	document.getElementById("cnumber").innerHTML = txt;
}

function expmonth(){
	var x = document.forms["billingForm"]["month"].value;
	var txt = "";
	if (x == -1)
	{
		txt = "Month is a required field.";
	}
	document.getElementById("expmonth").innerHTML = txt;
}

function expyear(){
	var x = document.forms["billingForm"]["year"].value;
	var txt = "";
	if(x == -1){
		txt = "Year is a required field.";
	}
	else{
		currentMonth = new Date().getMonth() + 1,
        currentYear  = new Date().getFullYear();
		var y = document.forms["billingForm"]["month"].value;
		if(y < currentMonth && x <= currentYear){
			txt = "Expiration Date is incorrect";
			document.getElementById("expmonth").innerHTML = txt;
			return false;
		}
	}
	document.getElementById("expyear").innerHTML = txt;
}

function securitycode(){
	var x = document.forms["billingForm"]["cvv"].value;
	var letters = /^[0-9]+$/;
	var txt = "";
	var n = x.length;
	if(n == 3){
		if (x == null || x == "")
		{
			txt = "Security Code is a required field.";
		}
		else if(x.match(letters))
		{
			txt = "";
		}
		else  
		{
			txt = "Security Code can contain only Integer value.";
		}
	}
	else{
		txt = "The Security code needs to be 3 digits in length.";
	}
	document.getElementById("scode").innerHTML = txt;
}

function zipCode(){
	var x = document.forms["billingForm"]["zipcode"].value;
	var letters = /^[0-9]+$/;
	var txt = "";
	var n = x.length;
	
	if(n == 5 || n == 9)
	{
		if(x.match(letters))
		{
			txt = "";
		}
		else  
		{
			txt = "Zip codes must have 5 or 9 digits, for example: 12345 or 12345-6789.";
		}
	}
	else{
		txt = "Zip codes must have 5 or 9 digits, for example: 12345 or 12345-6789.";
	}
	if (x == null || x == "")
	{
		txt = "Zip/Postal Code is a required field.";
	}
	document.getElementById("zipCodeid").innerHTML = txt;
}

function cityFunction(){
	var x = document.forms["billingForm"]["city"].value;
	var letters = /^[A-Za-z]+$/;
	var txt = "";
	if (x == null || x == "")
	{
		txt = "City name is a required field.";
	}
	else if(x.match(letters))
	{}
	else  
	{
		txt = "City name should contain only characters";
	}
	document.getElementById("cityid").innerHTML = txt;
}

function stateFunction()
{
	var x = document.forms["billingForm"]["state"].value;
	var txt = "";
	if(x == -1){
		txt = "State is a required field.";
	}
	document.getElementById("stateid").innerHTML = txt;
}

function countryFunction()
{
	var x = document.forms["billingForm"]["country"].value;
	var txt = "";
	if(x == -1){
		txt = "country is a required field.";
	}
	document.getElementById("countryid").innerHTML = txt;
}

function mobileFunction(){
	var x = document.forms["billingForm"]["mobileno"].value;
	var letters = /^[0-9]+$/;
	var txt = "";
	if (x == null || x == "")
	{
	}
	else
	{
		if(x.match(letters)){
			var len = x.length;
			if(len < 10 || len > 10){
				txt = "Mobile number must contain 10 digits";
			}
		}
		else{
			txt = "Mobile number must contain numbers only";
		}
	}
	document.getElementById("mobileNumberID").innerHTML = txt;
}

function logout()
{
	$.ajax({
		type : 'GET',
		url : '/bin/epcortex/signout',
		cache : false,
		success : function(data)
		{
			window.location.href = "/content/symantec/landingpage.html?wcmmode=disabled";
		}
	});
}