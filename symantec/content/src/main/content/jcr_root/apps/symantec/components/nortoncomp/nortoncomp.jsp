<%@ include file="/libs/foundation/global.jsp"%>
<cq:includeClientLib categories="symantec_lib" />
<html>
<head>

</head>
<body>
	<%
		String title = properties.get("maintitle", "default title");
		String loginTitle = properties.get("title2", "default text");
		String signTitle = properties.get("title3", "default text");
		String loginText = properties.get("textarea1", "default text");
		String uname = properties.get("uname", "default text");
		String password = properties.get("password", "default text");
		String link = properties.get("link", "default text");
		String button1 = properties.get("button1", "default text");
		String textarea2 = properties.get("textarea2", "default text");
		String button2 = properties.get("button2", "default text");
	%>
  
    <% 
    	String name = request.getParameter("name");
    	String price = request.getParameter("price");
    	String desc = request.getParameter("desc");
    %>


	<div id="loginSection" style="width: 570px;" class="container">
		<div style="width: 570px;" class="container">
			<h2><%=title%></h2>
			<div style="width: 260px;" class="left">
				<h3><%=loginTitle%></h3>
				<p><%=loginText%></p>
				<form name="loginform" class="login" id="loginF">
					<b class="big"><%=uname%></b><br /> <input class="text"
						type="email" name="email" placeholder="name@email.com"
						onblur="validateEmail();" 
						onCopy="return false" onDrag="return false" onDrop="return false"
						onPaste="return false" />
                   <br>
					<p id="eml"></p>

					<b class="big"><%=password%></b><br /> <input class="text"
						type="password" name="password"
						placeholder="At least 8 alpha-numeric characters"
						onblur="validatePassword();" 
						onCopy="return false" onDrag="return false" onDrop="return false"
						onPaste="return false" /><br />
					<p id="pswd"></p>

					<a href="#"><%=link%></a><br />
					<input class="button" type="submit" value="<%=button1%>" id="login" />
				</form>

				<div class="login2" style="position: relative;">
					<input class="button" type="submit" value="<%=button1%>" />
				</div>
			</div>

			<div style="width: 260px;" class="right">
				<h3><%=signTitle%></h3>
				<p><%=textarea2%></p>
				<form class="signup" name="signupform" id="signupform">

					<b class="big"><%=uname%></b><br> <input class="text"
						type="email" name="email" placeholder="name@email.com"
						onblur="validateEmail1();" 
						onCopy="return false" onDrag="return false" onDrop="return false"
						onPaste="return false" /><br />
					<p id="eml1"></p>

					<b class="big"> <%=password%></b><br />
                    <input class="text"
						type="password" name="password1"
						placeholder="At least 8 alpha-numeric characters"
						onblur="validatePassword1();" 
						onCopy="return false" onDrag="return false" onDrop="return false"
						onPaste="return false" /><br />
					<p id="pswd1"></p>

					<b class="big"> Re-enter your <%=password%></b><br> <input
						class="text" type="password" name="password2"
						placeholder="Re-enter your password" onblur="validatePassword2();" onCopy="return false"
						onDrag="return false" onDrop="return false" onPaste="return false" /><br />
					<p id="pswd2"></p>
                    <input type="hidden" value="abcd" name="family-name">
                    <input type="hidden" value="efgh" name="given-name">

					<input id="signup" class="button1" type="submit" value="<%=button2%>" />
				</form>

				<div class="signup2" style="position: relative;">
					<input class="button1" type="submit" value="<%=button2%>" />
				</div>
			</div>
		</div>
		<div style="width: 570px;" class="container">
			<h2 style="background: #9B9B9B">Billing Information</h2>
			<h2 style="background: #9B9B9B">Review Your Order</h2>
		</div>
	</div>
</body>

    <script>

	$(document).ready(function()
	{
		$("#billingSection").hide();
		$("#reviewSection").hide();
    });

    $('#login').click(function(e)
	{
        e.preventDefault();
		$.ajax({
			type:'GET',
			url:'/bin/login',
			data: $('#loginF').serialize(),
			cache:false,
			success:function(data)
			{
                //alert(data);
				$("#billingSection").show();
            	$("#loginSection").hide();
            	$("#reviewSection").hide();
				$("#user_name1").text(data);
				$("#user_name2").text(data);
			},
			error:function(error)
			{
				alert(error);
            }
		});
	});

	$('#signup').click(function(e)
	{
        e.preventDefault();
		$.ajax({
			type:'GET',
			url:'/bin/registration',
			data: $('#signupform').serialize(),
			cache:false,
			success:function(data)
			{
				$("#billingSection").show();
            	$("#loginSection").hide();
            	$("#reviewSection").hide();
				$("#user_name1").text(data);
				$("#user_name2").text(data);
			},
			error:function(error)
			{
				alert("User Already Exist");
            }
		});
	});
    </script>
</html>